import 'dart:convert';
import 'dart:io';
import 'package:arabacustomer/dialog/PhoneOtpDialog.dart';
import 'package:arabacustomer/model/SocialLoginResponse.dart';
import 'package:arabacustomer/widgets/SubmitButtonRed.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:arabacustomer/PostJob1New.dart';
import 'package:arabacustomer/model/LoginResponse.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/MyRequestTabsScreen.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:toast/toast.dart';

import 'RegistrationTirth.dart';
import 'Utils/Api_constant.dart';
import 'Utils/Consts.dart';
import 'Utils/MyPreferenceManager.dart';
import 'Utils/Utility.dart';
import 'dialog/ForgotPassword.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:google_sign_in/google_sign_in.dart';
/*import 'package:apple_sign_in/apple_sign_in.dart';*/

import 'model/LoginResponseProfile.dart';
import 'model/VerifyPhooneOtpResponse.dart';
import 'model/getOtpResponse.dart';

class LoginTirth extends StatefulWidget {
  static const routeName = '/LoginTirthPage';
  bool IsResendTimerShow = false;

  @override
  LoginTirthState createState() {
    return LoginTirthState();
  }
}

class LoginTirthState extends State<LoginTirth>
    implements CallbackOfForgotPasswordDialog {
  bool IsProgressIndicatorShow = false;

  String phonenumber_otp = '';
  String phonenumberr = '';
  bool IsPhoneOtpSendComplete = false;

  Location location = new Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;

  Future<void> EnableLocation() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
//      Toast.show(_serviceEnabled.toString(), context, gravity: Toast.CENTER);
      if (!_serviceEnabled) {
        EnableLocation();
      } else {
        getContinuesBackgroundLocation();
      }
    } else {
      getContinuesBackgroundLocation();
    }
  }

  Future<void> requestPermission() async {
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
//      Toast.show(_permissionGranted.toString(), context, gravity: Toast.CENTER);
      if (_permissionGranted != PermissionStatus.granted) {
        requestPermission();
      } else {
        EnableLocation();
      }
    } else {
      EnableLocation();
    }
  }

  void getContinuesBackgroundLocation() {}

  int _start = 60;
  Timer _timerr;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
  /*  _timerr = new Timer.periodic(
      oneSec,
      (Timer timer) =>

        setState(() {
        if (_start < 1) {
          timer.cancel();
          if (mounted) setState(){ widget.IsResendTimerShow = true;}
        } else {
          _start = _start - 1;
        }
      },
      ),
    );*/

    Timer.periodic(Duration(seconds: 1), (timer) {
      if (mounted){
        setState(() {
          if (_start < 1) {
            timer.cancel();
            if (mounted) setState(){ widget.IsResendTimerShow = true;}
          } else {
            _start = _start - 1;
          }
        });
      }

    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    update_token();

    requestPermission();
  }

  Future update_token() async {
    var _myPreferenceManager = await MyPreferenceManager.getInstance();

    String token = _myPreferenceManager.getString(MyPreferenceManager.DEVICE_TOKEN);

    if (null == token || token.isEmpty) {
      FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
      _firebaseMessaging.getToken().then((token) {
        print("token====" + token);

        _myPreferenceManager.setString(MyPreferenceManager.DEVICE_TOKEN, token.toString());
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: SafeArea(
          child: Stack(
        children: <Widget>[
          new Container(
            height: double.infinity,
            child: ListView(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.fromLTRB(30, 25, 30, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          textAlign: TextAlign.start,
                          text: TextSpan(
//                style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                text: Api_constant.login,
                                style: TextStyle(
                                    color: Colors.black54.withOpacity(0.8),
                                    fontSize: 22,
                                    fontWeight: FontWeight.w800),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 30.0),
                        Center(
                          child: Image(
                            image: AssetImage('assets/images/login_icon.png'),
//                              height: 22,
//                              width: 22,
                          ),
                        ),
                        SizedBox(height: 30.0),
                        CommonWidget.lblText('PHONE NUMBER'),
                        Visibility(
                          visible: true,
//                          visible: !IsPhoneOtpSendComplete,
                          child: TextField(
                            maxLength: 10,
                            readOnly: IsPhoneOtpSendComplete,
                            style: CommonWidget.TFCommnTextStyle(),
                            decoration: CommonWidget.ETInputNumberDecoration(),
                            showCursor: true,
                            autofocus: true,

                            cursorColor: Colors.red,
                            keyboardType: TextInputType.number,
                            onChanged: (val) {
                              phonenumberr = val;
                            },
                          ),
                        ),

                        Visibility(
                          visible: IsPhoneOtpSendComplete,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(height: 30.0),
                              CommonWidget.lblText('OTP'),
                              TextField(
                                style: CommonWidget.TFCommnTextStyle(),
                                decoration: CommonWidget.ETInputDecoration(),
//                            maxLength: Consts.TextField_maxLength_otp,
                                showCursor: true,
                                autofocus: true,
                                cursorColor: Colors.red,
                                keyboardType: TextInputType.number,
                                onChanged: (val) {
                                  phonenumber_otp = val;
                                },
                              ),
                              SizedBox(height: 30.0),
                              Center(
                                child: widget.IsResendTimerShow
                                    ? InkWell(
                                        child: Text(
                                          Api_constant.Resend_new_code +
                                              "(" +
                                              _start.toString() +
                                              ")",
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Theme.of(context)
                                                  .primaryColor),
                                        ),
                                        onTap: () {
                                          sendOtp();
                                        },
                                      )
                                    : Text(
                                        Api_constant.Resend_new_code +
                                            "(" +
                                            _start.toString() +
                                            ")",
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.black26)),
                              )
                            ],
                          ),
                        ),

//                        CommonWidget.divider(),

                        SizedBox(height: 40.0),

                        Visibility(
                          visible: IsPhoneOtpSendComplete,
                          child: SubmitButtonRed(
                              Api_constant.confirm_access_code, onChanged, 10),
                        ), // verify otp
                        Visibility(
                          visible: !IsPhoneOtpSendComplete,
                          child: SubmitButtonRed(
                              Api_constant.send_the_access_code, onSendOtp, 10),
                        ), //send otp

                        SizedBox(height: 10.0),

                        Center(
                          child: CommonWidget.submitButtonBottomLine(),
                        ),
                        SizedBox(height: 15.0),
                      ],
                    ))
              ],
            ),
          ),
          Center(
            child: Visibility(
              visible: IsProgressIndicatorShow,
              child: SizedBox(
                child: CircularProgressIndicator(),
              ),
            ),
          ),
        ],
      )),
    );
  }

  @override
  void forgotPasswdSendEmail(String email) {
    Toast.show(email.toString(), context,
        duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
  }

  String removeCountryCode(String phone) {
    if (phone != null && phone.isNotEmpty && phone.contains('+91')) {
      return phone.replaceFirst('+91', '');
    } /*else if (phone != null && phone.isNotEmpty && phone.contains('91')) {
      return phone.replaceFirst('91', '');
    }*/ else {
      return phone;
    }
  }

  void onSendOtp() {
    if (phonenumberr.trim().isEmpty || phonenumberr == '213') {
      Toast.show(Api_constant.mobile_empty, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    } else if (phonenumberr.trim().length < 10) {
      Toast.show("Mobile number should be at least 10 digits", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    } else {
      sendOtp();
    }
  }

  void onChanged() {
    if (phonenumber_otp.isNotEmpty) {
      verifyOtp(phonenumber_otp);
    } else {
      Toast.show(Api_constant.Please_enter_code, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    }
  }

  Future<void> verifyOtp(String phone_otp) async {
    var map = new Map<String, dynamic>();
//    map[Api_constant.phone_number] = widget.phoneNumber.trim();
    map[Api_constant.otp_param] = phone_otp;

    if (Platform.isIOS) {
      map[Api_constant.device_type] = Api_constant.DEVICE_TYPE_IOS;
    } else {
      map[Api_constant.device_type] = Api_constant.DEVICE_TYPE;
    }

    MyPreferenceManager _myPreferenceManager =
        await MyPreferenceManager.getInstance();

    String token = _myPreferenceManager.getString(MyPreferenceManager.DEVICE_TOKEN);

    print(token.toString() + "Logintoken..............");

    map['device_token'] = token;

    map['phone_number'] = removeCountryCode(phonenumberr.trim());

    print(map.toString() + "login_customer_new..............");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    print(Api_constant.login_customer_new);

    var response = await http.post(Api_constant.login_customer_new, body: map);
    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
          new LoginResponse.fromJsonMap(userMap) as LoginResponse;

      if (loginResponseObj.status == 1) {
        var loginUserObj = loginResponseObj.data;

        if (loginResponseObj.profile_complete == 1) {
          MyPreferenceManager _myPreferenceManager =
              await MyPreferenceManager.getInstance();
          _myPreferenceManager.setBool(MyPreferenceManager.IS_USER_LOGIN, true);
          print(loginUserObj.id.toString() + "..............");

          _myPreferenceManager.setString(
              MyPreferenceManager.CURRENT_LOGIN_USER_ID,
              loginUserObj.id.toString());
          _myPreferenceManager.setString(
              MyPreferenceManager.CORPORATE, loginUserObj.corporate.toString());

          _myPreferenceManager.setString(MyPreferenceManager.CORPORATE_ID,
              loginUserObj.corporate_id.toString());
          _myPreferenceManager.setString(MyPreferenceManager.PHONE_NUMBER,
              loginUserObj.phone_number.toString());

          _myPreferenceManager.setString(
              MyPreferenceManager.LOGIN_NAME,
              CommonWidget.replaceNullWithEmpty(loginUserObj.first_name) +
                  " " +
                  CommonWidget.replaceNullWithEmpty(loginUserObj.last_name));
          print(_myPreferenceManager
                  .getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID) +
              "..............");

          print("userid " + loginUserObj.id.toString());

          Navigator.of(context).pushReplacementNamed(PostJob1New.routeName);
        } else {
//          Navigator.pushNamed(context, '/RegistrationPage', arguments: obj.email);
          Navigator.of(context).pushReplacementNamed(
              RegistrationTirth.routeName,
              arguments: loginUserObj.id.toString());
        }
//      Navigator.pushNamed(context, MyRequestTabsScreen.routeName);

      } else {
        Toast.show(loginResponseObj.message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  Future<void> sendOtp() async {
    var map = new Map<String, dynamic>();
//    map[Api_constant.phone_number] = widget.phoneNumber.trim();
    map['phone_number'] = removeCountryCode(phonenumberr.trim());

    print(map.toString() + "login_otp..............");
    print(Api_constant.login_otp);

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.post(Api_constant.login_otp, body: map);
    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
          new GetOtpResponse.fromJsonMap(userMap) as GetOtpResponse;

      if (loginResponseObj.status == Api_constant.STATUS) {
        _start = 60;
        setState(() {
          if (!IsPhoneOtpSendComplete) {
            IsPhoneOtpSendComplete = true;
          }

          widget.IsResendTimerShow = false;
        });
        startTimer();
      } else {
        Toast.show(loginResponseObj.message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  Future<void> LoginUser() async {
    var map = new Map<String, dynamic>();

    map[Api_constant.lng] = Api_constant.en;

    var instance = await MyPreferenceManager.getInstance();
    MyPreferenceManager _myPreferenceManager =
        await MyPreferenceManager.getInstance();
    String token = _myPreferenceManager.getString(MyPreferenceManager.DEVICE_TOKEN);

    print(token.toString() + "Logintoken..............");

    map[Api_constant.device_token] = token;
    map[Api_constant.device_type] = Api_constant.DEVICE_TYPE;

    print(map.toString() + "Login..............");
    print(Api_constant.login_customer_api + "Login..............");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.post(Api_constant.login_customer_api, body: map);
    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
          new LoginResponse.fromJsonMap(userMap) as LoginResponse;

      if (loginResponseObj.status == 1) {
        var loginUserObj = loginResponseObj.data;

        MyPreferenceManager _myPreferenceManager =
            await MyPreferenceManager.getInstance();
        _myPreferenceManager.setBool(MyPreferenceManager.IS_USER_LOGIN, true);
        print(loginUserObj.id.toString() + "..............");

        _myPreferenceManager.setString(
            MyPreferenceManager.CURRENT_LOGIN_USER_ID,
            loginUserObj.id.toString());
        _myPreferenceManager.setString(
            MyPreferenceManager.CORPORATE, loginUserObj.corporate.toString());
        _myPreferenceManager.setString(
            MyPreferenceManager.LOGIN_NAME,
            CommonWidget.replaceNullWithEmpty(loginUserObj.first_name) +
                " " +
                CommonWidget.replaceNullWithEmpty(loginUserObj.last_name));
        print(_myPreferenceManager
                .getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID) +
            "..............");

        print("userid " + loginUserObj.id.toString());

//      Navigator.pushNamed(context, MyRequestTabsScreen.routeName);
        Navigator.of(context).pushReplacementNamed(PostJob1New.routeName);
      } else {
        Toast.show(loginResponseObj.message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  static final FacebookLogin facebookSignIn = new FacebookLogin();

  void _showMessage(String message) {
    setState(() {
      print(message.toString() + "loginnn..............");
//      _message = message;
    });
  }

  Future<void> socialLoginApiCall(
      String email, String fname, String lname, String social_logintype) async {
    var map = new Map<String, dynamic>();
    map[Api_constant.email] = email.trim();
//    map[Api_constant.password] = pswdValue.trim();
    map[Api_constant.lng] = Api_constant.en;

    map[Api_constant.first_name] = fname;
    map[Api_constant.last_name] = lname;
//    map[Api_constant.phone_number] = mobileValue;

    MyPreferenceManager _myPreferenceManager =
        await MyPreferenceManager.getInstance();
    String token =
        _myPreferenceManager.getString(MyPreferenceManager.DEVICE_TOKEN);

    print(token.toString() + "Logintoken..............");

    map[Api_constant.device_token] = token;
    if (Platform.isIOS) {
      map[Api_constant.device_type] = Api_constant.DEVICE_TYPE_IOS;
    } else {
      map[Api_constant.device_type] = Api_constant.DEVICE_TYPE;
    }

    print(map.toString() + "Login..............");
    print(Api_constant.social_login_customer_api + "Login..............");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response =
        await http.post(Api_constant.social_login_customer_api, body: map);
    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var loginResponseObj =
          new SocialLoginResponse.fromJsonMap(userMap) as SocialLoginResponse;

      if (loginResponseObj.status == 1) {
//        Toast.show(loginResponseObj.message, context,duration: Toast.LENGTH_LONG,
//            gravity: Toast.CENTER);

        if (loginResponseObj.first_time_login.toString() == '1') {
          //go to phone number verification
          showDialog(
              context: context,
              builder: (BuildContext context) =>
                  PhoneOtpDialog(email, fname, lname, ""));
        } else {
          var loginUserObj = loginResponseObj.data;

          MyPreferenceManager _myPreferenceManager =
              await MyPreferenceManager.getInstance();
          _myPreferenceManager.setBool(MyPreferenceManager.IS_USER_LOGIN, true);
          _myPreferenceManager.setString(
              MyPreferenceManager.CURRENT_LOGIN_USER_ID,
              loginUserObj.id.toString());
          _myPreferenceManager.setString(
              MyPreferenceManager.CORPORATE, loginUserObj.corporate.toString());

          _myPreferenceManager.setString(
              MyPreferenceManager.LOGIN_NAME,
              CommonWidget.replaceNullWithEmpty(loginUserObj.first_name) +
                  " " +
                  CommonWidget.replaceNullWithEmpty(loginUserObj.last_name));

          print("userid " + loginUserObj.id.toString());

//      Navigator.pushNamed(context, MyRequestTabsScreen.routeName);
          Navigator.of(context).pushReplacementNamed(PostJob1New.routeName);
        }
      } else {
        Toast.show(loginResponseObj.message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }
}

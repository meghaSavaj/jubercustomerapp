import 'dart:async';
import 'dart:convert';



import 'package:arabacustomer/PostJob1New.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/Utils/MyPreferenceManager.dart';
import 'package:arabacustomer/model/LoginResponse.dart';
import 'package:arabacustomer/model/VerifyPhooneOtpResponse.dart';
import 'package:arabacustomer/model/getOtpResponse.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

import '../Utils/Consts.dart';

class PhoneOtpDialog extends StatefulWidget {

  PhoneOtpDialog(this.email, this.fname, this.lname, this.socialkey);

  String email;
  String fname;
  String lname;
  String socialkey;

  bool IsResendTimerShow = false;

  @override
  forgotPasswordState createState() => forgotPasswordState();
}

class forgotPasswordState extends State<PhoneOtpDialog> {
  String  phonenumber_otp = '';
  String  phonenumberr = '';
  bool IsPhoneOtpSendComplete = false;

  void confirmationPopup(BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(Api_constant.Araba),
          content: new Text(Api_constant.cancel_regitration_confirmation_msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(Api_constant.NO),
              textColor: Theme.of(context).primaryColor,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),

            new FlatButton(
              child: new Text(Api_constant.YES),
              textColor: Theme.of(context).primaryColor,
              onPressed: () async {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  Future<bool> _onWillPop() async {
    print("_onWillPop===========" );
    confirmationPopup(context);
    return true;
  }

  @override
  void initState() {
    super.initState();
  }

  Timer _timerr;
  int _start = 60;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timerr = new Timer.periodic(
      oneSec,
          (Timer timer) => setState(() {
          if (_start < 1) {
            timer.cancel();
            setState(() {
              widget.IsResendTimerShow =  true;
            });
          } else {
            _start = _start - 1;
          }
        },
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
      child:  SafeArea(
        child: Scaffold(
          appBar: new AppBar(
            iconTheme: new IconThemeData(color: Colors.black),
            leading: IconButton(
              icon: Icon(
                Icons.clear,
                color: Colors.black87,
                size: 20,
              ),
              onPressed: () => {

                confirmationPopup(context)
              },
            ),
            title: const Text( Api_constant.Check_mobile_number,
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 14,
                  fontWeight: FontWeight.w600),),
            backgroundColor: Colors.white,

          ),
          body: Padding(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox(height: 15.0),
                        Visibility(
                          visible: true,
//                          visible: !IsPhoneOtpSendComplete,
                          child: TextField(
                            readOnly: IsPhoneOtpSendComplete,
//                            maxLength: Consts.TextField_phonenumber_maxLength,
//                      decoration: InputDecoration(labelText: 'Email'),
                            decoration: InputDecoration(
                              border: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Colors.white,
                                      style: BorderStyle.solid)
                              ),
                              hintText: "",
                              labelStyle: TextStyle(color: Colors.black87),
                              labelText: Api_constant.enter_mobile_number,
                              alignLabelWithHint: true,

                              hintStyle: TextStyle(color: Colors.grey, fontSize: 13),
                            ),
                            showCursor: true,
                            autofocus: true,
                            cursorColor: Colors.red,
                            keyboardType: TextInputType.number,
                            onChanged: (val) {
                              phonenumberr = val;
                            },
                          ),
                        ),
                        SizedBox(height: 15.0),
                        Visibility(
                          visible: IsPhoneOtpSendComplete,
                          child: TextField(
//                            maxLength: Consts.TextField_maxLength_otp,
//                      decoration: InputDecoration(labelText: 'Email'),
                            decoration: InputDecoration(
                              border: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Colors.white,
                                      style: BorderStyle.solid)
                              ),
                              hintText: "",
                              labelStyle: TextStyle(color: Colors.black87),
                              labelText: Api_constant.phone_otp_hint,
                              alignLabelWithHint: true,

                              hintStyle: TextStyle(color: Colors.grey, fontSize: 13),
                            ),
                            showCursor: true,
                            autofocus: true,
                            cursorColor: Colors.red,
                            keyboardType: TextInputType.number,
                            onChanged: (val) {
                              phonenumber_otp = val;
                            },
                          ),
                        ),

//                        CommonWidget.divider(),
                        SizedBox(height: 30.0),

                        widget.IsResendTimerShow ?
                        InkWell(
                          child: Text(Api_constant.Resend_new_code + "("+_start.toString()+")",
                          style: TextStyle(fontSize: 15, color: Theme.of(context).primaryColor),),
                          onTap: (){
                            sendOtp();
                          },
                        ) : Text(Api_constant.Resend_new_code+ "("+_start.toString()+")",
                            style: TextStyle(fontSize: 15, color: Colors.black26)),

                        SizedBox(height: 20.0),

                        Visibility(visible: IsPhoneOtpSendComplete, child: SubmitButton(Api_constant.confirm_access_code, onChanged, 10),), // verify otp
                        Visibility(visible: !IsPhoneOtpSendComplete, child: SubmitButton(Api_constant.send_the_access_code, onSendOtp, 10),), //send otp

                        SizedBox(height: 15.0),

                        CommonWidget.submitButtonBottomLine(),
                        SizedBox(height: 15.0),

                      ],

                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      onWillPop: _onWillPop,
    );
//      Dialog(
//
//      shape: RoundedRectangleBorder(
//        borderRadius: BorderRadius.circular(Consts.alert_dialog_padding),
//      ),
//      elevation: 0.0,
//      backgroundColor: Colors.white,
//      child: Wrap(
//        children: <Widget>[
//
//        ],
//      ),
//    );
  }
  void onSendOtp() {
    if(phonenumberr.isNotEmpty && phonenumberr!= 213){
//      verifyOtp(otp);
      sendOtp();
//      widget.callbackOfForgotPasswordDialog.getotp(phonenumber_otp, email_otp);
//      Navigator.pop(context);
    } else{
      Toast.show(Api_constant.mobile_empty, context,
          duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
    }
  }

  void onChanged() {

    if(phonenumber_otp.isNotEmpty ){
//      verifyOtp(otp);
      verifyOtp(phonenumber_otp);
//      widget.callbackOfForgotPasswordDialog.getotp(phonenumber_otp, email_otp);
//      Navigator.pop(context);
    } else{
      Toast.show(Api_constant.Please_enter_code, context,
          duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
    }

//     apiCAllForgotPswd();
  }
  bool IsProgressIndicatorShow = false;

  Future<void> verifyOtp(String phone_otp) async {

    var map = new Map<String, dynamic>();
//    map[Api_constant.phone_number] = widget.phoneNumber.trim();
    map[Api_constant.otp_param] = phone_otp;
    map['email'] = widget.email.trim();
    map['phone_number'] =  removeCountryCode(phonenumberr.trim());

    print(map.toString() +"verifyOtp..............");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    print(Api_constant.verify_phone_otp );

    var response = await http.post(Api_constant.verify_phone_otp, body: map);
    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str +"..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj = new VerifyPhooneOtpResponse.fromJsonMap(userMap) as VerifyPhooneOtpResponse;
      if (loginResponseObj.status == 1) {

        RegistrationUser();

//        Toast.show(userMap.toString(), context, duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
//        Toast.show(loginResponseObj.message.toString(), context, duration: Toast.LENGTH_LONG, duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
//        widget.callbackOfForgotPasswordDialog.getotp(phonenumber_otp, email_otp);




      } else{
        Toast.show(loginResponseObj.message.toString(), context, duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context, duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  Future<void> sendOtp() async {
    var map = new Map<String, dynamic>();
//    map[Api_constant.phone_number] = widget.phoneNumber.trim();
    map['phone_number'] = removeCountryCode(phonenumberr.trim());
    map['email'] = widget.email.trim();

    print(map.toString() + "send otp..............");
    print(Api_constant.send_otp);

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.post(Api_constant.send_phoenotp, body: map);
    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj = new GetOtpResponse.fromJsonMap(userMap) as GetOtpResponse;

      if (loginResponseObj.status == Api_constant.STATUS) {

//        Toast.show(userMap.toString(), context,duration: Toast.LENGTH_LONG, duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
//        Toast.show(loginResponseObj.message, context, duration: Toast.LENGTH_LONG,
//            duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
        _start = 60;
        setState(() {

          if(!IsPhoneOtpSendComplete){
            IsPhoneOtpSendComplete = true;
          }

          widget.IsResendTimerShow = false;
        });
        startTimer();
      } else {
        Toast.show(loginResponseObj.message, context,
            duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  String removeCountryCode(String phone) {
    if(phone != null && phone.isNotEmpty && phone.contains('213')){
      return phone.replaceFirst('213', '');
    } else{
      return phone;
    }
  }

  void RegistrationUser() async {
    var map = new Map<String, dynamic>();
    map[Api_constant.email] = widget.email;
    map[Api_constant.password] = "";
    map[Api_constant.lng] = Api_constant.en;

    map[Api_constant.first_name] = widget.fname;
    map[Api_constant.last_name] = widget.lname;
    map[Api_constant.phone_number] = phonenumberr;
    map[Api_constant.lng] = Api_constant.en;
    map[Api_constant.gender] = '';

    var instance = await MyPreferenceManager.getInstance();
    map[Api_constant.device_token] =
        instance.getString(MyPreferenceManager.DEVICE_TOKEN);
    map[Api_constant.device_type] = Api_constant.DEVICE_TYPE;

    setState(() {
      IsProgressIndicatorShow = true;
    });

    print(map.toString()+ "..............");
    print(Api_constant.social_signup_customer_api + "..............");
    var response = await http.post(Api_constant.social_signup_customer_api, body: map);

    setState(() {
      IsProgressIndicatorShow = false;
    });


    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var loginResponseObj = new LoginResponse.fromJsonMap(userMap) as LoginResponse;

      if(loginResponseObj.status ==1){
//        Toast.show(loginResponseObj.message, context,duration: Toast.LENGTH_LONG,
//            duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);

        var loginUserObj = loginResponseObj.data;

        MyPreferenceManager _myPreferenceManager =
        await MyPreferenceManager.getInstance();
        _myPreferenceManager.setBool(MyPreferenceManager.IS_USER_LOGIN, true);
        _myPreferenceManager.setString(MyPreferenceManager.CURRENT_LOGIN_USER_ID,
            loginUserObj.id.toString());
        _myPreferenceManager.setString(MyPreferenceManager.CORPORATE, loginUserObj.corporate.toString());

        _myPreferenceManager.setString(MyPreferenceManager.LOGIN_NAME, CommonWidget.replaceNullWithEmpty(loginUserObj.first_name)+" "+
            CommonWidget.replaceNullWithEmpty(loginUserObj.last_name));

        print("userid " + loginUserObj.id.toString());

//      Navigator.pushNamed(context, MyRequestTabsScreen.routeName);
//        Navigator.pop(context);
        Navigator.of(context).pushReplacementNamed(PostJob1New.routeName);
      } else{
        Toast.show(loginResponseObj.message, context,duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }


}
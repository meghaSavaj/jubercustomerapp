import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../Utils/Consts.dart';

class CallbackOfratingDialog {
  void ratingSubmit(String email) {}
}

class EnjoyServiceRatingDialog extends StatefulWidget {

  EnjoyServiceRatingDialog(this.job_id, this.review_text_by_customer);
//  EnjoyServiceRatingDialog(this.callbackOfForgotPasswordDialog);

  String job_id;
  String review_text_by_customer;
  CallbackOfratingDialog callbackOfForgotPasswordDialog;

  @override
  EnjoyServiceRatingDialogState createState() => EnjoyServiceRatingDialogState();
}

class EnjoyServiceRatingDialogState extends State<EnjoyServiceRatingDialog> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    String forgotPswdValue = '';
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(170.0),
//        color: Colors.white,
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      child:  Container(
//        decoration: new BoxDecoration(
//          image: new DecorationImage(
//            image: new AssetImage('assets/images/login_background.png'),
//            fit: BoxFit.cover,
//
//          ),
//        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 35.0),
                    RichText(
                      textAlign: TextAlign.start,
                      text: TextSpan(
//                style: DefaultTextStyle.of(context).style,
                        children: <TextSpan>[
                          TextSpan(
                            text: "Enjoying Service \n ",
                            style: TextStyle(
                                color: Colors.black54.withOpacity(0.6),
                                fontSize: 20,
                                fontWeight: FontWeight.w600),
                          ),
                          TextSpan(
                            text: "Tap a star to rate it on.",
                            style: TextStyle(
                                color: Colors.black54.withOpacity(0.8),
                                fontSize: 14,
                                fontWeight: FontWeight.w300),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30.0),
                    RatingBar(
                      initialRating: 2,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      unratedColor: Colors.amber.withAlpha(50),
                      itemCount: 5,
                      itemSize: 32.0,
                      itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                      itemBuilder: (context, _) =>
                          Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                      onRatingUpdate: (rating) {
                        setState(() {
                          //    _rating = rating;
                          //    }),
                        });
                      },),
                    SizedBox(height: 30.0),
                  ],
                ),
              ),
              CommonWidget.divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    child: Text(Api_constant.CANCEL), onPressed: () {},

                  ),
                  CommonWidget.divider(),
                  RaisedButton(
                    child: Text(Api_constant.SUBMIT), onPressed: () {},

                  )
                ],
              )
            ],

          ),
        ),
      ),
    );
  }

}
import 'dart:async';
import 'dart:convert';

import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/dialog/CancelConfirmationDialog.dart';
import 'package:arabacustomer/dialog/NotAcceptDialog.dart';
import 'package:arabacustomer/model/StatusMsgResponse.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/lib_dart_noti_center/src/dart_notification_center_base.dart';
//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import 'package:wakelock/wakelock.dart';

import '../Utils/Consts.dart';

class CallbackOfLocationScanningDialog {
  void jobaccepted() {}
}

class LocationscanningDialog extends StatefulWidget {

  LocationscanningDialog(this.job_id);
//  EnjoyServiceRatingDialog(this.callbackOfForgotPasswordDialog);
  int job_id;
  CallbackOfLocationScanningDialog callbackOflocationScanningDialog;

  @override
  LocationscanningDialogState createState() => LocationscanningDialogState();
}

class LocationscanningDialogState extends State<LocationscanningDialog> implements CallbackOfNotAcceptDialog {
  Timer timer,timerJobAcceptedChek;
  int i = 1;

  @override
  initState()  {

    super.initState();
    DartNotificationCenter.subscribe(
      channel: Consts.JobCancelTimer,
      observer: i,
      onNotification: (result) => {
        Wakelock.disable(),
        timer.cancel()
      },
    );

    Wakelock.enable();
     /* setState(() {

      });*/

    timer = Timer.periodic(
        Duration(seconds: Consts.fourtyFiveSecDelay),
            (Timer t) => {
              Wakelock.disable(),
              timer.cancel(),
          onCanceljob()
        });

  /*  timerJobAcceptedChek = Timer.periodic(
        Duration(seconds: Consts.fourSecDelay),
            (Timer t) => {
            isJobAcceptApiCall()
        });*/

  }



  void onCanceljob() {

    cancleJobApiCall();
  }

  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Dialog(
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: Wrap(
        children: <Widget>[
          Container(
//            height: 450,
//        decoration: new BoxDecoration(
//          image: new DecorationImage(
//            image: new AssetImage('assets/images/login_background.png'),
//            fit: BoxFit.cover,
//
//          ),
//        ),

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Center(
                  child: Column(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[

                           /* GifImage(
                              controller: controller,
                              image: AssetImage("assets/images/location_highlighted.gif"),
                            ),*/
                            Image.asset(
                              "assets/images/location_highlighted.gif",
                              height: 200.0,
                              width: 200.0,
                            ),
                            SizedBox(height: 15.0),
                            RichText(
                              textAlign: TextAlign.start,
                              text: TextSpan(
//                style: DefaultTextStyle.of(context).style,
                                children: <TextSpan>[
//                              TextSpan(
//                                text: Api_constant.Cancel_dialog,
//                                style: TextStyle(
//                                    color: Colors.black87,
//                                    fontSize: 23,
//                                    fontWeight: FontWeight.w600),
//                              ),
                                  TextSpan(
                                    text: "Looking for Driver",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 22,
                                        height: 1.2,
                                        fontWeight: FontWeight.w500),
                                  ),

                                ],
                              ),
                            ),
                            SizedBox(height: 15.0),
                            InkWell(
                                onTap: () {
                                  Consts.isCancelText=true;
                                  Navigator.of(context, rootNavigator: true).pop();
                                  cancleJobApiCall();
                                },
                                child: new Padding(
                                  padding: new EdgeInsets.all(10.0),
                                  child: new Text("Cancel",
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 22,
                                        height: 1.2,
                                        fontWeight: FontWeight.w500),)

                                ),
                              ),
                            SizedBox(height: 20.0),
                          ],

                        ),),
// Navigator.of(context, rootNavigator: true).pop();
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  void retryComplete() {
    // TODO: implement retryComplete
  }

  Future<void> cancleJobApiCall() async {
    print('cancel api called' + "..............");

    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = widget.job_id.toString();
    map[Api_constant.reasone_for_cancel] = "time out";

    print(map.toString() + ".....cancel api .........");

    var response = await http.post(Api_constant.cancelJob, body: map);

    print(response.statusCode.toString() + "..............");
    print(response.body + "..............");
    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
      new StatusMsgResponse.fromJsonMap(userMap) as StatusMsgResponse;

      var loginUserObj = loginResponseObj.message;

      if(loginResponseObj.status == Api_constant.STATUS) {
       // Toast.show(Api_constant.cancel_job_success_msg, context, gravity: Toast.CENTER);

        if(Consts.isCancelText){
          Consts.isCancelText=false;
          Navigator.of(context, rootNavigator: true).pop();
        }else{
          Navigator.of(context, rootNavigator: true).pop();
          showDialog(context: context, builder: (BuildContext context) =>
              NotAcceptDialog(this,widget.job_id)
          );
        }
      } else{
        Toast.show(loginUserObj.toString(), context, gravity: Toast.CENTER);
      }

      setState(() {
      });
    }
  }

 /* Future<void> isJobAcceptApiCall() async {
    print('cancel api called' + "..............");

    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = widget.job_id.toString();


    print(map.toString() + ".....is_job_accept .........");

    var response = await http.post(Api_constant.is_job_accepted, body: map);

    print(response.statusCode.toString() + "..............");
    print(response.body + "..............");
    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
      new StatusMsgResponse.fromJsonMap(userMap) as StatusMsgResponse;

      var loginUserObj = loginResponseObj.message;

      if(loginResponseObj.status == Api_constant.STATUS) {
        // Toast.show(Api_constant.cancel_job_success_msg, context, gravity: Toast.CENTER);
          if(Consts.isLokkingForDriverPopupOpen){
            timerJobAcceptedChek.cancel();
              Navigator.of(context, rootNavigator: true).pop();
          }

      } else{
        isJobAcceptApiCall();
        //Toast.show(loginUserObj.toString(), context, gravity: Toast.CENTER);
      }

      setState(() {
      });
    }
  }*/
}
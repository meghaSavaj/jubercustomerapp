import 'dart:convert';

import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/model/StatusMsgResponse.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';

//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

import '../Utils/Consts.dart';

class CallbackOfForgotPasswordDialog {
  void forgotPasswdSendEmail(String toastmsg) {}
}

class forgotPassword extends StatefulWidget {

  forgotPassword(this.callbackOfForgotPasswordDialog);

  CallbackOfForgotPasswordDialog callbackOfForgotPasswordDialog;

  @override
  forgotPasswordState createState() => forgotPasswordState();
}

class forgotPasswordState extends State<forgotPassword> {
  String forgotPswdValue = '';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(

      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.alert_dialog_padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: Container(
        height: 400,
//        decoration: new BoxDecoration(
//          image: new DecorationImage(
//            image: new AssetImage('assets/images/login_background.png'),
//            fit: BoxFit.cover,
//
//          ),
//        ),
        child: ListView(
          reverse: true,
          children: <Widget>[
            Column(
              children: <Widget>[
//                Align(
//                  alignment: Alignment.topRight,
//                  child: Image.asset('assets/images/forgot_password_bg.png'),
//                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,

//                mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
//                        SizedBox(height: 45.0),

                        RichText(
                          textAlign: TextAlign.start,
                          text: TextSpan(
//                style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                text: Api_constant.forgot_password,
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 23,
                                    fontWeight: FontWeight.w600),
                              ),
                              TextSpan(
                                text: Api_constant.forgot_title,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                    height: 1.2,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 20.0),
                        TextField(
//                            maxLength: Consts.TextField_maxLength_for_email,
//                      decoration: InputDecoration(labelText: 'Email'),
                          decoration: InputDecoration(
                            border: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Colors.white,
                                    style: BorderStyle.solid)
                            ),
                            hintText: Api_constant.login_email,
                            labelText: Api_constant.login_email,
                            alignLabelWithHint: true,

                            hintStyle: TextStyle(color: Colors.black87),
                          ),
                          showCursor: true,
                          autofocus: true,
                          cursorColor: Colors.red,
                          onChanged: (val) {
                            forgotPswdValue = val;
                          },
                        ),
//                        CommonWidget.divider(),
                        SizedBox(height: 30.0),

                        SubmitButton(Api_constant.forgot_send_password, onChanged, 10),

                        SizedBox(height: 15.0),

                        CommonWidget.submitButtonBottomLine(),
                        SizedBox(height: 15.0),

                      ],

                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
  void onChanged() {
     apiCAllForgotPswd();
  }
  Future<void> apiCAllForgotPswd() async {
    print('called' + "..............");

    if (forgotPswdValue.isEmpty) {
      Toast.show(Api_constant.email_empty, context, gravity: Toast.CENTER);
    } else {
      var map = new Map<String, dynamic>();
      map[Api_constant.email] = forgotPswdValue;


      print(Api_constant.forgot_password_api + "..............");
      var response = await http.post(
          Api_constant.forgot_password_api, body: map);

      print(response.statusCode.toString() + "..............");
      print(response.body + "..............");
      if (response.statusCode == 200) {
        String response_json_str = response.body;
        print(response_json_str + "..............");
        Map userMap = jsonDecode(response_json_str);

        var loginResponseObj = new StatusMsgResponse.fromJsonMap(
            userMap) as StatusMsgResponse;

        var loginUserObj = loginResponseObj.message;

        widget.callbackOfForgotPasswordDialog.forgotPasswdSendEmail(loginUserObj);
        Navigator.pop(context);
      }
    }
  }
}
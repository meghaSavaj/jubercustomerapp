import 'dart:convert';
import 'dart:core';
import 'dart:ffi';

import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/Utils/Utility.dart';
import 'package:arabacustomer/model/AboutUsResponse.dart';
import 'package:arabacustomer/model/CommanModal.dart';
import 'package:arabacustomer/model/StatusMsgResponse.dart';
import 'package:arabacustomer/model/Vehicle_category.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';

//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../Utils/Consts.dart';

class CallbackOfPrivacyPolicydDialog {
  void ppCallback() {}
}

class PrivacyPolicyDialog extends StatefulWidget {

//  VehicleList();
//  PrivacyPolicyDialog(this.callbackOfForgotPasswordDialog);
//  CallbackOfPrivacyPolicydDialog callbackOfForgotPasswordDialog;


  @override
  PrivacyPolicyDialogState createState() => PrivacyPolicyDialogState();
}

class PrivacyPolicyDialogState extends State<PrivacyPolicyDialog> {
  bool IsProgressIndicatorShow = false;

  String aboutContent="";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
//    vehivlelist.add(VehicleCategory())
    return  new Scaffold(
        appBar: new AppBar(
          iconTheme: new IconThemeData(color: Colors.black),
          title: const Text(Api_constant.ABOUT_ARABA, style: TextStyle(color: Colors.red)),
          backgroundColor: Colors.white,
        ),
        body: Padding(
        padding: EdgeInsets.fromLTRB(12, 2, 12, 8),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
//              Align(
//                alignment: Alignment.topRight,
//                child: Image.asset('assets/images/aboutus_bg.png'),
//              ),
              /*Align(
                alignment: Alignment.topRight,
                child:  IconButton(
                  icon: Icon(Icons.close, color: Colors.grey[600], size: 20,),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),*/
            /*  Padding(
                padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                child:
                CommonWidget.redHeaderBigLbl(Api_constant.ABOUT_ARABA),
              ),*/
              new Expanded(
                  flex: 1,
                  child: SingleChildScrollView(
                      child: Padding(
                          padding: EdgeInsets.all(15),
                          child:Html(
                              data:  aboutContent
                          ) /*CommonWidget.blackDiscriptionText(
                            aboutContent
                            //'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
                             )*/
                      )
                  )
              )
//                  Padding(
//                      padding: EdgeInsets.all(15),
//                      child: )
            ],
          ),
        ),
      )
    );
  }

  Future<String> getData() async {
    String data="";
    setState(() {
      IsProgressIndicatorShow = true;
    });
    var response = await http.get(Api_constant.terms);
    setState(() {
      IsProgressIndicatorShow = false;
    });
    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var CommanModal_obj = new AboutUsResponse.fromJsonMap(userMap) as AboutUsResponse;
      data = CommanModal_obj.data;
      setState(() {
        aboutContent= data;
        //Toast.show(aboutContent, context, gravity: Toast.CENTER);
      });
      return data;
    } else{
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);

    }
  }
}
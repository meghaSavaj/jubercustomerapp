import 'dart:convert';
//import 'package:flutter/cupertino.dart';
import 'package:arabacustomer/ProcessingTrackingDetail2.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/model/GetDriverLatLongResponse.dart';
import 'package:arabacustomer/model/PassDataToTrackingScreen.dart';
import 'package:arabacustomer/model/ProfileResponse.dart';
import 'package:arabacustomer/model/getDriverLatLong.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';
import 'package:arabacustomer/model/GetOtpResponse.dart';

import '../Login.dart';
import '../Utils/Consts.dart';
import '../myProfileEdit.dart';

class CallbackOfTrackItNowDialog {
  void trackitNumber(PassDataToTrackingScreen passDataToTrackingScreenObj, BuildContext context) {}
}

class TrackItNowDialog extends StatefulWidget {

  TrackItNowDialog(this.callbackOfForgotPasswordDialog);

  CallbackOfTrackItNowDialog callbackOfForgotPasswordDialog;

  @override
  forgotPasswordState createState() => forgotPasswordState();
}

class forgotPasswordState extends State<TrackItNowDialog> {
  String  otp = '';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(

      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.alert_dialog_padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: Stack(
        children: <Widget>[
          Container(
            height: 400,
//        decoration: new BoxDecoration(
//          image: new DecorationImage(
//            image: new AssetImage('assets/images/login_background.png'),
//            fit: BoxFit.cover,
//
//          ),
//        ),
              child:  ListView(
                reverse: true,
                children: <Widget>[
//                  Align(
//                    alignment: Alignment.bottomRight,
//                    child: Image.asset('assets/images/forgot_password_bg.png'),
//                  ),
                  Center(
                    child: Column(
//                mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
//                        SizedBox(height: 45.0),
//                        Align(
//                          alignment: Alignment.bottomRight,
//                          child: Image.asset('assets/images/forgot_password_bg.png'),
//                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,

                            children: <Widget>[
                              RichText(
                                textAlign: TextAlign.start,
                                text: TextSpan(
//                style: DefaultTextStyle.of(context).style,
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: Api_constant.TRACKIT_NOW+'\n\n',
                                      style: TextStyle(
                                          color: Colors.black87,
                                          fontSize: 23,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    TextSpan(
                                      text: 'Entrez le numéro de localisation pour suivre le transporteur',
//                                      text: 'entrez le numéro du tracking pour suivre le transporteur',
//                                      text: 'Entrez le numéro de piste pour la piste',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 18,
                                          height: 1.2,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 20.0),
                              TextField(
//                                maxLength: Consts.TextField_maxLength_otp,
//                      decoration: InputDecoration(labelText: 'Email'),
                                decoration: InputDecoration(
                                  border: new UnderlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: Colors.white,
                                          style: BorderStyle.solid)
                                  ),
                                  hintText: 'Numéro de localisation',
                                  labelText: '',
                                  alignLabelWithHint: true,

                                  hintStyle: TextStyle(color: Colors.black26,),
                                ),
                                showCursor: true,
                                autofocus: true,
                                cursorColor: Colors.red,
                                onChanged: (val) {
                                  otp = val;
                                },
                              ),
//                        CommonWidget.divider(),
                              SizedBox(height: 30.0),

                              SubmitButton(Api_constant.TRACKIT_NOW, onChanged, 10),

                              SizedBox(height: 10.0),

                              CommonWidget.submitButtonBottomLine(),
//                           SizedBox(height: 15.0),

                            ],
                          ),
                        )
                      ],

                    ),
                  ),
                ],
              )
          ),
//          Expanded(
//            flex: 1,
//            child:
            Container(
              height: 300,
              child: Center(child:  Visibility(
                visible: IsProgressIndicatorShow,
                child: SizedBox(
                  child: CircularProgressIndicator(),
                ),
              ),
              ),
            ),
//          )
        ],
      ),
    );
  }

  void onChanged() {

    if(otp.isNotEmpty){
//      Navigator.pop(context);

      verifyOtp(otp);
    } else{
      Toast.show('Veuillez entrer le numéro Trackit', context,
          gravity: Toast.TOP);
    }

//     apiCAllForgotPswd();
  }
  bool IsProgressIndicatorShow = false;

  Future<void> verifyOtp(String otp) async {

    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = otp;

    print(map.toString() + "..............");
    print(Api_constant.getdriver_location_api+ "..............");
    setState(() {
      IsProgressIndicatorShow = true;
    });
    var response = await http.post(Api_constant.getdriver_location_api, body: map);
    setState(() {
      IsProgressIndicatorShow = false;
    });
    print(response.statusCode.toString() + "..............");
    print(response.body + "..............");

    if (response.statusCode == 200) {
      String response_json_str = response.body;

      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
      print(userMap.toString() + "..............");

      var loginResponseObj = new GetDriverLatLongResponse.fromJsonMap(userMap) as GetDriverLatLongResponse;

      if (loginResponseObj.status == Api_constant.STATUS) {
        GetDriverLatLong myRequestListObj =  loginResponseObj.data;

        PassDataToTrackingScreen passDataToTrackingScreenObj = new PassDataToTrackingScreen();

        passDataToTrackingScreenObj.job_id = myRequestListObj.id;
        passDataToTrackingScreenObj.pickup_address = '';
        passDataToTrackingScreenObj.pickup_lattitude = myRequestListObj.pickup_lattitude;
        passDataToTrackingScreenObj.pickup_longitude = myRequestListObj.pickup_longitude;
        passDataToTrackingScreenObj.delivery_address = '';
        passDataToTrackingScreenObj.delivery_lattitude = myRequestListObj.delivery_lattitude;
        passDataToTrackingScreenObj.delivery_longitude = myRequestListObj.delivery_longitude;
        passDataToTrackingScreenObj.driver_phone_number = '';
        passDataToTrackingScreenObj.IsFromSideMenu = true;

//        Toast.show(widget.callbackOfForgotPasswordDialog.toString(), context, gravity: Toast.CENTER);

//        widget.callbackOfForgotPasswordDialog.trackitNumber(passDataToTrackingScreenObj, context);
//        Navigator.of(context).pushReplacementNamed(ProcessingTrackingDetail2.routeName, arguments: passDataToTrackingScreenObj);

//        Navigator.pop(context);
//        Navigator.push(context, MaterialPageRoute(builder: (context) =>
//            ProcessingTrackingDetail2(myRequestListObj: args as PassDataToTrackingScreen)));
        Navigator.of(context).pushReplacementNamed(ProcessingTrackingDetail2.routeName, arguments: passDataToTrackingScreenObj);

      } else if(loginResponseObj.status == 2){
        Toast.show("Le travail est déjà terminé", context, gravity: Toast.TOP);
        Navigator.pop(context);
      } else{
        Toast.show('Numéro de suivi non valide', context, gravity: Toast.TOP);

      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.TOP);
      throw Exception('Failed to load internet');
    }
  }

//  Future<void> apiCAllForgotPswd() async {
//    print('called' + "..............");
//
//    if (forgotPswdValue.isEmpty) {
//      Toast.show(Api_constant.email_empty, context, gravity: Toast.CENTER);
//    } else {
//      var map = new Map<String, dynamic>();
//      map[Api_constant.email] = forgotPswdValue;
//
//
//      print(Api_constant.forgot_password_api + "..............");
//      var response = await http.post(
//          Api_constant.forgot_password_api, body: map);
//
//      print(response.statusCode.toString() + "..............");
//      print(response.body + "..............");
//      if (response.statusCode == 200) {
//        String response_json_str = response.body;
//        print(response_json_str + "..............");
//        Map userMap = jsonDecode(response_json_str);
//
//        var loginResponseObj = new StatusMsgResponse.fromJsonMap(
//            userMap) as StatusMsgResponse;
//
//        var loginUserObj = loginResponseObj.message;
//        Toast.show(loginUserObj.toString(), context, gravity: Toast.CENTER);
//        Navigator.pop(context);
//      }
//    }
//  }
}
import 'dart:convert';
import 'dart:core';
import 'dart:ffi';

import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/Utils/Utility.dart';
import 'package:arabacustomer/model/CommanModal.dart';
import 'package:arabacustomer/model/StatusMsgResponse.dart';
import 'package:arabacustomer/model/Vehicle_category.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';

//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

import '../Utils/Consts.dart';

class CallbackOfvlistdDialog {
  void vlCallback(int id, String vahicle_name, String per_km_rate, String min_price, String img) {}
}

class VehicleList extends StatefulWidget {

//  VehicleList();
  VehicleList(this.callbackOfForgotPasswordDialog);
  CallbackOfvlistdDialog callbackOfForgotPasswordDialog;


  @override
  VehicleListState createState() => VehicleListState();
}

class VehicleListState extends State<VehicleList> {
  String forgotPswdValue = '';

//  List<VehicleCategory> vehivlelist = [];
  List<VehicleCategory> vehivlelist = new List();

  void fetchData() {
    getData().then((res) {
      setState(() {
        vehivlelist.addAll(res);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    Utility.checkInternetConnection().then((intenet) {
      if (intenet != null && intenet) {
        fetchData();
      }else {
        Toast.show(Api_constant.no_internet_connection, context,
            gravity: Toast.CENTER);
      }
    });


  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
//    vehivlelist.add(VehicleCategory())
    return  new Scaffold(
      appBar: new AppBar(
        iconTheme: new IconThemeData(color: Colors.black),
        title: const Text(Api_constant.Select_Vehicle, style: TextStyle(color: Colors.red)),
        backgroundColor: Colors.white,
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(0, 2, 0, 8),

        child:  Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
           /* Align(
              alignment: Alignment.topRight,
              child:  IconButton(
                icon: Icon(Icons.close, color: Colors.grey[600], size: 20,),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Center(
              child: CommonWidget.redHeaderLbl(Api_constant.Select_Vehicle)
//              Text(Api_constant.Select_Vehicle,  style: TextStyle(
//                  color: Colors.black87,
//                  fontSize: 18,
//                  fontWeight: FontWeight.w600),),
            ),*/
            SizedBox(height: 15,),
           /* Container(
              height: 0.8,
              color: Colors.grey,
            ),*/
//            SizedBox(height: 5,),
            SizedBox(
//              height: 350,
              height: MediaQuery.of(context).size.height * 0.7,
              child: ListView.builder(
                itemCount: vehivlelist.length,
                itemBuilder: (context, index){
                  return GestureDetector(
                    onTap: () {
                      widget.callbackOfForgotPasswordDialog.vlCallback(vehivlelist[index].id, vehivlelist[index].category_name,
                          vehivlelist[index].rate_per_km, vehivlelist[index].min_price,
                      vehivlelist[index].image.toString());

                      Navigator.pop(context);
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: ListTile(
                            onTap: () {
                              Consts.vahicle_name = vehivlelist[index].category_name.toString();
                              Consts.vehicle_id = vehivlelist[index].id.toString();
                              Consts.vehicle_img = vehivlelist[index].image.toString();
                              Consts.rate_per_km = vehivlelist[index].rate_per_km.toString();
                              Consts.min_price = vehivlelist[index].min_price.toString();
                              widget.callbackOfForgotPasswordDialog.vlCallback(vehivlelist[index].id, vehivlelist[index].category_name,
                                  vehivlelist[index].rate_per_km,vehivlelist[index].min_price,
                              vehivlelist[index].image.toString());

                              Navigator.pop(context);
                            },
                            leading: SizedBox(
                              child: Image.network(vehivlelist[index].image,
                                  width: 40,
                                  height: 40,
                                  fit: BoxFit.cover,
                                  alignment: Alignment.center),
                              width: 40,
                              height: 40,
                            ),
                            title: Text(vehivlelist[index]
                                .category_name
                                .toString(),
                                style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 16)),
                            /*subtitle: Text(
                                                Api_constant.rate_per_km +
                                                    vehivlelist[index]
                                                        .rate_per_km_with_currency
                                                        .toString(),
                                                style: TextStyle(fontSize: 16),
                                              ),*/
                          ),
//                          color: _getColorOfItem(index),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                          child: Container(
                            height: 0.8,
                            color: Colors.grey,
                          ),
                        )
//                                          Divider(
//                                            thickness: 0.8,
//                                            color: Colors.grey,
//                                          )
                      ],
                    ),
//                    child: Padding(
//                        padding: EdgeInsets.fromLTRB(5, 20, 5, 5),
//                        child: Column(
//                          crossAxisAlignment: CrossAxisAlignment.center,
//                          children: <Widget>[
//                            Text(vehivlelist[index].category_name,
//                              style: TextStyle(
//                                  color: Colors.black87,
//                                  fontSize: 16,
//                                  fontWeight: FontWeight.w400),),
//                            Divider(
//                              thickness: 1,
//                            )
//                          ],
//                        )
//                    ),
                  );
                },
              ),
            )
            //          Container(
//            height: 450,
////        decoration: new BoxDecoration(
////          image: new DecorationImage(
////            image: new AssetImage('assets/images/login_background.png'),
////            fit: BoxFit.cover,
////
////          ),
////        ),
//            child: Column(
//              children: <Widget>[
//                Align(
//                  alignment: Alignment.topRight,
//                  child: Image.asset('assets/images/forgot_password_bg.png'),
//                ),
//                Padding(
//                  padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
//                  child: Center(
//                    child: Column(
//                      mainAxisSize: MainAxisSize.min,
//                      children: <Widget>[
//                        RichText(
//                          textAlign: TextAlign.start,
//                          text: TextSpan(
//                            children: <TextSpan>[
//                              TextSpan(
//                                text: 'Forgot Password\n\n',
//                                style: TextStyle(
//                                    color: Colors.black87,
//                                    fontSize: 23,
//                                    fontWeight: FontWeight.w600),
//                              )
//                            ],
//                          ),
//                        ),
//
//                      ],
//
//                    ),
//                  ),
//                )
//              ],
//            ),
//          )
          ],
        ),
      ),
    );
  }

  Void onChanged()  {

  }

  Future<List<VehicleCategory>> getData() async {
    var response = await http.get(Api_constant.get_service_api);

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str +"..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var CommanModal_obj = new CommanModal.fromJson(userMap) as CommanModal;
      List<Object> resultList = CommanModal_obj.data;

      List<VehicleCategory> myContributionList = new List(resultList.length);

      for(int i=0; i<resultList.length; i++) {
        Object obj = resultList[i];
        VehicleCategory g = new VehicleCategory.fromJsonMap(obj);
        myContributionList[i] = g;
      }


      return myContributionList;
    }else{
      return  new List<VehicleCategory>();
    }
  }
}
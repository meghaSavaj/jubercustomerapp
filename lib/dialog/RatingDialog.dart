import 'dart:convert';

import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/model/StatusMsgResponse.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;

import '../Utils/Consts.dart';

class CallbackOfRatinngdDialog {
  void ratingComplete() {}
}

class RatingDialog extends StatefulWidget {

  RatingDialog(this.job_id, this.review_text_by_customer, this.callbackOfratingdDialog);
//  EnjoyServiceRatingDialog(this.callbackOfForgotPasswordDialog);
  CallbackOfRatinngdDialog callbackOfratingdDialog;

  String job_id;
  String review_text_by_customer;
  double rating_by_customer=0.0;

  @override
  RatingDialogState createState() => RatingDialogState();
}

class RatingDialogState extends State<RatingDialog> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(

      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.alert_dialog_padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: Wrap(
        children: <Widget>[
          Container(
//            height: 450,
//        decoration: new BoxDecoration(
//          image: new DecorationImage(
//            image: new AssetImage('assets/images/login_background.png'),
//            fit: BoxFit.cover,
//
//          ),
//        ),
            child: Column(
              children: <Widget>[
//                Align(
//                  alignment: Alignment.topRight,
//                  child: Image.asset('assets/images/forgot_password_bg.png'),
//                ),
                Center(
                  child: Column(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[

                            RichText(
                              textAlign: TextAlign.start,
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                    text: Api_constant.Enjoying_Service,
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: 23,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  TextSpan(
                                    text: Api_constant.Tap_a_star,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        height: 1.2,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 20.0),
                            RatingBar(
                              initialRating: widget.rating_by_customer,
                              minRating: 0,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              unratedColor: Colors.amber.withAlpha(50),
                              itemCount: 5,
                              itemSize: 32.0,
                              itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                              itemBuilder: (context, _) =>
                                  Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  ),
                              onRatingUpdate: (rating) {
                                setState(() {
                                  widget.rating_by_customer = rating;
                                  //    }),
                                });
                              },),
                          ],

                        ),),
                      CommonWidget.divider(),


                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          InkWell(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
                              child: Text(Api_constant.CANCEL,
                                style: TextStyle(color: Colors.grey[400],
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20),
                              ),
                            ),
                            onTap: () {
                              Navigator.pop(context);
                            },
                          ),
//                          VerticalDivider(color: Colors.red, width: 20),
                          Container(
                            color: Colors.grey[400],
                            width: 0.8,
                            height: 70,
                            //height: -> setting to maximum of its parent
                          ),
                          InkWell(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
                              child: Text(Api_constant.SUBMIT,
                                  style: TextStyle(color: Theme.of(context).primaryColor,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 20)),
                            ),
                            onTap: () {
                              ratingAPiCall();
                            },
                          )

                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Future<void> ratingAPiCall() async {
    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = widget.job_id;
    map[Api_constant.ratings_by_customer] = widget.rating_by_customer.toString();
    map[Api_constant.review_text_by_customer] = widget.review_text_by_customer;

    var response = await http.post(
        Api_constant.customer_side_rating, body: map);

     print(Api_constant.customer_side_rating + "..............");
     print(map.toString() + "..............");
     print(response.statusCode.toString() + "..............");
     print(response.body.toString() + "..............");

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj = new StatusMsgResponse.fromJsonMap(userMap) as StatusMsgResponse;

      var loginUserObj = loginResponseObj.message;
     // Toast.show(loginUserObj.toString(), context, gravity: Toast.CENTER);
      Toast.show(Api_constant.rating_msg, context, gravity: Toast.CENTER);
      widget.callbackOfratingdDialog.ratingComplete();
      Navigator.pop(context);
    }
  }
}
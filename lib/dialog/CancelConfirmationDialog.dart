import 'dart:convert';

import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/model/StatusMsgResponse.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';

//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

import '../Utils/Consts.dart';

class CallbackOfCanceldDialog {
  void cancelJob(String jobid, String cancel_reason) {}
}

class CancelConfirmationDialog extends StatefulWidget {

  CancelConfirmationDialog(this._callbackOfCanceldDialog, this.job_id);

  String job_id;
  CallbackOfCanceldDialog _callbackOfCanceldDialog;

  @override
  CancelConfirmationDialogState createState() => CancelConfirmationDialogState();
}

class CancelConfirmationDialogState extends State<CancelConfirmationDialog> {
  String cancelReasonValue = '';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(

      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.alert_dialog_padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: Container(
            height: 325,
//        decoration: new BoxDecoration(
//          image: new DecorationImage(
//            image: new AssetImage('assets/images/login_background.png'),
//            fit: BoxFit.cover,
//
//          ),
//        ),
        child: ListView(
          reverse: true,
          children: <Widget>[
            Column(
              children: <Widget>[
//                Align(
//                  alignment: Alignment.topRight,
//                  child: Image.asset('assets/images/forgot_password_bg.png'),
//                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 10.0),
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,

//                mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
//                        SizedBox(height: 45.0),

                        RichText(
                          textAlign: TextAlign.start,
                          text: TextSpan(
//                style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
//                              TextSpan(
//                                text: Api_constant.Cancel_dialog,
//                                style: TextStyle(
//                                    color: Colors.black87,
//                                    fontSize: 23,
//                                    fontWeight: FontWeight.w600),
//                              ),
                              TextSpan(
                                text: Api_constant.Cancel_dialog_msg,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                    height: 1.2,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 25.0),
                        TextField(
//                          maxLength: Consts.TextField_maxLength_cancelreason,
//                      decoration: InputDecoration(labelText: 'Email'),
                          keyboardType: TextInputType.multiline,
                          decoration: InputDecoration(
//                            border: new UnderlineInputBorder(
//                                borderSide: new BorderSide(
//                                    color: Colors.white,
//                                    style: BorderStyle.solid)
//                            ),
                            border: new OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(4.0),
                              ),
                            ),
                            hintText: Api_constant.cancel_reason,
//                            labelText: Api_constant.cancel_reason,
                            alignLabelWithHint: true,
//                            labelStyle: TextStyle(color: Colors.black87, fontSize: 17),
                            hintStyle: TextStyle(color: Colors.grey),
                          ),
                          showCursor: true,
                          autofocus: true,
                          minLines: 4,
                          maxLines: 20,
                          cursorColor: Colors.red,
                          onChanged: (val) {
                            cancelReasonValue = val;
                          },
                        ),
//                        TextField(
//                          style: TextStyle(color: Colors.black87,
//                              fontWeight: FontWeight.w400,
//                              fontSize: 16),
//                          keyboardType: TextInputType.multiline,
//                          decoration: new InputDecoration(
//                              border: new OutlineInputBorder(
//                                borderRadius: const BorderRadius.all(
//                                  const Radius.circular(4.0),
//                                ),
//                              ),
//                              filled: true,
//                              hintText: Api_constant.cancel_reason,
//                              labelText: Api_constant.cancel_reason,
//                              alignLabelWithHint: true,
//                              hintStyle: TextStyle(color: Colors.black87),
//                              fillColor: Colors.white70),
//                          showCursor: true,
//                          autofocus: true,
//                          cursorColor: Colors.red,
//                          onChanged: (val) {
//                            cancelReasonValue = val;
//                          },
//                          minLines: 4,
//                          maxLines: 20,
//                        ),
//                        CommonWidget.divider(),
                        SizedBox(height: 25.0),

                        SubmitButton(Api_constant.cancel_job, onChanged, 10),

                        SizedBox(height: 10.0),

                        CommonWidget.submitButtonBottomLine(),
                        SizedBox(height: 15.0),

                      ],

                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
  void onChanged() {
    if(cancelReasonValue.isEmpty){
      Toast.show(Api_constant.Cancel_reson_validation_msg, context, gravity: Toast.CENTER);

    } else{
      widget._callbackOfCanceldDialog.cancelJob(widget.job_id, cancelReasonValue);
      Navigator.pop(context);
    }
  }
}
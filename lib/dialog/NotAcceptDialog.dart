import 'dart:async';
import 'dart:convert';

import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/model/StatusMsgResponse.dart';

import 'package:flutter/material.dart';

import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import 'package:wakelock/wakelock.dart';

import '../Utils/Consts.dart';
import 'LocationScanningDialog.dart';

class CallbackOfNotAcceptDialog {
  void retryComplete() {}
}

class NotAcceptDialog extends StatefulWidget {

  NotAcceptDialog(this.callbackOfnotacceptDialog,this.job_id);
  int job_id;
//  EnjoyServiceRatingDialog(this.callbackOfForgotPasswordDialog);
  CallbackOfNotAcceptDialog callbackOfnotacceptDialog;

  @override
  NotAcceptDialogState createState() => NotAcceptDialogState();
}

class NotAcceptDialogState extends State<NotAcceptDialog> {



  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Dialog(

     /* shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.alert_dialog_padding),
      ),*/
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: Wrap(
        children: <Widget>[
          Container(
//            height: 450,
//        decoration: new BoxDecoration(
//          image: new DecorationImage(
//            image: new AssetImage('assets/images/login_background.png'),
//            fit: BoxFit.cover,
//
//          ),
//        ),
            child: Column(
              children: <Widget>[
//                Align(
//                  alignment: Alignment.topRight,
//                  child: Image.asset('assets/images/forgot_password_bg.png'),
//                ),
                Center(
                  child: Column(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
                        child: Column(

                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[

                           /* GifImage(
                              controller: controller,
                              image: AssetImage("assets/images/location_highlighted.gif"),
                            ),*/

                            SizedBox(height: 15.0),
                            RichText(
                              textAlign: TextAlign.start,
                              text: TextSpan(
//                style: DefaultTextStyle.of(context).style,
                                children: <TextSpan>[
//                              TextSpan(
//                                text: Api_constant.Cancel_dialog,
//                                style: TextStyle(
//                                    color: Colors.black87,
//                                    fontSize: 23,
//                                    fontWeight: FontWeight.w600),
//                              ),
                                  TextSpan(
                                    text: "Sorry, No driver available",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 22,
                                        height: 1.2,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 20.0),

                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                InkWell(
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Text(Api_constant.CANCEL,
                                      style: TextStyle(color: Colors.grey[400],
                                          fontWeight: FontWeight.w600,
                                          fontSize: 20),
                                    ),
                                  ),
                                  onTap: () {
                                  //  Navigator.pop(context);
                                    Navigator.of(context, rootNavigator: true).pop();
                                  },
                                ),
//                          VerticalDivider(color: Colors.red, width: 20),
                                Container(
                                  color: Colors.grey[400],
                                  width: 0.8,
                                  height: 60,
                                  //height: -> setting to maximum of its parent
                                ),
                                InkWell(
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Text(Api_constant.RETRY,
                                        style: TextStyle(color: Theme.of(context).primaryColor,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 20)),
                                  ),
                                  onTap: () {
                                    retryAPiCall();
                                    //ratingAPiCall();
                                  },
                                )

                              ],
                            ),
                            SizedBox(height: 20.0),
                          ],

                        ),),

                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Future<void> retryAPiCall() async {
    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = widget.job_id.toString();


    var response = await http.post(
        Api_constant.retry_for_job, body: map);

    print(Api_constant.retry_for_job + "...retry api...........");
    print(map.toString() + "........retry api.......");
    print(response.statusCode.toString() + "..............");
    print(response.body.toString() + "..............");
   // var loginResponseObj = new StatusMsgResponse.fromJsonMap(userMap) as StatusMsgResponse;
    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);



    //  var loginUserObj = loginResponseObj.message;
      // Toast.show(loginUserObj.toString(), context, gravity: Toast.CENTER);
      Toast.show("Job Retry Successfully", context, gravity: Toast.CENTER);
      widget.callbackOfnotacceptDialog.retryComplete();
      //Navigator.pop(context);
      Navigator.of(context, rootNavigator: true).pop();
      Consts.FromRetryDialog = true;
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) =>
              LocationscanningDialog(widget.job_id)
      );
    }else{
      Toast.show("Job Not Retry", context, gravity: Toast.CENTER);
    }
  }
}
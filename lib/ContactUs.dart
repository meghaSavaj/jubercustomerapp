import 'dart:convert';

import 'package:arabacustomer/PostJob1New.dart';
import 'package:arabacustomer/model/ContactUsResponse.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SideMenu.dart';
//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';
import 'Utils/Api_constant.dart';
import 'Utils/Consts.dart';
import 'dialog/EnjoyServiceRatingDialog.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactUs extends StatefulWidget  {
  static const routeName = '/ContactUs';

  @override
  ContactUsState createState() {
    return ContactUsState();
  }

}

class ContactUsState extends State<ContactUs> {
  bool IsProgressIndicatorShow = false;

  String ContacUsContent="";
  String ContacUsEmail="";
  String ContacUsEmail2="";
  String ContacUsPhone="";
  String ContacUsPhone2="";
  String ContacUsAddress="";

  @override
  void initState() {
    getData();
  }

  Future<String> getData() async {
    String data="";
    setState(() {
      IsProgressIndicatorShow = true;
    });
    var response = await http.get(Api_constant.contactus);
    setState(() {
      IsProgressIndicatorShow = false;
    });
    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var CommanModal_obj = new ContactUsResponse.fromJsonMap(userMap) as ContactUsResponse;
      data = CommanModal_obj.data;
      setState(() {
        ContacUsContent= data;
        ContacUsEmail= CommanModal_obj.email;
        ContacUsEmail2= CommanModal_obj.email2;
        ContacUsPhone= CommanModal_obj.phone;
        ContacUsPhone2= CommanModal_obj.phone2;
        ContacUsAddress= CommanModal_obj.address;
      });
      return data;
    } else{
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
      return data;
    }
  }
  Future<bool> _onWillPop() async {
    print("_onWillPop===========" );
    Consts.SideMenuCurrentlySelectedPosition = 1;

    Navigator.of(context)
        .pushNamedAndRemoveUntil(
        PostJob1New.routeName, (Route<dynamic> route) => false, arguments: 0);

    return true;
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
            centerTitle: true,
            title: CommonWidget.getActionBarTitleText(Api_constant.Contact_Us.toUpperCase()),
            flexibleSpace: CommonWidget.ActionBarBg(context),
            elevation: 0,
          ),
          drawer: MainDrawer(context),
          body: Stack(
            children: <Widget>[
              Stack(
                children: <Widget>[
//          Container(
//            color: Colors.red,
//          ),
//                  CommonWidget.ActionBarBg(context),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    child: Container(
                      color: Colors.grey[200],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(15, 17, 15, 15),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                          BorderRadius.all(Radius.circular(5.0))
                      ),
                      child: ListView(
                        children: <Widget>[
//                          Align(
//                            alignment: Alignment.topRight,
//                            child: Image.asset('assets/images/aboutus_bg.png'),
//                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(left: 15, top: 15),
                              child: CommonWidget.redHeaderBigLbl(Api_constant.CONTACT_ARABA, context),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(15, 0, 15, 10),
                            child: Column(
                              children: <Widget>[
                                Html(
                                    data:  ContacUsContent
                                ),
//                        CommonWidget.blackDiscriptionText(
//                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
////                            'If you want to round corners with transparent  2019, use want to round corners with transparent background, the best approach is using ClipRRect. As of May 1st 2019, use BottomSheetThem( theme: ThemeData( // Draw all modals with a white background and top rounded corners bottomSheetTheme'),
                                SizedBox(height: 10),
                                InkWell(
                                  child: CommonWidget.gryIconNdBlackTextheaderLayout(
                                      ContacUsPhone, Icons.call),
                                  onTap: () {
                                    launch("tel://"+ContacUsPhone);
                                  },
                                ),
//                            SizedBox(height: 5),
                                InkWell(
                                  child: gryIconHideNdBlackTextheaderLayout(ContacUsPhone2),
                                  onTap: () {
                                    launch("tel://"+ContacUsPhone2);
                                  },
                                ),
                                SizedBox(height: 10),
                                InkWell(
                                  child: CommonWidget.gryIconNdBlackTextheaderLayout(
                                      ContacUsEmail, Icons.email),
                                  onTap: () {
//                                launch(ContacUsEmail);
                                  },
                                ),
//                            SizedBox(height: 5),
                                InkWell(
                                  child: gryIconHideNdBlackTextheaderLayout(ContacUsEmail2),
                                  onTap: () {
//                                launch(ContacUsEmail2);
                                  },
                                ),
                                SizedBox(height: 10),
                                CommonWidget.gryIconNdBlackTextheaderLayout(
                                    ContacUsAddress, Icons.location_on)
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
//          Expanded(
//            flex: 1,
//            child:
              Center(child:  Visibility(
                visible: IsProgressIndicatorShow,
                child: SizedBox(
                  child: CircularProgressIndicator(),
                ),
              ),
              ),
//          )
            ],
          ),
        ),
        onWillPop: _onWillPop);
  }

  static Widget gryIconHideNdBlackTextheaderLayout(String title ) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: 22,
          ),
          SizedBox(
            width: 15,
          ),
          CommonWidget.blackDiscriptionText(title),
        ],
      ),
    );
  }

 }



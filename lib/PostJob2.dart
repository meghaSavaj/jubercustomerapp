import 'dart:convert';

import 'package:arabacustomer/Utils/Consts.dart';
import 'package:arabacustomer/dialog/LocationScanningDialog.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/MyRequestTabsScreen.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

import 'Utils/Api_constant.dart';
import 'Utils/MyPreferenceManager.dart';
import 'Utils/Utility.dart';
import 'dialog/PrivacyPolicyDialog.dart';
import 'model/PostJobGetTax.dart';
import 'model/StatusMsgResponse.dart';

class PostJob2 extends StatefulWidget {
  static const routeName = '/postjob2';

  @override
  PostJob2State createState() {
    return PostJob2State();
  }
}

class PostJob2State extends State<PostJob2> {
//  static final String payment_type1 = 'Paiment au Chargement';
//  static final String payment_type2 = 'Paiment à la Livraison';
  static final String payment_type1 = Api_constant.payment_type1;
  static final String payment_type2 = Api_constant.payment_type2;
  static final String Payement_NONE = Api_constant.Payement_NONE;

  String dropdownValue = Payement_NONE;
  String taxValue = '';
  String TotalPrice = '';
  bool IsProgressIndicatorShow = false;
  bool IsAgree = false;

  bool IsCard = true;
  bool IsCash = false;

  String _radioValue; //Initial definition of radio button value
  String choice;
  String PaymentType = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios, color: Colors.white, size: 20,),
              onPressed: () => Navigator.pop(context)
          ),
          title: CommonWidget.getActionBarTitleText(Api_constant.POST_A_JOB),
          flexibleSpace: CommonWidget.ActionBarBg(context),
        ),
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child:
//                Expanded(
//                  flex: 1,
//                  child:
              Column(
                children: <Widget>[
//                    LeadId(''),
                  Container(
//                        height: double.infinity,
                      color: Colors.grey[200],
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(
                              18.0, 15.0, 18.0, 20.0),
                          child: Column(
                            children: <Widget>[
                              Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(5.0))
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 15.0, 15.0, 15.0),
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment
                                          .start,
                                      children: <Widget>[
                                        CommonWidget.jobnameText(
                                            Consts.job_name),
                                        SizedBox(height: 15.0),
//                                      CommonWidget.priceDarkRedcolorText('\$9870.00'),
                                        CommonWidget.priceDarkRedcolorText(
//                                                Consts.currencySymbol+ priceWitTax()),
                                            Consts.currencySymbol+ TotalPrice, context),
//                                                Consts.currencySymbol+ TotalPrice),
                                        SizedBox(height: 10.0),
//                                            CommonWidget
//                                                .gryIconNdGrayTextheaderLayout(
//                                                    Api_constant.tax_capital+": "+taxValue+"%",
//                                                'assets/images/post_job/price.png'),
//                                            CommonWidget
//                                                .gryIconNdGrayTextheaderLayout(
//                                                "Prix​TTC: "+TotalPrice,
//                                                'assets/images/post_job/price.png'),

//                                            CommonWidget
//                                                .gryIconNdGrayTextheaderLayout(
//                                                Utility.convertdateFormat(
//                                                    Consts.date),
//                                                'assets/images/post_job/date_copy.png'),
//                                          CommonWidget.gryIconNdGrayTextheaderLayout('Wire/Roads/Sheets', 'assets/images/post_job/cart.png'),
                                        CommonWidget
                                            .gryIconNdGrayTextheaderLayout(
                                            Consts.vahicle_name,
                                            'assets/images/post_job/vehicle_copy.png'),
                                        CommonWidget
                                            .gryIconNdGrayTextheaderLayout(
                                            Consts.sourceAdd,
                                            'assets/images/post_job/location_copy.png'),
                                        CommonWidget
                                            .gryIconNdGrayTextheaderLayout(
                                            Consts.dstAdd,
                                            'assets/images/post_job/location.png'),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              35, 10, 0, 6),
                                          child: Row(
                                            children: <Widget>[
                                              CommonWidget.redHeaderLbl(
                                                  Api_constant.Total, context),
                                              CommonWidget
                                                  .grayDiscriptionText(
                                                  Consts.distance.toString() +
                                                      Api_constant.KM),
                                            ],
                                          ),
                                        ),
                                        SizedBox(height: 20.0),

                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            CommonWidget.redHeaderLbl(Api_constant.Payement_lbl, context),
//                                                CommonWidget.redHeaderLbl("A QUEL MOMENT LE PAIEMENT S'EFFECTUERA"),
//                                                CommonWidget.redHeaderLbl(Api_constant.Payment_Type),
//                                                SizedBox(height: 20.0),

                                            Padding(
                                              padding: EdgeInsets.fromLTRB(20,0,0,0),
                                              child:  DropdownButton<String>(
                                                value: dropdownValue,
                                                icon: Icon(Icons.arrow_drop_down),
                                                iconSize: 24,
                                                elevation: 16,
                                                style: TextStyle(color: Colors.black87),
//                                                underline: Container(
//                                                  height: 2,
//                                                  color:
//                                                      Colors.deepPurpleAccent,
//                                                ),
                                                onChanged: (String newValue) {
                                                  setState(() {
                                                    dropdownValue = newValue;
                                                    if(dropdownValue == payment_type1){
                                                      PaymentType = '1';
                                                    } else if(dropdownValue == payment_type2){
                                                      PaymentType = '2';
                                                    } else if(dropdownValue == Payement_NONE){
                                                      PaymentType = '';
                                                    }
                                                  });
                                                },
                                                items: <String>[
                                                  Payement_NONE,
                                                  payment_type1,
                                                  payment_type2
                                                ].map<DropdownMenuItem<String>>(
                                                        (String value) {
                                                      return DropdownMenuItem<
                                                          String>(
                                                        value: value,
                                                        child: Text(value,
                                                            style: new TextStyle(
                                                                fontSize: 14.0,
                                                                fontWeight: FontWeight.w500,
                                                                color: Colors.grey[700])),
                                                      );
                                                    }).toList(),
                                              ),
                                            ),

                                            Visibility(
                                              visible: false,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: <Widget>[
                                                  new Container(
//                                                      width: 135.0,
//                                                      height: 35.0,
                                                    decoration: new BoxDecoration(
                                                      color: Colors.white,
                                                      border: new Border.all(
                                                          color: Colors.redAccent[700],
                                                          width: 1.0),
                                                      borderRadius: new BorderRadius
                                                          .circular(5.0),
                                                    ),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Radio(
                                                          value: 'one',
                                                          groupValue: _radioValue,
                                                          onChanged: radioButtonChanges,
                                                        ),
                                                        Text(
//                                                            'chargement ',
//                                                          "paiement au\nchargement ",
                                                          "Paiment au\nChargement ",
                                                          style: new TextStyle(
                                                              fontSize: 14.0,
                                                              fontWeight: FontWeight.w500,
                                                              color: Colors.grey[700]),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  new Container(
//                                                      width: 135.0,
//                                                      height: 35.0,
                                                    decoration: new BoxDecoration(
                                                      color: Colors.white,
                                                      border: new Border.all(
                                                          color: Colors.redAccent[700],
                                                          width: 1.0),
                                                      borderRadius: new BorderRadius
                                                          .circular(5.0),
                                                    ),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Radio(
                                                          value: 'two',
                                                          groupValue: _radioValue,
                                                          onChanged: radioButtonChanges,
                                                        ),
                                                        Text(
//                                                            'à la livraison ',
//                                                            'paiement a\nla livraison ',
                                                          'Paiment à\nla Livraison ',
                                                          style: new TextStyle(
                                                              fontSize: 14.0,
                                                              fontWeight: FontWeight.w500,
                                                              color: Colors.grey[700]),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            SizedBox(height: 20.0),
                                          ],
                                        ),
//                                          CommonWidget.divider(),
//
//                                          SizedBox(height: 10.0),

                                        new TextField(
//                                              maxLength: Consts.TextField_maxLength_cancelreason,
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 16),
                                          keyboardType: TextInputType
                                              .multiline,
                                          decoration: new InputDecoration(
                                              border: new OutlineInputBorder(
                                                borderRadius: const BorderRadius
                                                    .all(
                                                  const Radius.circular(4.0),
                                                ),
                                              ),
                                              filled: true,
                                              hintStyle: new TextStyle(
                                                  color: Colors.black45),
                                              hintText: Api_constant.decription_hint,
//                                                  hintText: "description de la marchandise a transporter",
                                              fillColor: Colors.white70),
                                          onChanged: (val) {
                                            Consts.comments = val;
                                          },
                                          minLines: 4,
                                          maxLines: 20,
                                        )
                                      ],
                                    ),
                                  )),

                              SizedBox(height: 15.0),

                              Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Checkbox(
                                      value: IsAgree,
                                      onChanged: (bool value) {
                                        setState(() {
                                          IsAgree = value;
                                        });
                                      },
                                    ),
                                    RichText(
                                      textAlign: TextAlign.start,
                                      text: TextSpan(
                                        children: <TextSpan>[
                                          TextSpan(
                                            text: Api_constant.accept_all,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          TextSpan(
                                            text: Api_constant.tcs,
                                            style: TextStyle(
                                                color: Theme.of(context).primaryColor,
                                                fontSize: 15,
                                                fontWeight: FontWeight.w600),
                                            recognizer: new TapGestureRecognizer()
                                              ..onTap = () => {
                                                /*  showDialog(
                                                context: context,
                                                builder: (BuildContext context) =>
                                                   PrivacyPolicyDialog(),
                                                )*/
                                                Navigator.of(context).push(new MaterialPageRoute<Null>(
                                                    builder: (BuildContext context) {
                                                      return new PrivacyPolicyDialog();
                                                    },
                                                    fullscreenDialog: true
                                                ))
                                              },
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(height: 15.0),

                              SubmitButton(
                                  Api_constant.POST_A_JOB, onChanged, 3),
                              SizedBox(height: 15.0),
                              CommonWidget.submitButtonBottomLine()
                            ],
                          ),
                        ),
                      ))
                ],
              ),
//                ),
            ),
//              Expanded(
//                flex: 1,
//                child:
            Center(child: Visibility(
              visible: IsProgressIndicatorShow,
              child: SizedBox(
                child: CircularProgressIndicator(),
              ),
            ),
            ),
//              )
          ],
        )
    );
  }

  void radioButtonChanges(String value) {
    setState(() {
      _radioValue = value;
      switch (value) {
        case 'one':
          PaymentType = '1';
          choice = value;
          break;
        case 'two':
          PaymentType = '2';
          choice = value;
          break;
//        case 'three':
//          choice = value;
//          break;
        default:
          choice = null;
      }
//      Toast.show(PaymentType, context,
//          gravity: Toast.CENTER);
      debugPrint(choice); //Debug the choice in console
    });
  }

  void onChanged() {
    if(PaymentType.isEmpty){
      Toast.show(Api_constant.payment_validation, context,
          gravity: Toast.CENTER);
    }else{
    Utility.checkInternetConnection().then((intenet) {
      if (intenet != null && intenet) {
        try {
//          if (Consts.comments.isEmpty) {
//            Toast.show(Api_constant.comments_empty, context,
//                gravity: Toast.CENTER);
//          } else
            if (!IsAgree) {
            Toast.show(Api_constant.privacy_policy_empty, context,
                gravity: Toast.CENTER);
          } else {
            PostJobUser();
          }
        } catch (e) {

        }
//          Toast.show("Distance"+Consts.distance.toString(), context,
//              gravity: Toast.CENTER);
      } else {
        Toast.show(Api_constant.no_internet_connection, context,
            gravity: Toast.CENTER);
      }
    });
    }
  }

  /*double distance =0.0;

  Future<String> getdistance() async {
    distance = await Geolocator().distanceBetween(
        double.parse(Consts.pickup_lattitude),
        double.parse(Consts.pickup_longitude),
        double.parse(Consts.delivery_lattitude),
        double.parse(Consts.delivery_long));
  }*/

  String priceWithoutTax() {
    double priceWithoutTaxValue = ((Consts.distance) *
        double.parse(Consts.rate_per_km)) + double.parse(Consts.min_price);
    return priceWithoutTaxValue.toStringAsFixed(2);
    //return Consts.distance.toString()+"   "+Consts.rate_per_km.toString();


    /*if(distance==0){
      Toast.show("Distance value blank", context, gravity: Toast.CENTER);
      return "Distance value blank";
    }else if(double.parse(Consts.rate_per_km)==0){
      Toast.show("rate_per_km", context, gravity: Toast.CENTER);
      return "rate_per_km";
    }else{
      double toda =( (distance/1000) * double.parse(Consts.rate_per_km));
      return toda.toString();
    }*/

  }

  String priceWitTax() {
    double priceWitTaxValue = ((Consts.distance) *
        double.parse(Consts.rate_per_km)) + double.parse(Consts.min_price);
    double finalVal = (priceWitTaxValue * double.parse(taxValue)) / 100;
    double total_price = finalVal + priceWitTaxValue;
    return total_price.toStringAsFixed(2);
  }

  Future<void> PostJobUser() async {
    print('test...........');

    var map = new Map<String, dynamic>();
    MyPreferenceManager _myPreferenceManager = await MyPreferenceManager
        .getInstance();
    String id = _myPreferenceManager.getString(
        MyPreferenceManager.CURRENT_LOGIN_USER_ID);

    map[Api_constant.user_id] = id;
    map[Api_constant.job_title] = Consts.job_name;
    map[Api_constant.service_id] = Consts.vehicle_id;

    map[Api_constant.total_price] = priceWitTax();
    map[Api_constant.pickup_address] = Consts.sourceAdd;
    map[Api_constant.delivery_address] = Consts.dstAdd;
    map[Api_constant.description] = Consts.comments;
    map[Api_constant.date] = Consts.date;
    map[Api_constant.pickup_street] =  Consts.sourceAddCity;
    map[Api_constant.pickup_postal_code] =Consts.sourcepostal_code;
    map[Api_constant.pickup_state] = Consts.sourcestate ;
    map[Api_constant.pickup_country] = Consts.sourcecountry ;
    map[Api_constant.pickup_lattitude] = Consts.pickup_lattitude;
    map[Api_constant.pickup_longitude] = Consts.pickup_longitude;
    map[Api_constant.delivery_street] = Consts.dstAddCity;
    map[Api_constant.delivery_postal_code] = Consts.dstpostal_code;
    map[Api_constant.delivery_state] = Consts.dststate;
    map[Api_constant.delivery_country] = Consts.dstcountry;
    map[Api_constant.delivery_lattitude] = Consts.delivery_lattitude;
    map[Api_constant.delivery_longitude] = Consts.delivery_long;
    map[Api_constant.start_with_date] = Consts.date_bool;
    map[Api_constant.online_payment] = '1';
//    map[Api_constant.online_payment] = PaymentType;
    map['payment_type'] = PaymentType;
    map[Api_constant.distance] = (Consts.distance).toString();
//    map[Api_constant.total_price_without_tax] = '100';
    map[Api_constant.total_price_without_tax] = priceWithoutTax();
    map[Api_constant.service_price_per_km] = Consts.rate_per_km;
    map[Api_constant.tax] = taxValue;

    print(map.toString() + "..............");


    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.post(Api_constant.post_job_api, body: map);

    setState(() {
      IsProgressIndicatorShow = false;
    });

    print(response.statusCode.toString() + "..............");
    print(response.body + "PostJob..............");

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var createJobResponse = new StatusMsgResponse.fromJsonMap(
          userMap) as StatusMsgResponse;

      if (createJobResponse.status == Api_constant.STATUS) {
        //   Toast.show(Api_constant.create_post, context, gravity: Toast.CENTER);

//        Consts.job_name="";
//        Consts.date="";
        /*  Consts.vehicle_id= "";
        Consts.vahicle_name= "";
        Consts.date_bool= "";
        Consts.comments="";
        Consts.distance=0.0;
        Consts.rate_per_km="";
        Consts.sourceAdd = "";
        Consts.dstAdd = "";
        Consts.delivery_lattitude = "0.0";
        Consts.delivery_long = "0.0";
        Consts.pickup_longitude = "0.0";
        Consts.pickup_lattitude = "0.0";*/

//        Navigator.removeRouteBelow(context,  MyRequestTabsScreen.routeName)
//        Navigator.push(context, MaterialPageRoute(builder: (context) => MyRequestTabsScreen()));
        Toast.show(Api_constant.create_post, context, gravity: Toast.CENTER);

        /*  Navigator.of(context)
            .pushNamedAndRemoveUntil(MyRequestTabsScreen.routeName, (Route<dynamic> route) => false);*/

        Navigator.of(context)
            .pushNamedAndRemoveUntil(
            MyRequestTabsScreen.routeName, (Route<dynamic> route) => false, arguments: 0);


      } else {
        Toast.show(Api_constant.Error, context, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(
          Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  @override
  void initState() {
    super.initState();
    GetTax();
//    getdistance();
  }

  void GetTax() async {
    var response = await http.get(Api_constant.postjob_gettax__api);
    print(Api_constant.postjob_gettax__api + "..............");

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var PostJobGetTax_obj = new PostJobGetTax.fromJsonMap(
          userMap) as PostJobGetTax;

      setState(() {
        taxValue = PostJobGetTax_obj.tax;
        TotalPrice = priceWitTax();
      });

      print(Consts.rate_per_km.toString() + 'test...........');
      print(Consts.distance.toString() + 'test...........');
      print(Consts.min_price.toString() + 'test...........');
      print(taxValue.toString() + 'test...........');
      print(Consts.pickup_lattitude.toString() + 'test...........');
      print(Consts.pickup_longitude.toString() + 'test...........');
      print(Consts.delivery_lattitude.toString() + 'test...........');
      print(Consts.delivery_long.toString() + 'test...........');
      // Toast.show(PostJobGetTax_obj.tax, context, gravity: Toast.CENTER);

    } else {
      setState(() {
        taxValue = "";
        TotalPrice = priceWithoutTax();
      });
      Toast.show(
          Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }
}
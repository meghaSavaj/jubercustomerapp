import 'dart:convert';

import 'package:arabacustomer/PostJob1New.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SideMenu.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:toast/toast.dart';

import 'MyRequestDetail.dart';
import 'MyRequestDetailNew.dart';
import 'MyRequestDetailNew2.dart';
import 'Utils/Api_constant.dart';
import 'Utils/Consts.dart';
import 'Utils/MyPreferenceManager.dart';
import 'model/CommanModal.dart';
import 'model/MyRequestListObj.dart';
import 'package:http/http.dart' as http;

class RatingList extends StatefulWidget {
  static const routeName = '/MyRating';

  @override
  RatingListState createState() {
     return RatingListState();
  }
}

class RatingListState extends State<RatingList> {

  Future<List<MyRequestListObj>> geMyRequestOpenObjListtData() async {
    MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
    String id =  _myPreferenceManager.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);

    String page_offset = '0';
    final uri = Uri.encodeFull(Api_constant.RatingList +
        '?id=${id}' +
        '&page_offset=${page_offset}');
    print(uri.toString() + "..............");

    var response = await http.get(uri);

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var CommanModal_obj = new CommanModal.fromJson(userMap) as CommanModal;
      if(CommanModal_obj.status ==1) {
        List<Object> resultList = CommanModal_obj.data;

        List<MyRequestListObj> myContributionList = new List(resultList.length);

        for (int i = 0; i < resultList.length; i++) {
          Object obj = resultList[i];
          MyRequestListObj g = new MyRequestListObj.fromJsonMap(obj);
          myContributionList[i] = g;
        }

        return myContributionList;
      } else{
//        Toast.show(Api_constant.no_record_found, context,
//            gravity: Toast.CENTER);

        return new List<MyRequestListObj>();
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);

      return new List<MyRequestListObj>();
    }
  }

  Future<bool> _onWillPop() async {
    Consts.SideMenuCurrentlySelectedPosition = 1;

    print("_onWillPop===========" );
    Navigator.of(context)
        .pushNamedAndRemoveUntil(
        PostJob1New.routeName, (Route<dynamic> route) => false, arguments: 0);

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(child: Scaffold(
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        title: CommonWidget.getActionBarTitleText(Api_constant.MY_RATING.toUpperCase()),
        flexibleSpace: CommonWidget.ActionBarBg(context),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
              size: 24,
            ),
            onPressed: () {},
          )
        ],
      ),
      drawer: MainDrawer(context),
      body: Container(
          color: Colors.grey[200],
          child: FutureBuilder<List<MyRequestListObj>>(
            future: geMyRequestOpenObjListtData(),
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return Center(child: CircularProgressIndicator());
              List<MyRequestListObj> MyRequestOpenObjList = snapshot.data;

              return Container(
                color: Colors.grey[200],
                child: Stack(
                  alignment: Alignment.bottomRight,
                  children: <Widget>[
                    Center(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0.0), //AnimationLimiter
                        child: AnimationLimiter(
                          child: ListView.builder(
                            itemBuilder: (context, index) {
                              return AnimationConfiguration.staggeredList(
                                position: index,
                                duration: const Duration(milliseconds: 375),
                                child: SlideAnimation(
                                  verticalOffset: 50.0,
                                  child: FadeInAnimation(
                                    child: Column(
                                      children: <Widget>[
                                        InkWell(
                                          onTap: () {
                                            var obj = MyRequestOpenObjList[index];
                                            obj.JobListType= Consts.JobListTyperating;
                                            Consts.FromRatingScreen = true;

                                            Navigator.of(context).pushNamed(MyRequestDetailNew2.routeName, arguments: obj.job_id.toString());
                                          },

                                          child: Container(
                                              width: double.infinity,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius: BorderRadius.all(Radius.circular(4.0))),
                                              child: Stack(
                                                alignment: AlignmentDirectional.bottomEnd,
                                                children: <Widget>[
                                                  Padding(
                                                    padding: EdgeInsets.fromLTRB(15, 12, 15, 12),
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: <Widget>[


                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: [
                                                            CommonWidget.leadidDarkRedcolorText(MyRequestOpenObjList[index].job_number),

                                                            Column(
                                                              mainAxisAlignment: MainAxisAlignment.end,
                                                              crossAxisAlignment: CrossAxisAlignment.end,
                                                              children: [
                                                                Row(
                                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: [
                                                                    Visibility(
                                                                      child:  Text('Price',
                                                                        style: TextStyle(
                                                                            fontWeight: FontWeight.w400, color: Colors.grey[400], fontSize: 13
                                                                        ),),
                                                                      visible: !CommonWidget.ISCorporateUser(MyRequestOpenObjList[index].corporate_id),
                                                                    ),
                                                                    SizedBox(width: 5,),
                                                                    Visibility(
                                                                      child:  Text(CommonWidget.replaceNullWithEmpty(MyRequestOpenObjList[index].price),
                                                                        style: TextStyle(
                                                                            fontWeight: FontWeight.w700, color: Theme.of(context).primaryColor, fontSize: 14
                                                                        ),),
                                                                      visible: !CommonWidget.ISCorporateUser(MyRequestOpenObjList[index].corporate_id),
                                                                    ),
                                                                  ],
                                                                ),

                                                              ],
                                                            )
                                                          ],
                                                        ),
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: <Widget>[
//                                            CommonWidget.leadidDarkRedcolorText(MyRequestOpenObjList[index].job_number),
//                                            CommonWidget.priceDarkRedcolorText(getPrice(MyRequestOpenObjList[index]))
                                                            Text(MyRequestOpenObjList[index].date,
                                                              style: TextStyle(
                                                                  fontWeight: FontWeight.w700, color: Colors.black87, fontSize: 14
                                                              ),),
                                                          ],
                                                        ),


                                                        SizedBox(
                                                          height: 5,
                                                        ),

                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: <Widget>[
//                                            CommonWidget.leadidDarkRedcolorText(MyRequestOpenObjList[index].job_number),
//                                            CommonWidget.priceDarkRedcolorText(getPrice(MyRequestOpenObjList[index]))

                                                            Container(
                                                              width: MediaQuery.of(context).size.height * 0.30,
                                                              child:  Row(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                children: [
                                                                  Padding(
                                                                    padding: EdgeInsets.fromLTRB(0, 2, 2, 0),
                                                                    child:  Icon(
                                                                      Icons.place,
                                                                      color: Theme.of(context).primaryColor,
                                                                      size: 14.0,
                                                                    ),
                                                                  ),
                                                                  Flexible(
                                                                    child: Text('To: '+MyRequestOpenObjList[index].delivery_address,
                                                                      style: TextStyle(
                                                                          fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                                      ),),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),

                                                        SizedBox(
                                                          height: 5,
                                                        ),
                                                        Container(
//                                                        width: MediaQuery.of(context).size.height * 0.30,
                                                          child:  Row(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            children: [
                                                              Padding(
                                                                  padding: EdgeInsets.fromLTRB(0, 2, 4, 0),
                                                                  child:   CommonWidget.getIconImgeWithCustomSize(
                                                                      'assets/images/black_circle.png', 10)
//                                                              Icon(
//                                                                Icons.place,
//                                                                color: Colors.black87,
//                                                                size: 14.0,
//                                                              ),
                                                              ),
                                                              Flexible(
                                                                child: Text("From: "+MyRequestOpenObjList[index].pickup_address,
                                                                  style: TextStyle(
                                                                      fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                                  ),),
                                                              ),
                                                            ],
                                                          ),
                                                        ),

                                                        Padding(
                                                          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                          child: Container(
                                                            height: 0.7,
                                                            color: Colors.black26,
                                                            width: MediaQuery.of(context).size.width * 0.90,
                                                          ),
                                                        ),

                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          children: [
                                                            Column(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                              children: <Widget>[
//                                                                CommonWidget.redHeadersmallLbl(
//                                                                    AppLocalizations.of(context).translate('customer_ratings')),
                                                                Text('DRIVER',
                                                                  style: TextStyle(
                                                                      fontWeight: FontWeight.w700, color: Colors.black87, fontSize: 13
                                                                  ),),
                                                                Row(
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  children: <Widget>[
                                                                    Text(
                                                                        CommonWidget.replaceNullWithEmpty(CommonWidget.replaceNullWithEmpty(MyRequestOpenObjList[index]
                                                                            .transporter_name)),
                                                                        style: TextStyle(
                                                                            fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                                        )),
                                                                  ],
                                                                )
                                                              ],
                                                            ),
                                                            RatingBarIndicator(
                                                              rating: CommonWidget.getDoubleFromStrForRating(MyRequestOpenObjList[index].transporter_ratings),
                                                              direction: Axis.horizontal,
                                                              unratedColor: Colors.grey.withAlpha(95),
                                                              itemCount: 5,
                                                              itemSize: 16.0,
                                                              itemPadding: EdgeInsets
                                                                  .symmetric(
                                                                  horizontal:
                                                                  1.2),
                                                              itemBuilder:
                                                                  (context, _) =>
                                                                  Icon(
                                                                    Icons.star,
                                                                    color:
                                                                    Colors.amber,
                                                                  ),
                                                            ),
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  ),
//                                      IconButton(
//                                        icon: Icon(
//                                          Icons.arrow_forward_ios,
//                                          color: Colors.black87,
//                                          size: 12,
//                                        ),
//                                        onPressed: () =>  {},
//                                      )
                                                ],
                                              ) ),
                                        ),

                                        SizedBox(
                                          height: 15,
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                            itemCount: MyRequestOpenObjList.length,
                          ),
                        ),
                      ),
                    ),
                    Center(
                      child: Visibility(
                        visible: MyRequestOpenObjList.isEmpty,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image(image: AssetImage('assets/images/no_record_found_small_img.png'),width: MediaQuery.of(context).size.width * 0.8,),
                            SizedBox(height: 20),
                            Text(Api_constant.No_offer_available, style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black54, fontSize: 17),)
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );

//                Stack(
//                children: <Widget>[
//                  Center(
//                    child: Padding(
//                      padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0.0),
//                      child: ListView.builder(
//                        itemBuilder: (context, index) {
//                          return Column(
//                            children: <Widget>[
//                              Container(
//                                  width: double.infinity,
//                                  decoration: BoxDecoration(
//                                      color: Colors.white,
//                                      borderRadius:
//                                      BorderRadius.all(Radius.circular(3.0))),
//                                  child: Column(
//                                    children: <Widget>[
//                                      Padding(
//                                        padding: EdgeInsets.fromLTRB(15, 15, 15, 5),
//                                        child: Row(
//                                          mainAxisAlignment:
//                                          MainAxisAlignment.spaceBetween,
//                                          children: <Widget>[
//                                            CommonWidget.leadidDarkRedcolorText(CommonWidget.replaceNullWithEmpty(MyRequestOpenObjList[index].job_number).toString()),
//                                            CommonWidget.priceDarkRedcolorText(CommonWidget.replaceNullWithEmpty(MyRequestOpenObjList[index].price), context)
//                                          ],
//                                        ),
//                                      ),
//                                      CommonWidget.divider(),
//                                      Padding(
//                                        padding:
//                                        EdgeInsets.fromLTRB(10.0, 5.0, 15.0, 0.0),
//                                        child: Stack(
//                                          alignment: Alignment.bottomRight,
//                                          children: <Widget>[
//                                            Column(
//                                              crossAxisAlignment: CrossAxisAlignment.start,
//                                              mainAxisAlignment: MainAxisAlignment.start,
//                                              children: <Widget>[
//                                                CommonWidget.jobnameText(CommonWidget.replaceNullWithEmpty(MyRequestOpenObjList[index].job_title)),
//                                                SizedBox(height: 5.0),
//                                                CommonWidget.gryIconNdGrayTextheaderLayout(
//                                                    CommonWidget.replaceNullWithEmpty(MyRequestOpenObjList[index].date),
//                                                    'assets/images/post_job/date_copy.png'),
//                                                CommonWidget.gryIconNdGrayTextheaderLayout(
//                                                    CommonWidget.replaceNullWithEmpty(MyRequestOpenObjList[index].vehicle_category),
//                                                    'assets/images/post_job/vehicle_copy.png'),
//                                                SizedBox(height: 8.0),
//                                                CommonWidget.redHeaderLbl(Api_constant.DISTANCE, context),
//                                                SizedBox(height: 3.0),
//                                                CommonWidget.gryIconNdGrayTextheaderLayout(
//                                                    Api_constant.From+ CommonWidget.replaceNullWithEmpty(MyRequestOpenObjList[index].pickup_address),
//                                                    'assets/images/post_job/location_copy.png'),
//                                                CommonWidget.gryIconNdGrayTextheaderLayout(
//                                                    Api_constant.To+CommonWidget.replaceNullWithEmpty(MyRequestOpenObjList[index].delivery_address),
//                                                    'assets/images/post_job/location.png'),
//                                                Padding(
//                                                  padding: EdgeInsets.fromLTRB(35, 3, 0, 6),
//                                                  child: Row(
//                                                    children: <Widget>[
//                                                      CommonWidget.redHeaderLbl(Api_constant.Total, context),
//                                                      CommonWidget.grayDiscriptionText(
//                                                          CommonWidget.replaceNullWithEmpty(MyRequestOpenObjList[index].distance.toString())+Api_constant.KM),
//                                                    ],
//                                                  ),
//                                                ),
//                                              ],
//                                            ),
//                                            Padding(
//                                              padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
//                                              child: SizedBox(
//                                                width: 37,
//                                                height: 37,
//                                                child: FlatButton(
//                                                    onPressed: (){
//                                                      var obj = MyRequestOpenObjList[index];
//                                                      obj.JobListType= Consts.JobListTyperating;
//                                                      Consts.FromRatingScreen = true;
//
//                                                      Navigator.of(context).pushReplacementNamed(MyRequestDetailNew2.routeName, arguments: obj.job_id.toString());
//                                                    },
//                                                    padding:
//                                                    EdgeInsets.all(0.0),
//                                                    child: Image.asset(
//                                                        'assets/images/goto_detail_square.png')),
//                                              ),
//                                            )
//                                          ],
//                                        ),
//                                      ),
//                                      CommonWidget.divider(),
//                                      Padding(
//                                        padding: EdgeInsets.fromLTRB(10, 7, 10, 10),
//                                        child: Row(
//                                          crossAxisAlignment: CrossAxisAlignment.center,
//                                          mainAxisAlignment: MainAxisAlignment.end,
////                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                          children: <Widget>[
////                                      Column(
////                                        crossAxisAlignment: CrossAxisAlignment.start,
////                                        mainAxisAlignment: MainAxisAlignment.start,
////                                        children: <Widget>[
////                                          CommonWidget.redHeadersmallLbl(
////                                              Api_constant.TRANSPORT_NAME),
////                                          Row(
////                                            mainAxisAlignment: MainAxisAlignment.start,
////                                            children: <Widget>[
////                                              Text(CommonWidget.replaceNullWithEmpty(CommonWidget.replaceNullWithEmpty(MyRequestOpenObjList[index].transporter_name))
////                                                  +' \('+
////                                                  CommonWidget.replaceNullWithEmpty(CommonWidget.replaceNullWithEmpty(MyRequestOpenObjList[index].transporter_ratings)+' '),
////                                                  style: TextStyle(
////                                                      color: Colors.black87,
////                                                      fontWeight: FontWeight.w400,
////                                                      fontSize: 16)),
////                                              Icon(
////                                                Icons.star,
////                                                color: Colors.amber,
////                                                size: 16,
////                                              ),
////                                              Text('\) ',
////                                                style: TextStyle(color: Colors.black87,
////                                                    fontWeight: FontWeight.w400,
////                                                    fontSize: 16),)
////                                            ],
////                                          )
////                                        ],
////                                      ),
//                                            RatingBarIndicator(
//                                              rating: CommonWidget.getDoubleFromStrForRating(MyRequestOpenObjList[index].job_rating),
//                                              direction: Axis.horizontal,
//                                              unratedColor: Colors.grey.withAlpha(95),
//                                              itemCount: 5,
//                                              itemSize: 22.0,
//                                              itemPadding:
//                                              EdgeInsets.symmetric(horizontal: 1.2),
//                                              itemBuilder: (context, _) => Icon(
//                                                Icons.star,
//                                                color: Colors.amber,
//                                              ),
//                                            )
//                                          ],
//                                        ),
//                                      )
//                                    ],
//                                  )),
//                              SizedBox(
//                                height: 15,
//                              )
//                            ],
//                          );
//                        },
//                        itemCount: MyRequestOpenObjList.length,
//                      ),
//                    ),
//                  ),
//                  Center(
//                    child: Visibility(
//                      visible: MyRequestOpenObjList.isEmpty,
//                      child: Column(
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        children: <Widget>[
//                          Image(image: AssetImage('assets/images/no_record_found_small_img.png'),width: MediaQuery.of(context).size.width * 0.8,),
//                          SizedBox(height: 20),
//                          Text(Api_constant.No_rating_yet, style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black54, fontSize: 17),)
//                        ],
//                      ),
//                    ),
//                  ),
//                ],
//              );
            },
          )
      ),
    ), onWillPop: _onWillPop);
  }

}

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:arabacustomer/widgets/SubmitButton.dart';
import 'package:arabacustomer/widgets/lib_dart_noti_center/src/dart_notification_center_base.dart';
import 'package:geocoder/geocoder.dart';
import 'package:http/http.dart' as http;
import 'dart:math';

import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';
import 'MyRequestDetail.dart';
import 'MyRequestDetailNew.dart';
import 'MyRequestDetailNew2.dart';
import 'Utils/Api_constant.dart';
import 'Utils/Consts.dart';
import 'map_request.dart';
import 'model/CommanModalWithObj.dart';
import 'model/GetDriverLatLongResponse.dart';
import 'model/MyRequestListObj.dart';
import 'model/PassDataToTrackingScreen.dart';
import 'model/StatusMsgResponse.dart';
import 'model/getDriverLatLong.dart';

class ProcessingTrackingDetail2 extends StatefulWidget {
  static const routeName = '/ProcessingTrackingDetail2';

 /* PassDataToTrackingScreen myRequestListObj;

  ProcessingTrackingDetail2(
//      PassDataToTrackingScreen passDataToTrackingScreenObj,
      {Key key,
      this.myRequestListObj})
      : super(key: key);*/
  MyRequestListObj myRequestListObj;

//  MyRequestDetail({Key key, this.myRequestListObj}) : super(key: key)

  String postid;


  ProcessingTrackingDetail2({Key key, this.postid}) : super(key: key);

  @override
  ProcessingTrackingDetail2State createState() {
    return ProcessingTrackingDetail2State();
  }
}

class ProcessingTrackingDetail2State extends State<ProcessingTrackingDetail2> {
  Completer<GoogleMapController> _mapController = Completer();
  GoogleMapController _controller;
  GoogleMapsServices _googleMapsServices = GoogleMapsServices();
  final Set<Polyline> _polyLines = {};
  bool IsFromSideMenu=false;
  Set<Polyline> get polyLines => _polyLines;
  String driverAddress;
  final Set<Marker> _markers = {};
  int i = 1;
  Timer timer;
  var marker = null;
  bool IsProgressIndicatorShow = false;
  @override
  void dispose() {
    timer?.cancel();
    DartNotificationCenter.unsubscribe(observer: i, channel: Consts.DriverArrivedChanel);
    super.dispose();
  }

  BitmapDescriptor pinLocationIcon;

  @override
  void initState() {

    getDetail(widget.postid.toString());
   /* DartNotificationCenter.subscribe(
      channel: Consts.confirmedStatusText,
      observer: i,
      onNotification: (result) => {
        print('received:DetialChanel****** $result'),
        if(widget.postid != null && widget.postid.isNotEmpty){
          getDetail(widget.postid.toString())
        }

//        setState(() {
////          new MyRequestDetail();
//        })
      },
    );*/
    DartNotificationCenter.subscribe(
      channel: Consts.DriverArrivedChanel,
      observer: i,
      onNotification: (result) => {
        print('received:DetialChanel****** $result'),
        if(widget.postid != null && widget.postid.isNotEmpty){
          getDetail(widget.postid.toString())
        }


      },
    );
    timer = Timer.periodic(Duration(seconds: Consts.driverApiCallDelay),
        (Timer t) => {driverCurrentLocationUpdate()});

   /* print("delivery lat" + widget.myRequestListObj.delivery_lattitude.toString());
    print("delivery lng" + widget.myRequestListObj.delivery_longitude.toString());
    print("delivery address" + widget.myRequestListObj.delivery_address.toString());
    print("pickup lat " + widget.myRequestListObj.pickup_lattitude);
    print("pickup lng" + widget.myRequestListObj.pickup_longitude);
    print("pickup address" + widget.myRequestListObj.pickup_address);

//    widget.myRequestListObj.pickup_lattitude= '22.3039';
//    widget.myRequestListObj.pickup_longitude= '70.8022';
    if (null == widget.myRequestListObj.delivery_lattitude ||
        widget.myRequestListObj.delivery_lattitude.isEmpty) {
      widget.myRequestListObj.delivery_lattitude = '0.0';
    }
    if (null == widget.myRequestListObj.delivery_longitude ||
        widget.myRequestListObj.delivery_longitude.isEmpty) {
      widget.myRequestListObj.delivery_longitude = '0.0';
    }

    if (null == widget.myRequestListObj.pickup_lattitude ||
        widget.myRequestListObj.pickup_lattitude.isEmpty) {
      widget.myRequestListObj.pickup_lattitude = '0.0';
    }

    if (null == widget.myRequestListObj.pickup_longitude ||
        widget.myRequestListObj.pickup_longitude.isEmpty) {
      widget.myRequestListObj.pickup_longitude = '0.0';
    }

    _onAddMarkerButtonPressed();
    sendRequest();*/
    super.initState();
  }

  List<LatLng> _convertToLatLng(List points) {
    List<LatLng> result = <LatLng>[];
    for (int i = 0; i < points.length; i++) {
      if (i % 2 != 0) {
        result.add(LatLng(points[i - 1], points[i]));
      }
    }
    return result;
  }

  void sendRequest() async {
    if(widget.myRequestListObj.status.toLowerCase()=='confirmed'){
      LatLng destination = LatLng(
          CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.pickup_lattitude),
          CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.pickup_longitude));
      LatLng origin = LatLng(
          CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.gprs_lat),
          CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.gprs_lng));
      String route = await _googleMapsServices.getRouteCoordinates(origin, destination);
      print("route===" + route);
      createRoute(route);
      _addMarker(destination,  widget.myRequestListObj.pickup_address,
          widget.myRequestListObj.job_id.toString());
    }else{
      LatLng destination = LatLng(
          CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.delivery_lattitude),
          CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.delivery_longitude));
      LatLng origin = LatLng(
          CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.pickup_lattitude),
          CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.pickup_longitude));
      String route = await _googleMapsServices.getRouteCoordinates(origin, destination);
      print("route===" + route);
      createRoute(route);
      _addMarker(destination, widget.myRequestListObj.delivery_address,
          widget.myRequestListObj.job_id.toString());
    }

    // _addMarker(origin,widget.myRequestListObj.pickup_address, widget.myRequestListObj.job_id.toString());

//    LatLngBounds bound = LatLngBounds(southwest: origin,
//        northeast: destination);
//    CameraUpdate u2 = CameraUpdate.newLatLngBounds(bound, 50);
//    controller.animateCamera(u2).then((void v){
//      check(u2,this.controller);
//    });
  }

  void check(CameraUpdate u, GoogleMapController c) async {
    c.animateCamera(u);
    _controller.animateCamera(u);
    LatLngBounds l1 = await c.getVisibleRegion();
    LatLngBounds l2 = await c.getVisibleRegion();
    print(l1.toString());
    print(l2.toString());
    if (l1.southwest.latitude == -90 || l2.southwest.latitude == -90)
      check(u, c);
  }

  void createRoute(String encondedPoly) {
    _polyLines.add(Polyline(
        polylineId: PolylineId("Track"),
        width: 4,
        points: _convertToLatLng(_decodePoly(encondedPoly)),
        color: Colors.red));

    if(mounted)setState(() {});
    // print("_polyLines"+_polyLines.toString());
  }

  void _addMarker(LatLng location, String address, String markerid) {
    if(mounted)
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId("112"),
          position: location,
          // infoWindow: InfoWindow(title: address, snippet: "go here"),
          infoWindow: InfoWindow(title: address),
//          icon: pinLocationIcon,
//          icon: BitmapDescriptor.fromAsset("assets/images/post_job/comments.png"),
          icon: BitmapDescriptor.defaultMarker));

      var bounds = getBounds(_markers);
      CameraPosition cPosition = CameraPosition(
        zoom: 18,
        tilt: 80,
        bearing: 30,
        target: LatLng(Consts.currentLat,Consts.currentLong),
      );
      CameraUpdate u2 = CameraUpdate.newLatLngBounds(bounds, 50);
      _controller.animateCamera(u2).then((void v) async {
        check(u2, this._controller);


        final GoogleMapController controller = await _mapController.future;
        controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));
      });
    });
  }

  LatLngBounds getBounds(Set<Marker> markers) {
    var lngs = markers.map<double>((m) => m.position.longitude).toList();
    var lats = markers.map<double>((m) => m.position.latitude).toList();

    double topMost = lngs.reduce(max);
    double leftMost = lats.reduce(min);
    double rightMost = lats.reduce(max);
    double bottomMost = lngs.reduce(min);

    LatLngBounds bounds = LatLngBounds(
      northeast: LatLng(rightMost, topMost),
      southwest: LatLng(leftMost, bottomMost),
    );

    return bounds;
  }

  List _decodePoly(String poly) {
    var list = poly.codeUnits;
    var lList = new List();
    int index = 0;
    int len = poly.length;
    int c = 0;
    do {
      var shift = 0;
      int result = 0;

      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      } while (c >= 32);
      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lList.add(result1);
    } while (index < len);

    for (var i = 2; i < lList.length; i++) lList[i] += lList[i - 2];

    print(lList.toString());

    return lList;
  }

  void _onAddMarkerButtonPressed() {
    if(mounted)
    setState(() {
      if(widget.myRequestListObj.status.toLowerCase()=='confirmed') {
        _markers.add(Marker(
          markerId: MarkerId("111"),
          position: LatLng(CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.gprs_lat),
              CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.gprs_lng)),
          // infoWindow: InfoWindow(title: address, snippet: "go here"),
          infoWindow: InfoWindow(title:Consts.sourceAdd.toString()),
        ));
      }else{
        _markers.add(Marker(
          markerId: MarkerId("111"),
          position: LatLng(CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.pickup_lattitude),
              CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.pickup_longitude)),
          // infoWindow: InfoWindow(title: address, snippet: "go here"),
          infoWindow: InfoWindow(title: widget.myRequestListObj.pickup_address),
        ));
      }


      /* _markers.add(Marker(
        markerId: MarkerId("112"),
        position: LatLng(double.parse(widget.myRequestListObj.delivery_lattitude), double.parse(widget.myRequestListObj.delivery_longitude)),
        // infoWindow: InfoWindow(title: address, snippet: "go here"),
        infoWindow: InfoWindow(title: widget.myRequestListObj.delivery_address),
      ));*/
    });
  }

  Future<bool> _onWillPop() async {
    print("_onWillPop===========");

    if (IsFromSideMenu) {
      Navigator.of(context).pop();
    } else {
      Navigator.of(context).pushReplacementNamed(
          MyRequestDetailNew2.routeName,
          arguments:
          widget.myRequestListObj.job_id.toString());
    }

//    Navigator.of(context)
//        .pushNamedAndRemoveUntil(MyJobTabsScreen.routeName,
//            (Route<dynamic> route) => false);

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            leading: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                  size: 20,
                ),
                onPressed: () => {
                  if (IsFromSideMenu)
                    {Navigator.of(context).pop()}
                  else
                    {
                      Navigator.of(context).pushReplacementNamed(
                          MyRequestDetailNew2.routeName,
                          arguments:
                          widget.myRequestListObj.job_id.toString())
                    }
                }),

            title: CommonWidget.getActionBarTitleText(Api_constant.Track_transportation),
            flexibleSpace: CommonWidget.ActionBarBg(context),
          ),
          body: IsProgressIndicatorShow ? SizedBox(
            height: double.infinity,
            width: double.infinity,
            child: Center(
              child:  CircularProgressIndicator(),
            ),
          ) :SafeArea(
            child: Column(
              children: <Widget>[
                Visibility(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Container(
                      child: widget.myRequestListObj.status.toLowerCase()=='confirmed'?CommonWidget.gryIconNdGrayTextheaderLayout(widget.myRequestListObj.pickup_address,
                          'assets/images/post_job/location.png'):CommonWidget.gryIconNdGrayTextheaderLayout(widget.myRequestListObj.delivery_address,
                          'assets/images/post_job/location.png'),
                    ),
                  ),
                  visible: widget.myRequestListObj.status.toLowerCase()=='confirmed'?CommonWidget.forEmptyReturnHide(widget.myRequestListObj.pickup_address):CommonWidget.forEmptyReturnHide(widget.myRequestListObj.delivery_address),
                ),
                Expanded(
                  flex: 1,
                  child: GoogleMap(
                    polylines: polyLines,
                    markers: _markers,
                    mapType: MapType.normal,
                    initialCameraPosition: CameraPosition(
                      target: LatLng(
                          CommonWidget.returnAlgeriaLatWhenGettingBlanck(
                              widget.myRequestListObj.pickup_lattitude),
                          CommonWidget.returnAlgeriaLatWhenGettingBlanck(
                              widget.myRequestListObj.pickup_longitude)),
                      zoom: 14,
                    ),

//                myLocationEnabled: true,
//                myLocationButtonEnabled: true,
                    onMapCreated: (GoogleMapController controller) {
                      _controller = controller;
                      _mapController.complete(controller);
                     // controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));
                      //   _getLocation();
                      // sourceDestinationMarkerSet();
                    },
                    // markers: _markers.values.toSet(),
                  ),
                ),
                Visibility(
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      color: Theme.of(context).primaryColor,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(25, 10, 0, 10),
                        child: InkWell(
                          onTap: () {
                            launch("tel://" +
                                widget.myRequestListObj.driver_phone_number);
                          },
                          child:
                          CommonWidget.PHONEfooterLayout(
                              CommonWidget.replaceNullWithEmpty(
                                      widget.myRequestListObj
                                          .driver_phone_number),
                              'assets/images/about/white_call.png',
                              20, context),
                        ),
                      ),
                    ),
                  ),
                  visible: CommonWidget.forEmptyReturnHide(widget.myRequestListObj.driver_phone_number),

                )
              ],
            ),
          )),
      onWillPop: _onWillPop,
    );
  }

  Future<void> driverCurrentLocationUpdate() async {
    print('called' + "..............");
//    https://reds.a2hosted.com/araba/api/web/v1/post-jobs/fetch-gprs-tracking

    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] =widget.postid.toString();

    print(map.toString() + "..............");
    print(Api_constant.getdriver_location_api + ".driver current location.............");
//    setState(() {
//      IsProgressIndicatorShow = true;
//    });
    var response = await http.post(Api_constant.getdriver_location_api, body: map);
//    setState(() {
//      IsProgressIndicatorShow = false;
//    });
    print(response.statusCode.toString() + "..............");
    print(response.body + "..............");
    if (response.statusCode == 200) {
      String response_json_str = response.body;

      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
      print(userMap.toString() + "..............");

      var loginResponseObj = new GetDriverLatLongResponse.fromJsonMap(userMap)
          as GetDriverLatLongResponse;

      if (loginResponseObj.status == Api_constant.STATUS) {
        GetDriverLatLong obj = loginResponseObj.data;
        if (obj != null && obj.gprs_lat != null && obj.gprs_lng != null) {
//          if(marker != null) {
//            remove old marker
//            setState(() {
//              _markers.remove(marker);
//            });
//          }
          print(obj.gprs_lat + "obj.gprs_lat..............");
          print(obj.gprs_lng + "obj.gprs_lat..............");
          if(obj.status=="assigned") {
            getAddressFromLatlong(
                Coordinates(double.parse(widget.myRequestListObj.gprs_lat),
                    double.parse(widget.myRequestListObj.gprs_lng)),
                true);
          }

         /* if(obj.status=="assigned"){
            widget.myRequestListObj.pickup_lattitude=obj.gprs_lat;
            widget.myRequestListObj.pickup_longitude=obj.gprs_lng;
            widget.myRequestListObj.delivery_longitude=obj.pickup_lattitude;
            widget.myRequestListObj.delivery_longitude=obj.pickup_longitude;
          }else{
            widget.myRequestListObj.pickup_lattitude=obj.pickup_lattitude;
            widget.myRequestListObj.pickup_longitude=obj.pickup_longitude;
            widget.myRequestListObj.delivery_longitude=obj.delivery_lattitude;
            widget.myRequestListObj.delivery_longitude=obj.delivery_longitude;
          }*/


          Marker marker = Marker(
            markerId: MarkerId(obj.id.toString()),
            position: LatLng(CommonWidget.returnAlgeriaLatWhenGettingBlanck(obj.gprs_lat),
                CommonWidget.returnAlgeriaLongWhenGettingBlanck(obj.gprs_lng)),
//            position: LatLng(double.parse(obj.gprs_lat),
//                double.parse(obj.gprs_lng)),

            // infoWindow: InfoWindow(title: address, snippet: "go here"),
            infoWindow: InfoWindow(
                title: CommonWidget.replaceNullWithEmpty(obj.gprs_address)),
            icon: pinLocationIcon,
//             icon: BitmapDescriptor.fromAsset("assets/images/post_job/comments.png"),
//             icon: BitmapDescriptor.defaultMarker
          );

          if(mounted){
            setState(() {
              //   Toast.show('add', context, gravity: Toast.CENTER);
              // Toast.show(obj.gprs_lat+"  Response", context, gravity: Toast.CENTER);
              _markers.add(marker);
              /* var bounds = getBounds(_markers);
            CameraUpdate u2 = CameraUpdate.newLatLngBounds(bounds, 50);
            _controller.animateCamera(u2).then((void v){
              check(u2,this._controller);
            });*/
            });
          }

        }
      } else {
//        Toast.show(loginUserObj.toString(), context, gravity: Toast.CENTER);
      }

     // if(mounted){setState(() {});}
    }
  }

  Future<void> getDetail(String id) async {

    final uri = Uri.encodeFull(Api_constant.detailScreenApi + '?id=${id}' );
    print(uri.toString() + "...detail api...........");
    if (mounted) {
      setState(() {
        IsProgressIndicatorShow = true;
      });
    }

    var response = await http.get(uri);



    if (response.statusCode == 200) {
      if (mounted) {
        setState(() {
          IsProgressIndicatorShow = false;
        });
      }

      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var CommanModal_obj = new CommanModalWithObj.fromJson(userMap) as CommanModalWithObj;
      if (CommanModal_obj.status == 1) {

        Object obj = CommanModal_obj.data;
        MyRequestListObj g = new MyRequestListObj.fromJsonMap(obj);

        if(mounted){


          setState(() {
            widget.myRequestListObj = g;
            print("***===========" +g.toString());
            print('outside');
            try {
              if (Platform.isIOS) {
                BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.5),
                    'assets/images/car.png')
                    .then((onValue) {
                  pinLocationIcon = onValue;
                });
              }else{
                BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.5),
                    'assets/images/car_original.png')
                    .then((onValue) {
                  pinLocationIcon = onValue;
                });
              }

            } catch (e) {
              print("pinLocationIcon===" + e.toString());
            }

            if (null == widget.myRequestListObj.delivery_lattitude ||
                widget.myRequestListObj.delivery_lattitude.isEmpty) {
              widget.myRequestListObj.delivery_lattitude = '0.0';
            }
            if (null == widget.myRequestListObj.delivery_longitude ||
                widget.myRequestListObj.delivery_longitude.isEmpty) {
              widget.myRequestListObj.delivery_longitude = '0.0';
            }

            if (null == widget.myRequestListObj.pickup_lattitude ||
                widget.myRequestListObj.pickup_lattitude.isEmpty) {
              widget.myRequestListObj.pickup_lattitude = '0.0';
            }

            if (null == widget.myRequestListObj.pickup_longitude ||
                widget.myRequestListObj.pickup_longitude.isEmpty) {
              widget.myRequestListObj.pickup_longitude = '0.0';
            }

            _onAddMarkerButtonPressed();

            getAddressFromLatlong(
                Coordinates(double.parse(widget.myRequestListObj.gprs_lat),
                    double.parse(widget.myRequestListObj.gprs_lng)),
                true);
            sendRequest();

            /* if(widget.myRequestListObj.go_for_payment=='1' && widget.myRequestListObj.payment_type==1){
            _showPriceDialog(widget.myRequestListObj.budget_from);
          }else if(widget.myRequestListObj.payment_type==2){
           // _showCalculationRelatedDialog(widget.myRequestListObj.budget_from);
          }*/
           /* getAddressFromLatlong(
                Coordinates(double.parse(widget.myRequestListObj.gprs_lat),
                    double.parse(widget.myRequestListObj.gprs_lng)),
                true);
            sendRequest();

            if(IsGoForPayment()){
              onwalletAmountChanged();
            }*/
//          setData();
          });
        }



      } else {
        Toast.show(Api_constant.no_record_found, context, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
    }
  }
  Future getAddressFromLatlong(Coordinates coordinates, bool hint) async {
    var addresses =
//    await Geocoder.google(Consts.googleMapApiKEY, language: 'fr').findAddressesFromCoordinates(coordinates);
    await Geocoder.local.findAddressesFromCoordinates(coordinates);
    Consts.sourceAdd = addresses.first.addressLine;
    Consts.pickup_lattitude = coordinates.latitude.toString();
    Consts.pickup_longitude = coordinates.longitude.toString();

    if (hint) {
      if(mounted)
      setState(() {
        driverAddress = addresses.first.addressLine;
        print("driverAddress"+driverAddress.toString());
      });
    }
  }
}

import 'dart:async';
import 'dart:convert';
//import 'dart:html';

import 'package:arabacustomer/ProcessingTrackingDetail2.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/Utils/Consts.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/MyRequestTabsScreen.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';
import 'package:arabacustomer/widgets/lib_dart_noti_center/src/dart_notification_center_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

import 'MyRequestCompleted.dart';
import 'RatingList.dart';
import 'dialog/CancelConfirmationDialog.dart';
import 'dialog/RatingDialog.dart';
import 'model/CommanModalWithObj.dart';
import 'model/MyRequestListObj.dart';
import 'model/PassDataToTrackingScreen.dart';
import 'model/StatusMsgResponse.dart';

class MyRequestDetail extends StatefulWidget {
  static const routeName = '/MyRequestDetail';

  MyRequestListObj myRequestListObj;

//  MyRequestDetail({Key key, this.myRequestListObj}) : super(key: key);

  String  postid;
  MyRequestDetail({Key key, this.postid}) : super(key: key);

  @override
  MyRequestDetailState createState() {
    return MyRequestDetailState();
  }
}

class MyRequestDetailState extends State<MyRequestDetail> implements CallbackOfCanceldDialog , CallbackOfRatinngdDialog {
  bool IsTrackingBtnShow(String status){
    if(status == null || status.isEmpty){
      return false;
    } else{
      if(status.toLowerCase()=='in progress' || status.toLowerCase() == 'driver arrived' ||
          status.toLowerCase()=='on the way'){
//        status.toLowerCase()=='on the way'){
        return true;
      } else{
        return false;
      }
    }

  }
  bool IsProgressIndicatorShow = false;

  bool IsPandingPaymentShow(String status){
    if(status != null &&
        status.isNotEmpty && status.toLowerCase() =='payment pending'){
      return true;
    } else{
      return false;
    }
  }

  bool IsExtraChargesShow(String status){
    if(status == null || status.isEmpty ){
      return false;
    } else if(status.toLowerCase() == Consts.paymentpendingStatusText || status.toLowerCase() == Consts.completedStatusText){
      return true;
    }else{
      return false;
    }
  }

  Future<bool> _onWillPop() async {
    print("_onWillPop===========" );
    goToDetailScreen();
//    Navigator.of(context)
//        .pushNamedAndRemoveUntil(MyJobTabsScreen.routeName,
//            (Route<dynamic> route) => false);

    return true;
  }
  void goToDetailScreen() {
//    Toast.show(widget.myRequestListObj.status, context,
//        gravity: Toast.CENTER);

     int arg = 0;
      if(widget.myRequestListObj.status == null &&
          widget.myRequestListObj.status.isEmpty){
        int arg = 0;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);

      }else if(widget.myRequestListObj.status.toLowerCase() == Consts.pendingStatusText){
        arg = 0;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);

      }else if(widget.myRequestListObj.status.toLowerCase() == Consts.confirmedStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.onthewayStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.driverarrivedStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.inprogressStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.paymentpendingStatusText){
        arg = 0;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);

      } else if(widget.myRequestListObj.status.toLowerCase() == Consts.completedStatusText){
        arg = 0;
        if(Consts.FromRatingScreen){
          Navigator.of(context).pushReplacementNamed(RatingList.routeName);
        } else{
//          Navigator.of(context).pushReplacementNamed(MyRequestCompleted.routeName, arguments: arg);
          Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);
        }

      } else if(widget.myRequestListObj.status.toLowerCase() == Consts.cancelledStatusText){
        arg = 1;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);

      }else{
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);
      }

  }
  int i = 3;

  @override
  void initState() {
    getDetail(widget.postid);

    DartNotificationCenter.subscribe(
      channel: Consts.DetialChanel,
      observer: i,
      onNotification: (result) => {
        print('received:DetialChanel****** $result'),
        if(widget.postid != null && widget.postid.isNotEmpty){
          getDetail(widget.postid)
        }

//        setState(() {
////          new MyRequestDetail();
//        })
      },
    );

    super.initState();


  }

  @override
  void dispose() {
    DartNotificationCenter.unsubscribe(observer: i, channel: Consts.DetialChanel);
    print('received:dispose****** ');
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
//    Toast.show(widget.myRequestListObj.toString(), context,
//        gravity: Toast.CENTER);
    print('received:build****');

    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
              size: 20,
            ),
            onPressed: () =>  goToDetailScreen(),
          ),
          title: CommonWidget.getActionBarTitleText(Api_constant.My_Request),
          flexibleSpace: CommonWidget.ActionBarBg(context),
        ),
        body: Stack(
          children: <Widget>[
//          Container(
////            color: Colors.red,
//            child: GoogleMap(
//              mapType: MapType.normal,
//              initialCameraPosition: _initialCamera,
//              onMapCreated: (GoogleMapController controller) {
//                _mapController.complete(controller);
//              },
//            ),
//          ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Container(
                color: Colors.grey[200],
              ),
            ),
            widget.myRequestListObj != null ? Padding(
              padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
              child: SingleChildScrollView(
                child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(5.0))),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                CommonWidget.leadidDarkRedcolorText(
                                    CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.job_number)),
                                CommonWidget.priceDarkRedcolorText(getprice(CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.status)), context),

                              ],
                            )
                        ),
                        Divider(
                          thickness: 0.2,
                          color: Colors.black87,
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              CommonWidget.jobnameText(
                                  CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.job_title)),
                              SizedBox(height: 5.0),
                              Visibility(
                                visible: false,
//                              visible: CommonWidget.forEmptyReturnHide(widget.myRequestListObj.transporter_name),
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      CommonWidget.redHeaderLbl(
                                          Api_constant.TRANSPORT_NAME, context),
                                      SizedBox(height: 10.0),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          CommonWidget.getIconImgeWithCustomSize( 'assets/images/about/name.png' ,20),
                                          SizedBox(
                                            width: 15,
                                          ),
                                          Text(Api_constant.Name+': '+CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.transporter_name)
                                              +CommonWidget.RatingStrWithBracket(widget.myRequestListObj.customer_ratings),
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 16)),
                                          Visibility(
                                            visible: CommonWidget.IsDisplayRating(widget.myRequestListObj.customer_ratings),
                                            child: Row(
                                              children: <Widget>[
                                                Icon(
                                                  Icons.star,
                                                  color: Colors.amber,
                                                  size: 16,
                                                ),
                                                Text('\) ',
                                                  style: TextStyle(color: Colors.black87,
                                                      fontWeight: FontWeight.w400,
                                                      fontSize: 16),)
                                              ],
                                            ),
                                          )
                                        ],
                                      )

                                    ],
                                  ),
                                ),
                              ),

                              SizedBox(height: 10.0),
                              CommonWidget.gryIconNdGrayTextheaderLayout(
                                  CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.date),
                                  'assets/images/post_job/date_copy.png'),

                              Visibility(
                                child: CommonWidget.gryIconNdGrayTextheaderLayout(Api_constant.Minuteslate +CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.delay_mins),
                                    'assets/images/post_job/date_copy.png'),
                                visible: IsExtraChargesShow(widget.myRequestListObj.status),
                              ),

                              Visibility(
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(height: 3,),
                                    Divider(
                                      thickness: 0.2,
                                      color: Colors.black87,
                                    ),
                                    SizedBox(height: 3,),
                                    CommonWidget.gryIconNdGrayTextheaderLayout(Api_constant.Latefee+CommonWidget.addCurrency(widget.myRequestListObj.delay_charges),
                                        'assets/images/post_job/price.png'),
                                    CommonWidget.gryIconNdGrayTextheaderLayout(Api_constant.Price+CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.price),
                                        'assets/images/post_job/price.png'),
                                    SizedBox(height: 3,),
                                    Divider(
                                      thickness: 0.2,
                                      color: Colors.black87,
                                    ),
                                    SizedBox(height: 3,),
                                  ],
                                ),
                                visible: IsExtraChargesShow(widget.myRequestListObj.status),
                              ),


                              CommonWidget.gryIconNdGrayTextheaderLayout(
                                  CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.vehicle_category),
                                  'assets/images/post_job/vehicle_copy.png'),
                              SizedBox(height: 8.0),
                              Visibility(
                                visible: IsDisplayDriverinfo(widget.myRequestListObj.JobListType),
//                              visible: CommonWidget.forEmptyReturnHide(widget.myRequestListObj.driver_name),
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          CommonWidget.redHeaderLbl(
                                              Api_constant.DRIVER_NAME, context),
                                          SizedBox(height: 15.0),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 6.0),
                                    CommonWidget.gryCustomIconNdGrayTextheaderLayout(
                                        CommonWidget.replaceNullWithEmpty(Api_constant.Name+': '+widget.myRequestListObj.driver_name),
                                        'assets/images/about/name.png' ,20),
                                    InkWell(
                                      onTap: () {
                                        launch("tel://"+widget.myRequestListObj.driver_phone_number);
                                      },
//                                      'assets/images/about/call.png'
                                      child: CommonWidget.gryCustomIconNdGrayTextheaderLayout(
                                          CommonWidget.replaceNullWithEmpty(Api_constant.Phone+': '+widget.myRequestListObj.driver_phone_number),
                                          'assets/images/about/call.png' ,20),
                                    ),
                                    SizedBox(height: 10.0),

                                    RatingBarIndicator(
                                      rating: CommonWidget.getDoubleFromStrForRating(widget.myRequestListObj.transporter_ratings),
                                      direction: Axis.horizontal,
                                      unratedColor: Colors.grey.withAlpha(95),
                                      itemCount: 5,
                                      itemSize: 22.0,
                                      itemPadding: EdgeInsets.symmetric(horizontal: 1.2),
                                      itemBuilder: (context, _) => Icon(
                                        Icons.star,
                                        color: Colors.amber,
                                      ),
                                    ),
                                    SizedBox(height: 15.0),
                                  ],
                                  mainAxisAlignment:  MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                ),
                              ),

                              CommonWidget.redHeaderLbl(Api_constant.DISTANCE, context),
                              SizedBox(height: 3.0),
                              CommonWidget.gryIconNdGrayTextheaderLayout(
                                  Api_constant.From +
                                      CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.pickup_address),
                                  'assets/images/post_job/location_copy.png'),
                              CommonWidget.gryIconNdGrayTextheaderLayout(
                                  Api_constant.To +
                                      CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.delivery_address),
                                  'assets/images/post_job/location.png'),
                              Padding(
                                padding: EdgeInsets.fromLTRB(35, 3, 0, 6),
                                child: Row(
                                  children: <Widget>[
                                    CommonWidget.redHeaderLbl(Api_constant.Total, context),
                                    CommonWidget.grayDiscriptionText(CommonWidget.replaceNullWithEmpty(widget
                                        .myRequestListObj.distance
                                        .toString()) +
                                        Api_constant.KM),
                                  ],
                                ),
                              ),
                              SizedBox(height: 5.0),
                              Visibility(
                                visible: CommonWidget.forEmptyReturnHide(widget.myRequestListObj.description),
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Divider(
                                      thickness: 0.2,
                                      color: Colors.black87,
                                    ),
                                    SizedBox(height: 5.0),
                                    Text(
                                      CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.description),
                                      style: TextStyle(
                                          color: Colors.black87,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 17),
                                    )
                                  ],
                                ),
                              ),
//                             SizedBox(height: 15.0),
//                             CommonWidget.redHeaderLbl('VEHICLE DETAILS: '),
//                             SizedBox(height: 10.0),
//                             Text(CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.vrn), style: TextStyle(color: Colors.grey[700],
//                                 fontWeight: FontWeight.w400,
//                                 fontSize: 16),),
//                                 Padding(
//                                   padding: EdgeInsets.fromLTRB(0, 6, 0, 6),
//                                   child: Row(
//                                     mainAxisAlignment: MainAxisAlignment.start,
//                                     children: <Widget>[
//                                       CommonWidget.getIconImgeWithCustomSize('assets/images/about/call.png', 15),
//                                       SizedBox(
//                                         width: 15,
//                                       ),
//                                       CommonWidget.grayDiscriptionText(CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.phone_number)),
//                                     ],
//                                   ),
//                                 ),

                              Visibility(
                                visible: IsStatusDisplayShow(widget.myRequestListObj.status),
                                child: Column(
                                  children: <Widget>[
                                          SizedBox(height: 20.0),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                            child:
                                            ButtonTheme(
                                              minWidth: double.infinity,
                                              height: 50.0,
                                              child: RaisedButton(
                                                onPressed: () {},
                                                color: CommonWidget.getColorFromStatus(widget.myRequestListObj.status),
                                                child: Text(
                                                  CommonWidget.getstatusFrenchNameFordisplay(widget.myRequestListObj.status),
                                                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400 ,color: Colors.white),
                                                ),
                                              ),
                                            ),
//                                            FlatButton(
//                                              child: Text(
//                                                CommonWidget
//                                                    .getstatusFrenchNameFordisplay(widget.myRequestListObj.status),
//                                                style: TextStyle(
//                                                    color: Colors.white),
//                                              ),
//                                              color: CommonWidget.getColorFromStatus(widget.myRequestListObj.status),
//                                              onPressed: () {},
//                                            ),
                                          )
                                        ],
                                ),
                              ),

                              Visibility(
                                visible: IsTrackingBtnShow(widget.myRequestListObj.status),
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(height: 20.0),
                                    SubmitButton(
                                        Api_constant.TRACK_THE_JOB, onChanged, 10),
                                  ],
                                ),
                              ),
                              Visibility(
                                visible: IsRateDispllay(),
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(height: 20.0),
                                    SubmitButton(
                                        Api_constant.RATE, onRate, 10),
                                    SizedBox(height: 15.0),
                                    Center(
                                      child: CommonWidget.submitButtonBottomLine(),
                                    )
                                  ],
                                ),
                              ),

                              Visibility(
                                visible: IsRatingBarDispllay(),
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(height: 20.0),
                                    RatingBarIndicator(
                                      rating: CommonWidget.getDoubleFromStrForRating(widget.myRequestListObj.customer_ratings),
                                      direction: Axis.horizontal,
                                      unratedColor: Colors.grey.withAlpha(95),
                                      itemCount: 5,
                                      itemSize: 22.0,
                                      itemPadding:
                                      EdgeInsets.symmetric(horizontal: 1.2),
                                      itemBuilder: (context, _) => Icon(
                                        Icons.star,
                                        color: Colors.amber,
                                      ),
                                    ),
                                    SizedBox(height: 10.0),

                                  ],
                                ),
                              ),

                              Visibility(
                                visible: IsCancelShow(widget.myRequestListObj.status),
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(height: 20.0),
                                    SubmitButton(
                                        Api_constant.CANCEL, onCanceljob, 10),
                                    SizedBox(height: 15.0),
                                    Center(
                                      child: CommonWidget.submitButtonBottomLine(),
                                    )
                                  ],
                                ),
                              ),

                              Visibility(
                                  visible: false,
//                                  visible: IsPandingPaymentShow(widget.myRequestListObj.status),
                                  child: Column(
                                    children: <Widget>[
                                      SizedBox(height: 15.0),

                                      Container(
                                          width: double.infinity,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                              BorderRadius.all(Radius.circular(5.0))
                                          ),
                                          child:Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: <Widget>[
                                              CommonWidget.redHeaderLbl(Api_constant.Payment_Type, context),
                                              SizedBox(height: 20.0),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: <Widget>[
                                                  new Container(
                                                    width: 130.0,
                                                    height: 35.0,
                                                    decoration: new BoxDecoration(
                                                      color: Colors.white,
                                                      border: new Border.all(
                                                          color: Colors.redAccent[700],
                                                          width: 1.0),
                                                      borderRadius: new BorderRadius
                                                          .circular(5.0),
                                                    ),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Radio(
                                                          value: 'one',
                                                          groupValue: _radioValue,
                                                          onChanged: radioButtonChanges,
                                                        ),
                                                        Text(
                                                          'Carte',
                                                          style: new TextStyle(
                                                              fontSize: 16.0,
                                                              fontWeight: FontWeight.w700,
                                                              color: Colors.grey[700]),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  new Container(
                                                    width: 130.0,
                                                    height: 35.0,
                                                    decoration: new BoxDecoration(
                                                      color: Colors.white,
                                                      border: new Border.all(
                                                          color: Colors.redAccent[700],
                                                          width: 1.0),
                                                      borderRadius: new BorderRadius
                                                          .circular(5.0),
                                                    ),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Radio(
                                                          value: 'two',
                                                          groupValue: _radioValue,
                                                          onChanged: radioButtonChanges,
                                                        ),
                                                        Text(
                                                          'Espèces',
                                                          style: new TextStyle(
                                                              fontSize: 16.0,
                                                              fontWeight: FontWeight.w700,
                                                              color: Colors.grey[700]),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                              SizedBox(height: 20.0),
                                              SubmitButton(
                                                  Api_constant.PAY_NOW, onppCompleteJobApiCall, 10),
                                              SizedBox(height: 15.0),
                                              Center(
                                                child: CommonWidget.submitButtonBottomLine(),
                                              )
                                            ],
                                          )),
                                    ],
                                  )
                              ),


                            ],
                          ),
                        )
                      ],
                    )),
              ),
            ) :
//                : Expanded(
////              flex: 1,
//              child:
              Center(child:  Visibility(
                visible: IsProgressIndicatorShow,
                child: SizedBox(
                  child: CircularProgressIndicator(),
                ),
              ),
//              ),
            ),

//            Expanded(
////              flex: 1,
//              child:
              Center(child:  Visibility(
                visible: IsProgressIndicatorShow,
                child: SizedBox(
                  child: CircularProgressIndicator(),
                ),
              ),
              ),
//            )
          ],
        ),
      ),
      onWillPop: _onWillPop,

    );
  }

  String _radioValue; //Initial definition of radio button value
  String choice;
  String PaymentType='';

  void radioButtonChanges(String value) {
    setState(() {
      _radioValue = value;
      switch (value) {
        case 'one':
          PaymentType= '1';
          print(PaymentType + "..............");
          choice = value;
          break;
        case 'two':
          PaymentType= '0';
          print(PaymentType + "..............");
          choice = value;
          break;
//        case 'three':
//          choice = value;
//          break;
        default:
          choice = null;
      }
//      Toast.show(PaymentType, context,
//          gravity: Toast.CENTER);
      debugPrint(choice); //Debug the choice in console
    });
  }

  void onChanged() {
   // Toast.show("Track the job", context,gravity: Toast.CENTER);
    PassDataToTrackingScreen passDataToTrackingScreenObj = new PassDataToTrackingScreen();
    passDataToTrackingScreenObj.job_id = widget.myRequestListObj.job_id;
    passDataToTrackingScreenObj.pickup_address = widget.myRequestListObj.pickup_address;
    passDataToTrackingScreenObj.pickup_lattitude = widget.myRequestListObj.pickup_lattitude;
    passDataToTrackingScreenObj.pickup_longitude = widget.myRequestListObj.pickup_longitude;
    passDataToTrackingScreenObj.delivery_address = widget.myRequestListObj.delivery_address;
    passDataToTrackingScreenObj.delivery_lattitude = widget.myRequestListObj.delivery_lattitude;
    passDataToTrackingScreenObj.delivery_longitude = widget.myRequestListObj.delivery_longitude;
    passDataToTrackingScreenObj.driver_phone_number = widget.myRequestListObj.driver_phone_number;
    passDataToTrackingScreenObj.IsFromSideMenu = false;
    Navigator.of(context).pushReplacementNamed(ProcessingTrackingDetail2.routeName, arguments: passDataToTrackingScreenObj);
  }

  IsDisplayDriverinfo(int jobListType) {
    if((CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.status)).toLowerCase()  == Consts.pendingStatusText ){
      return false;
    } else{
      return CommonWidget.forEmptyReturnHide(widget.myRequestListObj.driver_name);
    }
//    if(jobListType==Consts.JobListTypeInprogress || jobListType == Consts.JobListTypeComple){
//    } else{
//      return false;
//    }
  }

  Future<void> apiCAllPandingPaymentComplete() async {
    print('called' + "pending payment complete");

      var map = new Map<String, dynamic>();
      map[Api_constant.job_id] = widget.myRequestListObj.job_id.toString();
      map[Api_constant.online_payment] = PaymentType.toString();

      print(Api_constant.pp_complete_job_api + "pending_payment_api");
   //cancel
   
      var response = await http.post(
          Api_constant.pp_complete_job_api, body: map);

      print(response.statusCode.toString() + "payment_status_code");
      print(response.body + "payment_body");
      if (response.statusCode == 200) {
        String response_json_str = response.body;
        print(response_json_str + "payment_response_body");
        Map userMap = jsonDecode(response_json_str);

        var loginResponseObj = new StatusMsgResponse.fromJsonMap(
            userMap) as StatusMsgResponse;

        if(loginResponseObj.status == 1) {
          Toast.show(Api_constant.job_complete_msg, context, gravity: Toast.CENTER);
          getDetail(widget.myRequestListObj.job_id.toString());

//          Navigator.of(context)
//              .pushNamedAndRemoveUntil(MyRequestTabsScreen.routeName, (Route<dynamic> route) => false);
        }else{
          var loginUserObj = loginResponseObj.message;
          Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
        }
      }
  }


  void onppCompleteJobApiCall() {
    if(PaymentType.isNotEmpty){

      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text(Api_constant.Araba),
            content: new Text("Voulez-vous procéder au paiement?"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text(Api_constant.CANCEL),
                textColor: Theme.of(context).primaryColor,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),

              new FlatButton(
                child: new Text('CONTINUE'),
                textColor: Theme.of(context).primaryColor,
                onPressed: () async {

                  apiCAllPandingPaymentComplete();
                  Navigator.of(context).pop();

                },
              ),
            ],
          );
        },
      );

    } else{
      Toast.show(Api_constant.choose_payment, context, gravity: Toast.CENTER);
    }
  }

  Future<void> getDetail(String id) async {

    final uri = Uri.encodeFull(Api_constant.detailScreenApi +
        '?id=${id}' );
    print(uri.toString() + "..............");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.get(uri);

    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var CommanModal_obj = new CommanModalWithObj.fromJson(userMap) as CommanModalWithObj;
      if (CommanModal_obj.status == 1) {

        Object obj = CommanModal_obj.data;
        MyRequestListObj g = new MyRequestListObj.fromJsonMap(obj);

        setState(() {
          widget.myRequestListObj = g;
          print("***===========" +g.toString());

//          setData();
        });

//        setState(() {
//        });

      } else {
        Toast.show(Api_constant.no_record_found, context,
            gravity: Toast.CENTER);

      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);

    }
  }

  IsCancelShow(String status) {
    if (null == status || status.isEmpty) {
      return false;
    } else if (status.toLowerCase() == Consts.confirmedStatusText ||
        status.toLowerCase() == 'driver arrived' ||
        status.toLowerCase() == 'on the way'||
        status.toLowerCase() == Consts.pendingStatusText) {
      return true;
    } else {
      return false;
    }
  }


  void onCanceljob() {
    showDialog(
        context: context,
        builder: (BuildContext
        context) =>
            CancelConfirmationDialog(
                this, widget.myRequestListObj.job_id.toString()));

  }

  @override
  void cancelJob(String jobid, String cancel_reason) {
    cancleJobApiCall(jobid, cancel_reason);
  }
  Future<void> cancleJobApiCall(String job_id, String cancel_reason) async {
    print('called' + "..............");

    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = job_id;
    map[Api_constant.reasone_for_cancel] = cancel_reason;

    print(map.toString() + "..............");

    var response = await http.post(Api_constant.cancelJob, body: map);

    print(response.statusCode.toString() + "..............");
    print(response.body + "..............");
    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
      new StatusMsgResponse.fromJsonMap(userMap) as StatusMsgResponse;

      var loginUserObj = loginResponseObj.message;

      if(loginResponseObj.status == Api_constant.STATUS) {
        Toast.show(Api_constant.cancel_job_success_msg, context, gravity: Toast.CENTER);

        getDetail(widget.myRequestListObj.job_id.toString());

      } else{
        Toast.show(loginUserObj.toString(), context, gravity: Toast.CENTER);
      }

//      setState(() {
//
//      });
//      _MyRequestOpenObjList.forEach((element) {
//        //not working
//        if (element.job_id == job_id) {
//          int index = _MyRequestOpenObjList.indexOf(element);
//
//          setState(() {
//            _MyRequestOpenObjList.remove(index);
//          });
//        }
//      });
    }
  }

  IsRateDispllay() {
    if(widget.myRequestListObj.status != null &&
        widget.myRequestListObj.status.isNotEmpty &&
        widget.myRequestListObj.status.toLowerCase()==Consts.completedStatusText ) {

      if(CommonWidget.getboolFromInt(widget.myRequestListObj.customer_rated)){
        return true;
      } else{
        return false;
      }
    }else{
      return  false;
    }

  }

  IsRatingBarDispllay() {
    if(widget.myRequestListObj.status != null &&
        widget.myRequestListObj.status.isNotEmpty &&
        widget.myRequestListObj.status.toLowerCase()==Consts.completedStatusText ) {

      if(CommonWidget.getboolFromInt(widget.myRequestListObj.customer_rated)){
        return false;
      } else{
        return true;
      }
    }else{
      return  false;
    }

  }


  void onRate() {
    showDialog(
        context: context,
        builder: (BuildContext context) =>
            RatingDialog(widget.myRequestListObj.job_id.toString(),'',
//                                                                 MyRequestcompletedObjList[index].transporter_name
                this)
    );
  }

  @override
  void ratingComplete() {
    getDetail(widget.myRequestListObj.job_id.toString());

  }

  bool IsStatusDisplayShow(String status) {
    if(null == status || status.isEmpty){
      return false;
    }  else{
      if(widget.myRequestListObj.status.toLowerCase() == Consts.confirmedStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.onthewayStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.driverarrivedStatusText ){
        return true;
      } else {
        return false;
      }
    }
  }

  String getprice(String status) {
    if(status != null && status.isNotEmpty && status.toLowerCase() == Consts.paymentpendingStatusText){

      double total = 0;

      String pricee = CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.price);
      String delay_chargess = CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.delay_charges);

//      if(delay_chargess.contains(Consts.currencySymbol)){
        if((delay_chargess.replaceFirst(Consts.currencySymbol, '')).isEmpty){
          return widget.myRequestListObj.price;
        }

        if((delay_chargess.replaceFirst(Consts.currencySymbol, '')).isNotEmpty &&
            (pricee.replaceFirst(Consts.currencySymbol, '')).isNotEmpty){
          total = double.parse(pricee.replaceFirst(Consts.currencySymbol, '')) +
              double.parse(delay_chargess.replaceFirst(Consts.currencySymbol, ''));

          return Consts.currencySymbol+ total.toString();
        }

//      } else if(delay_chargess.contains(Consts.oldcurrencySymbol)){
//        if((delay_chargess.replaceFirst(Consts.oldcurrencySymbol, '')).isEmpty){
//          return widget.myRequestListObj.price;
//        }
//
//        if((delay_chargess.replaceFirst(Consts.oldcurrencySymbol, '')).isNotEmpty &&
//            (pricee.replaceFirst(Consts.oldcurrencySymbol, '')).isNotEmpty){
//          total = double.parse(pricee.replaceFirst(Consts.oldcurrencySymbol, '')) +
//              double.parse(delay_chargess.replaceFirst(Consts.oldcurrencySymbol, ''));
//
//          return Consts.oldcurrencySymbol+ total.toString();
//        }
//      }

    } else if(status != null && status.isNotEmpty && status.toLowerCase() == Consts.paymentpendingStatusText){
      double total = 0;
      String pricee = CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.price);
      String delay_chargess = CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.delay_charges);

      total = double.parse(pricee) + double.parse(delay_chargess);
      return total.toString();

    } else if(IsExtraChargesShow(status)){
      return CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.total_amount);
    } else{
      return CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.price);
    }
  }
}
//https://reds.a2hosted.com/araba/api/web/v1/post-jobs/customer-complete-job
//
//GET
//
//job_id
//online_payment (1/0)
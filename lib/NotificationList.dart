import 'dart:convert';
//import 'dart:js';

import 'package:arabacustomer/PostJob1New.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SideMenu.dart';
//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';
import 'Utils/Consts.dart';
import 'Utils/MyPreferenceManager.dart';
import 'dialog/EnjoyServiceRatingDialog.dart';
import 'model/CommanNotificationModal.dart';
import 'model/NotificationModal.dart';
import 'model/ReadNotificationModal.dart';



class NotificationList extends StatefulWidget {
  static const routeName = '/NotificationList';
  @override
  NotificationListstate createState() {
    return NotificationListstate();
  }
}


class NotificationListstate extends State<NotificationList> {


  List<NotificationModal> _NotificationObjList;

  Future<List<NotificationModal>> geNotificationList() async {
    MyPreferenceManager _myPreferenceManager =
    await MyPreferenceManager.getInstance();
    String id = _myPreferenceManager
        .getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);

    String page_offset = '0';
    final uri = Uri.encodeFull(Api_constant.get_notification +
        '?id=${id}' +
        '&page_offset=${page_offset}');
    print(uri.toString() + "..............");

    var response = await http.get(uri);

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var CommanModal_obj = new CommanNotificationModal.fromJson(userMap) as CommanNotificationModal;
      if (CommanModal_obj.status == 1) {
        List<Object> resultList = CommanModal_obj.data;
        List<NotificationModal> myNotificationList = new List(resultList.length);
        for (int i = 0; i < resultList.length; i++) {
          Object obj = resultList[i];
          try {
            NotificationModal g = new NotificationModal.fromJson(obj);
            myNotificationList[i] = g;
          } catch (Exception) {
            print("Exception.............." + Exception.toString());
          }
        }
        return myNotificationList;

      } else {
//        Toast.show(Api_constant.no_record_found, context,
//            gravity: Toast.CENTER);

        return new List<NotificationModal>();
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);

      return new List<NotificationModal>();
    }
  }



  Future makeReadNotification() async {
    MyPreferenceManager _myPreferenceManager =
    await MyPreferenceManager.getInstance();
    String id = _myPreferenceManager
        .getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);

    final uri = Uri.encodeFull(Api_constant.make_read_notification +
        '?id=${id}');
    print(uri.toString() + "..............");

    var response = await http.get(uri);

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var CommanModal_obj = new ReadNotificationModal.fromJson(userMap) as ReadNotificationModal;
      if (CommanModal_obj.status == 1) {

      } else {
//        Toast.show(Api_constant.no_record_found, context,
//            gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);
    }
  }

  @override
  void initState() {
    makeReadNotification();
  }
  Future<bool> _onWillPop() async {
    Consts.SideMenuCurrentlySelectedPosition = 1;

    print("_onWillPop===========" );
    Navigator.of(context)
        .pushNamedAndRemoveUntil(
        PostJob1New.routeName, (Route<dynamic> route) => false, arguments: 0);

    return true;
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(child: Scaffold(
        appBar: AppBar(
            centerTitle: true,
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
            title: CommonWidget.getActionBarTitleText(Api_constant.NOTIFICATION.toUpperCase()),
            flexibleSpace: CommonWidget.ActionBarBg(context)
        ),
        drawer: MainDrawer(context),
        body: FutureBuilder<List<NotificationModal>>(
          future: geNotificationList(),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return Center(child: CircularProgressIndicator());

            _NotificationObjList = snapshot.data;

            return
              Container(
              color: Colors.grey[100],
              child: Stack(
                alignment: Alignment.bottomRight,
                children: <Widget>[
                  Center(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0.0),
                      child: ListView.builder(
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                            child:  Container(
//                                  width: double.infinity,
                                decoration: BoxDecoration(
                                    color: Colors.white,
//                                        color: Colors.grey[100],
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(4.0))
                                ),
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(15, 12, 15, 12),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[

                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
//                                            CommonWidget.leadidDarkRedcolorText(MyRequestcompletedObjList[index].job_number),
//                                            CommonWidget.priceDarkRedcolorText(getPrice(MyRequestcompletedObjList[index]))
                                          Text(_NotificationObjList[index].date,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w700, color: Colors.black87, fontSize: 12
                                            ),),

//                                          Text('Price',
//                                            style: TextStyle(
//                                                fontWeight: FontWeight.w400, color: Colors.grey[400], fontSize: 11
//                                            ),),

                                        ],
                                      ),


                                      SizedBox(
                                        height: 5,
                                      ),

                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[

                                          Flexible(
                                            child: Text( _NotificationObjList[index].message,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 11
                                              ),),
                                          ),

                                        ],
                                      ),

                                      Padding(
                                        padding: EdgeInsets.fromLTRB(0, 10, 0, 7),
                                        child: Container(
                                          height: 0.7,
                                          color: Colors.black26,
                                          width: MediaQuery.of(context).size.width * 0.90,
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(getFrenchtype(CommonWidget.replaceNullWithEmpty(_NotificationObjList[index].type)),
                                            style: TextStyle(
                                                color: CommonWidget.getColorFromStatus(_NotificationObjList[index].type),
                                                fontSize: 11,
                                                fontWeight:
                                                FontWeight
                                                    .w600),
                                          ),

                                        ],
                                      )
                                    ],
                                  ),
                                )),
                          );
                        },
                        itemCount: _NotificationObjList.length,
                      ),
                    ),
                  ),
                  Center(
                    child: Visibility(
                      visible: _NotificationObjList.isEmpty,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image(image: AssetImage('assets/images/no_record_found_small_img.png'),width: MediaQuery.of(context).size.width * 0.8,),
                          SizedBox(height: 20),
                          Text(Api_constant.NO_NOTIFICATION, style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black54, fontSize: 17),)
                        ],
                      ),
                    ),
                  ),
//              Padding(
//                padding: EdgeInsets.fromLTRB(0, 0, 40, 40),
//                child: FloatingActionButton(
//                  onPressed: () {},
//                  child: Icon(Icons.add),
//                ),
//              )
                ],
              ),
            );
          },
        )
    ),
        onWillPop: _onWillPop);

  }

  @override
  void ratingSubmit(String email) {}


  getColorOfLine(String status) {
    if(status== 'Job Accepted'){
      return Colors.blue;
    } else if(status== 'Job Completed'){
      return Colors.green;
    }else if(status== 'Driver Arrived'){
      return Colors.deepPurple;
    }else if(status== 'Job Started'){
      return Colors.red[900];
    }else if(status== 'Job Cancelled'){
      return Colors.red;
    } else if(status== 'Driver Assigned'){
      return Colors.greenAccent;
    }else if(status== 'Payment Request'){
      return Colors.grey;
    }else if(status== 'Payment Completed'){
      return Colors.lightGreen;
    } else if(status== 'Driver on the way'){
      return Colors.grey[600];
    } else if(status== 'Successfull Payment'){
      return Colors.brown;
    } else if(status== 'Demande de paiement'){
      return Colors.indigo;
    }
  }

  String getFrenchtype(String status) {
    if(status== 'Job Accepted'){
      return Api_constant.JobAccepted_NOTI;
    } else if(status== 'Job Completed'){
      return Api_constant.JobCompleted_NOTI;
    }else if(status== 'Driver Arrived'){
      return Api_constant.DriverArrived_NOTI;
//      return 'Livreur Arrivè';
    }else if(status== 'Job Started'){
      return Api_constant.JobStarted_NOTI;
    }else if(status== 'Job Cancelled'){
      return Api_constant.JobCancelled_NOTI;
    } else if(status== 'Driver Assigned'){
//      return 'Pilote attribué';
      return Api_constant.DriverAssigned_NOTI;
    } else if(status== 'Payment Request'){
      return Api_constant.PaymentRequest_NOTI;
    }else if(status== 'Driver on the way'){
      return Api_constant.Driverontheway_NOTI;
    }else if(status== 'Payment Completed'){
      return Api_constant.PaymentCompleted_NOTI;
    } else if(status== 'Successfull Payment'){
      return Api_constant.SuccessfullPayment_NOTI;
    } else{
      return status;
    }
  }
}

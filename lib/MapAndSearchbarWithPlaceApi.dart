import 'dart:async';
import 'dart:convert';
import 'package:arabacustomer/widgets/SubmitButtonRed.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/Utils/Utility.dart';
import 'package:arabacustomer/VehicleCategoryList.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';
import 'package:arabacustomer/widgets/lib_searchWidget/search_map_place.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/services/distant_google.dart';
import 'package:http/http.dart' as http;

//import 'package:search_map_place/search_map_place.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:toast/toast.dart';
import 'Utils/Consts.dart';
import 'dart:convert' as JSON;
import 'Utils/MyPreferenceManager.dart';
import 'model/CommanModal.dart';
import 'model/DriverLocationObj.dart';
import 'model/StatusMsgResponse.dart';
import 'model/distanceGoogleResponse/distanceResponse.dart';
import 'model/distanceGoogleResponse/rows.dart';

class MapAndSearchbarWithPlaceApi extends StatefulWidget {
  static const routeName = '/mapAndSearchbarWithPlaceApi';



  @override
  State<MapAndSearchbarWithPlaceApi> createState() =>
      MapAndSearchbarWithPlaceApiState();
}

class MapAndSearchbarWithPlaceApiState extends State<MapAndSearchbarWithPlaceApi> {
  Completer<GoogleMapController> _mapController = Completer();


  final CameraPosition _initialCamera = CameraPosition(
    target: LatLng(
        CommonWidget.returnAlgeriaLatWhenGettingBlanck(
            Consts.currentLat.toString()),
        CommonWidget.returnAlgeriaLongWhenGettingBlanck(
            Consts.currentLong.toString())),
    zoom: 14.0,
  );

  /*Set<Circle> circles = Set.from([Circle(
    circleId: CircleId("1"),
    center: LatLng(Consts.currentLat, Consts.currentLong),
    strokeWidth: 1,
    strokeColor: Colors.black,
    radius: 2000,
  )]);*/

  static final CameraPosition _kInitialPosition = CameraPosition(
    target: LatLng(
        CommonWidget.returnAlgeriaLatWhenGettingBlanck(
            Consts.currentLat.toString()),
        CommonWidget.returnAlgeriaLongWhenGettingBlanck(
            Consts.currentLong.toString())),
    zoom: 14.0,
  );

  CameraPosition _position = _kInitialPosition;
  String _address;

  bool IsSourceFocused = true;

  bool IsSourceMapZoomDataSetDisable = false;
  bool IsDestiMapZoomDataSetDisable = false;
  List<DriverLocationObj> MyRequestcompletedObjList;
  Set<Marker> markers = Set();

  @override
  void initState() {

//    setState(() {
////      sourceLatlong = LatLng(widget.currentLat, widget.currentLong);
//
//      getAddressFromLatlong(
//          Coordinates(Consts.currentLat,
//              Consts.currentLong ),
//          true);
//    });

  }

//  void _getLocation() async {
//    var currentLocation = await Geolocator()
//        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
//
//    LatLng latLng =
//        new LatLng(currentLocation.latitude, currentLocation.longitude);
//    CameraUpdate cameraUpdate = CameraUpdate.newLatLngZoom(latLng, 15);
//    final GoogleMapController controller = await _mapController.future;
//    controller.animateCamera(cameraUpdate);
//
//    Consts.pickup_lattitude = currentLocation.latitude.toString();
//    Consts.pickup_longitude = currentLocation.longitude.toString();
//

//  }

  String sourcePlaceHolder = Api_constant.source_empty;
  String destiPlaceHolder = Api_constant.destination_empty;

//  LatLng sourceLatlong = null;

  GlobalKey<SearchMapPlaceWidgetState> _keysource = GlobalKey();
  GlobalKey<SearchMapPlaceWidgetState> _keydesti = GlobalKey();

  Future<List<DriverLocationObj>> getDriverLocationListData() async {

    var map = new Map<String, dynamic>();
//    map[Api_constant.phone_number] = widget.phoneNumber.trim();
    map['live_latitude'] = Consts.pickup_lattitude;
    map['live_longitude'] = Consts.pickup_longitude;

    print(map.toString() + "get_driver_locations..............");
    print(Api_constant.get_driver_locations);

    var response = await http.post(Api_constant.get_driver_locations, body: map);

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var CommanModal_obj = new CommanModal.fromJson(userMap) as CommanModal;
      if(CommanModal_obj.status == 1) {
        List<Object> resultList = CommanModal_obj.data;

        List<DriverLocationObj> myDriverList = new List(resultList.length);
        markers.clear();
        for (int i = 0; i < resultList.length; i++) {
          Object obj = resultList[i];
          try{
            DriverLocationObj g = new DriverLocationObj.fromJsonMap(obj);
            myDriverList[i] = g;

            var dLattiude = double.parse(myDriverList[i].latitude);
            var dLongitude = double.parse(myDriverList[i].longitude);
            final icon = await BitmapDescriptor.fromAssetImage(
                ImageConfiguration(size: Size(24, 24)), 'assets/images/car.png');
            var markerIdVal = markers.length + 1;
            String mar = markerIdVal.toString();
            MarkerId markerId = MarkerId(mar);
            Marker marker2=Marker(markerId: markerId,position: LatLng(dLattiude, dLongitude),
                infoWindow: InfoWindow(
                    title: myDriverList[i].driver_name,onTap: (){},
                    //snippet: myDriverList[i].phone_number
                ),
             // icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
              icon: icon,
           );
            markers.add(marker2);
          } catch(e){
            print( "CompleteListException.............."+e.toString());
          }
        }

        return myDriverList;
      } else {
//        Toast.show(Api_constant.no_record_found, context,
//            gravity: Toast.CENTER);

        return new List<DriverLocationObj>();
      }

    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
      return new List<DriverLocationObj>();
    }
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
//        appBar: AppBar(
//          centerTitle: true,
//          leading: IconButton(
//              icon: Icon(
//                Icons.arrow_back_ios,
//                color: Colors.white,
//                size: 20,
//              ),
//              onPressed: () => Navigator.pop(context)),
//          title: CommonWidget.getActionBarTitleText(Api_constant.POST_A_JOB),
//          flexibleSpace: CommonWidget.ActionBarBg(context),
//        ),
      resizeToAvoidBottomPadding: false,
      body: FutureBuilder<List<DriverLocationObj>>(
          future: getDriverLocationListData(),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return Center(child: CircularProgressIndicator());
            List<DriverLocationObj> MyRequestcompletedObjList = snapshot.data;

       var values=MyRequestcompletedObjList.length;

       return Column(
          children: <Widget>[
            SizedBox(height: 20,),
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                  padding: EdgeInsets.fromLTRB(5, 5, 0, 0),
                  child:  IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                        size: 20,
                      ),
                      onPressed: () => Navigator.pop(context))
              ),
            ),
//                Expanded(
//                  flex: 2,
//                  child:
            Container(
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: Wrap(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                      child: Row(
                        children: <Widget>[
                          CommonWidget.getIconImgeWithCustomSize(
                              'assets/images/black_circle.png', 15),
                          SizedBox(
                            width: 10,
                          ),
                          SearchMapPlaceWidget(
                            key: _keysource,
                            apiKey: Consts.googleMapApiKEY,
                            placeholder: sourcePlaceHolder,
                            chooseType: 1,
                            onClick: () {
                              IsSourceFocused = true;
                            },
                            onSelected: (place) async {
                              final geolocation = await place.geolocation;

                              if (geolocation.coordinates != null &&
                                  geolocation.coordinates
                                      .toString()
                                      .isNotEmpty) {
                                String str11 = geolocation.coordinates
                                    .toString()
                                    .split('\(')[1];

                                Consts.pickup_lattitude =
                                str11.split('\,')[0];
                                Consts.pickup_longitude =
                                    str11.split('\,')[1].replaceAll('\)', '');

                                print(geolocation.coordinates.toString() +
                                    "ad..............");
                                print(Consts.pickup_lattitude +
                                    "LATTI..............");
                                print(Consts.pickup_longitude +
                                    "LONG..............");

//                      getAddressFromLatlong(geolocation.coordinates);
                                /* getAddressFromLatlong(
                                      Coordinates(double.parse(Consts.pickup_lattitude),
                                          double.parse(Consts.pickup_longitude)),
                                      false);*/
                              }

                              IsSourceMapZoomDataSetDisable = true;
//                                _goToThatLocation(geolocation.coordinates);
                              final GoogleMapController controller =
                              await _mapController.future;

//                                controller.moveCamera(CameraUpdate.newLatLngZoom(geolocation.coordinates, 10));
                              controller.animateCamera(CameraUpdate.newLatLng(
                                  geolocation.coordinates));
                              controller.animateCamera(
                                  CameraUpdate.newLatLngBounds(
                                      geolocation.bounds, 0));
                              getDriverLocationListData();
                            },
//                    location: sourceLatlong,
                            IsSource: true,
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(10, 10, 5, 0),
                      child: Row(
                        children: <Widget>[
                          CommonWidget.getIconImgeWithCustomSize(
                              'assets/images/red_square.png', 15),
                          SizedBox(
                            width: 10,
                          ),
                          SearchMapPlaceWidget(
                            key: _keydesti,
                            apiKey: Consts.googleMapApiKEY,
                            placeholder: destiPlaceHolder,
                            chooseType: 2,
//                              placeholder: Api_constant.destination_empty,
                            onClick: () {
                              IsSourceFocused = false;
                            },
                            onSelected: (place) async {
                              final geolocation2 = await place.geolocation;

                              if (geolocation2.coordinates != null &&
                                  geolocation2.coordinates
                                      .toString()
                                      .isNotEmpty) {
                                String str1 = geolocation2.coordinates
                                    .toString()
                                    .split('\(')[1];

                                Consts.delivery_lattitude =
                                str1.split('\,')[0];
                                Consts.delivery_long =
                                    str1.split('\,')[1].replaceAll('\)', '');

                                print(geolocation2.coordinates.toString() +
                                    "ad..............");
                                print(Consts.delivery_lattitude +
                                    "LATTI..............");
                                print(Consts.delivery_long +
                                    "LONG..............");

                                /*getdeliveryAddressFromLatlong(Coordinates(
                                      double.parse(Consts.delivery_lattitude),
                                      double.parse(Consts.delivery_long)));*/
                              }

                              IsDestiMapZoomDataSetDisable = true;
                              final GoogleMapController controller =
                              await _mapController.future;
                              controller.animateCamera(CameraUpdate.newLatLng(
                                  geolocation2.coordinates));
                              controller.animateCamera(
                                  CameraUpdate.newLatLngBounds(
                                      geolocation2.bounds, 0));
                            },
                            IsSource: false,
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
//                SizedBox(height: 15,),
                  ],
                ),
              ),
            ),
//                ),
//              SizedBox(
//                height: 100,
//                Expanded(
//                    child:
//                  flex: 4,
//                ),
            Expanded(
//              height: MediaQuery.of(context).size.height * 0.55,
              child: Stack(

                children: <Widget>[
                  GoogleMap(
                    mapType: MapType.normal,
                    myLocationEnabled: true,
                    myLocationButtonEnabled: true,
                    initialCameraPosition: _initialCamera,
                    onMapCreated: (GoogleMapController controller) {
                      Toast.show(
                          Consts.currentLat.toString() +
                              " " +
                              Consts.currentLong.toString(),
                          context,
                          gravity: Toast.CENTER);

                      controller.animateCamera(CameraUpdate.newLatLng(LatLng(
                          CommonWidget
                              .returnAlgeriaLatWhenGettingBlanck(
                              Consts.currentLat.toString()),
                          CommonWidget
                              .returnAlgeriaLongWhenGettingBlanck(
                              Consts.currentLong.toString()))));
                      _mapController.complete(controller);

//                  _getLocation();
                    },

                    onCameraMove: ((pinPosition) {
                      _position = pinPosition;
                    }),
                    onCameraIdle: _getAddress,
//                          gestureRecognizers: Set()
//                            ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer())),
                    gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                      new Factory<OneSequenceGestureRecognizer>(
                            () => new EagerGestureRecognizer(),
                      ),
                    ].toSet(),
                    markers: markers,

                    ),
                  Center(
                    child: Image.asset(
                      'assets/images/blackCenterpin.png',
                      height: 45,
                      width: 45,
                      color: Theme.of(context).primaryColor,
                    ),
                  )
                ],
              ),
            ),

            Align(
              child: Wrap(
                children: <Widget>[
                  SizedBox(height: 10.0),
                  Padding(
                    padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child:  SubmitButtonRed(
                        getDoneText(), onChanged, 12),
                  ),
                  SizedBox(height: 10.0),
//                        CommonWidget.submitButtonBottomLine(),
//                        SizedBox(height: 15.0)
                ],
              ),
              alignment: Alignment.bottomCenter,
            ),
//                Expanded(
//                  flex: 1,
//                  child: ,
//                )
          ],
        );
          },)
      );

  }

  String getDoneText(){
//    if(Consts.date.isNotEmpty){
//      print(Consts.date);
      return  "CONFIRM";
//      return "SCHEDULE PREMIER \n" + Utility.convertdateFormat(Consts.date);
//    } else{
//      DateTime selectedDate = DateTime.now();
//
//      String dateValue = selectedDate.year.toString() +
//          '-' +
//          selectedDate.month.toString() +
//          '-' +
//          selectedDate.day.toString()+
//          ' ' +
//          selectedDate.hour.toString() +
//          ':' +
//          selectedDate.minute.toString() +
//          ':00';
//      Consts.date = dateValue;
//      print(Consts.date);
//      return "SCHEDULE PREMIER \n" + Utility.convertdateFormat(Consts.date);
//    }
  }

  Future<void> _goToThatLocation(coordinates) async {
    final GoogleMapController controller = await _mapController.future;
    final CameraPosition _kLake = CameraPosition(
        bearing: 192.8334901395799,
        target: coordinates,
        tilt: 59.440717697143555,
        zoom: 19.151926040649414);
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }

  void onChanged() {
    if(Consts.pickup_lattitude.isEmpty || Consts.pickup_longitude.isEmpty ){
      Toast.show(Api_constant.source_empty, context, gravity: Toast.CENTER);

    }else if (Consts.sourceAdd.isEmpty) {
      Toast.show(Api_constant.source_empty, context, gravity: Toast.CENTER);

    } else if(Consts.delivery_lattitude.isEmpty || Consts.delivery_long.isEmpty ){
      Toast.show(Api_constant.destination_empty, context, gravity: Toast.CENTER);

    } else if (Consts.dstAdd.isEmpty) {
      Toast.show(Api_constant.destination_empty, context,
          gravity: Toast.CENTER);

    } else {
      getdistance();
//      Navigator.push(context,
//          MaterialPageRoute(builder: (context) => VehicleCategoryList()));
    }
//    Navigator.pop(context);
  }

  void getdistance() async {
    String dist = "";
    final response = await http
        .get(Utility.getDirectionsUrl(Consts.sourceAdd, Consts.dstAdd));
    Map json = JSON.jsonDecode(response.body);

    DistanceResponse distanceResponse =
        new DistanceResponse.fromJsonMap(json) as DistanceResponse;
    if (distanceResponse.rows != null && distanceResponse.rows.isNotEmpty) {
      Rows rowsObj = distanceResponse.rows[0];

      if (rowsObj != null) {
        if (rowsObj.elements != null) {
          if (rowsObj.elements[0].distance != null) {
            dist = rowsObj.elements[0].distance.text.toString();
            print("distance===" + dist);
          }
        }
      }
    }

    try {
      getSourceCity(Coordinates(double.parse(Consts.pickup_lattitude), double.parse(Consts.pickup_longitude)));
    } catch (e) {
      print(e);
    }

    try {
      getDestinationCity(Coordinates(double.parse(Consts.delivery_lattitude),
          double.parse(Consts.delivery_long)));
    } catch (e) {
      print(e);
    }

    print("distance===" + dist);

    if (dist.isNotEmpty) {
      var arraDis = dist.split(" ");
      Consts.distance = double.parse(arraDis[0]);
      print(Consts.distance.toString()+"Google distance");
    }

    if(Consts.sourceAdd.toLowerCase().contains('vadodara') ||
        Consts.sourceAdd.toLowerCase().contains('bharuch') ||
        Consts.sourceAdd.toLowerCase().contains('baroda') ||

        Consts.sourceAddCity.toLowerCase().contains('vadodara') ||
        Consts.sourceAddCity.toLowerCase().contains('bharuch') ||
        Consts.sourceAddCity.toLowerCase().contains('baroda')
    ){
      Navigator.push(context, MaterialPageRoute(builder: (context) => VehicleCategoryList()));
    } else{
      print(Consts.sourceAddCity+" Checkkkk");
      print(Consts.sourceAdd+" Checkkkk");

      Toast.show("Please enter source address of vadodara or bharuch", context,
          duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
    }
  }


  Future<void> getSourceCity(Coordinates coordinates) async {
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    _address = addresses.first.addressLine;
    var first = addresses.first;
    print('+++++++++++++++++++' +
        ' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');
    print('+++++++++++++++++++' + ' +${first.subAdminArea}+${first.locality}');
    Consts.sourceAddCity =
        CommonWidget.replaceNullWithEmpty(first.subAdminArea);
    Consts.sourcepostal_code =
        CommonWidget.replaceNullWithEmpty(first.postalCode);
    Consts.sourcecountry = CommonWidget.replaceNullWithEmpty(first.countryName);
    Consts.sourcestate = CommonWidget.replaceNullWithEmpty(first.adminArea);
  }

  Future<void> getDestinationCity(Coordinates coordinates) async {
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    _address = addresses.first.addressLine;
    var first = addresses.first;
    print('+++++++++++++++++++' +
        ' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');
    print('+++++++++++++++++++' + ' +${first.subAdminArea}');
    Consts.dstAddCity = CommonWidget.replaceNullWithEmpty(first.subAdminArea);
    Consts.dstpostal_code = CommonWidget.replaceNullWithEmpty(first.postalCode);
    Consts.dstcountry = CommonWidget.replaceNullWithEmpty(first.countryName);
    Consts.dststate = CommonWidget.replaceNullWithEmpty(first.adminArea);
  }

  /*void getdistance() async {
    double distance = await Geolocator().distanceBetween(
        double.parse(Consts.pickup_lattitude),
        double.parse(Consts.pickup_longitude),
        double.parse(Consts.delivery_lattitude),
        double.parse(Consts.delivery_long));

    var convetDecimalVal = distance.toStringAsFixed(2);
    distance = double.parse(convetDecimalVal) / 1000;

    var convetKMVal = distance.toStringAsFixed(2);
    Consts.distance = double.parse(convetKMVal);

    Navigator.push(context,
        MaterialPageRoute(builder: (context) => VehicleCategoryList()));
  }*/

  Future getAddressFromLatlong(Coordinates coordinates, bool hint) async {
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    Consts.sourceAdd = addresses.first.addressLine;

    if (hint) {
      setState(() {
//        sourcePlaceHolder = Consts.sourceAdd;
      });
    }
  }

  Future getdeliveryAddressFromLatlong(Coordinates coordinates) async {
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    Consts.dstAdd = addresses.first.addressLine;
  }

  Future _getAddress() async {
   /* double pickupLat=Consts.pickup_lattitude as double;
    double pickupLang=Consts.pickup_longitude as double;*/

    List<Placemark> placemark = await Geolocator().placemarkFromCoordinates(_position.target.latitude, _position.target.longitude); // fr_FR, fr_DZ, DZA 	fre/fra
   // fr_FR, fr_DZ, DZA 	fre/fra

    print(placemark.elementAt(0).locality);
    print(_position.target.latitude);
    final coordinates = new Coordinates(_position.target.latitude, _position.target.longitude);


    print(coordinates.toString() + "kkkkkkkkkkkkkkkkkkkk");

    getAddressFromLatlong2(coordinates);

  }

  Future getAddressFromLatlong2(Coordinates coordinates) async {

    var addresses = await GoogleGeocoding(Consts.googleMapApiKEY, language: 'en').findAddressesFromCoordinates(coordinates); //final

    _address = addresses.first.addressLine;


    print(_address);

    if (IsSourceFocused) {
      //update source address
      if (IsSourceMapZoomDataSetDisable) {
        IsSourceMapZoomDataSetDisable = false;
      } else {
        setState(() {
          Consts.pickup_lattitude = coordinates.latitude.toString();
          Consts.pickup_longitude = coordinates.longitude.toString();
//        sourcePlaceHolder = _address;
          Consts.sourceAdd = _address;
          _keysource.currentState.setSerachText(_address);

          showToast(_address, gravity: Toast.CENTER);
        });
      }
    } else {
      if (IsDestiMapZoomDataSetDisable) {
        IsDestiMapZoomDataSetDisable = false;
      } else {
        //update desti
        setState(() {
          Consts.delivery_lattitude = coordinates.latitude.toString();
          Consts.delivery_long = coordinates.longitude.toString();
//        destiPlaceHolder = _address;
          Consts.dstAdd = _address;
          _keydesti.currentState.setSerachText(_address);
          showToast( _address, gravity: Toast.CENTER);
//          showToast('desti.. ' + _address, gravity: Toast.CENTER);
        });
      }
    }
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }



 /* Future<List<MyRequestListObj>> geMyRequestOpenObjListtData() async {
//    String id = '38';

    MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
    String id =  _myPreferenceManager.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);
    String page_offset = '0';
    final uri = Uri.encodeFull(Api_constant.MyRequestListInprogress +
        '?id=${id}' +
        '&page_offset=${page_offset}');
    print(uri.toString() + ".....joblist.........");

    var response = await http.get(uri);

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var CommanModal_obj = new CommanModal.fromJson(userMap) as CommanModal;
      if(CommanModal_obj.status ==1){
        List<Object> resultList = CommanModal_obj.data;
        List<MyRequestListObj> myContributionList = new List(resultList.length);

        for (int i = 0; i < resultList.length; i++) {
          Object obj = resultList[i];
          try{
            MyRequestListObj g = new MyRequestListObj.fromJsonMap(obj);
            myContributionList[i] = g;
          } catch(e){
            print(e);
          }
        }
        return myContributionList;
      } else{
//        Toast.show(Api_constant.no_record_found, context,
//            gravity: Toast.CENTER);
        return new List<MyRequestListObj>();
      }

    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);

      return new List<MyRequestListObj>();
    }
  }*/
}

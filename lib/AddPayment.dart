import 'dart:convert';

import 'package:arabacustomer/PostJob1New.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SideMenu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_html/flutter_html.dart';
//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';
import 'dialog/EnjoyServiceRatingDialog.dart';
import 'model/AboutUsResponse.dart';
import 'model/CommanModal.dart';

class AddPayment extends StatefulWidget{
  static const routeName = '/AddPayment';

  @override
  AddPaymentState createState() {
    return AddPaymentState();
  }

}

class AddPaymentState extends State<AddPayment> {
  bool IsProgressIndicatorShow = false;

  String aboutContent="";
  @override
  void initState() {
  }

  Future<bool> _onWillPop() async {
//    print("_onWillPop===========" );
//    Navigator.of(context)
//        .pushNamedAndRemoveUntil(
//        PostJob1New.routeName, (Route<dynamic> route) => false, arguments: 0);

//    return true;
    Navigator.of(context).pop();
  return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(child: Scaffold(
//      appBar: AppBar(
//        centerTitle: true,
//        title: CommonWidget.getActionBarTitleText(Api_constant.Add_payment),
//        flexibleSpace: CommonWidget.ActionBarBg(),
//        elevation: 0,
//      ),
      body: ListView(
        children: <Widget>[

            Container(
              color: Colors.deepOrange[700],
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
//              size: 20,
                    ),
                    onPressed: () {
                      _onWillPop();
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(15, 3, 0, 15),
                    child:  CommonWidget.getNewActionBarTitleText('Add_payment'),
                  )
                ],
              ),
            ),
           Column(
             children: <Widget>[
               ListTile(
                 leading: IconButton(
                   icon: Icon(
                     Icons.payment,
                     color: Colors.black87,
                     size: 20,
                   ),
                   onPressed: () {},
                 ),
                 title: Text('Cash', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.black87 ),),

               ),
               Container(
                 height: 0.5,
                 color: Colors.grey[300],
               ),

               ListTile(
                 leading: IconButton(
                   icon: Icon(
                     Icons.payment,
                     color: Colors.black87,
                     size: 20,
                   ),
                   onPressed: () {},
                 ),
                 title: Text(Api_constant.online_payment, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.black87 ),),

               )
             ],
           )

        ],
      ),
    ),
        onWillPop: _onWillPop);
  }
}
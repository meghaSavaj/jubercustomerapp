import 'dart:convert';

import 'package:arabacustomer/PostJob1New.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SideMenu.dart';
import 'package:flutter_html/flutter_html.dart';
//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';
import 'Utils/Consts.dart';
import 'dialog/EnjoyServiceRatingDialog.dart';
import 'model/AboutUsResponse.dart';
import 'model/CommanModal.dart';

class AboutUs extends StatefulWidget{
  static const routeName = '/AboutUs';

  @override
  AboutUsState createState() {
    return AboutUsState();
  }

}

class AboutUsState extends State<AboutUs> {
  bool IsProgressIndicatorShow = false;

  String aboutContent="";
  @override
  void initState() {
    getData();
  }

  Future<bool> _onWillPop() async {
    Consts.SideMenuCurrentlySelectedPosition = 1;
    print("_onWillPop===========" );
    Navigator.of(context).pushNamedAndRemoveUntil(PostJob1New.routeName, (Route<dynamic> route) => false, arguments: 0);
    return true;
  }

  Future<String> getData() async {
    String data="";
    setState(() {
      IsProgressIndicatorShow = true;
    });
    var response = await http.get(Api_constant.aboutus);
    setState(() {
      IsProgressIndicatorShow = false;
    });
    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var CommanModal_obj = new AboutUsResponse.fromJsonMap(userMap) as AboutUsResponse;
      data = CommanModal_obj.data;
      setState(() {
        aboutContent= data;
        //Toast.show(aboutContent, context, gravity: Toast.CENTER);
      });
      return data;
    } else{
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);

    }
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(child: Scaffold(
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        title: CommonWidget.getActionBarTitleText(Api_constant.About_Us.toUpperCase()),
        flexibleSpace: CommonWidget.ActionBarBg(context),
        elevation: 0,
      ),
      drawer: MainDrawer(context),
      body: Stack(
        children: <Widget>[
          Stack(
            children: <Widget>[
//              CommonWidget.ActionBarBg(context),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: Container(
                  color: Colors.grey[200],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(5.0))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
//                      Align(
//                        alignment: Alignment.topRight,
//                        child: Image.asset('assets/images/aboutus_bg.png'),
//                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15, 15, 0, 0),
                        child:
                        CommonWidget.redHeaderBigLbl(Api_constant.ABOUT_ARABA, context),
                      ),
                      new Expanded(
                          flex: 1,
                          child: SingleChildScrollView(
                              child: Padding(
                                  padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
                                  child:Html(
                                      data:  aboutContent
                                  ) /*CommonWidget.blackDiscriptionText(
                            aboutContent
                            //'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
                             )*/
                              )
                          )
                      )
//                  Padding(
//                      padding: EdgeInsets.all(15),
//                      child: )
                    ],
                  ),
                ),
              )
            ],
          ),
//          Expanded(
//            flex: 1,
//            child:
          Center(child:  Visibility(
            visible: IsProgressIndicatorShow,
            child: SizedBox(
              child: CircularProgressIndicator(),
            ),
          ),
          ),
//          )
        ],
      ),
    ),
        onWillPop: _onWillPop);
  }
}
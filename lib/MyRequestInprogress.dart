import 'package:arabacustomer/dialog/LocationScanningDialog.dart';
import 'package:arabacustomer/dialog/LocationScanningDialog.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

import 'package:url_launcher/url_launcher.dart';

import 'MyRequestDetailNew2.dart';
import 'PostJob1New.dart';
import 'dialog/LocationScanningDialog.dart';
import 'dialog/LocationScanningDialog.dart';
import 'model/StatusMsgResponse.dart';

import 'MyRequestDetail.dart';
import 'Utils/Consts.dart';
import 'model/MyRequestListObj.dart';
import 'Utils/MyPreferenceManager.dart';
import 'Utils/Api_constant.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dialog/CancelConfirmationDialog.dart';

import 'package:toast/toast.dart';

import 'model/CommanModal.dart';

class MyRequestInprogress extends StatefulWidget {
  @override
  MyRequestInProgressState createState() {
    return MyRequestInProgressState();
  }
}

class MyRequestInProgressState extends State<MyRequestInprogress> implements CallbackOfCanceldDialog  {

  @override
  void initState() {
   /* _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);*/
  }

  Future<List<MyRequestListObj>> geMyRequestOpenObjListtData() async {
//    String id = '38';

    MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
    String id =  _myPreferenceManager.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);
    String page_offset = '0';
    final uri = Uri.encodeFull(Api_constant.MyRequestListInprogress +
        '?id=${id}' +
        '&page_offset=${page_offset}');
    print(uri.toString() + ".....joblist.........");

    var response = await http.get(uri);

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var CommanModal_obj = new CommanModal.fromJson(userMap) as CommanModal;
      if(CommanModal_obj.status ==1){
        List<Object> resultList = CommanModal_obj.data;
        List<MyRequestListObj> myContributionList = new List(resultList.length);

        for (int i = 0; i < resultList.length; i++) {
          Object obj = resultList[i];
            try{
              MyRequestListObj g = new MyRequestListObj.fromJsonMap(obj);
              myContributionList[i] = g;
            } catch(e){
              print(e);
            }
        }
        return myContributionList;
      } else{
//        Toast.show(Api_constant.no_record_found, context,
//            gravity: Toast.CENTER);
        return new List<MyRequestListObj>();
      }

    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);

      return new List<MyRequestListObj>();
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<MyRequestListObj>>(
        future: geMyRequestOpenObjListtData(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(child: CircularProgressIndicator());
          List<MyRequestListObj> MyRequestcompletedObjList = snapshot.data;

          return Container(
            color: Colors.grey[200],
            child: Stack(
              alignment: Alignment.bottomRight,
              children: <Widget>[
                Center(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0.0), //AnimationLimiter
                    child: AnimationLimiter(
                      child: ListView.builder(
                        itemBuilder: (context, index) {
                          return AnimationConfiguration.staggeredList(
                              position: index,
                              duration: const Duration(milliseconds: 375),
                              child: SlideAnimation(
                                verticalOffset: 50.0,
                                child: FadeInAnimation(
                                  child: Column(
                                    children: <Widget>[
                                      InkWell(
                                        onTap: () {
                                         /* if(MyRequestcompletedObjList[index].status=="CONFIRMER"){

                                          }*/
                                         // print("job_id"+MyRequestcompletedObjList[index].job_id.toString());
                                          Navigator.of(context).pushNamed(MyRequestDetailNew2.routeName, arguments: MyRequestcompletedObjList[index].job_id.toString());

                                        },
                                        child: Container(
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: BorderRadius.all(Radius.circular(4.0))),
                                            child: Stack(
                                              alignment: AlignmentDirectional.bottomEnd,
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.fromLTRB(15, 12, 15, 12),
                                                  child: Column(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[

                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          CommonWidget.leadidDarkRedcolorText(MyRequestcompletedObjList[index].job_number),

                                                          Column(
                                                            mainAxisAlignment: MainAxisAlignment.end,
                                                            crossAxisAlignment: CrossAxisAlignment.end,
                                                            children: [
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                children: [
                                                                  Visibility(
                                                                    child:  Text('Price',
                                                                      style: TextStyle(
                                                                          fontWeight: FontWeight.w400, color: Colors.grey[400], fontSize: 13
                                                                      ),),
                                                                    visible: !CommonWidget.ISCorporateUser(MyRequestcompletedObjList[index].corporate_id),
                                                                  ),
                                                                  SizedBox(width: 5,),
                                                                  Visibility(
                                                                    child:  Text(CommonWidget.replaceNullWithEmpty(MyRequestcompletedObjList[index].price),
                                                                      style: TextStyle(
                                                                          fontWeight: FontWeight.w700, color: Theme.of(context).primaryColor, fontSize: 14
                                                                      ),),
                                                                    visible: !CommonWidget.ISCorporateUser(MyRequestcompletedObjList[index].corporate_id),
                                                                  ),
                                                                ],
                                                              ),

                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: <Widget>[
//                                            CommonWidget.priceDarkRedcolorText(getPrice(MyRequestcompletedObjList[index]))
                                                          Text(MyRequestcompletedObjList[index].date,
                                                            style: TextStyle(
                                                                fontWeight: FontWeight.w700, color: Colors.black87, fontSize: 14
                                                            ),),

                                                        ],
                                                      ),


                                                      SizedBox(
                                                        height: 5,
                                                      ),

                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: <Widget>[
//                                            CommonWidget.leadidDarkRedcolorText(MyRequestcompletedObjList[index].job_number),
//                                            CommonWidget.priceDarkRedcolorText(getPrice(MyRequestcompletedObjList[index]))

                                                          Container(
                                                            width: MediaQuery.of(context).size.height * 0.30,
                                                            child:  Row(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                              children: [
                                                                Padding(
                                                                  padding: EdgeInsets.fromLTRB(0, 2, 2, 0),
                                                                  child:  Icon(
                                                                    Icons.place,
                                                                    color: Theme.of(context).primaryColor,
                                                                    size: 14.0,
                                                                  ),
                                                                ),
                                                                Flexible(
                                                                  child: Text('To: '+MyRequestcompletedObjList[index].delivery_address,
                                                                    style: TextStyle(
                                                                        fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                                    ),),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
//                                                          Text(getPrice(MyRequestcompletedObjList[index]),

                                                        ],
                                                      ),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Container(
//                                                        width: MediaQuery.of(context).size.height * 0.30,
                                                        child:  Row(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: [
                                                            Padding(
                                                              padding: EdgeInsets.fromLTRB(0, 2, 4, 0),
                                                              child:   CommonWidget.getIconImgeWithCustomSize(
                                                                  'assets/images/black_circle.png', 10)
//                                                              Icon(
//                                                                Icons.place,
//                                                                color: Colors.black87,
//                                                                size: 14.0,
//                                                              ),
                                                            ),
                                                            Flexible(
                                                              child: Text("From: "+MyRequestcompletedObjList[index].pickup_address,
                                                                style: TextStyle(
                                                                    fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                                ),),
                                                            ),
                                                          ],
                                                        ),
                                                      ),

                                                    ],
                                                  ),
                                                ),
//                                      IconButton(
//                                        icon: Icon(
//                                          Icons.arrow_forward_ios,
//                                          color: Colors.black87,
//                                          size: 12,
//                                        ),
//                                        onPressed: () =>  {},
//                                      )
                                              ],
                                            ) ),
                                      ),

                                      SizedBox(
                                        height: 15,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                        },
                        itemCount: MyRequestcompletedObjList.length,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 40, 40),
                  child: FloatingActionButton(
                    onPressed: () {
                      Consts.SideMenuCurrentlySelectedPosition = 1;
                      Navigator.of(context).pushReplacementNamed(PostJob1New.routeName);

                    },
                    backgroundColor: Theme.of(context).primaryColor,
                    child: Icon(Icons.add),
                  ),
                ),
                Center(
                  child: Visibility(
                    visible: MyRequestcompletedObjList.isEmpty,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image(image: AssetImage('assets/images/no_record_found_small_img.png'),width: MediaQuery.of(context).size.width * 0.8,),
                        SizedBox(height: 20),
                        Text(Api_constant.NO_DELIVERY_IN_PROGRESS, style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black54, fontSize: 17),)
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      );

  }
  Future<void> cancleJobApiCall(String job_id, String cancel_reason) async {
    print('called' + "..............");

    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = job_id;
    map[Api_constant.reasone_for_cancel] = cancel_reason;

    print(map.toString() + "..............");

    var response = await http.post(Api_constant.cancelJob, body: map);

    print(response.statusCode.toString() + "..............");
    print(response.body + "..............");
    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
      new StatusMsgResponse.fromJsonMap(userMap) as StatusMsgResponse;

      var loginUserObj = loginResponseObj.message;

      if(loginResponseObj.status == Api_constant.STATUS) {
        Toast.show(Api_constant.cancel_job_success_msg, context, gravity: Toast.CENTER);
      } else{
        Toast.show(loginUserObj.toString(), context, gravity: Toast.CENTER);
      }

      setState(() {

      });

    }
  }

  @override
  void cancelJob(String jobid, String cancel_reason) {
    cancleJobApiCall(jobid, cancel_reason);

  }

  bool getCancelBtnShow(String status) {
    if(status == null || status.isEmpty){
      return false;
    } else if(status.toLowerCase()== Consts.pendingStatusText ||
        status.toLowerCase()== Consts.confirmedStatusText ||
        status.toLowerCase()=='on the way'){
      return true;
    }else{
      return false;
    }
  }

  String getPrice(MyRequestListObj myRequestcompletedObjList) {
    if(myRequestcompletedObjList.status != null && myRequestcompletedObjList.status.isNotEmpty && myRequestcompletedObjList.status.toLowerCase() == Consts.paymentpendingStatusText){

      double total = 0;

      String pricee = CommonWidget.replaceNullWithEmpty(myRequestcompletedObjList.price);
      String delay_chargess = CommonWidget.replaceNullWithEmpty(myRequestcompletedObjList.delay_charges);

//      if(delay_chargess.contains(Consts.currencySymbol)){
        if((delay_chargess.replaceFirst(Consts.currencySymbol, '')).isEmpty){
          return myRequestcompletedObjList.price;
        }

        if((delay_chargess.replaceFirst(Consts.currencySymbol, '')).isNotEmpty &&
            (pricee.replaceFirst(Consts.currencySymbol, '')).isNotEmpty){
          total = double.parse(pricee.replaceFirst(Consts.currencySymbol, '')) +
              double.parse(delay_chargess.replaceFirst(Consts.currencySymbol, ''));

          return Consts.currencySymbol+ total.toString();
        }
//      } else  if(delay_chargess.contains(Consts.oldcurrencySymbol)){
//        if((delay_chargess.replaceFirst(Consts.oldcurrencySymbol, '')).isEmpty){
//          return myRequestcompletedObjList.price;
//        }
//
//        if((delay_chargess.replaceFirst(Consts.oldcurrencySymbol, '')).isNotEmpty &&
//            (pricee.replaceFirst(Consts.oldcurrencySymbol, '')).isNotEmpty){
//          total = double.parse(pricee.replaceFirst(Consts.oldcurrencySymbol, '')) +
//              double.parse(delay_chargess.replaceFirst(Consts.oldcurrencySymbol, ''));
//
//          return Consts.oldcurrencySymbol+ total.toString();
//        }
//      }

    } else  if(myRequestcompletedObjList.status != null  &&
        myRequestcompletedObjList.status.isNotEmpty &&
        myRequestcompletedObjList.status.toLowerCase()  ==  Consts.paymentpendingStatusText){
      return myRequestcompletedObjList.total_amount;

    } else{
      return myRequestcompletedObjList.price;
    }
   }


}

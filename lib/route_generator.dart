import 'package:arabacustomer/MyRequestDetailNew.dart';
import 'package:arabacustomer/MyWallet.dart';
import 'package:arabacustomer/ProcessingTrackingDetail2.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/myProfile.dart';
import 'package:arabacustomer/widgets/MyCompletedJobTabsScreen.dart';
import 'package:arabacustomer/widgets/MyRequestTabsScreen.dart';
import 'package:flutter/material.dart';

import 'AboutUs.dart';
import 'AddPayment.dart';
import 'ContactUs.dart';
import 'Login.dart';
import 'LoginTirth.dart';
import 'MapAndSearchbarWithPlaceApi.dart';
import 'MyRequestCompleted.dart';
import 'MyRequestDetail.dart';
import 'MyRequestDetailNew2.dart';
import 'NotificationList.dart';
import 'PostJob1New.dart';
import 'PostJob2.dart';
import 'RatingList.dart';
import 'Registration.dart';
import 'RegistrationTirth.dart';
import 'ReportNeedyPage.dart';
import 'SplashPage.dart';
import 'VehicleCategoryList.dart';
import 'dialog/EnjoyServiceRatingDialog.dart';
import 'model/MyRequestListObj.dart';
import 'model/PassDataToTrackingScreen.dart';
import 'model/ProfileResponse.dart';
import 'myProfileEditNew.dart';
import 'myProfileEdit.dart';
import 'myProfileNew.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    switch (settings.name) {
//      case '/Debug':
//        return MaterialPageRoute(builder: (_) => DebugWidget(routeArgument: args as RouteArgument));
//      case '/Walkthrough':
//        return MaterialPageRoute(builder: (_) => Walkthrough());
      case SplashPage.routeName:
        return MaterialPageRoute(builder: (_) => SplashPage());

      case Registration.routeName:
        return MaterialPageRoute(builder: (_) => Registration(email: args as String));
//        return MaterialPageRoute(builder: (_) => Registration(email: args as String, fname: args as String, lname: args as String, fbphone: args as String,));

      case RegistrationTirth.routeName:
        return MaterialPageRoute(builder: (_) => RegistrationTirth(id : args as String));

      case Login.routeName:
        return MaterialPageRoute(builder: (_) => Login());

      case LoginTirth.routeName:
        return MaterialPageRoute(builder: (_) => LoginTirth());

      case PostJob1New.routeName:
        return MaterialPageRoute(builder: (_) => PostJob1New());

      case MyProfile.routeName:
        return MaterialPageRoute(builder: (_) => MyProfile());

      case myProfileNew.routeName:
        return MaterialPageRoute(builder: (_) => myProfileNew());

      case MyProfileEdit.routeName:
        return MaterialPageRoute(
            builder: (_) =>
                MyProfileEdit(profileResponseObj: args as ProfileResponse));

      case myProfileEditNew.routeName:
        return MaterialPageRoute(
            builder: (_) =>
                myProfileEditNew(profileResponseObj: args as ProfileResponse));

      case PostJob2.routeName:
        return MaterialPageRoute(builder: (_) => PostJob2());

      case AboutUs.routeName:
        return MaterialPageRoute(builder: (_) => AboutUs());

      case AddPayment.routeName:
        return MaterialPageRoute(builder: (_) => AddPayment());

      case ContactUs.routeName:
        return MaterialPageRoute(builder: (_) => ContactUs());

      case NotificationList.routeName:
        return MaterialPageRoute(builder: (_) => NotificationList());

      case MyRequestTabsScreen.routeName:
        return MaterialPageRoute(builder: (_) => MyRequestTabsScreen(iinitialIndex: args as int));

      case MyCompletedJobTabsScreen.routeName:
        return MaterialPageRoute(builder: (_) => MyCompletedJobTabsScreen(iinitialIndex: args as int));

//        case MyRequestCompleted.routeName:
//        return MaterialPageRoute(builder: (_) => MyRequestCompleted());

      case RatingList.routeName:
        return MaterialPageRoute(builder: (_) => RatingList());

      case MapAndSearchbarWithPlaceApi.routeName:
        return MaterialPageRoute(builder: (_) => MapAndSearchbarWithPlaceApi());
//        return MaterialPageRoute(builder: (_) => MapAndSearchbarWithPlaceApi(currentLat: args as double, currentLong: args as double));

    case VehicleCategoryList.routeName:
        return MaterialPageRoute(builder: (_) => VehicleCategoryList());

    case MyRequestDetail.routeName:
        return MaterialPageRoute(
            builder: (_) => MyRequestDetail(postid: args as String));

    case MyRequestDetailNew.routeName:
        return MaterialPageRoute(
            builder: (_) => MyRequestDetailNew(postid: args as String));

      case MyRequestDetailNew2.routeName:
        return MaterialPageRoute(
            builder: (_) => MyRequestDetailNew2(postid: args as String));

    case ProcessingTrackingDetail2.routeName:
            return MaterialPageRoute(
                builder: (_) =>
                  //  ProcessingTrackingDetail2(myRequestListObj: args as PassDataToTrackingScreen));
                    ProcessingTrackingDetail2(postid: args as String));

      case ReportNeedyPage.routeName:
        return MaterialPageRoute(builder: (_) => ReportNeedyPage());
      case MyWallet.routeName:
        return MaterialPageRoute(builder: (_) => MyWallet());
//      case '/Pages':
//        return MaterialPageRoute(builder: (_) => PagesTestWidget(currentTab: args));
//      case '/Menu':
//        return MaterialPageRoute(builder: (_) => MenuWidget(routeArgument: args as RouteArgument));
//      case '/PayOnPickup':
//        return MaterialPageRoute(
//            builder: (_) => OrderSuccessWidget(routeArgument: RouteArgument(param: 'Pay on Pickup')));
//      case '/PayPal':
//        return MaterialPageRoute(builder: (_) => PayPalPaymentWidget(routeArgument: args as RouteArgument));

//      default:
//        // If there is no such named route in the switch statement, e.g. /third
//        return MaterialPageRoute(builder: (_) => PagesTestWidget(currentTab: 2));
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text(Api_constant.ERROR),
        ),
        body: Center(
          child: Text(Api_constant.ERROR),
        ),
      );
    });
  }
}

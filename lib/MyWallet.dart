import 'dart:convert';

import 'package:arabacustomer/Utils/Internationalization/app_localizations.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SideMenu.dart';
import 'package:arabacustomer/widgets/SubmitButtonRedNew.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

import 'Utils/Api_constant.dart';
import 'Utils/MyPreferenceManager.dart';


import 'model/MyWalletResponse/MyWalletResponseList.dart';
import 'model/MyWalletResponse/MyWalletResponseListObj.dart';
import 'package:share/share.dart';

class MyWallet extends StatefulWidget {
  static const routeName = '/MyWallet';

  @override
  MyWalletState createState() {
    return MyWalletState();
  }
}

class MyWalletState extends State<MyWallet> {
  List<MyWalletResponseListObj> MyWalletListObj = new List();
  bool IsProgressIndicatorShow = false;

  Future<void> myWalletApi() async {
    MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
    String id = _myPreferenceManager.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);

    var map = new Map<String, dynamic>();

    map['id'] = id;
    print(map.toString() + "....MyWallet");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    print(Api_constant.my_wallet_api);

    var response = await http.post(Api_constant.my_wallet_api, body: map);

    setState(() {
      IsProgressIndicatorShow = true;
    });


    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var CommanModal_obj = new MyWalletResponseList.fromJsonMap(userMap) as MyWalletResponseList;

      if (CommanModal_obj.status == 1) {
        IsProgressIndicatorShow = false;
        setState(() {
          payabel_amount=  CommanModal_obj.payabel_amount;
          my_reference_code=  CommonWidget.replaceNullWithEmpty( CommanModal_obj.my_reference_code);
          MyWalletListObj=  CommanModal_obj.data;
        });

      } else {
       Toast.show(CommanModal_obj.message, context, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(AppLocalizations.of(context).translate('something_went_wrong'), context,
          gravity: Toast.CENTER);

    }
  }

  int payabel_amount= 0;
  String my_reference_code= '';


  @override
  void initState() {
    super.initState();
    myWalletApi();
  }

  Future<bool> _onWillPop() async {
    print("_onWillPop===========" );
  /*  Consts.SideMenuCurrentlySelectedPosition = 0;
    Navigator.of(context)
        .pushNamedAndRemoveUntil(
        AvailableJobTabsScreen.routeName, (Route<dynamic> route) => false, arguments: 0);*/

    return true;
  }
  void onChanged() {
    final RenderBox box = context.findRenderObject();
    Share.share('Share your refferal code:'+my_reference_code+' with your friends & earn rupees.',
        subject: '',
        sharePositionOrigin:
        box.localToGlobal(Offset.zero) &
        box.size);
  }

  @override
  Widget build(BuildContext context) {
    return  WillPopScope(
        child: Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          centerTitle: true,
          title: CommonWidget.getActionBarTitleText(Api_constant.MY_WALLET.toUpperCase()),
          flexibleSpace: CommonWidget.ActionBarBg(context),

        ),
        drawer: MainDrawer(context),
        body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.grey[200],
          child: Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(15, 15, 15, 10),
                  child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(8.0))),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(0.0, 12.0, 0.0, 10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 10.0),
                            Center(
                              child: Text(
                                'Payable Amount : '+payabel_amount.toString(),
//                                "Statistiques jusqu'à la date",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 19),
                              ),),
                            SizedBox(height: 10.0),
                            Center(
                              child: Text(
                                'My Refferal Code',
//                                "Statistiques jusqu'à la date",
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14),
                              ),
                            ),
                            SizedBox(height: 10.0),
                            Center(
                              child: Text(
                              my_reference_code,
//                                "Statistiques jusqu'à la date",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 17),
                            ),),
                            Divider(color: Colors.grey[400],),
                            SizedBox(height: 10.0),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Column(
                                    children: <Widget>[
//                                      Text('Total des gains',
                                      SubmitButtonRedNew('Reffer & Earn', onChanged, 1),

                                    ],
                                  ),
                                ),

                              ],
                            ),
                            SizedBox(height: 5.0),


                          ],
                        ),
                      )),
                ),
              ),

              Container(
                child: Align(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
                    child: Container(
                      child: Text('Wallet',
                        style: TextStyle(
                            color: Colors.black87,
                            fontWeight: FontWeight.w600,
                            fontSize: 14),
                      ),
                    ),
                  ),
                  alignment: Alignment.topLeft,
                ),
              ),
              Expanded(
                flex: 1,
                child: Stack(
                  children: <Widget>[
                    Container(
                      child: Center(
                        child: Padding(
                            padding: EdgeInsets.fromLTRB(15.0, 7.0, 15.0, 0.0),
                            child: ListView.builder(
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                  child: Container(
                                    width: double.infinity,
                                    child:  Container(
//                                  width: double.infinity,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8.0))),
                                        child: InkWell(
                                          child: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(15, 10, 15, 5),
                                                child: Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                                  children: <Widget>[
                                                    CommonWidget.priceBlackText(
                                                        '₹'+MyWalletListObj[index].amount),

                                                  ],
                                                ),
                                              ),
                                              Divider(
                                                thickness: 0.4,
                                                color: Colors.grey[400],),
                                              SizedBox(height: 5.0),
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(10, 5, 0, 5),
                                                child: Text(MyWalletListObj[index].text,
                                                  style: TextStyle(color: Colors.black87, fontSize: 12, fontWeight: FontWeight.w400),),
                                              ),
                                            ],
                                          ),
                                          onTap: (){

                                          },
                                        )
                                    ),
                                  ),
                                );
                              },
                              itemCount: MyWalletListObj.length,
                            )),
                      ),
                    ),
                    Center(
                      child: Visibility(
                        visible: IsProgressIndicatorShow,
                        child: SizedBox(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    ),
                    Center(
                      child: Visibility(
                        visible: MyWalletListObj.isEmpty,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image(image: AssetImage('assets/images/no_wallet.png'),width: MediaQuery.of(context).size.width * 0.8,),
                            SizedBox(height: 20),
                            Text('Wallet amount not available', style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black54, fontSize: 14),)
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ))), onWillPop: _onWillPop);

  }
}
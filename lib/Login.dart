import 'dart:convert';
import 'dart:io';
import 'package:arabacustomer/dialog/PhoneOtpDialog.dart';
import 'package:arabacustomer/model/SocialLoginResponse.dart';
import 'package:arabacustomer/widgets/SubmitButtonRed.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:arabacustomer/PostJob1New.dart';
import 'package:arabacustomer/model/LoginResponse.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/MyRequestTabsScreen.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:toast/toast.dart';

import 'Utils/Api_constant.dart';
import 'Utils/Consts.dart';
import 'Utils/MyPreferenceManager.dart';
import 'Utils/Utility.dart';
import 'dialog/ForgotPassword.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:google_sign_in/google_sign_in.dart';
//import 'package:apple_sign_in/apple_sign_in.dart';

import 'model/LoginResponseProfile.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>['email'],
);


class Login extends StatefulWidget {
  static const routeName = '/LoginPage';

  @override
  LoginState createState() {
    // TODO: implement createState
    return LoginState();
  }
}

class LoginState extends State<Login> implements CallbackOfForgotPasswordDialog{
  String emailValue = '';
  String pswdValue = '';
  bool rememberMe = false;
  bool IsProgressIndicatorShow = false;

  final TextEditingController emailValueController = TextEditingController();
  final TextEditingController pswdValueController = TextEditingController();

  Future<void> getRememberMe() async {
    var instance = await MyPreferenceManager.getInstance();
    emailValue = instance.getString(MyPreferenceManager.REMEMBERME_ID);
    pswdValue = instance.getString(MyPreferenceManager.REMEMBERME_PASWD);

    emailValueController.text =emailValue;
    pswdValueController.text =pswdValue;

    if(emailValue.isNotEmpty){
      rememberMe = true;
    } else{
      rememberMe = false;
    }
  }


  Location location = new Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;

  Future<void> EnableLocation() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
//      Toast.show(_serviceEnabled.toString(), context, gravity: Toast.CENTER);
      if (!_serviceEnabled) {
        EnableLocation();
      } else{
        getContinuesBackgroundLocation();
      }
    } else{
      getContinuesBackgroundLocation();
    }
  }

  Future<void> requestPermission() async {
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
//      Toast.show(_permissionGranted.toString(), context, gravity: Toast.CENTER);
      if (_permissionGranted != PermissionStatus.granted) {
        requestPermission();
      } else{
        EnableLocation();
      }
    } else{
      EnableLocation();
    }
  }

  void getContinuesBackgroundLocation() {

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    update_token();
    getRememberMe();

    requestPermission();
    GoogleSignInAccount _currentUser;

    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      setState(() {
        _currentUser = account;
      });
      if (_currentUser != null) {
//        Toast.show(
//            'google login success\nemail=' +
//                _currentUser.email +
//                " displayName" +
//                _currentUser.displayName,
//            context, duration: Toast.LENGTH_SHORT, gravity: Toast.CENTER);

//        print('myDetails email=' + _currentUser.email + " displayName" +
//            _currentUser.displayName + " photoUrl=" + _currentUser.photoUrl);
        _googleSignIn.signOut();

//        emailValueController.text =  _currentUser.email;
//        emailValue =   _currentUser.email;

        var fname ='';
        var lname ='';

        if(_currentUser.displayName != null && _currentUser.displayName.isNotEmpty){
          var names  = _currentUser.displayName.split(' ');
          try{
            fname=names[0];
            lname=names[1];
          } catch(e){

          }
        }
        socialLoginApiCall(_currentUser.email, fname, lname, 'google');

//        Navigator.pushNamed(
//            context, '/RegistrationPage', arguments: _currentUser.email);

      }
    });
//    _googleSignIn.signInSilently();
   /* if(Platform.isIOS){
      //check for ios if developing for both android & ios
      AppleSignIn.onCredentialRevoked.listen((_) {
        Toast.show('Credentials revoked', context, gravity: Toast.CENTER);
        print("Credentials revoked");
      });
    }*/
  }


  Future update_token() async {
    var _myPreferenceManager = await MyPreferenceManager.getInstance();

    String token = _myPreferenceManager.getString(MyPreferenceManager.DEVICE_TOKEN);

    if(null == token || token.isEmpty){
      FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
      _firebaseMessaging.getToken().then((token) {

        print("token===="+token);

        _myPreferenceManager.setString(MyPreferenceManager.DEVICE_TOKEN, token.toString());

      });
    }
  }

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      print(error);
    }
  }

  @override
  Widget build(BuildContext context) {


    return  new Scaffold(
      body: SafeArea(
          child:  Stack(
            children: <Widget>[
              new Container(

                height: double.infinity,
                child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        InkWell(
                          onTap: () {

                            Navigator.pushNamed(context, '/RegistrationPage', arguments: '');
                          },
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 10, 10, 0),
                              child: Text(Api_constant.login_register_now,
                                style: TextStyle(
                                    color: Colors.red,
                                    fontSize: 17,
                                    fontWeight: FontWeight.w600
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                        Padding(
//                padding: const EdgeInsets.symmetric(
//                    vertical: 55.0, horizontal: 25.0),
                          padding: EdgeInsets.fromLTRB(30, 25, 30, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              RichText(
                                textAlign: TextAlign.start,
                                text: TextSpan(
//                style: DefaultTextStyle.of(context).style,
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: Api_constant.login,
                                      style: TextStyle(
                                          color: Colors.black54.withOpacity(0.8),
                                          fontSize: 32,
                                          fontWeight: FontWeight.w800),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 30.0),
                              CommonWidget.lblText( Api_constant.login_email),
                              TextField(
//                                maxLength: Consts.TextField_maxLength_for_email,
                                controller: emailValueController,
                                onChanged: (val) {
                                  emailValue = val;
                                },
                                style: CommonWidget.TFCommnTextStyle(),
                                decoration: CommonWidget.ETInputDecoration(),
                              ),
                              SizedBox(height: 30.0),
                              CommonWidget.lblText( Api_constant.login_password),
                              TextField(
//                                maxLength: Consts.TextField_maxLength_password,
                                controller: pswdValueController,
                                obscureText: true,
                                style: CommonWidget.TFCommnTextStyle(),
                                decoration: CommonWidget.ETInputDecoration(),
                                onChanged: (val) {
                                  pswdValue = val;
                                },
                              ),
                              SizedBox(height: 6.0),
                            ],
                          ),

                        ),

                        Padding(
                          padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
                          child:  Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
//                              Row(
//                                children: <Widget>[
//                                  Checkbox(
//                                    value: rememberMe,
//                                    onChanged: (bool value) {
//                                      setState(() {
//                                        rememberMe = value;
//                                      });
//                                    },
//                                  ),
//                                  Text(Api_constant.login_remember,
//                                      style: TextStyle(
//                                          color: Colors.black54.withOpacity(0.8),
//                                          fontSize: 14,
//                                          fontWeight: FontWeight.w300))
//                                ],
//                              ),
                              InkWell(
                                onTap: (){
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          forgotPassword(this)
//                                  SearchDialog(filtered_data, this),
                                  );
                                } ,
                                child: Text(Api_constant.login_Forgot_Password,
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w600
                                  ),),
                              )
                            ],
                          ),
                        ),

                        Padding(
//                padding: const EdgeInsets.symmetric(
//                    vertical: 55.0, horizontal: 25.0),
                          padding: EdgeInsets.fromLTRB(30, 0, 30, 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
//                    SizedBox(
//                      width: double.infinity,
//                      child: _buildRaisedButtonWithGragient(),
//                    ),
                              SizedBox(height: 20.0),
                              SubmitButtonRed(Api_constant.login_text, onChanged,2),

                              SizedBox(height: 20.0),
                             /* Visibility(
                                child: AppleSignInButton(
                                  style: ButtonStyle.black,
                                  type: ButtonType.continueButton,
                                  onPressed: appleLogIn,
                                ),
//                                visible: true,
                                visible: Platform.isIOS,
                              ),*/
//                              SizedBox(height: 20.0),
//                              Image(image: AssetImage('assets/images/or.png')),
                              SizedBox(height: 25.0),
                              Text(Api_constant.or_connect_using_social_acc,
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 11,
                                    fontWeight: FontWeight.w500
                                ),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 5.0),
                              Padding(
                                padding: EdgeInsets.fromLTRB(2, 0, 2, 0),
                                child:  Center(
                                  child: SizedBox(
                                    width: double.infinity,
                                    child: RaisedButton(
                                      onPressed: () {
                                        _fblogin();
                                      },
                                      textColor: Colors.white,
                                      padding: const EdgeInsets.all(0.0),
                                      child: Container(
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                            color: Colors.blue[900],
                                            borderRadius: BorderRadius.all(Radius.circular(5.0))
                                        ),
                                        padding: const EdgeInsets.fromLTRB(0, 11, 0, 11),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            SizedBox(
                                              width: 20,
                                            ),
                                            CommonWidget.getIconImgeWithCustomSize('assets/images/fb.png', 18),
                                            SizedBox(
                                              width: 30,
                                            ),
//                                              Expanded(
//                                                child:
                                            Text(Api_constant.fblogin_text,
                                              style: TextStyle(color: Colors.white,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),),
//                                              ),
                                          ],
                                        ),
                                      ),),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(2, 0, 2, 0),
                                child:  Center(
                                  child: SizedBox(
                                    width: double.infinity,
                                    child: RaisedButton(
                                      onPressed: () {
                                        _handleSignIn();
                                      },
                                      textColor: Colors.white,
                                      padding: const EdgeInsets.all(0.0),
                                      child: Container(
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                            color: Colors.deepOrange[800],
                                            borderRadius: BorderRadius.all(Radius.circular(5.0))
                                        ),
                                        padding: const EdgeInsets.fromLTRB(0, 11, 0, 11),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            SizedBox(
                                              width: 20,
                                            ),
                                            CommonWidget.getIconImgeWithCustomSize('assets/images/google.png', 18),
                                            SizedBox(
                                              width: 30,
                                            ),
//                                              Expanded(
//                                                child:
                                            Text(Api_constant.googlelogin_text,
                                              style: TextStyle(color: Colors.white,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),),
//                                              ),
                                          ],
                                        ),
                                      ),),
                                  ),
                                ),
                              ),

                              SizedBox(height: 20.0),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child:  Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[

                                    SizedBox(height: 20.0),

                                    CommonWidget.submitButtonBottomLine()

                                  ],
                                ),
                              ),
                            ],
                          ),

                        ),
                      ],
                    )
                ) /* add child content here */,
              ),
//              Expanded(
//                flex: 1,
//                child:
              Center(child:  Visibility(
                visible: IsProgressIndicatorShow,
                child: SizedBox(
                  child: CircularProgressIndicator(),
                ),
              ),
              ),
//              )
            ],
          )

      ),
    );
  }

  @override
  void forgotPasswdSendEmail(String email) {
    Toast.show(email.toString(), context,duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
  }

  void onChanged() {
    if(emailValue.isEmpty){
//      flutterToast.showToast(
//        child: toast,
//        gravity: ToastGravity.BOTTOM,
//        toastDuration: Duration(seconds: 2),
//      );
      Toast.show(Api_constant.email_empty, context, gravity: Toast.CENTER);
      return;
    } else if(pswdValue.isEmpty){
      Toast.show(Api_constant.pswd_empty, context, gravity: Toast.CENTER);
      return;
    } else {
      Utility.checkInternetConnection().then((intenet) {
        if (intenet != null && intenet) {
          LoginUser();
        }else {
          Toast.show(Api_constant.no_internet_connection, context,
              gravity: Toast.CENTER);
        }
      });
     // LoginUser();
    }
  }

  Future<void> LoginUser() async {

    var map = new Map<String, dynamic>();
    map[Api_constant.email] = emailValue.trim();
    map[Api_constant.password] = pswdValue.trim();
    map[Api_constant.lng] = Api_constant.en;

    var instance = await MyPreferenceManager.getInstance();
    MyPreferenceManager _myPreferenceManager =
    await MyPreferenceManager.getInstance();
    String token = _myPreferenceManager
        .getString(MyPreferenceManager.DEVICE_TOKEN);


    print(token.toString() +"Logintoken..............");


    map[Api_constant.device_token] = token;
    map[Api_constant.device_type] = Api_constant.DEVICE_TYPE;

    print(map.toString() +"Login..............");
    print(Api_constant.login_customer_api +"Login..............");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.post(Api_constant.login_customer_api, body: map);
    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str +"..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj = new LoginResponse.fromJsonMap(userMap) as LoginResponse;

      if(loginResponseObj.status==1){
        var loginUserObj = loginResponseObj.data;

        MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
        _myPreferenceManager.setBool(MyPreferenceManager.IS_USER_LOGIN, true);
        print( loginUserObj.id.toString() +"..............");

        _myPreferenceManager.setString(MyPreferenceManager.CURRENT_LOGIN_USER_ID, loginUserObj.id.toString());
        _myPreferenceManager.setString(MyPreferenceManager.CORPORATE, loginUserObj.corporate.toString());
        _myPreferenceManager.setString(MyPreferenceManager.LOGIN_NAME, CommonWidget.replaceNullWithEmpty(loginUserObj.first_name)+" "+
        CommonWidget.replaceNullWithEmpty(loginUserObj.last_name));
        print(_myPreferenceManager.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID) +"..............");

        if(rememberMe){
          _myPreferenceManager.setString(MyPreferenceManager.REMEMBERME_ID, emailValue);
          _myPreferenceManager.setString(MyPreferenceManager.REMEMBERME_PASWD, pswdValue);

        } else{
          _myPreferenceManager.setString(MyPreferenceManager.REMEMBERME_ID, '');
          _myPreferenceManager.setString(MyPreferenceManager.REMEMBERME_PASWD, '');
        }

        print("userid "+loginUserObj.id.toString());

//      Navigator.pushNamed(context, MyRequestTabsScreen.routeName);
        Navigator.of(context).pushReplacementNamed(PostJob1New.routeName);
      } else{
        Toast.show(loginResponseObj.message, context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);

      }

    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }
  static final FacebookLogin facebookSignIn = new FacebookLogin();

  void _showMessage(String message) {
    setState(() {
      print(message.toString() + "loginnn..............");
//      _message = message;
    });
  }

  Future<Null> _fblogin() async {
    final FacebookLoginResult result = await facebookSignIn.logIn(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final FacebookAccessToken accessToken = result.accessToken;
        _showMessage('''
         Logged in!
       
         Token: ${accessToken.token}
         User id: ${accessToken.userId}
         Expires: ${accessToken.expires}
         Permissions: ${accessToken.permissions}
         Declined permissions: ${accessToken.declinedPermissions}
         ''');

        var graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${result
                .accessToken.token}');

        var profile = json.decode(graphResponse.body);
        print(profile.toString());

        LoginResponseProfile obj = LoginResponseProfile.fromJson(profile) as LoginResponseProfile;
//        emailValueController.text =  obj.email;
//        emailValue = obj.email;
        await facebookSignIn.logOut();

        Consts.fbfisrtname = CommonWidget.replaceNullWithEmpty(obj.first_name);
        Consts.fblastname = CommonWidget.replaceNullWithEmpty(obj.last_name);
        Consts.fbphone = CommonWidget.replaceNullWithEmpty(obj.phonenumber);

        if(obj.email == null || obj.email.isEmpty){
          Navigator.pushNamed(
              context, '/RegistrationPage', arguments: obj.email);
        } else{
          socialLoginApiCall(obj.email, Consts.fbfisrtname, Consts.fblastname, 'facebook');
        }

//        showDialog(
//            context: context,
//            builder: (BuildContext context) =>
//                GetPhoneOrEmailForLoginVerify('', CommonWidget.replaceNullWithEmpty(obj.email), this));

        break;
      case FacebookLoginStatus.cancelledByUser:
        _showMessage('Login cancelled by the user.');
        break;
      case FacebookLoginStatus.error:
        _showMessage('Something went wrong with the login process.\n'
            'Here\'s the error Facebook gave us: ${result.errorMessage}');
        break;
    }
  }
/*
  Future<void> appleLogIn() async {
    if(await AppleSignIn.isAvailable()) {
      //Check if Apple SignIn isn available for the device or not
      final AuthorizationResult result = await AppleSignIn.performRequests([
        AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])
      ]);
      switch (result.status) {
        case AuthorizationStatus.authorized:
          Toast.show(result.credential.toString(), context, gravity: Toast.CENTER);
          print(result.credential.toString());//All the required credentials
          if(result.credential != null){
//            Navigator.pushNamed(
//                context, '/RegistrationPage', arguments: result.credential.email);

            var fname ='';
            var lname ='';
            if(result.credential.fullName != null ){
              try{
                fname=CommonWidget.replaceNullWithEmpty(result.credential.fullName.middleName);
                lname=CommonWidget.replaceNullWithEmpty(result.credential.fullName.familyName);
              } catch(e){

              }
            }

            socialLoginApiCall(result.credential.email, fname, lname, 'apple');
          }

          break;
//          print(result.user);//All the required credentials
        case AuthorizationStatus.error:
          Toast.show("Sign in failed: ${result.error.localizedDescription}", context, gravity: Toast.CENTER);
          print("Sign in failed: ${result.error.localizedDescription}");
          break;
        case AuthorizationStatus.cancelled:
          Toast.show('User cancelled', context, gravity: Toast.CENTER);
          print('User cancelled');
          break;
      }
    }else{
      Toast.show('Apple SignIn is not available for your device', context, gravity: Toast.CENTER);
      print('Apple SignIn is not available for your device');
    }
  }
*/

  Future<void> socialLoginApiCall(String email, String fname, String lname, String social_logintype) async {

    var map = new Map<String, dynamic>();
    map[Api_constant.email] = email.trim();
//    map[Api_constant.password] = pswdValue.trim();
    map[Api_constant.lng] = Api_constant.en;

    map[Api_constant.first_name] = fname;
    map[Api_constant.last_name] = lname;
//    map[Api_constant.phone_number] = mobileValue;

    MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
    String token = _myPreferenceManager
        .getString(MyPreferenceManager.DEVICE_TOKEN);

    print(token.toString() +"Logintoken..............");


    map[Api_constant.device_token] = token;
    if(Platform.isIOS){
      map[Api_constant.device_type] = Api_constant.DEVICE_TYPE_IOS;
    } else{
      map[Api_constant.device_type] = Api_constant.DEVICE_TYPE;
    }

    print(map.toString() +"Login..............");
    print(Api_constant.social_login_customer_api +"Login..............");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.post(Api_constant.social_login_customer_api, body: map);
    setState(() {
      IsProgressIndicatorShow = false;
    });


    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var loginResponseObj = new SocialLoginResponse.fromJsonMap(userMap) as SocialLoginResponse;

      if(loginResponseObj.status ==1){
//        Toast.show(loginResponseObj.message, context,duration: Toast.LENGTH_LONG,
//            gravity: Toast.CENTER);

        if(loginResponseObj.first_time_login.toString() == '1'){
          //go to phone number verification
          showDialog(
              context: context,
              builder: (BuildContext context) =>
                  PhoneOtpDialog(email,fname, lname,""));

        } else{
          var loginUserObj = loginResponseObj.data;

          MyPreferenceManager _myPreferenceManager =
          await MyPreferenceManager.getInstance();
          _myPreferenceManager.setBool(MyPreferenceManager.IS_USER_LOGIN, true);
          _myPreferenceManager.setString(MyPreferenceManager.CURRENT_LOGIN_USER_ID,
              loginUserObj.id.toString());
          _myPreferenceManager.setString(MyPreferenceManager.CORPORATE, loginUserObj.corporate.toString());

          _myPreferenceManager.setString(MyPreferenceManager.LOGIN_NAME, CommonWidget.replaceNullWithEmpty(loginUserObj.first_name)+" "+
              CommonWidget.replaceNullWithEmpty(loginUserObj.last_name));

          print("userid " + loginUserObj.id.toString());

//      Navigator.pushNamed(context, MyRequestTabsScreen.routeName);
          Navigator.of(context).pushReplacementNamed(PostJob1New.routeName);
        }
      } else{
        Toast.show(loginResponseObj.message, context,duration: Toast.LENGTH_LONG,
            gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }

}
}

import 'dart:convert';
import 'dart:io';
//import 'dart:html';

import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SideMenu.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';
import 'package:arabacustomer/widgets/image_picker_handler.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:http/http.dart' as http;
//import 'package:search_map_place/search_map_place.dart';
import 'package:toast/toast.dart';

import 'Utils/Api_constant.dart';
import 'Utils/Consts.dart';
import 'Utils/MyPreferenceManager.dart';
import 'model/ProfileResponse.dart';

class MyProfileEdit extends StatefulWidget {
  static const routeName = '/myprofileEdit';

  ProfileResponse profileResponseObj;

  MyProfileEdit({Key key, this.profileResponseObj}) : super(key: key);

  @override
  MyProfileState createState() {
    return MyProfileState();
  }
}

class MyProfileState extends State<MyProfileEdit>  with TickerProviderStateMixin,ImagePickerListener{
  File _image=null;
  AnimationController _controller;
  ImagePickerHandler imagePicker;

  bool IsProgressIndicatorShow = false;

  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController paswdrController = TextEditingController();
  final TextEditingController confirmPaswdController = TextEditingController();
  final TextEditingController fnameController = TextEditingController();
  final TextEditingController lnameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController addressController = TextEditingController();

  String latitudee='';
  String longitudee='';

  @override
  void initState() {
    super.initState();
    fnameController.text = widget.profileResponseObj.first_name;
    lnameController.text = widget.profileResponseObj.last_name;
    phoneNumberController.text = widget.profileResponseObj.phone_number;
    emailController.text = widget.profileResponseObj.email;
    addressController.text = widget.profileResponseObj.address;
    latitudee = widget.profileResponseObj.latitude;
    latitudee = widget.profileResponseObj.longitude;

    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );

    imagePicker=new ImagePickerHandler(this,_controller);
    imagePicker.init();
  }


  bool profilepic(){
    if(widget.profileResponseObj.profile_pic.isNotEmpty){

    }/*else if(!_image.exists()){

    }*/
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        title: CommonWidget.getActionBarTitleText(''),
        flexibleSpace: CommonWidget.ActionBarBg(context),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 20,),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Stack(
        children: <Widget>[
          Stack(
            children: <Widget>[
              CommonWidget.ActionBarBg(context),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 100, 0, 0),
                child: Container(
                  color: Colors.grey[200],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(20, 55, 20, 15),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                      BorderRadius.all(Radius.circular(5.0))),
                  child:  Padding(padding: EdgeInsets.fromLTRB(15, 55, 15, 10),
                      child: SingleChildScrollView(
                        child:  Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            TextField(
                              readOnly: true,
                              decoration: InputDecoration(labelText: Api_constant.register_firstname,
                                labelStyle: TextStyle(color: Colors.grey[500], fontSize: 12),),
                              controller: fnameController,
                            ),
                            SizedBox(height: 20.0),
                            TextField(
                              readOnly: true,
                              decoration: InputDecoration(labelText: Api_constant.register_lastname,
                                labelStyle: TextStyle(color: Colors.grey[500], fontSize: 12),),
                              controller: lnameController,
                            ),
                            SizedBox(height: 20.0),
                            TextField(
                              readOnly: true,
                              decoration: InputDecoration(labelText: Api_constant.login_email,
                                labelStyle: TextStyle(color: Colors.grey[500], fontSize: 12),),
                              controller: emailController,
                            ),
                            SizedBox(height: 20.0),
                            TextField(
                              readOnly: true,
                              decoration: InputDecoration(labelText: Api_constant.register_mobile_number,
                                labelStyle: TextStyle(color: Colors.grey[500], fontSize: 12),),
                              controller: phoneNumberController,
                            ),
                            SizedBox(height: 20.0),
                            TextField(
//                                maxLength: Consts.TextField_maxLength_password,
                              obscureText: true,
                              decoration: InputDecoration(labelText: Api_constant.password_lbl,
                                labelStyle: TextStyle(color: Colors.grey[500], fontSize: 12),),
                              controller: paswdrController,
                            ),
                            SizedBox(height: 20.0),
                            TextField(
//                                maxLength: Consts.TextField_maxLength_password,
                              obscureText: true,
                              decoration: InputDecoration(labelText: Api_constant.register_confirm_password,
                                labelStyle: TextStyle(color: Colors.grey[500], fontSize: 12),),
                              controller: confirmPaswdController,
                            ),
                            SizedBox(height: 20.0),
//                              TextField(
//                                decoration: InputDecoration(labelText: 'Address',
//                                  labelStyle: TextStyle(color: Colors.grey[500], fontSize: 12),
//                                ),
//                                controller: addressController,
//                              ),
//                              SearchMapPlaceWidget(
//                                apiKey: Consts.googleMapApiKEY,
//                                placeholder: 'Please select source address',
//                                onSelected: (place) async {
//                                  final geolocation = await place.geolocation;
//
//                                  if(geolocation.coordinates !=null && geolocation.coordinates.toString().isNotEmpty){
//                                    String str11 = geolocation.coordinates.toString().split('\(')[1];
//
//                                    latitudee  = str11.split('\,')[0];
//                                    longitudee = str11.split('\,')[1].replaceAll('\)', '');
//
//                                    print(geolocation.coordinates.toString()+"ad..............");
//                                    print(latitudee+"LATTI..............");
//                                    print(longitudee+"LONG..............");
//
//                                    getAddressFromLatlong(Coordinates(double.parse(Consts.pickup_lattitude), double.parse(Consts.pickup_longitude)));
//                                  }
//                                },
//
//                              ),
                            SizedBox(height: 40.0),

//                              SizedBox(height: 20.0),
                            SubmitButton(Api_constant.UPDATE_PROFILE, onChanged,10),
                            SizedBox(height: 15.0),

                            Center(
                              child: CommonWidget.submitButtonBottomLine(),
                            )
                          ],
                        ),
                      )),

                ),
              ),

              Align(alignment : Alignment.topCenter,
                child: InkWell(
                  child: Container(
                      width: 90.0,
                      height: 90.0,
                      //child:getProfileImageDisplay(),
                      //_image == null ? new Image.asset("assets/images/camera.png") : Image.file(_image),
                      //child: (_image == null)? Image.file(_image):(widget.profileResponseObj.profile_pic.isNotEmpty)? Image.file(_image): new Image.asset("assets/images/camera.png"),
                      // _image == null ? new Image.asset("assets/images/camera.png") : Image.file(_image),
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                            fit: BoxFit.fill,
                            image: getProfileImage(),
                          )
                      )
                  ),
                  onTap: () {
                    imagePicker.showDialog(context);
                  },
                ),

//                ClipRRect(
//                  borderRadius: BorderRadius.circular(30.0),
//                  child:Image(image: AssetImage('assets/images/splash.png'), height: 90, width: 90,),
//                ),
              )
            ],
          ),
//            Expanded(
//              flex: 1,
//              child:
          Center(child:  Visibility(
            visible: IsProgressIndicatorShow,
            child: SizedBox(
              child: CircularProgressIndicator(),
            ),
          ),
          ),
//            )
        ],
      ),
    );
  }

  Future getAddressFromLatlong(Coordinates coordinates) async {
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    addressController.text = addresses.first.addressLine;
    print(addressController.text+"addressController..............");
  }

  static const String edit_profile_customer_api = Api_constant.BASE_URL+'users/customer-profile';

  void onChanged() {
    if(paswdrController.text.toString().isNotEmpty){
      if(!CommonWidget.isPasswordCompliant(paswdrController.text.toString(), context)){

      } else{
        gofurther();
      }
    } else{
      gofurther();
    }
  }
  void gofurther() {
    if(paswdrController.text.toString() != confirmPaswdController.text.toString()) {
      Toast.show(Api_constant.compare_paswd_error_msg, context, gravity: Toast.CENTER);
    } else{
      EditUserProfile();
    }
  }

  Future<void> EditUserProfile() async {
    MyPreferenceManager _myPreferenceManager =
    await MyPreferenceManager.getInstance();
    String id = _myPreferenceManager
        .getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);


    var request = new http.MultipartRequest(
        Api_constant.POST, Uri.parse(edit_profile_customer_api));
    request.fields[Api_constant.email] = emailController.text;
    request.fields[Api_constant.password] = paswdrController.text;
    request.fields[Api_constant.first_name] = fnameController.text;
    request.fields[Api_constant.last_name] = lnameController.text;
    request.fields[Api_constant.lng] = Api_constant.en;
    request.fields[Api_constant.id] = id;
   // request.fields[Api_constant.profile_pic] = '';
    request.fields[Api_constant.phone_number] = phoneNumberController.text;
    request.fields[Api_constant.address] = addressController.text;
    request.fields[Api_constant.country] = '';
    request.fields[Api_constant.state] = '';
    request.fields[Api_constant.street] = '';
    request.fields[Api_constant.pincode] = '';
    request.fields[Api_constant.latitude] = '';
    request.fields[Api_constant.longitude] = '';
    request.fields[Api_constant.gender] = '';
    request.fields[Api_constant.birth_date] = '';



    if (_image != null) {
      final length = await _image.length();

      final file = await http.MultipartFile.fromPath(Api_constant.profile_pic, _image.path);
      request.files.add(file);
     /* request.files.add(
          new http.MultipartFile(Api_constant.profile_pic, _image.openRead(), length));*/
    }else{
      request.fields[Api_constant.profile_pic] = '';
    }

//    else{
//      request.files.add(
//          new http.MultipartFile(Api_constant.profile_pic, File));
//    }

    print(request.fields.toString()+"request..........");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    http.Response response =
    await http.Response.fromStream(await request.send());

   /* var map = new Map<String, dynamic>();
    map[Api_constant.email] = emailController.text;
    map[Api_constant.password] = paswdrController.text;
    map[Api_constant.first_name] = fnameController.text;
    map[Api_constant.last_name] = lnameController.text;
    map[Api_constant.lng] = Api_constant.en;
    map[Api_constant.id] = instance.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);
    map[Api_constant.profile_pic] = '';
    map[Api_constant.phone_number] = phoneNumberController.text;
    map[Api_constant.address] = addressController.text;
    map[Api_constant.country] = '';
    map[Api_constant.state] = '';
    map[Api_constant.street] = '';
    map[Api_constant.pincode] = '';
    map[Api_constant.latitude] = '';
    map[Api_constant.longitude] = '';
    map[Api_constant.gender] = '';
    map[Api_constant.birth_date] = '';

    print(map.toString() +"map..............");
    print(edit_profile_customer_api.toString() +"edit_profile_customer_api..............");*/


  //  var response = await http.post(edit_profile_customer_api, body: map);
    print(response.statusCode.toString() +"statusCode..............");
    print(response.body.toString() +"response..............");
    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str +"..............");
      Map userMap = jsonDecode(response_json_str);
      var data = userMap["data"];
      var profileResponseObj = new ProfileResponse.fromJsonMap(data) as ProfileResponse;

      var message = userMap["message"];
      var status = userMap["status"];
      Toast.show(Api_constant.profile_update, context, gravity: Toast.CENTER);
      //Toast.show(message, context, gravity: Toast.CENTER);

      if(status==1) {

        MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();

        if(_myPreferenceManager.getString(MyPreferenceManager.REMEMBERME_ID).isNotEmpty){
          _myPreferenceManager.setString(MyPreferenceManager.REMEMBERME_ID, profileResponseObj.email);

          if(paswdrController.text.isNotEmpty){
            _myPreferenceManager.setString(MyPreferenceManager.REMEMBERME_PASWD, paswdrController.text);
          }
        }

        Consts.tempSaveProfileFname = profileResponseObj.first_name.toString();

        Consts.tempSaveProfileLname = profileResponseObj.last_name.toString();
        Consts.tempSaveProfileEmail = profileResponseObj.email.toString();
        Consts.tempSaveProfilePhoneNumber = profileResponseObj.phone_number.toString();
        Consts.tempSaveProfilePic = profileResponseObj.profile_pic.toString();
      }
//      Consts.tempSaveProfilePswd =paswdrController.text;

      Navigator.pop(context);


    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  @override
  userImage(File _image) {
    this._image = _image;
    setState(() {

    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget getProfileImageDisplay(){
    if(_image!=null){
      return Image.file(_image);
    }else if(widget.profileResponseObj.profile_pic.isNotEmpty){
      return Image.network(
          widget.profileResponseObj.profile_pic);
    }else{
      return Image.asset("assets/images/camera.png");
    }
  }

  ImageProvider getProfileImage(){
    if(_image!=null){
      return FileImage(_image);
    }else if(widget.profileResponseObj.profile_pic.isNotEmpty){
      return  new NetworkImage(
          widget.profileResponseObj.profile_pic);
    }else{
      return AssetImage("assets/images/camera.png");
    }
  }
}

import 'dart:convert';
import 'package:arabacustomer/model/StatusMsgResponse.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/MyRequestTabsScreen.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';
import 'package:arabacustomer/widgets/SubmitButtonRed.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/gestures.dart';

//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';
import 'package:arabacustomer/PostJob1New.dart';
import 'Utils/Consts.dart';
import 'dialog/OtpDialog.dart';
import 'Utils/Api_constant.dart';
import 'Utils/MyPreferenceManager.dart';
import 'Utils/Utility.dart';
import 'dialog/PrivacyPolicyDialog.dart';
import 'model/LoginResponse.dart';
import 'package:arabacustomer/model/GetOtpResponse.dart';
import 'package:email_validator/email_validator.dart';

class RegistrationTirth extends StatefulWidget {
  static const routeName = '/RegistrationTirth';
  String id;

  RegistrationTirth({Key key, this.id}) : super(key: key);
  @override
  RegistrationState createState() {
    return RegistrationState();
  }
}

class RegistrationState extends State<RegistrationTirth> {
  bool IsAgree = false;

  final TextEditingController email_controller = TextEditingController();
  final TextEditingController fname_controller = TextEditingController();
  final TextEditingController lname_controller = TextEditingController();
  final TextEditingController reference_code_controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }



  String fnameValue = '';
  String lnameValue = '';
  String emalValue = '';
  String referenceCodeValue = '';
  bool IsProgressIndicatorShow = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Stack(
            children: <Widget>[
              SingleChildScrollView(
                child: Column(
                  children: <Widget>[

                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          RichText(
                            textAlign: TextAlign.start,
                            text: TextSpan(
//                style: DefaultTextStyle.of(context).style,
                              children: <TextSpan>[
                                TextSpan(
                                  text: Api_constant.login_register_now,
                                  style: TextStyle(
                                      color: Colors.black54.withOpacity(0.8),
                                      fontSize: 32,
                                      fontWeight: FontWeight.w800),
                                ),
//                            TextSpan(
//                              text: Api_constant.register_continue,
//                              style: TextStyle(
//                                  color: Colors.black,
//                                  fontSize: 20,
//                                  fontWeight: FontWeight.w300),
//                            ),
                              ],
                            ),
                          ),
                          SizedBox(height: 30.0),
//                          Row(
//                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                            children: <Widget>[
                          CommonWidget.lblText( 'FIRST NAME*'),
                          TextField(
                            controller: fname_controller,
//                        maxLength: Consts.TextField_maxLength_forname,
                            style: CommonWidget.TFCommnTextStyle(),
                            decoration: CommonWidget.ETInputDecoration(),
                            onChanged: (val) {
                              fnameValue = val;
                            },
                          ),
                          SizedBox(height: 20.0),
                          CommonWidget.lblText( 'LAST NAME*'),
                          TextField(
                            controller: lname_controller,
//                        maxLength: Consts.TextField_maxLength_forname,
                            style: CommonWidget.TFCommnTextStyle(),
                            decoration: CommonWidget.ETInputDecoration(),
                            onChanged: (val) {
                              lnameValue = val;
                            },
                          ),

                          SizedBox(height: 20.0),
                          CommonWidget.lblText( Api_constant.register_email),
                          TextField(
//                        maxLength: Consts.TextField_maxLength_for_email,
                            controller: email_controller,
                            style: CommonWidget.TFCommnTextStyle(),
                            decoration: CommonWidget.ETInputDecoration(),
                            onChanged: (val) {
                              emalValue = val.trim();
                            },
                          ),

                          SizedBox(height: 20.0),
                          CommonWidget.lblText( "REFERENCE CODE"),
                          TextField(
                            controller: reference_code_controller,
//                        maxLength: Consts.TextField_maxLength_forname,
                            style: CommonWidget.TFCommnTextStyle(),
                            decoration: CommonWidget.ETInputDecoration(),
                            onChanged: (val) {
                              referenceCodeValue = val;
                            },
                          ),
                          SizedBox(height: 45.0),
                          SubmitButtonRed('SUBMIT', onChanged, 1),
                          SizedBox(height: 15.0),

                          Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
////
                                CommonWidget.submitButtonBottomLine(),
                                SizedBox(height: 15.0),
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),

              Center(
                child: Visibility(
                  visible: IsProgressIndicatorShow,
                  child: SizedBox(
                    child: CircularProgressIndicator(),
                  ),
                ),
              ),
//            )
            ],
          )),
    );
  }


  void onChanged() {
    if (fnameValue.isEmpty) {
      Toast.show(Api_constant.firstname_empty, context, gravity: Toast.CENTER);
    } else if (lnameValue.isEmpty) {
      Toast.show(Api_constant.lastname_empty, context, gravity: Toast.CENTER);
    } else {
      Utility.checkInternetConnection().then((intenet) {
        if (intenet != null && intenet) {

          RegistrationUser();
        } else {
          Toast.show(Api_constant.no_internet_connection, context,
              gravity: Toast.CENTER);
        }
      });
    }
  }

  void RegistrationUser() async {
    var map = new Map<String, dynamic>();
    map[Api_constant.first_name] = fnameValue;
    map[Api_constant.last_name] = lnameValue;
    map['id'] = widget.id.toString();
    map['reference_code'] =referenceCodeValue;

    setState(() {
      IsProgressIndicatorShow = true;
    });
    print(map.toString() +"insert_user_data..............");
    print(Api_constant.insert_user_data);

    var response = await http.post(Api_constant.insert_user_data, body: map);

    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var loginResponseObj = new LoginResponse.fromJsonMap(userMap) as LoginResponse;

      if(loginResponseObj.status==1){
        var loginUserObj = loginResponseObj.data;

        MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
        _myPreferenceManager.setBool(MyPreferenceManager.IS_USER_LOGIN, true);
        print( loginUserObj.id.toString() +"..............");

        _myPreferenceManager.setString(MyPreferenceManager.CURRENT_LOGIN_USER_ID, loginUserObj.id.toString());
        _myPreferenceManager.setString(MyPreferenceManager.CORPORATE, loginUserObj.corporate.toString());

        _myPreferenceManager.setString(MyPreferenceManager.CORPORATE_ID, loginUserObj.corporate_id.toString());

        _myPreferenceManager.setString(MyPreferenceManager.LOGIN_NAME, CommonWidget.replaceNullWithEmpty(loginUserObj.first_name)+" "+
            CommonWidget.replaceNullWithEmpty(loginUserObj.last_name));
        print(_myPreferenceManager.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID) +"..............");

        print("userid "+loginUserObj.id.toString());

        if(loginResponseObj.profile_complete == 1){
          Navigator.of(context).pushReplacementNamed(PostJob1New.routeName);
        } else{
          Navigator.of(context).pushReplacementNamed(RegistrationTirth.routeName);

        }
//      Navigator.pushNamed(context, MyRequestTabsScreen.routeName);

      } else{
        Toast.show(loginResponseObj.message, context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);

      }

    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }





}

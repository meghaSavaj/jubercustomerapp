import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';

class Consts {
  Consts._();

  static const double alert_dialog_padding = 16.0;
  static int driverApiCallDelay = 5; //2 min
  static int fourtyFiveSecDelay = 45; //45 Sec
  static int fourSecDelay = 4; //45 Sec

  static const String something_went_wrong = 'Something went wrong';

//  static const String googleMapApiKEY="AIzaSyAjS7Vmcweo858wu_WGNU5B5tBg3QBjVK4";
 // static const String googleMapApiKEY ="AIzaSyDHeaktztO4RfLi_ZnkcbdlmT6HYAayB8Q"; //Limer account
  // static const String googleMapApiKEY ="AIzaSyCgaj0DdLpLOYYUiqnPsYeI3wYoGO5x9gM"; //Tirth's account
   static const String googleMapApiKEY ="AIzaSyCGyRUZuuTsq4GZ81mrG-h1WVJlp5Cgxgs"; //Tirth's New key

//      "AIzaSyBU5gYHNznbiMRnpfcVPXKUSeLLybE34Rw"; //radar

//  AIzaSyDIW-bQQf3yTXjTkf94BUfjfKa7ieflutter build apk --release0EIA8             -----araba client ni
  static String insurance_rate;

  static String sourceAdd = "";
  static String dstAdd = "";

  static String sourceAddCity = "";
  static String dstAddCity = "";

  static String sourcepostal_code = "";
  static String dstpostal_code = "";

  static String sourcestate = "";
  static String dststate = "";

  static String sourcecountry = "";
  static String dstcountry = "";

//  static double currentLat = 0.0;
//  static double currentLong = 0.0;
  static double currentLat = 20.5937;
  static double currentLong = 78.9629;

  static String delivery_lattitude = "0.0";
  static String delivery_long = "0.0";

  static String pickup_longitude = "0.0";
  static String pickup_lattitude = "0.0";

  static String vehicle_id;
  static String vehicle_img;
  static String job_name;
  static String vahicle_name;
  static String date;
  static String is_corporate;
  static String date_bool;
  static String comments = '';
  static double distance = 0.0;
  static String rate_per_km;
  static String min_price;

  static String tempSaveProfileFname;
  static String tempSaveProfileLname;
  static String tempSaveProfileEmail;
  static String tempSaveProfilePhoneNumber;
  static String tempSaveProfilePswd;
  static String tempSaveProfilePic;

  static int JobListTypeOpne = 11;
  static int JobListTypeInprogress = 12;
  static int JobListTypeComple = 13;
  static int JobListTypeCancel = 14;
  static int JobListTyperating = 15;

//  New job posted
//  Job Accepted
//  Job Started
//  Successfull Payment
//  Driver Arrived
//  Driver on the way
//  Job Completed
//  Successfull Payment
//  Payment Request
//  Payment Completed
//  Job Cancelled
//  driver arrived
  static String DetialChanel = 'Detial';
  static String JobAcceptedChanel = 'Job Accepted';
  static String DriverArrivedChanel = 'Driver Arrived';
  static String driverarrivedChanel = 'driver arrived';
  static String JobStartedChanel = 'Job Started';
  static String DriveronthewayChanel = 'Driver on the way';
  static String PaymentRequestChanel = 'Payment Request';
  static String PaymentCompletedChanel = 'Payment Completed';
  static String GoForPayment = 'Go For Payment';
  static String SuccessfullPaymentChanel = 'Successfull Payment';
  static String JobCompletedChanel = 'Job Completed';
  static String JobCancelTimer = 'Job Cancel Timer';

  static String CurrentChanelType = '';

  static String pendingStatusText = 'pending';

//  static String confirmedStatusText= 'confirmed';
  static String confirmedStatusText = 'confirmed';

//  static String confirmedStatusText= 'confirmation';

  static String onthewayStatusText = 'on the way';
  static String driverarrivedStatusText = 'driver arrived';
  static String inprogressStatusText = 'in progress';
  static String paymentpendingStatusText = 'payment pending';
  static String completedStatusText = 'completed';
  static String cancelledStatusText = 'cancelled';
  static String SuccessfullPaymentStatusText = 'Successfull Payment';

//  static String currencySymbol= 'Rs. ';
  static String currencySymbol = '₹ ';

//  static String currencySymbol= '\र';
//  static String currencySymbol= '\$ ';\र
  static String oldcurrencySymbol = 'DZD ';

  static int TextField_maxLength = 40;
  static int TextField_maxLength_password = 15;
  static int TextField_maxLength_otp = 6;
  static int TextField_maxLength_for_email = 40;
  static int TextField_maxLength_forname = 30;
  static int TextField_maxLength_cancelreason = 150;
  static int TextField_phonenumber_maxLength = 14;
  static int SideMenuCurrentlySelectedPosition = 0;

  static bool FromRatingScreen = false;
  static bool FromRetryDialog = false;
  static bool isAcceptedJob = false;
  static bool FromJobCompletedScreen = false;
  static bool isCancelText = false;
  static bool isLokkingForDriverPopupOpen = false;
  static GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  static String fbfisrtname = '';
  static String fblastname = '';
  static String fbphone = '';

  static Color primayryColor = Colors.grey;

//  static Color primayryColor = Colors.deepOrange[700];

  static String PAYMENT_TYPE_ONLINE = "1";
  static String PAYMENT_TYPE_CASH = "2";
  static String PAYMENT_TYPE_COMPANY = "0";

  static String notificationJobId="0";
}

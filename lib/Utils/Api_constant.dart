class Api_constant {
  // static const String BASE_URL = "https://reds.a2hosted.com/redcab/api/web/v1/"; //DEMO USER
  static const String BASE_URL =
      "https://tirthtravels.com/api/web/v1/"; //LIVE USER

  static const String POST = "POST";
  static const String logout = BASE_URL + 'users/logout'; //------------new

  static const int STATUS = 1;
  static const String send_otp = BASE_URL + 'users/send-otp'; //------
  static const String verify_email = BASE_URL + 'users/verify-email'; //------
  static const String verify_otp = BASE_URL + 'users/verify-otp'; //------
  static const String phone_number_param = 'phone_number'; //------
  static const String otp_param = 'otp'; //------
  static const String verifyOtp = "CONFIRMÉ VOTRE CODE D'ACCÉE PRIX ";

//   static const String verifyOtp = 'VERIFY CODE';
  static const String enter_otp_for_verification =
      "Veuillez saisir une adresse e-mail et un numéro de téléphone de vérification Otp";
  static const String otp_hint = 'veuillez saisir otp';
  static const String phone_otp_hint = 'Code reçu par SMS';
  static const String email_otp_hint = 'Code reçu par e-mail';
  static const String detailScreenApi =
      BASE_URL + 'post-jobs/get-customer-job-by-id';
  static const String login_customer_api = BASE_URL + 'users/login-customer';
  static const String Online_payment_success = BASE_URL + 'post-jobs/online-payment-success';
  static const String Cash_payment_success = BASE_URL + 'post-jobs/cash-payment-success';

  static const String login_customer_new =
      BASE_URL + 'users/login-customer-new';
  static const String login_otp = BASE_URL + 'users/login-otp';
  static const String insert_user_data = BASE_URL + 'users/insert-user-data';
  static const String get_drivers_live_locations = BASE_URL + 'users/get-drivers-live-location';

//   static const String social_login_customer_api = BASE_URL+'users/social-signup-transporter';
  static const String social_login_customer_api =
      BASE_URL + 'users/social-login-customer';
  static const String social_signup_customer_api =
      BASE_URL + 'users/social-signup-customer';
  static const String signup_customer_api = BASE_URL + 'users/signup-customer';
  static const String forgot_password_api = BASE_URL + 'users/forgot-password';
  static const String post_job_api = BASE_URL + 'post-jobs/job-create';
  static const String get_service_api = BASE_URL + 'users/get-services';
  static const String postjob_gettax__api = BASE_URL + 'post-jobs/get-tax';
  static const String is_job_accepted = BASE_URL + 'post-jobs/is-job-accepted';
  static const String my_wallet = BASE_URL + 'post-jobs/my-wallet';
  static const String payment_use = BASE_URL + 'post-jobs/payment-use';
  static const String get_driver_locations = BASE_URL + 'users/get-driver-locations';

  static const String MyRequestListOpen =
      BASE_URL + 'post-jobs/get-customer-jobs';
  static const String MyRequestListInprogress =
      BASE_URL + 'post-jobs/get-customer-jobs-in-progress';
  static const String MyRequestListComplete =
      BASE_URL + 'post-jobs/get-customer-completed-jobs';
  static const String MyRequestListcancelJobList =
      BASE_URL + 'post-jobs/get-customer-cancelled-jobs';

  static const String complete_job_txt =
      "Voulez-vous vraiment terminer ce travail?";
  static const String complete_job_capital = 'TERMINER LE TRAVAIL';

  static const String id = 'id';
  static const String page_offset = 'page_offset';

  static const String RatingList = BASE_URL + 'post-jobs/customer-rating-list';
  static const String get_profile_customer_api =
      BASE_URL + 'users/get-customer-profile';
  static const String pp_complete_job_api =
      BASE_URL + 'post-jobs/customer-complete-job';
  static const String getdriver_location_api =
      BASE_URL + 'post-jobs/fetch-gprs-tracking';

  static const String customer_side_rating =
      BASE_URL + 'post-jobs/customer-side-rating';
  static const String update_token = BASE_URL + 'users/update-token';

  static const String job_id = 'job_id';
  static const String reasone_for_cancel = 'reasone_for_cancel';
  static const String ratings_by_customer = 'ratings_by_customer';
  static const String review_text_by_customer = 'review_text_by_customer';
  static const String aboutus = BASE_URL + 'post-jobs/aboutus';
  static const String terms = BASE_URL + 'post-jobs/terms';
  static const String contactus = BASE_URL + 'post-jobs/contactus';
  static const String my_wallet_api = BASE_URL + 'users/rewards';
  static const String cancelJob = BASE_URL + 'post-jobs/cancel-job';
  static const String applycoupon = BASE_URL + 'post-jobs/applycoupon';

  static const String get_notification = BASE_URL + 'users/get-notifications';
  static const String make_read_notification = BASE_URL + 'users/make-read-notifications';
  static const String notification_unread_count = BASE_URL + 'users/get-notifications-unread-count';
  static const String send_phoenotp = BASE_URL + 'users/send-otp-to-mobile'; //------
  static const String verify_phone_otp = BASE_URL + 'users/verify-otp-mobile'; //------
  static const String retry_for_job = BASE_URL + 'post-jobs/retry-for-job'; //---------

  static const String address = 'address';
  static const String profile_pic = 'profile_pic';
  static const String country = 'country';
  static const String state = 'state';
  static const String street = 'street';
  static const String pincode = 'pincode';
  static const String latitude = 'latitude';
  static const String longitude = 'longitude';
  static const String birth_date = 'birth_date';

  static const String action = "";
  static const String email = 'email';
  static const String password = 'password';
  static const String lng = 'lng';
  static const String device_token = 'device_token';
  static const String device_type = 'device_type';

  static const String first_name = 'first_name';
  static const String last_name = 'last_name';
  static const String phone_number = 'phone_number';
  static const String gender = 'gender';

  static const String user_id = 'user_id';
  static const String job_title = 'job_title';
  static const String service_id = 'service_id';
  static const String total_price = 'total_price';
  static const String pickup_address = 'pickup_address';
  static const String delivery_address = 'delivery_address';
  static const String description = 'description';
  static const String date = 'date';
  static const String is_corporate = 'is_corporate';
  static const String pickup_street = 'pickup_street';
  static const String pickup_postal_code = 'pickup_postal_code';
  static const String pickup_state = 'pickup_state';
  static const String pickup_country = 'pickup_country';
  static const String pickup_lattitude = 'pickup_lattitude';
  static const String pickup_longitude = 'pickup_longitude';
  static const String delivery_street = 'delivery_street';
  static const String delivery_postal_code = 'delivery_postal_code';
  static const String delivery_state = 'delivery_state';
  static const String delivery_country = 'delivery_country';
  static const String delivery_lattitude = 'delivery_lattitude';
  static const String delivery_longitude = 'delivery_longitude';
  static const String start_with_date = 'start_with_date';
  static const String online_payment = 'online_payment';
  static const String distance = 'distance';
  static const String total_price_without_tax = 'total_price_without_tax';
  static const String service_price_per_km = 'service_price_per_km';
  static const String tax = 'tax';
  static const String tax_capital = 'Tax';

  static const String DEVICE_TYPE = 'android';
  static const String DEVICE_TYPE_IOS = 'ios';
  static const String en = 'en';

  static const String cancel_job_success_msg =
      'your ride has been cancelled successfully';
  static const String date_hint = 'Please Select Date and Time';
  static const String something_went_wrong = 'Something went wrong';
  static const String no_internet_connection =
      'Internet connection not available';
  static const String email_empty = 'Please enter email address';
  static const String pswd_empty = 'Please enter password';

  static const String vehicle_empty = 'Please select vehicle type';

  // static const String vehicle_empty = 'Please select Types of vehicle';
  static const String jobname_empty = 'Please enter ride Name';
  static const String source_empty = 'Please select Source address';
  static const String destination_empty = 'Please select Destination address';
  static const String pickup_date_empty = 'Please select Pickup date';
  static const String comments_empty = 'Please enter comments';
  static const String firstname_empty = 'Please enter first name';
  static const String lastname_empty = 'Please enter last name';
  static const String mobile_empty = 'Please enter mobile number';
  static const String promocode_empty = 'Please enter promo code';
  static const String promocode_already_apply = 'Promo code already applied';
  static const String cpassword_empty = 'Please enter confirm password';

  static const String login = 'Log In\n';
  static const String cancel_reason = 'Reason for cancel Ride';

  static const String login_email = 'EMAIL';
  static const String login_password = 'PASSWORD';
  static const String login_remember = 'Remember me';
  static const String login_Forgot_Password = 'Forgot Password';
  static const String login_text = 'LOGIN';
  static const String fblogin_text = 'Connect with Facebook'; //.........
  static const String googlelogin_text = 'Connect with Google'; //.....
  static const String confirm_text = 'CONFIRM';
  static const String BOOK_NOW_text = 'BOOK NOW'; //......................
  static const String book_now =
      'BOOK NOW'; //'Confirm JuberGo';  //......................
  static const String Confirm_JuberGo =
      'Confirm Ride'; //'Confirm JuberGo';  //......................
  static const String rate_per_km = 'Rate per km: ';
  static const String DONE = 'DONE'; //......................
  static const String confirm_destination_text = 'CONFIRM DESTINATION';
  static const String or_connect_using_social_acc =
      'Or connect using social account';
  static const String login_register_now = 'Sign Up'; //.
  static const String signup = 'SIGN UP'; //....

  static const String register = 'REGISTER\n';
  static const String register_continue = 'to continue\n';
  static const String register_firstname = 'FIRST NAME';
  static const String register_lastname = 'LAST NAME';
  static const String register_email = 'EMAIL ID';
  static const String profiler_email = 'EMAIL';
  static const String register_mobile_number = 'MOBILE NUMBER';
  static const String register_phone_number = 'PHONE NUMBER';
  static const String register_confirm_password = 'CONFIRM PASSWORD';
  static const String register_text = 'REGISTER';
  static const String register_agree1 =
      'By clicking "Sign Up" you agree to our ';
  static const String register_agree2 = 'terms and conditions';
  static const String register_agree3 = ' as well as our ';
  static const String register_agree4 = 'privacy policy';
  static const String register_member = 'Are You a Member?';
  static const String register_login = 'Log in';

  static const String Cancel_dialog = 'Cancel Dialog\n\n';
  static const String Cancel_dialog_msg =
      'Are you sure want to cancel this ride?';
  static const String Cancel_reson_validation_msg =
      'Please enter Cancel reason for cancel the ride';
  static const String forgot_password = 'Forgot Password\n\n';
  static const String forgot_title =
      'Enter your email below to receive your password reset instruction';
  static const String forgot_send_password = 'SEND PASSWORD';
  static const String cancel_job = 'CANCEL RIDE';

  static const String My_Request = 'My Request';
  static const String yourtrips = 'My Rides';
  static const String completed_rides = 'Completed Rides';

//   static const String yourtrips = 'Your Trips';
  static const String POST_A_JOB = 'Book Ride';

//   static const String POST_A_JOB = 'Post a Job';
  static const String MY_PROFILE = 'My Profile';
  static const String MY_RATING = 'My Rating';
  static const String MY_WALLET = 'My Wallet';
  static const String NOTIFICATION = 'Notification';
  static const String ABOUT_US = 'About Us';
  static const String CONTACT_US = 'Contact Us';
  static const String LOGOUT = 'Logout';

  static const String My_Request_OPEN = 'OPEN';
  static const String My_Request_IN_PROGRESS = 'IN PROGRESS';
  static const String UPCOMIN = 'UPCOMING';
  static const String My_Request_COMPLETED = 'COMPLETED';
  static const String My_Request_CANCEL = 'CANCELED';

  static const String Araba = 'Tirth Travels';
  static const String sure_logout = 'Are you sure you want to Logout?';

  static const String CANCEL = 'CANCEL';
  static const String CONTACT = 'CONTACT';
  static const String Total = 'Total: ';
  static const String To = 'To: ';
  static const String From = 'From: ';
  static const String DISTANCE = 'DISTANCE';
  static const String KM = 'km';
  static const String DRIVER_NAME = 'DRIVER NAME:';
  static const String TRANSPORT_NAME = 'TRANSPORT NAME';
  static const String Job_Name = 'RIDE NAME';
  static const String JOB_NAME = 'RIDE NAME';

//    static const String JOB_NAME = 'DRIVER NAME';
  static const String VEHICLE_TYPE = 'VEHICLE TYPE';
  static const String Types_of_vehicle_required = 'Types of vehicle required';
  static const String LOCATION = 'LOCATION';
  static const String SELECT_ON_MAP = 'SELECT ON MAP';
  static const String Source = 'Source';
  static const String where_to = 'Where to?'; //......
  static const String Now = 'Now'; //......
  static const String half_hour_later = 'Latter'; //......
  static const String cash_payment = 'Payment'; //......
//   static const String cash_payment = 'Cash Payment'; //......
  static String Promo = 'Coupon'; //'Promo'; //......
//   static const String Add_payment = 'Add Payment'; //......
  static const String Trip_Detail = 'Trip Details'; //......
  static const String where_to_2ed_line =
      'Book on demand or pre-scheduled rides'; //......
  static const String Destination = 'Destination';
  static const String PICKUP_DATE = 'PICKUP DATE';
  static const String Start_with_date = 'Start with date';
  static const String COMMENTS_TO_TRANSPORTER = 'COMMENTS TO TRANSPORTER';
  static const String Description_of_load_truck_type =
      'Description of load truck type';
  static const String NEXT = 'NEXT';
  static const String Payment_Type = 'Payment Type: ';
  static const String accept_all = 'I accept all the ';
  static const String tcs = 't&cs';
  static const String PAY_NOW = 'PAY NOW';
  static const String LEAD_ID = 'LEAD ID';
  static const String Error = 'Error';
  static const String ERROR = 'ERROR';

  static const String Select_Address = 'Select Address';
  static const String Enjoying_Service = 'Enjoying Service\n';
  static const String Tap_a_star = 'Tap a star to rate it on.';
  static const String SUBMIT = 'SUBMIT';
  static const String RETRY = 'RETRY';
  static const String TRACK_THE_JOB = 'TRACK THE CAB';

  static const String About_Us = 'About Us';
  static const String ABOUT_ARABA = 'About Tirth Travels';
  static const String Contact_Us = 'Contact Us';
  static const String CONTACT_ARABA = 'Contact Tirth Travels';
  static const String compare_paswd_error_msg =
      'Please Check Password or Confirm Passwords are same';
  static const String Select_Vehicle = 'Select Vehicle';
  static const String UPDATE_PROFILE = 'UPDATE PROFILE';
  static const String Address = 'ADDRESS';
  static const String Change_your_password = 'Change your password';
  static const String no_record_found = 'No record found';
  static const String choose_payment = 'Please choose payment type';

  static const String create_post = 'Your Ride has been posted.';
  static const String profile_update = 'Profile update successfully';
  static const String rating_msg = 'Thank you for your rating';

  static const String Track_transportation = 'TRACK YOUR CAB ';

  static const String privacy_policy_empty = 'Please Accept Privacy Policy';
  static const String Name = 'Name';
  static const String Phone = 'Phone';
  static const String job_complete_msg = 'Ride has been completed successfully';

  //........................
  static const String email_invalid = 'Please enter valid email';
  static const String password_lbl = 'PASSWORD';
  static const String TRACKIT_NOW = 'FOLLOW THE CARRIER';
  static const String RATE = 'RATE';
  static const String pending_rating = 'ALREADY RATED';
  static const String SELECT_DATE = 'select a date';
  static const String Reference = 'Reference ';
  static const String Locate = 'Locate ';

  static const String ontheway = 'ON THE WAY';
  static const String paymentpending = 'PENDING PAYMENT';
  static const String driverarrived = 'DRIVER ARRIVED';
  static const String inprogress = 'SHIPPING IN PROGRESS';
  static const String pending = 'PENDING';
  static const String confirmed = 'CONFIRMED';

  static const String JobAccepted_NOTI = 'Ride Accepted';
  static const String JobCompleted_NOTI = 'Ride Completed';
  static const String DriverArrived_NOTI = 'Driver Arrived';
  static const String JobStarted_NOTI = 'Ride Started';
  static const String JobCancelled_NOTI = 'Ride Cancelled';
  static const String DriverAssigned_NOTI = 'Driver Assigned';
  static const String PaymentRequest_NOTI = 'Payment Request';
  static const String Driverontheway_NOTI = 'Driver on the way';
  static const String PaymentCompleted_NOTI = 'Payment Completed';
  static const String SuccessfullPayment_NOTI = 'Successfull Payment';
  static String time_validation = "Please select the pickup time";
  static String past_time_validation =
      "You cannot set the time before the current time";

  static const String payment_type1 = 'Advance';
  static const String payment_type2 = 'At the delivery';
  static const String Payement_NONE = 'Payment';
  static const String Payement_lbl = "PAYMENT WILL BE MADE";
  static const String decription_hint =
      "Description of the goods to be transported";
  static const String payment_validation = "Please choose the payment method";

  static const String Latefee = "Late fee: ";
  static const String Price = "Price : ";
  static const String Minuteslate = "Minutes late: ";
  static const String passwd_validaion12 =
      "Please enter a password of up to 12 digits";
  static const String passwd_validaion6 =
      "Please enter a password of at least 6 digits";

  static const String No_notification_received = "No notification received";
  static const String No_workcompleted = "No data available";
  static const String No_offer_available = "No ratings available";
  static const String No_work_in_progress = "No data available";
  static const String No_rating_yet = "No rating yet";
  static const String NO_DELIVERY_COMPLETED = "No Ride completed";
  static const String NO_DELIVERY_CANCELED = "No ride cancelled";
  static const String NO_DELIVERY_IN_PROGRESS = "No ride available";
  static const String NO_NOTIFICATION = "NO NOTIFICATION";
  static const String cancel_regitration_confirmation_msg =
      "If you go back, the registration process will be canceled. Do you want to continue?";
  static const String ok = "Okay";
  static const String NO = "NO";
  static const String YES = "YES";
  static const String confirm_your_code = "confirm your code";
  static const String Resend_new_code = "Resend OTP ";
  static const String confirm_access_code = "Verify OTP";
  static const String Please_enter_code = "Please enter code";
  static const String Check_mobile_number = "Check mobile number";
  static const String enter_mobile_number = "Enter the mobile number";
  static const String send_the_access_code = "Send OTP";

  static const String Receipt = "Receipt"; //...........



}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:toast/toast.dart';

import 'package:location/location.dart';
import 'package:flutter/services.dart';


class ReportNeedyPage extends StatefulWidget {
  static const routeName = '/ReportNeedyPage';

  @override
  ReportNeedyPageState createState() => ReportNeedyPageState();
}

class ReportNeedyPageState extends State<ReportNeedyPage> {
  String _address;
  var geolocator = Geolocator();
  Position position;
  static final CameraPosition _kInitialPosition = const CameraPosition(
    target: LatLng(-33.852, 151.211),
    zoom: 11.0,
  );
  CameraPosition _position = _kInitialPosition;
  GoogleMapController mapController;
  double current_lat = null;
  double current_long = null;

  @override
  void initState() {
    super.initState();
    getlocation();
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.pinkAccent,
        title: new Text('Report Needy'),
        centerTitle: true,
      ),

      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            child:
            GoogleMap(
              onMapCreated: _onMapCreated,
              mapType:  MapType.normal,
              myLocationEnabled: true,
              initialCameraPosition: CameraPosition(
                target: LatLng(-33.852, 151.211),
                zoom: 11.0,
              ),
              onCameraMove: ((pinPosition) {
                _position = pinPosition;
              }),
              onCameraIdle : _getAddress,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Align(
              alignment: Alignment.bottomCenter,
              child:   new RaisedButton(
                onPressed:() {
                  double _lati = _position.target.latitude;
                  double _longi = _position.target.longitude;
//                  Navigator.push(context, MaterialPageRoute(builder: (context) => AddNeedyDetail(lati : _lati,
//                      longi : _longi,
//                      Address :_address)));

                  showToast(_address, gravity: Toast.TOP);


                },


//                onPressed: _getAddress,
                textColor: Colors.white,
                color: Colors.pink,
                padding: const EdgeInsets.fromLTRB(100.0,5.0,100.0,5.0),
                child: new Text(
                  "Next",
                ),
              ),
            ),
          ),
          Center(
            child: Image.asset(
              'assets/images/center_pin.png'
              ,height: 60,
              width: 60,
            ),
          )
        ],

      ),

    );


  }

  void _onMapCreated(GoogleMapController controller) {

    mapController = controller;
    moveToCurrentLocation();
    _extractMapInfo();
    setState(() {_onMapChanged();});

  }


  void _onMapChanged() {
    setState(() {
      _extractMapInfo();
    });
  }

  void _extractMapInfo() {
//    _position = mapController.cameraPosition;
//    print(_position.target.longitude);
//    _isMoving = mapController.isCameraMoving;
  }

  Future _getAddress() async {
    List<Placemark> placemark = await Geolocator().placemarkFromCoordinates(_position.target.latitude,_position.target.longitude);
     print(placemark.elementAt(0).locality);
    print( _position.target.latitude);
    final coordinates = new Coordinates(_position.target.latitude,_position.target.longitude);

    print(coordinates.toString()+ "kkkkkkkkkkkkkkkkkkkk");

    getAddressFromLatlong(coordinates);
//     var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
//    _address = addresses.first.addressLine;
//    showToast(_address, gravity: Toast.TOP);
//    print( _address);
  }

  Future getAddressFromLatlong(Coordinates coordinates) async {
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    _address = addresses.first.addressLine;
    showToast(_address, gravity: Toast.TOP);
    print( _address);
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  @override
  void dispose() {
//    _getPositionSubscription.cancel();
//    geolocator.getPositionStream(null).listen(null);
    super.dispose();
  }

  Future getlocation() async {

    var location = new Location();

    try {
      LocationData currentLocation  = (await location.getLocation()) ;
      print(currentLocation.toString() +'jjjjjjjjjjjj');

      current_lat= currentLocation.latitude;
      current_long= currentLocation.longitude;

      moveToCurrentLocation();

    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        print(e.message+' Permission denied'+'kkkkkkkkkkkkkk');
      }
    }
  }

  void moveToCurrentLocation() {
    if (mapController != null && current_long != null && current_lat != null) {
      mapController.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(current_lat, current_long), zoom: 20.0)
      ));
    }
  }
}



























//@override
//Widget build(BuildContext context) {
//
//  return new Scaffold(
//    appBar: new AppBar(
//      backgroundColor: Colors.pinkAccent,
//      title: new Text('Report Needy'),
//      centerTitle: true,
//    ),
//
//    body: new Container(
//      child: Padding(
//        padding: const EdgeInsets.symmetric(vertical: 32.0, horizontal: 16.0),
//        child: Column(
//          children: <Widget>[
//          ],
//        ),
//      ) /* add child content here */,
//    ),
//  );
//}


//  Future<Stream<Position>> geolocator_getposition() async {
//    _getPositionSubscription = await geolocator.getPositionStream(locationOptions);
//    _getPositionSubscription.listen( (Position position) {
//       this.position=position;
//       print(position == null ? 'Unknown' : position.latitude.toString() + ', ' + position.longitude.toString());
//       mapController.animateCamera(
//         CameraUpdate.newCameraPosition(
//           CameraPosition(
//               target: LatLng(position.latitude,position.longitude), zoom: 20.0),
//         ),
//       );
//    });
//  }
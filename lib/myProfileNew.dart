import 'dart:convert';
//import 'dart:html';

import 'package:arabacustomer/PostJob1New.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SideMenu.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

import 'Utils/Api_constant.dart';
import 'Utils/Consts.dart';
import 'Utils/MyPreferenceManager.dart';
import 'model/ProfileResponse.dart';
import 'myProfileEditNew.dart';

class myProfileNew extends StatefulWidget {
  static const routeName = '/myProfileNew';
  @override
  myProfileNewState createState() {
    return myProfileNewState();
  }
}

class myProfileNewState extends State<myProfileNew> {

  bool IsProgressIndicatorShow = false;
  ProfileResponse profileResponseObj = null ;
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController addressController = TextEditingController();
  final TextEditingController changepswdController = TextEditingController();
  String name= '';
  String imagePath= '';

  @override
  void initState() {
    super.initState();
    getProfileApiCall();
  }

  Future<bool> _onWillPop() async {
    print("_onWillPop===========" );
    Consts.SideMenuCurrentlySelectedPosition = 1;
    Navigator.of(context)
        .pushNamedAndRemoveUntil(
        PostJob1New.routeName, (Route<dynamic> route) => false, arguments: 0);

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(

          drawer: MainDrawer(context),
          body: Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                child: ListView(
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: (){
                              print("_onWillPop===========" );
                              Consts.SideMenuCurrentlySelectedPosition = 1;
                              Navigator.of(context)
                                  .pushNamedAndRemoveUntil(
                                  PostJob1New.routeName, (Route<dynamic> route) => false, arguments: 0);
                            },
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                              child: Icon(
                                Icons.close,
                                color: Colors.black,
                                size: 20.0,
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: (){
                              goToEditProfile();
                            },
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              child: Icon(
                                Icons.edit,
                                color: Colors.black,
                                size: 20.0,
                              ),
                            ),
                          )
                        ],
                      )
                    ),
                    SizedBox(height: 35.0),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          name,
                          textAlign: TextAlign.left,
                          style: TextStyle(fontSize: 26, fontWeight: FontWeight.w700, color: Colors.black87),
                        ),

                        Align(alignment : Alignment.center,
                          child: Container(
                            width: 65.0,
                            height: 65.0,
                              decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: getProfileImage(),
                                  ))),
                          ),
                      ],
                    ),

                    SizedBox(height: 20.0),
                    CommonWidget.plblText( Api_constant.profiler_email),
                    TextField(
                      controller: emailController,
                      readOnly: true,
                      style: CommonWidget.TFCommnTextStyle(),
                      decoration: CommonWidget.PETInputDecoration(),
                    ),
                    SizedBox(height: 25.0),
                    CommonWidget.plblText( Api_constant.register_phone_number),
                    TextField(
                      controller: phoneNumberController,
                      readOnly: true,
                      style: CommonWidget.TFCommnTextStyle(),
                      decoration: CommonWidget.PETInputDecoration(),
                    ),
//                    SizedBox(height: 20.0),
//                    CommonWidget.plblText( Api_constant.Address),
//                    TextField(
//                      controller: addressController,
//                      readOnly: true,
//                      style: CommonWidget.TFCommnTextStyle(),
//                      decoration: CommonWidget.PETInputDecoration(),
//                    ),

                    SizedBox(height: 15.0)
                  ],
                ),
              ),
              Align(alignment : Alignment.bottomCenter,
                child:  Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                  child: CommonWidget.submitButtonBottomLine(),
                ),
              ),
              Center(child:  Visibility(
                visible: IsProgressIndicatorShow,
                child: SizedBox(
                  child: CircularProgressIndicator(),
                ),
              ),
              ),
//            )
            ],
          ),
        ),
        onWillPop: _onWillPop);
  }

  Future<void> getProfileApiCall() async {

    MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
    String id =  _myPreferenceManager.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);

    print("id===="+id);

    setState(() {
      IsProgressIndicatorShow = true;
    });

    final uri = Uri.encodeFull(Api_constant.get_profile_customer_api +
        '?id=${id}');

    var response = await http.get(uri);

    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str +"..............");
      Map userMap = jsonDecode(response_json_str);
      var data = userMap["data"];
      profileResponseObj = new ProfileResponse.fromJsonMap(data) as ProfileResponse;

      updateData();
    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  void goToEditProfile() async{
    // Navigator.pushNamed(context, MapAndSearchbarWithPlaceApi.routeName);
    await Navigator.of(context).pushNamed(myProfileEditNew.routeName, arguments: profileResponseObj);

//    await Navigator.push(context, MaterialPageRoute(builder: (context) => MyProfileEdit(profileResponseObj: args as ProfileResponse)));
//    await Navigator.push(context, MaterialPageRoute(builder: (context) => MapAndSearchbarWithPlaceApi()));
    getProfileApiCall();
    setState(() {
//      profileResponseObj.first_name =Consts.tempSaveProfileFname;
//      profileResponseObj.last_name = Consts.tempSaveProfileLname;
//      profileResponseObj.email = Consts.tempSaveProfileEmail;
//      profileResponseObj.phone_number = Consts.tempSaveProfilePhoneNumber;
//      imagePath = Consts.tempSaveProfilePic;
//      updateData();
    });
  }

  void updateData() {
    setState(() {
      phoneNumberController.text = profileResponseObj.phone_number;
      emailController.text = profileResponseObj.email;
      addressController.text = profileResponseObj.address;
      imagePath = profileResponseObj.profile_pic;
      name = profileResponseObj.first_name+' '+profileResponseObj.last_name;
    });
  }


  ImageProvider getProfileImage(){
     if(imagePath.isNotEmpty){
      return  new NetworkImage(
          imagePath);
    }else{
      return AssetImage("assets/images/camera.png");
    }

  }
}
import 'dart:async';
import 'dart:convert';
//import 'dart:html';
import 'dart:math';

import 'package:arabacustomer/ProcessingTrackingDetail2.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/Utils/Consts.dart';
import 'package:arabacustomer/Utils/FadeIn.dart';
import 'package:arabacustomer/Utils/MyPreferenceManager.dart';
import 'package:arabacustomer/model/MyWalletResponseObj.dart';

import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/MyRequestTabsScreen.dart';
import 'package:arabacustomer/widgets/PayNowSubmitButtonRed.dart';
import 'package:arabacustomer/widgets/SubmitButtonRed.dart';
import 'package:arabacustomer/widgets/lib_dart_noti_center/src/dart_notification_center_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

import 'MyRequestCompleted.dart';
import 'RatingList.dart';
import 'dialog/CancelConfirmationDialog.dart';
import 'dialog/RatingDialog.dart';
import 'map_request.dart';
import 'model/CommanModalWithObj.dart';
import 'model/MyRequestListObj.dart';
import 'model/PassDataToTrackingScreen.dart';
import 'model/StatusMsgResponse.dart';

class MyRequestDetailNew2 extends StatefulWidget {
  static const routeName = '/MyRequestDetailNew2';

  MyRequestListObj myRequestListObj;

//  MyRequestDetail({Key key, this.myRequestListObj}) : super(key: key)

  String postid;


  MyRequestDetailNew2({Key key, this.postid}) : super(key: key);

  @override
  MyRequestDetailNewState createState() {
    return MyRequestDetailNewState();
  }
}

class MyRequestDetailNewState extends State<MyRequestDetailNew2> implements CallbackOfCanceldDialog ,
    CallbackOfRatinngdDialog {

  Completer<GoogleMapController> _mapController = Completer();

//  GoogleMapController _controller;
  GoogleMapsServices _googleMapsServices = GoogleMapsServices();
  final Set<Polyline> _polyLines = {};
  Set<Polyline> get polyLines => _polyLines;
  final Set<Marker> _markers = {};
  String driverAddress;
  bool isDoneOnlinePayment=false;
  bool isChangeWalletAmout=false;
  BitmapDescriptor SourcepinLocationIcon;
  BitmapDescriptor DestipinLocationIcon;
  int maxAmtUseFromWallet;
  int walletAmt;
  int customerEnteredWalletAmt = 0;
  String totalPaybleAmt="0";
  String countedTaxAmt="0";
  final CameraPosition _initialCamera = CameraPosition(
    target: LatLng(20.5937, 78.9629),
//    target: LatLng(28.0339, 1.6596),
    zoom: 14,
  );

  bool IsTrackingBtnShow(String status){
    if(status == null || status.isEmpty){
      return false;
    } else{
      if(status.toLowerCase()=='confirmed' || status.toLowerCase()=='in progress' || status.toLowerCase() == 'driver arrived' ||
          status.toLowerCase()=='on the way'){
        if(widget.myRequestListObj.go_for_payment=="1"){
          return false;
        }else{
          return true;
        }
//        status.toLowerCase()=='on the way'){

      } else{
        return false;
      }
    }
  }
  bool IsProgressIndicatorShow = false;

  bool IsPandingPaymentShow(String status){
    if(status != null &&
        status.isNotEmpty && status.toLowerCase() =='payment pending'){
      return true;
    } else{
      return false;
    }
  }

  bool IsExtraChargesShow(String status){
    if(status == null || status.isEmpty ){
      return false;
    } else if(status.toLowerCase() == Consts.paymentpendingStatusText || status.toLowerCase() == Consts.completedStatusText){
      return true;
    }else{
      return false;
    }
  }

  Future<bool> _onWillPop() async {
    print("_onWillPop===========" );

 /*   if(Consts.isAcceptedJob){
      Navigator.of(context).pushNamedAndRemoveUntil(MyRequestTabsScreen.routeName, (Route<dynamic> route) => false);
    }else{
      Navigator.of(context).pop();
    }*/

   // Navigator.of(context).pop();

    goToDetailScreen();
//    Navigator.of(context)
//        .pushNamedAndRemoveUntil(MyJobTabsScreen.routeName,
//            (Route<dynamic> route) => false);

    return true;
  }
  void goToDetailScreen() {
//    Toast.show(widget.myRequestListObj.status, context,
//        gravity: Toast.CENTER);

     int arg = 0;
      if(widget.myRequestListObj.status == null &&
          widget.myRequestListObj.status.isEmpty){
        int arg = 0;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);

      }else if(widget.myRequestListObj.status.toLowerCase() == Consts.pendingStatusText){
        arg = 0;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);

      }else if(widget.myRequestListObj.status.toLowerCase() == Consts.confirmedStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.onthewayStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.driverarrivedStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.paymentpendingStatusText){

        arg = 0;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);

      }
      else if(widget.myRequestListObj.status.toLowerCase() == Consts.completedStatusText){
        arg = 1;
        if(Consts.FromRatingScreen){
          Navigator.of(context).pushReplacementNamed(RatingList.routeName);
        } else{
//          Navigator.of(context).pushReplacementNamed(MyRequestCompleted.routeName, arguments: arg);
          Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);
        }

      } else if(widget.myRequestListObj.status.toLowerCase() == Consts.cancelledStatusText){
        arg = 2;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);
      }else if(widget.myRequestListObj.status.toLowerCase() == Consts.isAcceptedJob){
        arg = 0;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);

      }
      else{
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);
      }
  }
  int i = 3;

  @override
  void initState() {
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);

    getDetail(widget.postid);

    DartNotificationCenter.subscribe(
      channel: Consts.DetialChanel,
      observer: i,
      onNotification: (result) => {
        print('received:DetialChanel****** $result'),
        if(widget.postid != null && widget.postid.isNotEmpty){
          getDetail(widget.postid)
        }

//        setState(() {
////          new MyRequestDetail();
//        })
      },
    );
    DartNotificationCenter.subscribe(
      channel: Consts.GoForPayment,
      observer: i,
      onNotification: (result) => {
        if(widget.postid != null && widget.postid.isNotEmpty){
          getDetail(widget.postid),
          IsGoForPayment()
        }

//        setState(() {
////          new MyRequestDetail();
//        })
      },
    );

    super.initState();

    try {
      BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/red_circle.png')
          .then((onValue) {
        SourcepinLocationIcon = onValue;
      });

      BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/red_square.png')
          .then((onValue) {
        DestipinLocationIcon = onValue;
      });
    } catch (e) {
      print("pinLocationIcon===" + e.toString());
    }
  }
  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    onlinePaymentSuccessApi();
    print("Succeess payment gateway");
    Toast.show("SUCCESS: " + response.paymentId, context, gravity: Toast.TOP);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Toast.show("ERROR: " + response.code.toString() + " - " + response.message, context, gravity: Toast.TOP);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Toast.show("EXTERNAL_WALLET: " + response.walletName, context,
        gravity: Toast.TOP);
  }

  void sendRequest() async {
    _onAddMarkerButtonPressed();
    LatLng destination = LatLng(
        CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.delivery_lattitude),
        CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.delivery_longitude));
    LatLng origin = LatLng(
        CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.pickup_lattitude),
        CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.pickup_longitude));
    String route =
    await _googleMapsServices.getRouteCoordinates(origin, destination);
    print("route===" + route);
    createRoute(route);
    _addMarker(destination, widget.myRequestListObj.delivery_address);
  }

  void createRoute(String encondedPoly) {
    _polyLines.add(Polyline(
        polylineId: PolylineId("Track"),
        width: 4,
        points: _convertToLatLng(_decodePoly(encondedPoly)),
        color: Colors.red));

    setState(() {

    });
    // print("_polyLines"+_polyLines.toString());
  }
  List _decodePoly(String poly) {
    var list = poly.codeUnits;
    var lList = new List();
    int index = 0;
    int len = poly.length;
    int c = 0;
    do {
      var shift = 0;
      int result = 0;

      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      } while (c >= 32);
      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lList.add(result1);
    } while (index < len);

    for (var i = 2; i < lList.length; i++) lList[i] += lList[i - 2];

    print(lList.toString());

    return lList;
  }
  List<LatLng> _convertToLatLng(List points) {
    List<LatLng> result = <LatLng>[];
    for (int i = 0; i < points.length; i++) {
      if (i % 2 != 0) {
        result.add(LatLng(points[i - 1], points[i]));
      }
    }
    return result;
  }
  void _addMarker(LatLng location, String address) {
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId("112"),
          position: location,
          // infoWindow: InfoWindow(title: address, snippet: "go here"),
          infoWindow: InfoWindow(title: address),
          icon: DestipinLocationIcon));
//          icon: BitmapDescriptor.defaultMarker));

//      var bounds = getBounds(_markers);
//      CameraUpdate u2 = CameraUpdate.newLatLngBounds(bounds, 50);
//      _controller.animateCamera(u2).then((void v){
//        check(u2,this._controller);
//      });
    });
  }
//  void check(CameraUpdate u, GoogleMapController c) async {
//    c.animateCamera(u);
//    _controller.animateCamera(u);
//    LatLngBounds l1=await c.getVisibleRegion();
//    LatLngBounds l2=await c.getVisibleRegion();
//    print(l1.toString());
//    print(l2.toString());
//    if(l1.southwest.latitude==-90 ||l2.southwest.latitude==-90)
//      check(u, c);
//  }

  LatLngBounds getBounds(Set<Marker> markers) {

    var lngs = markers.map<double>((m) => m.position.longitude).toList();
    var lats = markers.map<double>((m) => m.position.latitude).toList();

    double topMost = lngs.reduce(max);
    double leftMost = lats.reduce(min);
    double rightMost = lats.reduce(max);
    double bottomMost = lngs.reduce(min);

    LatLngBounds bounds = LatLngBounds(
      northeast: LatLng(rightMost, topMost),
      southwest: LatLng(leftMost, bottomMost),
    );

    return bounds;
  }
  void _onAddMarkerButtonPressed() {
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId("111"),
          position: LatLng(double.parse(Consts.pickup_lattitude), double.parse(Consts.pickup_longitude)),
          // infoWindow: InfoWindow(title: address, snippet: "go here"),
          infoWindow: InfoWindow(title: Consts.sourceAdd),
          icon: SourcepinLocationIcon
      ));
    });
  }
  @override
  void dispose() {
    DartNotificationCenter.unsubscribe(observer: i, channel: Consts.DetialChanel);
    DartNotificationCenter.unsubscribe(observer: i, channel: Consts.GoForPayment);
    print('received:dispose****** ');
    super.dispose();
    _razorpay.clear();
  }


  @override
  Widget build(BuildContext context) {
//    Toast.show(widget.myRequestListObj.toString(), context,
//        gravity: Toast.CENTER);
    print('received:build****');

    return WillPopScope(
      child: SafeArea(
        child: Scaffold(
            body:  IsProgressIndicatorShow ? SizedBox(
              height: double.infinity,
              width: double.infinity,
              child: Center(
                child:  CircularProgressIndicator(),
              ),
            ) :
            Container(
              color: Colors.blueGrey[600],
              height: double.infinity,
                 child:  SingleChildScrollView(
                   child:  Container(
//                    height: double.infinity,
                     decoration: BoxDecoration(
                       color: Colors.blueGrey[600],
                     ),
                     child: Padding(
                       padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                       child: Column(
//                        alignment:  AlignmentDirectional.bottomCenter,
                         children: [
                           FadeIn(1.0, Row(
                             children: [
                               InkWell(
                                 child: Padding(
                                   padding: EdgeInsets.fromLTRB(0,10, 15, 10),
                                   child: Icon(
                                     Icons.close,
                                     color: Colors.white,
                                     size: 22.0,
                                   ),
                                 ),
                                 onTap: (){
                                   _onWillPop();
                                 },
                               ),
                               Expanded(
                                 child: Padding(
                                   padding: EdgeInsets.fromLTRB(0,10, 30, 10),
                                   child: Align(
                                     alignment: Alignment.topCenter,
                                     child: Text(
                                       widget.myRequestListObj.status, style: TextStyle( fontSize: 15, color: Colors.white, fontWeight: FontWeight.w600),
                                     ),
                                   ),
                                 ),
                               )

                             ],
                           ),),


                           SizedBox(
                             height: 20,
                           ),
                           Container(
                             decoration: BoxDecoration(
                               color: Colors.white,
                               borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0),
                                   bottomRight: Radius.circular(10.0), bottomLeft: Radius.circular(10.0)),
                             ),
                             child: Padding(
                               padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                               child: Wrap(
//                            mainAxisAlignment: MainAxisAlignment.start,
//                            crossAxisAlignment: CrossAxisAlignment.start,
                                 children: [
                                   FadeIn(
                                     2,
                                     Padding(
                                       padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                                       child: Column(
                                         mainAxisAlignment: MainAxisAlignment.start,
                                         crossAxisAlignment: CrossAxisAlignment.start,
                                         children: [
                                           Visibility(
                                             child: Column(
                                               mainAxisAlignment: MainAxisAlignment.start,
                                               crossAxisAlignment: CrossAxisAlignment.start,
                                               children: [
                                                 CommonWidget.leadidDarkRedcolorText(widget.myRequestListObj.job_number),

                                                 Visibility(
                                                     visible: CommonWidget.forCancelReturnHide(widget.myRequestListObj.status),
                                                      child: Padding(
                                                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                     child: Text("Driver Name : "+ widget.myRequestListObj.driver_name,
                                                       style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.black87),
                                                     ),
                                                   ),
                                                 ),
                                                 SizedBox(
                                                   height: 2,
                                                 ),
                                                 Visibility(
                                                   visible: CommonWidget.forCancelReturnHide(widget.myRequestListObj.status),
                                                   child: Padding(
                                                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                     child: InkWell(
                                                       onTap: () {
                                                         launch("tel://" +
                                                             widget.myRequestListObj.driver_phone_number);
                                                       },
                                                       child: Text(
                                                         "Phone Number : "+widget.myRequestListObj.driver_phone_number,
//                            textAlign: TextAlign.left,
                                                         style: TextStyle(
                                                             fontSize: 14,
                                                             fontWeight: FontWeight.w400,
                                                             color: Colors.deepOrangeAccent),
                                                       ),
                                                     ),
                                                   ),
                                                 ),
                                                 SizedBox(
                                                   height: 2,
                                                 ),
                                                 Visibility(
                                                   visible: CommonWidget.forCancelReturnHide(widget.myRequestListObj.status),
                                                   child: Padding(
                                                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                     child: RatingBarIndicator(
                                                       rating: CommonWidget.getDoubleFromStrForRating(widget.myRequestListObj.transporter_ratings),
                                                       direction: Axis.horizontal,
                                                       unratedColor: Colors.grey.withAlpha(95),
                                                       itemCount: 5,
                                                       itemSize: 18.0,
                                                       itemPadding:
                                                       EdgeInsets.symmetric(horizontal: 1.2),
                                                       itemBuilder: (context, _) => Icon(
                                                         Icons.star,
                                                         color: Colors.orangeAccent,
                                                       ),
                                                     ),
                                                   ),
                                                 ),

                                                 SizedBox(
                                                   height: 2,
                                                 ),
                                               ],
                                             ),
                                             visible: IsDisplayDriverinfo(widget.myRequestListObj.JobListType),
                                           ),
                                           Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                             children: <Widget>[
                                               Visibility(
                                                 child: Text(CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.vrn)+' - ',
                                                   style: TextStyle(
                                                       fontWeight: FontWeight.w600, color: Colors.black54, fontSize: 14
                                                   ),),
                                                 visible: CommonWidget.forEmptyReturnHide(widget.myRequestListObj.vrn)
                                               ),
                                               SizedBox(height: 4.0),
                                               Text("Cab Type : "+CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.vehicle_category),
                                                 style: TextStyle(
                                                     fontWeight: FontWeight.w600, color: Colors.black38, fontSize: 14
                                                 ),)
                                             ],
                                           ),
                                           Visibility(
                                             //visible:  CommonWidget.ISCompletedStatus(_status),
                                             child: Column(
                                               children: <Widget>[
                                                 SizedBox(height: 2),
                                                 Text("Start Time : "+widget.myRequestListObj.job_assigned_date,
                                                   style: TextStyle(
                                                       fontWeight:
                                                       FontWeight.w400,
                                                       color:
                                                       Colors.black87,
                                                       fontSize: 13),
                                                 ),
                                               ],
                                             ),
                                           ),
                                           Visibility(
                                             visible:  CommonWidget.ISCompletedStatus(widget.myRequestListObj.status),
                                             child: Column(
                                               children: <Widget>[
                                                 SizedBox(height: 2),
                                                 Text("End Time : "+widget.myRequestListObj.job_completed_date,
                                                   style: TextStyle(
                                                       fontWeight:
                                                       FontWeight.w400,
                                                       color:
                                                       Colors.black87,
                                                       fontSize: 13),
                                                 ),
                                               ],
                                             ),
                                           ),
                                           Visibility(
                                             visible:  CommonWidget.ISCompletedStatus(widget.myRequestListObj.status),
                                             child: Column(
                                               children: <Widget>[
                                                 SizedBox(height: 2),
                                                 Text("Distance : "+widget.myRequestListObj.distance+" km",
                                                   style: TextStyle(
                                                       fontWeight:
                                                       FontWeight.w400,
                                                       color:
                                                       Colors.black87,
                                                       fontSize: 13),
                                                 ),
                                               ],
                                             ),
                                           ),
                                           SizedBox(height: 5.0),
                                         ],
                                       ),
                                     ),
                                   ),


                                   Visibility(
                                     child:  Image(
                                       image: AssetImage('assets/images/line_dotted.png'),
//                            height: size,
                                       fit: BoxFit.fitWidth,
//                            width: MediaQuery.of(context).size.width * 0.95,
                                     ),
                                     visible: CommonWidget.ISPriceDisplay(widget.myRequestListObj.status,
                                         widget.myRequestListObj.corporate_id),
                                   ),

                                   Visibility(
                                     child: FadeIn(3, Padding(
                                       padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                                       child: Column(
                                         children: [
                                           //SizedBox(height: 10.0),

                                           Center(
                                             child: Text(CommonWidget.replaceNullWithEmpty('FARE'),
                                               style: TextStyle(
                                                   fontWeight: FontWeight.w600, color: Colors.black38, fontSize: 13
                                               ),),
                                           ),
                                           SizedBox(height: 5.0),

                                           Center(
                                             child:  Text(CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.price),
                                               style: TextStyle(
                                                   fontWeight: FontWeight.w900, color: Theme.of(context).primaryColor, fontSize: 28
                                               ),),
                                           ),
                                           Center(
                                             child: Text(CommonWidget.replaceNullWithEmpty('incl. Tax'),
                                               style: TextStyle(
                                                   fontWeight: FontWeight.w400, color: Colors.black38, fontSize: 12
                                               ),),
                                           ),

                                           SizedBox(height: 10.0),
                                           Container(
                                             height: 0.3,
                                             color: Colors.black26,
                                             width: MediaQuery.of(context).size.width * 0.90,
                                           ),
                                           SizedBox(height: 25.0),
                                         ],
                                       ),
                                     ),),
                                     visible: CommonWidget.ISPriceDisplay( widget.myRequestListObj.status,
                                         widget.myRequestListObj.corporate_id),
                                   ),
                                   FadeIn(4,
                                     Column(
                                       children: [
                                         Row(
                                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                           crossAxisAlignment: CrossAxisAlignment.start,
                                           children: [
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                                               child: CommonWidget.getIconImgeWithCustomSize(
                                                   'assets/images/black_circle.png', 15),
                                             ),
                                             SizedBox(
                                               width:  MediaQuery.of(context).size.width * 0.8,
                                               child: ListTile(
                                                 title: Column(
                                                   mainAxisAlignment: MainAxisAlignment.start,
                                                   crossAxisAlignment: CrossAxisAlignment.start,
                                                   children: [
                                                     Text(
                                                       'PICK UP ',
                                                       style: TextStyle(
                                                           fontWeight: FontWeight.w400, color: Theme.of(context).primaryColor, fontSize: 11
                                                       ),
                                                     ),
                                                     Text(
                                                       widget.myRequestListObj.pickup_address,
                                                       style:  TextStyle(
                                                           fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                       ),
                                                     ),
                                                   ],
                                                 ),
//
                                               ),
                                             )
                                           ],
                                         ),
                                         SizedBox(height: 5.0),

                                         Row(
                                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                           crossAxisAlignment: CrossAxisAlignment.start,
                                           children: [
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                                               child: CommonWidget.getIconImgeWithCustomSize(
                                                   'assets/images/red_square.png', 15),
                                             ),
                                             SizedBox(
                                               width:  MediaQuery.of(context).size.width * 0.8,
                                               child: ListTile(
                                                 title: Column(
                                                   mainAxisAlignment: MainAxisAlignment.start,
                                                   crossAxisAlignment: CrossAxisAlignment.start,
                                                   children: [
                                                     Text(
                                                       'DESTINATION',
                                                       style: TextStyle(
                                                           fontWeight: FontWeight.w400, color: Theme.of(context).primaryColor, fontSize: 11
                                                       ),
                                                     ),
                                                     Text(
                                                       widget.myRequestListObj.delivery_address,
                                                       style:  TextStyle(
                                                           fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                       ),
                                                     ),
                                                   ],
                                                 ),
//
                                               ),
                                             )
                                           ],
                                         ),
                                         SizedBox(height: 20.0),
                                       ],
                                     ),),

                                   Visibility(
                                     visible: IsTrackingBtnShow(widget.myRequestListObj.status),
                                     child: Column(
                                       children: <Widget>[
                                         SizedBox(height: 20.0),
                                         Padding(
                                           padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                           child: SubmitButtonRed(
                                               Api_constant.TRACK_THE_JOB, onChanged, 10),
                                         ),
                                       ],
                                     ),
                                   ),

                                   Visibility(
                                     visible: IsRateDispllay(),
                                     child: Column(
                                       children: <Widget>[
                                         SizedBox(height: 20.0),
                                     Padding(
                                       padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                     child:   SubmitButtonRed(
                                         Api_constant.RATE, onRate, 10),),

//                              /           SizedBox(height: 15.0),
//                                         Center(
//                                           child: CommonWidget.submitButtonBottomLine(),
//                                         )
                                       ],
                                     ),
                                   ),

                               SizedBox(width:30),
                                   Visibility(
                                     visible: IsGoForPayment(),
                                    // visible: IsTrackingBtnShow(widget.myRequestListObj.status),
                                     child: Column(
                                       mainAxisAlignment:
                                       MainAxisAlignment.start,
                                       crossAxisAlignment:
                                       CrossAxisAlignment.start,
                                       children: <Widget>[
                                         Row(
                                             children: <Widget> [
                                               Expanded(
                                                 flex: 1,
                                                 child: Column(
                                                     crossAxisAlignment: CrossAxisAlignment.start,
                                                     children: <Widget>[
                                                       Container(
                                                         child: Padding(
                                                           padding: EdgeInsets.fromLTRB(20, 10, 15, 0),
                                                           child: CommonWidget.lblText(
                                                               'Wallet Amount'),
                                                         ),
                                                       ),
                                                       Container(
                                                         child: Padding(
                                                           padding: EdgeInsets.fromLTRB(20, 10, 15, 0),
                                                           child: CommonWidget.blackBigText(
                                                               '₹'+walletAmt.toString()),
                                                         ),


                                                       ),
                                                     ]
                                                 ),
                                               ),
                                               SizedBox(width: 10.0),
                                               Expanded(
                                                 flex: 1,
                                                 child: Column(
                                                     crossAxisAlignment: CrossAxisAlignment.start,
                                                     children: <Widget>[
                                                       Container(
                                                         child: Padding(
                                                           padding: EdgeInsets.fromLTRB(10, 20, 5, 0),
                                                           child: CommonWidget.lblText(
                                                               'Enter Wallet Amount'),
                                                         ),
                                                       ),
                                                       Container(
                                                         child: Padding(
                                                           padding: EdgeInsets.fromLTRB(10,0,20, 0),
                                                           child: TextField(
                                                             keyboardType: TextInputType.number,
                                                             style: CommonWidget.TFCommnTextStyle(),
                                                             decoration: CommonWidget.ETInputDecoration(),
                                                             onChanged: (val) {
                                                               customerEnteredWalletAmt = int.parse(val);
                                                               //isChangeWalletAmout=true;
                                                               onwalletAmountChanged();
                                                             },
                                                           ),
                                                         ),

                                                       ),
                                                     ]
                                                 ),
                                               ),

                                             ]
                                         ),
                                        /* Visibility(
                                           visible: IsGoForPayment(),
                                           child: Column(
                                             children: <Widget>[
                                               SizedBox(height: 20.0),
                                               Padding(
                                                 padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                                 child:  Text(
                                                   "Total Amount :  "+'₹'+widget.myRequestListObj.budget_from,
                                                   style:  TextStyle(
                                                       fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                   ),
                                                 ),
                                               ),
                                               SizedBox(height: 5.0),
                                               Padding(
                                                 padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                 child:  Text(
                                                   "Wallet Amount :  "+'₹'+customerEnteredWalletAmt.toString(),
                                                   style:  TextStyle(
                                                       fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                   ),
                                                 ),
                                               ),
                                               SizedBox(height: 5.0),
                                               Padding(
                                                 padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                                 child:  Text(
                                                   "Total Amount Pay : "+'₹'+totalPaybleAmt.toString(),
                                                   style:  TextStyle(
                                                       fontWeight: FontWeight.w500, color: Colors.black, fontSize: 13
                                                   ),
                                                 ),
                                               ),
                                             ],
                                           ),
                                         ),*/
                               Visibility(
                                 visible: IsGoForPayment(),
                                   child:FadeIn(4,
                                     Column(
                                       children: [
                                         Row(
                                           /*mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                           crossAxisAlignment: CrossAxisAlignment.start,*/
                                           children: [
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                                               child: Text(
                                                 "max usable amount for this ride : ",
                                                 style:  TextStyle(
                                                     fontWeight: FontWeight.w400, color: Colors.red, fontSize: 12
                                                 ),
                                               ),
                                             ),
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(0, 10, 20, 0),
                                               child:  Text('₹'+maxAmtUseFromWallet.toString(),
                                                 style:  TextStyle(
                                                     fontWeight: FontWeight.w400, color: Colors.red, fontSize: 12
                                                 ),
                                               ),
                                             ),

                                           ],
                                         ),
                                         SizedBox(height: 10.0),
                                         Row(
                                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                           crossAxisAlignment: CrossAxisAlignment.start,
                                           children: [
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                               child: Text(
                                                 "Basic Amount :  ",
                                                 style:  TextStyle(
                                                     fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                 ),
                                               ),
                                             ),
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                               child:  Text('₹'+widget.myRequestListObj.amount_without_tax_string,
                                                 style:  TextStyle(
                                                     fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                 ),
                                               ),
                                             ),

                                           ],
                                         ),

                                         SizedBox(height: 5.0),
                                         Row(
                                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                           crossAxisAlignment: CrossAxisAlignment.start,
                                           children: [
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                               child: Text(
                                                 "Wallet Amount :  ",
                                                 style:  TextStyle(
                                                     fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                 ),
                                               ),
                                             ),
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                               child:  Text('-'+'₹'+customerEnteredWalletAmt.toString(),
                                                 style:  TextStyle(
                                                     fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                 ),
                                               ),
                                             ),
                                           ],
                                         ),
                                         SizedBox(height: 5.0),
                                         Row(
                                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                           crossAxisAlignment: CrossAxisAlignment.start,
                                           children: [
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                               child: Text(
                                                 "Tax Amount :  ",
                                                 style:  TextStyle(
                                                     fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                 ),
                                               ),
                                             ),
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                               child:  Text('₹'+countedTaxAmt.toString(),
                                                 style:  TextStyle(
                                                     fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 13
                                                 ),
                                               ),
                                             ),
                                           ],
                                         ),
                                         SizedBox(height: 5.0),
                                         Row(
                                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                           crossAxisAlignment: CrossAxisAlignment.start,
                                           children: [
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                               child: Text(
                                                 "Total Amount To Pay : ",
                                                 style:  TextStyle(
                                                     fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17
                                                 ),
                                               ),
                                             ),
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                               child:  Text('₹'+totalPaybleAmt.toString(),
                                                 style:  TextStyle(
                                                     fontWeight: FontWeight.w600, color: Colors.black, fontSize: 17
                                                 ),
                                               ),
                                             ),
                                             /*SizedBox(
                                               width:  MediaQuery.of(context).size.width * 0.3,
                                               child: ListTile(
                                                 title: Column(
                                                   mainAxisAlignment: MainAxisAlignment.start,
                                                   crossAxisAlignment: CrossAxisAlignment.start,
                                                   children: [
                                                     Text('₹'+totalPaybleAmt.toString(),
                                                       style:  TextStyle(
                                                           fontWeight: FontWeight.w400, color: Colors.black, fontSize: 13
                                                       ),
                                                     ),
                                                   ],
                                                 ),
//
                                               ),
                                             )*/
                                           ],
                                         ),
                                         SizedBox(height: 20.0),
                                       ],
                                     ),),
                               ),

                                       Column(
                                           children: <Widget>[
                                             SizedBox(height: 20.0),
                                             Padding(
                                               padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                               child: PayNowSubmitButtonRed(
                                                   'PAY NOW'+" ("+'₹'+totalPaybleAmt.toString()+")", onSubmitUserWalletAmt, 10),
                                             ),
                                           ],
                                         ),

                                       ],
                                     ),
                                   ),

                                   Visibility(
                                     visible: IsRatingBarDispllay(),
                                     child: Column(
                                       children: <Widget>[
                                         SizedBox(height: 20.0),
                                         Center(
                                           child: RatingBarIndicator(
                                             rating: CommonWidget.getDoubleFromStrForRating(widget.myRequestListObj.customer_ratings),
                                             direction: Axis.horizontal,
                                             unratedColor: Colors.grey.withAlpha(95),
                                             itemCount: 5,
                                             itemSize: 22.0,
                                             itemPadding:
                                             EdgeInsets.symmetric(horizontal: 1.2),
                                             itemBuilder: (context, _) => Icon(
                                               Icons.star,
                                               color: Colors.amber,
                                             ),
                                           ),
                                         ),
                                         SizedBox(height: 10.0),

                                       ],
                                     ),
                                   ),

                                   Visibility(
                                     child: FadeIn(5,
                                         Padding(
                                           padding: EdgeInsets.fromLTRB(25, 10, 25, 0),
                                           child: SubmitButtonRed("CANCEL RIDE", onCanceljob,2),
                                         )),
                                     visible: IsCancelShow(widget.myRequestListObj.status),
                                   )
                                 ],
                               ),
                             ),

                           ),
                           SizedBox(
                             height: 20,
                           ),
                           Align(
                             alignment: Alignment.bottomCenter,
                             child: Padding(
                               padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                               child: CommonWidget.submitButtonBottomLine(),
                             ),
                           )

                         ],
                       ),
                     ),
                   ),
                 ),
               )
        ),
      ),
      onWillPop: _onWillPop,

    );
  }

  String _radioValue; //Initial definition of radio button value
  String choice;
  String PaymentType='';
  Razorpay _razorpay;
  bool _isVisible = false ;
  void radioButtonChanges(String value) {
    setState(() {
      _radioValue = value;
      switch (value) {
        case 'one':
          PaymentType= '1';
          print(PaymentType + "..............");
          choice = value;
          break;
        case 'two':
          PaymentType= '0';
          print(PaymentType + "..............");
          choice = value;
          break;
//        case 'three':
//          choice = value;
//          break;
        default:
          choice = null;
      }
//      Toast.show(PaymentType, context,
//          gravity: Toast.CENTER);
      debugPrint(choice); //Debug the choice in console
    });
  }

  void onChanged() {
   // Toast.show("Track the job", context,gravity: Toast.CENTER);

   /* if(widget.myRequestListObj.status.toLowerCase()=='confirmed'){
      PassDataToTrackingScreen passDataToTrackingScreenObj = new PassDataToTrackingScreen();
      passDataToTrackingScreenObj.job_id = widget.myRequestListObj.job_id;
      passDataToTrackingScreenObj.pickup_address = driverAddress.toString();
      passDataToTrackingScreenObj.pickup_lattitude = widget.myRequestListObj.gprs_lat;
      passDataToTrackingScreenObj.pickup_longitude = widget.myRequestListObj.gprs_lng;
      passDataToTrackingScreenObj.delivery_address = widget.myRequestListObj.pickup_address;
      passDataToTrackingScreenObj.delivery_lattitude = widget.myRequestListObj.pickup_lattitude;
      passDataToTrackingScreenObj.delivery_longitude = widget.myRequestListObj.pickup_longitude;
      passDataToTrackingScreenObj.driver_phone_number = widget.myRequestListObj.driver_phone_number;
      passDataToTrackingScreenObj.IsFromSideMenu = false;
      Navigator.of(context).pushReplacementNamed(ProcessingTrackingDetail2.routeName, arguments: passDataToTrackingScreenObj);



    }else{
      PassDataToTrackingScreen passDataToTrackingScreenObj = new PassDataToTrackingScreen();
      passDataToTrackingScreenObj.job_id = widget.myRequestListObj.job_id;
      passDataToTrackingScreenObj.pickup_address = widget.myRequestListObj.pickup_address;
      passDataToTrackingScreenObj.pickup_lattitude = widget.myRequestListObj.pickup_lattitude;
      passDataToTrackingScreenObj.pickup_longitude = widget.myRequestListObj.pickup_longitude;
      passDataToTrackingScreenObj.delivery_address = widget.myRequestListObj.delivery_address;
      passDataToTrackingScreenObj.delivery_lattitude = widget.myRequestListObj.delivery_lattitude;
      passDataToTrackingScreenObj.delivery_longitude = widget.myRequestListObj.delivery_longitude;
      passDataToTrackingScreenObj.driver_phone_number = widget.myRequestListObj.driver_phone_number;
     *//* passDataToTrackingScreenObj.gprs_lat = widget.myRequestListObj.gprs_lat;
      passDataToTrackingScreenObj.gprs_lng = widget.myRequestListObj.gprs_lng;*//*
      passDataToTrackingScreenObj.IsFromSideMenu = false;
      Navigator.of(context).pushReplacementNamed(ProcessingTrackingDetail2.routeName, arguments: passDataToTrackingScreenObj);
    }*/
    Navigator.of(context).pushReplacementNamed(ProcessingTrackingDetail2.routeName,arguments: widget.myRequestListObj.job_id.toString());
  }

  IsDisplayDriverinfo(int jobListType) {
    if((CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.status)).toLowerCase()  == Consts.pendingStatusText ){
      return false;
    } else{
      return CommonWidget.forEmptyReturnHide(widget.myRequestListObj.driver_name);
    }
//    if(jobListType==Consts.JobListTypeInprogress || jobListType == Consts.JobListTypeComple){
//    } else{
//      return false;
//    }
  }

  Future<void> apiCAllPandingPaymentComplete() async {
    print('called' + "..............");

      var map = new Map<String, dynamic>();
      map[Api_constant.job_id] = widget.myRequestListObj.job_id.toString();
      map[Api_constant.online_payment] = PaymentType.toString();

      print(Api_constant.pp_complete_job_api + "..............");
      //cancel
   
      var response = await http.post(Api_constant.pp_complete_job_api, body: map);

      print(response.statusCode.toString() + "..............");
      print(response.body + "..............");
      if (response.statusCode == 200) {
        String response_json_str = response.body;
        print(response_json_str + "..............");
        Map userMap = jsonDecode(response_json_str);

        var loginResponseObj = new StatusMsgResponse.fromJsonMap(
            userMap) as StatusMsgResponse;

        if(loginResponseObj.status == 1) {
          Toast.show(Api_constant.job_complete_msg, context, gravity: Toast.CENTER);
          getDetail(widget.myRequestListObj.job_id.toString());

//          Navigator.of(context)
//              .pushNamedAndRemoveUntil(MyRequestTabsScreen.routeName, (Route<dynamic> route) => false);
        }else{
          var loginUserObj = loginResponseObj.message;
          Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
        }
      }
  }


  void onppCompleteJobApiCall() {
    if(PaymentType.isNotEmpty){

      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text(Api_constant.Araba),
            content: new Text("Do you want to proceed to payment?"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text(Api_constant.CANCEL),
                textColor: Theme.of(context).primaryColor,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),

              new FlatButton(
                child: new Text('CONTINUE'),
                textColor: Theme.of(context).primaryColor,
                onPressed: () async {

                  apiCAllPandingPaymentComplete();
                  Navigator.of(context).pop();

                },
              ),
            ],
          );
        },
      );

    } else{
      Toast.show(Api_constant.choose_payment, context, gravity: Toast.CENTER);
    }
  }

  Future<void> getDetail(String id) async {

    final uri = Uri.encodeFull(Api_constant.detailScreenApi + '?id=${id}' );
    print(uri.toString() + "...detail api...........");
    if (mounted) {
      setState(() {
        IsProgressIndicatorShow = true;
      });
    }

    var response = await http.get(uri);



    if (response.statusCode == 200) {
      if (mounted) {
        setState(() {
          IsProgressIndicatorShow = false;
        });
      }

      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var CommanModal_obj = new CommanModalWithObj.fromJson(userMap) as CommanModalWithObj;
      if (CommanModal_obj.status == 1) {

        Object obj = CommanModal_obj.data;
        MyRequestListObj g = new MyRequestListObj.fromJsonMap(obj);

        if(mounted){


        setState(() {
          widget.myRequestListObj = g;
          print("***===========" +g.toString());
          print('outside');
          myWalletApi(widget.myRequestListObj.job_id.toString());
         /* if(widget.myRequestListObj.go_for_payment=='1' && widget.myRequestListObj.payment_type==1){
            _showPriceDialog(widget.myRequestListObj.budget_from);
          }else if(widget.myRequestListObj.payment_type==2){
           // _showCalculationRelatedDialog(widget.myRequestListObj.budget_from);
          }*/
          getAddressFromLatlong(
              Coordinates(double.parse(widget.myRequestListObj.gprs_lat),
                  double.parse(widget.myRequestListObj.gprs_lng)),
              true);
          sendRequest();

          if(IsGoForPayment()){
            onwalletAmountChanged();
          }
         // calculateAmt(0);
//          setData();
        });
        }



      } else {
        Toast.show(Api_constant.no_record_found, context, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
    }
  }
  Future getAddressFromLatlong(Coordinates coordinates, bool hint) async {
    var addresses =
//    await Geocoder.google(Consts.googleMapApiKEY, language: 'fr').findAddressesFromCoordinates(coordinates);
    await Geocoder.local.findAddressesFromCoordinates(coordinates);
    Consts.sourceAdd = addresses.first.addressLine;
    Consts.pickup_lattitude = coordinates.latitude.toString();
    Consts.pickup_longitude = coordinates.longitude.toString();

    if (hint) {
      setState(() {
        driverAddress = addresses.first.addressLine;
      });
    }
  }
  IsCancelShow(String status) {
    if (null == status || status.isEmpty) {
      return false;
    } else if (status.toLowerCase() == Consts.pendingStatusText ||
        status.toLowerCase() == Consts.confirmedStatusText ||
        status.toLowerCase() == 'driver arrived' ||
        status.toLowerCase() == 'on the way') {
      return true;
    } else {
      return false;
    }
  }

  void onCanceljob() {
    showDialog(
        context: context,
        builder: (BuildContext
        context) =>
            CancelConfirmationDialog(
                this, widget.myRequestListObj.job_id.toString()));
  }

  @override
  void cancelJob(String jobid, String cancel_reason) {
    cancleJobApiCall(jobid, cancel_reason);
  }

  Future<void> cancleJobApiCall(String job_id, String cancel_reason) async {
    print('called' + "..............");

    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = job_id;
    map[Api_constant.reasone_for_cancel] = cancel_reason;

    print(map.toString() + "..............");

    var response = await http.post(Api_constant.cancelJob, body: map);

    print(response.statusCode.toString() + "..............");
    print(response.body + "..............");
    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
      new StatusMsgResponse.fromJsonMap(userMap) as StatusMsgResponse;

      var loginUserObj = loginResponseObj.message;

      if(loginResponseObj.status == Api_constant.STATUS) {
        Toast.show(Api_constant.cancel_job_success_msg, context, gravity: Toast.CENTER);

        getDetail(widget.myRequestListObj.job_id.toString());

      } else{
        Toast.show(loginUserObj.toString(), context, gravity: Toast.CENTER);
      }

//      setState(() {
//
//      });
//      _MyRequestOpenObjList.forEach((element) {
//        //not working
//        if (element.job_id == job_id) {
//          int index = _MyRequestOpenObjList.indexOf(element);
//
//          setState(() {
//            _MyRequestOpenObjList.remove(index);
//          });
//        }
//      });
    }
  }

  Future<void> onlinePaymentSuccessApi() async {
    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = widget.myRequestListObj.job_id.toString();

    var response = await http.post(Api_constant.Online_payment_success, body: map);
    print(map.toString() + "online para api..............");


      setState(() {
        IsProgressIndicatorShow = true;
      });


    if (response.statusCode == 200) {

      IsProgressIndicatorShow = false;
      String response_json_str = response.body;
      print(response_json_str + "online payment api..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];
      print( "inside online payment api..............");

      var CommanModal_obj = new CommanModalWithObj.fromJson(userMap) as CommanModalWithObj;
      if (CommanModal_obj.status == 1) {

        print( "success online payment api..............");
        Toast.show(CommanModal_obj.message, context, gravity: Toast.CENTER);
        getDetail(widget.postid);
        isDoneOnlinePayment=true;

      } else {
        Toast.show(Api_constant.no_record_found, context, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
    }
  }
  Future<void> cashPaymentSuccessApi() async {
    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = widget.myRequestListObj.job_id.toString();

    var response = await http.post(Api_constant.Cash_payment_success, body: map);
    print(map.toString() + "cash payment para api..............");

    setState(() {
      IsProgressIndicatorShow = true;
    });


    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "cash payment api..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];
      print( "inside cash payment api..............");

      var CommanModal_obj = new CommanModalWithObj.fromJson(userMap) as CommanModalWithObj;
      if (CommanModal_obj.status == 1) {

        print( "success cash payment api..............");
        Toast.show(CommanModal_obj.message, context, gravity: Toast.CENTER);
        getDetail(widget.postid);

      } else {
        Toast.show(Api_constant.no_record_found, context, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
    }
  }
  Future<void> myWalletApi(String id) async {
    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = id;

    var response = await http.post(Api_constant.my_wallet, body: map);
    print(Api_constant.my_wallet + "my wallet api ..........");
    print(map.toString() + "my wallet api ..........");

    setState(() {
      IsProgressIndicatorShow = true;
    });




    if (response.statusCode == 200) {
      IsProgressIndicatorShow = false;
      String response_json_str = response.body;
      print(response_json_str + "my wallet api..............");
      Map userMap = jsonDecode(response_json_str);
      var CommanModal_obj = new MyWalletResponseObj.fromJsonMap(userMap) as MyWalletResponseObj;
      if (CommanModal_obj.status == 1) {
        setState(() {
          maxAmtUseFromWallet=CommanModal_obj.max_amount_use_from_wallet;
          walletAmt=CommanModal_obj.wallet_balance;
        });

         print("walletAmt"+walletAmt.toString());
         print("maxAmtUseFromWallet"+maxAmtUseFromWallet.toString());

       // Toast.show(CommanModal_obj.message, context, gravity: Toast.CENTER);
       // getDetail(widget.postid);

      } else {
      /*  maxAmtUseFromWallet=49;
        walletAmt=120;*/
        Toast.show(Api_constant.no_record_found, context, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);

    }
  }
  Future<void> paymentUseApi(num fromWalletAmt,num afterWalletAmt) async {
    var map = new Map<String, dynamic>();
    num afterwllet=afterWalletAmt;
    map[Api_constant.job_id] = widget.myRequestListObj.job_id.toString();
    map['from_wallet_amount'] = fromWalletAmt.toString();
    map['after_wallet_amount'] = afterwllet.toString();

    var response = await http.post(Api_constant.payment_use, body: map);
    print(Api_constant.payment_use + "payment use api..............");
    print(map.toString() + "payment use api..............");



    setState(() {
      IsProgressIndicatorShow = true;
    });



    if (response.statusCode == 200) {
      IsProgressIndicatorShow = false;

      String response_json_str = response.body;
      print(response_json_str + "payment use api..............");
      Map userMap = jsonDecode(response_json_str);

      var CommanModal_obj = new CommanModalWithObj.fromJson(userMap) as CommanModalWithObj;
      if (CommanModal_obj.status == 1) {

        Toast.show(CommanModal_obj.message, context, gravity: Toast.CENTER);
        if(widget.myRequestListObj.payment_type==1){
          makeOnlinePayment(afterwllet);
         // Navigator.of(context).pop();
        }else if(widget.myRequestListObj.payment_type==2){
          cashPaymentSuccessApi();
        }
        getDetail(widget.myRequestListObj.job_id.toString());

      } else {
        Toast.show(Api_constant.no_record_found, context, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
    }
  }

  IsRateDispllay() {
    if(widget.myRequestListObj.status != null &&
        widget.myRequestListObj.status.isNotEmpty &&
        widget.myRequestListObj.status.toLowerCase()==Consts.completedStatusText ) {

      if(CommonWidget.getboolFromInt(widget.myRequestListObj.customer_rated)){
        return true;
      } else{
        return false;
      }
    }else{
      return  false;
    }
  }

  IsRatingBarDispllay() {
    if(widget.myRequestListObj.status != null &&
        widget.myRequestListObj.status.isNotEmpty &&
        widget.myRequestListObj.status.toLowerCase()==Consts.completedStatusText ) {

      if(CommonWidget.getboolFromInt(widget.myRequestListObj.customer_rated)){
        return false;
      } else{
        return true;
      }
    }else{
      return  false;
    }
  }
  IsGoForPayment() {
    if(widget.myRequestListObj.go_for_payment =="1") {
      return true;
    }else if(widget.myRequestListObj.go_for_payment =="2" && isDoneOnlinePayment==true){
      return  false;
    }
    else{
      return  false;
    }
  }

  void onRate() {
    showDialog(
        context: context,
        builder: (BuildContext context) =>
            RatingDialog(widget.myRequestListObj.job_id.toString(),'',
                this)
    );
  }

  @override
  void ratingComplete() {
    getDetail(widget.myRequestListObj.job_id.toString());

  }

  bool IsStatusDisplayShow(String status) {
    if(null == status || status.isEmpty){
      return false;
    }  else{
      if(widget.myRequestListObj.status.toLowerCase() == Consts.confirmedStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.onthewayStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.driverarrivedStatusText ){
        return true;
      } else {
        return false;
      }
    }
  }

  String getprice(String status) {
    if(status != null && status.isNotEmpty && status.toLowerCase() == Consts.paymentpendingStatusText){

      double total = 0;

      String pricee = CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.price);
      String delay_chargess = CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.delay_charges);

//      if(delay_chargess.contains(Consts.currencySymbol)){
        if((delay_chargess.replaceFirst(Consts.currencySymbol, '')).isEmpty){
          return widget.myRequestListObj.price;
        }

        if((delay_chargess.replaceFirst(Consts.currencySymbol, '')).isNotEmpty &&
            (pricee.replaceFirst(Consts.currencySymbol, '')).isNotEmpty){
          total = double.parse(pricee.replaceFirst(Consts.currencySymbol, '')) +
              double.parse(delay_chargess.replaceFirst(Consts.currencySymbol, ''));

          return Consts.currencySymbol+ total.toString();
        }

//      } else if(delay_chargess.contains(Consts.oldcurrencySymbol)){
//        if((delay_chargess.replaceFirst(Consts.oldcurrencySymbol, '')).isEmpty){
//          return widget.myRequestListObj.price;
//        }
//
//        if((delay_chargess.replaceFirst(Consts.oldcurrencySymbol, '')).isNotEmpty &&
//            (pricee.replaceFirst(Consts.oldcurrencySymbol, '')).isNotEmpty){
//          total = double.parse(pricee.replaceFirst(Consts.oldcurrencySymbol, '')) +
//              double.parse(delay_chargess.replaceFirst(Consts.oldcurrencySymbol, ''));
//
//          return Consts.oldcurrencySymbol+ total.toString();
//        }
//      }

    } else if(status != null && status.isNotEmpty && status.toLowerCase() == Consts.paymentpendingStatusText){
      double total = 0;
      String pricee = CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.price);
      String delay_chargess = CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.delay_charges);

      total = double.parse(pricee) + double.parse(delay_chargess);
      return total.toString();

    } else if(IsExtraChargesShow(status)){
      return CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.total_amount);
    } else{
      return CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.price);
    }
  }

  Future<void> _showPriceDialog(String price) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Online Payment'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Pay '+Consts.currencySymbol+price),
                Text(''),
              ],
            ),
          ),

          actions: <Widget>[
            /* TextButton(
              child: Text('Use Wallet Amount'),
              onPressed: () {
                Expanded(
                  flex: 1,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                            child: CommonWidget.lblText(
                                'Enter Amount'),
                          ),
                        ),
                        Container(
                          child: TextField(
                            keyboardType: TextInputType.number,
                            style: CommonWidget.TFCommnTextStyle(),
//                        inputFormatters: <TextInputFormatter>[
//                          FilteringTextInputFormatter.digitsOnly
//                        ],
                            decoration: CommonWidget.ETInputDecoration(),
                            onChanged: (val) {
                            //  customerEnteredWalletAmt = val as int;
//                          toll = val as int;
                            },
                          ),
                        ),
                      ]
                  ),
                );
              },
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: SubmitButtonRed('Calculate', onComplete, 2),
            )*/
            TextButton(
              child: Text('PayNow'),
              onPressed: () {
               // makeOnlinePayment();
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }

  /*Future<void> _showCalculationRelatedDialog(String price) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Cash Payment'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Pay '+Consts.currencySymbol+price),
                Text(''),
              ],
            ),
          ),


          actions: <Widget>[
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Use Wallet Amount'),
              onPressed: () {
                if (!mounted) return;
                setState(() {
                  _isVisible = true;
                  //OpenEnteredData();
                });


             *//*   setState(() {
                });*//*
              },
            ),


           *//* Padding(
              padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: SubmitButtonRed('Calculate', onComplete, 2),
            )*//*
           *//* TextButton(
              child: Text('Use Wallet Amount'),
              onPressed: () {

                Navigator.of(context).pop();
              },
            ),*//*
          ],
        );
      },
    );
  }*/


  void calculateAmt(int userUseWalletAmt){
    num withoutTaxRideAmt=widget.myRequestListObj.amount_without_tax;
    num taxPer=widget.myRequestListObj.tax_percentage;
   // int rideAmt=widget.myRequestListObj.amount_without_tax;
    //int rideAmt=custRideAmt.round();
   // int rideAmt=int.parse(widget.myRequestListObj.budget_from);
    //int rideAmt=246;

   // totalLeftAmt=rideAmt-userUseWalletAmt;

    num totalLeftAmtwithoutTax=withoutTaxRideAmt-userUseWalletAmt;
    num amtTax=0;
    num totalLeftAmt=0;
    if(widget.myRequestListObj.corporate_id != null && widget.myRequestListObj.corporate_id != '0'){
      amtTax=widget.myRequestListObj.charged_tax_amount_number;
      print("call from corporate user");
      countedTaxAmt=widget.myRequestListObj.charged_tax_amount;
      totalLeftAmt=totalLeftAmtwithoutTax+amtTax;
      totalPaybleAmt=totalLeftAmt.toStringAsFixed(2);

    }else{
      print("call from else");
     amtTax=(totalLeftAmtwithoutTax*(taxPer/100));
     countedTaxAmt=amtTax.toStringAsFixed(2);
     totalLeftAmt=totalLeftAmtwithoutTax+amtTax;
     totalPaybleAmt=totalLeftAmt.toStringAsFixed(2);

    }
    print('withoutTaxRideAmt'+withoutTaxRideAmt.toString());
    print('userWalletamt'+userUseWalletAmt.toString());
    print('totalLeftAmtwithoutTax'+totalLeftAmtwithoutTax.toString());
    print('taxamt'+amtTax.toString());

    num amtTax1=((amtTax)/100);




   // corporatetUserototalPaybleAmt=totalLeftAmt.toStringAsFixed(2);
    paymentUseApi(userUseWalletAmt,totalLeftAmt);
  }


 /* void OpenEnteredData(){
  *//*  Visibility(
        visible: _isVisible,
        child:);*//*
    Padding(
      padding: EdgeInsets.all(20),
      child: new Wrap(
        children: <Widget>[
          CommonWidget.lblText('Enter Amount'),
          TextField(
            keyboardType: TextInputType.number,
            style: CommonWidget.TFCommnTextStyle(),
            decoration: CommonWidget.ETInputDecoration(),
            onChanged: (val) {
              customerEnteredWalletAmt = val;
            },
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
            child: SubmitButtonRed('CALCULATE', onComplete, 2),
          )
        ],
      ),
    );
  }*/
  void onwalletAmountChanged(){
    int useWalletAmt=int.parse(customerEnteredWalletAmt.toString());
    num withoutTaxRideAmt=widget.myRequestListObj.amount_without_tax;
    num taxPer=widget.myRequestListObj.tax_percentage;
    // int rideAmt=widget.myRequestListObj.amount_without_tax;
    //int rideAmt=custRideAmt.round();
    // int rideAmt=int.parse(widget.myRequestListObj.budget_from);
    //int rideAmt=246;

    // totalLeftAmt=rideAmt-userUseWalletAmt;
    num totalLeftAmtwithoutTax=withoutTaxRideAmt-useWalletAmt;
    num amtTax=0;
    num totalLeftAmt=0;
    if(widget.myRequestListObj.corporate_id != null && widget.myRequestListObj.corporate_id != '0'){
      amtTax=widget.myRequestListObj.charged_tax_amount_number;
      print("call from corporate user");
      countedTaxAmt=widget.myRequestListObj.charged_tax_amount;
      totalLeftAmt=totalLeftAmtwithoutTax+amtTax;
      totalPaybleAmt=totalLeftAmt.toStringAsFixed(2);

    }else{
      print("call from else");
      amtTax=(totalLeftAmtwithoutTax*(taxPer/100));
      countedTaxAmt=amtTax.toStringAsFixed(2);
      totalLeftAmt=totalLeftAmtwithoutTax+amtTax;
      totalPaybleAmt=totalLeftAmt.toStringAsFixed(2);

    }




    num amtTax1=((amtTax)/100);
    print('withoutTaxRideAmt'+withoutTaxRideAmt.toString());
    print('taxPer'+taxPer.toString());
    print('userWalletamt'+useWalletAmt.toString());
    print('totalLeftAmtwithoutTax'+totalLeftAmtwithoutTax.toString());
    print('taxamt'+amtTax.toString());
   // countedTaxAmt=amtTax.toString();
    countedTaxAmt=amtTax.toStringAsFixed(2);
   // num cTaxAmt=countedTaxAmt;
    totalPaybleAmt=totalLeftAmt.toStringAsFixed(2);

  }
  void onSubmitUserWalletAmt(){

    int useWalletAmt=int.parse(customerEnteredWalletAmt.toString());
    if(useWalletAmt>maxAmtUseFromWallet){
      Toast.show('You can use only '+"₹"+maxAmtUseFromWallet.toString()+" from wallet", context, gravity: Toast.CENTER);
    } else {
      calculateAmt(useWalletAmt);

      myWalletApi(widget.myRequestListObj.job_id.toString());
    }
  }

  void makeOnlinePayment(double afterWalletAmt) async {
    //var budgetFromVal=double.parse(widget.myRequestListObj.budget_from);
   // var val=double.parse(widget.myRequestListObj.budget_from);
   // double rideAmt=double.parse(afterWalletAmt.round());
    var val=afterWalletAmt;
    var roundVal=val.round();
    print("roundVal"+roundVal.toString());
    var budgetFromVal=(roundVal*100);

    MyPreferenceManager _myPreferenceManager =
    await MyPreferenceManager.getInstance();
    String phone_number = _myPreferenceManager.getString(MyPreferenceManager.PHONE_NUMBER);
    String email = _myPreferenceManager.getString(MyPreferenceManager.EMAIL);
    // >Mechant id : EcMJf5xfm7lYwu
    var options = {
       //'key': 'rzp_test_TMa20FHiPiFsDT', //client test key
     // 'key': 'rzp_live_3PnhiM7vSXlEdJ', //client live key
      'key': 'rzp_live_8yzX5iSlQHxMsm', //client live key
      // 'key': 'rzp_test_hGmLEAthGerAr5', //testtechnology410 test key
      'amount':budgetFromVal,
      'name': '',
      'description': '',
      'prefill': {'contact': phone_number, 'email': email},
      // 'prefill': {'contact': '9999976052', 'email': ''},
      'external': {
        'wallets': ['paytm']
      }
    };

    print(options);

    try {
       _razorpay.open(options);
    //   Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: 0);
    } catch (e) {
      debugPrint(e);
    }
  }
}



//https://reds.a2hosted.com/araba/api/web/v1/post-jobs/customer-complete-job
//
//GET
//
//job_id
//online_payment (1/0)
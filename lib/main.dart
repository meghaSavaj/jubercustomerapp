import 'dart:convert';
import 'dart:io';
import 'dart:async';


import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/route_generator.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:flutter/cupertino.dart';

import 'package:toast/toast.dart';



import 'Utils/Consts.dart';
import 'Utils/MyPreferenceManager.dart';
import 'model/LoginResponse.dart';
import 'model/StatusMsgResponse.dart';
import 'package:http/http.dart' as http;

void main() {
//  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//      statusBarColor: Colors.white
//  ));
  runApp( MyApp());
}
//void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
//  Map<int, Color> colorr =
//  {
//    50:Color.fromRGBO(136,14,79, .1),
//    100:Color.fromRGBO(136,14,79, .2),
//    200:Color.fromRGBO(136,14,79, .3),
//    300:Color.fromRGBO(136,14,79, .4),
//    400:Color.fromRGBO(136,14,79, .5),
//    500:Color.fromRGBO(136,14,79, .6),
//    600:Color.fromRGBO(136,14,79, .7),
//    700:Color.fromRGBO(136,14,79, .8),
//    800:Color.fromRGBO(136,14,79, .9),
//    900:Color.fromRGBO(136,14,79, 1),
//  };
//  MaterialColor colorCustom = MaterialColor(0xFF880E4F, colorr);

//  Future<void> requestPermission() async {
//    Map<Permission, PermissionStatus> statuses = await
//    [
//      Permission.locationWhenInUse,
//      Permission.locationAlways
//    ].request();
//  }

  @override
  Widget build(BuildContext context) {
//    requestPermission();
    UpdateToken(context);
    return new MaterialApp(
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'),
        const Locale('fr')
      ],
      navigatorKey: Consts.navigatorKey,
      debugShowCheckedModeBanner: false,
      title: '',
      theme: CommonWidget.getAppTheme(),
//      home: new SplashPage(),
      initialRoute: '/SplashPage', // default is '/'
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
  Future<void> UpdateToken(BuildContext context) async {
    var map = new Map<String, dynamic>();

    MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
    String token = _myPreferenceManager.getString(MyPreferenceManager.DEVICE_TOKEN);
    String id = _myPreferenceManager.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);
    map['user_id'] = id;
    map['device_token'] = token;

    if (Platform.isIOS) {
      map[Api_constant.DEVICE_TYPE] = Api_constant.DEVICE_TYPE_IOS;
    } else {
      map[Api_constant.DEVICE_TYPE] = Api_constant.DEVICE_TYPE;
    }

    print(map.toString() + "==update token");
    print(Api_constant.update_token);

    var response = await http.post(Api_constant.update_token, body: map);

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
      new LoginResponse.fromJsonMap(userMap) as LoginResponse;

      if (loginResponseObj.status == 1) {

      } else {
        Toast.show(loginResponseObj.message, context,  duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
      }
    }

  }
}
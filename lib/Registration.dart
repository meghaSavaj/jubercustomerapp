import 'dart:convert';
import 'package:arabacustomer/model/StatusMsgResponse.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/MyRequestTabsScreen.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';
import 'package:arabacustomer/widgets/SubmitButtonRed.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/gestures.dart';

//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';
import 'package:arabacustomer/PostJob1New.dart';
import 'Utils/Consts.dart';
import 'dialog/OtpDialog.dart';
import 'Utils/Api_constant.dart';
import 'Utils/MyPreferenceManager.dart';
import 'Utils/Utility.dart';
import 'dialog/PrivacyPolicyDialog.dart';
import 'model/LoginResponse.dart';
import 'package:arabacustomer/model/GetOtpResponse.dart';
import 'package:email_validator/email_validator.dart';

class Registration extends StatefulWidget {
  static const routeName = '/RegistrationPage';

  String email;

  Registration({Key key, this.email}) : super(key: key);

//  Registration({Key key, this.email, this.fname, this.lname, this.fbphone}) : super(key: key);

  @override
  RegistrationState createState() {
    return RegistrationState();
  }
}

class RegistrationState extends State<Registration>
    implements CallbackOfOtpDialog {
  bool IsAgree = false;

  final TextEditingController email_controller = TextEditingController();
  final TextEditingController fname_controller = TextEditingController();
  final TextEditingController lname_controller = TextEditingController();
  final TextEditingController phone_controller = TextEditingController();

  @override
  void initState() {
    super.initState();

    emalValue = widget.email.trim();
    email_controller.text = widget.email;

    fname_controller.text = Consts.fbfisrtname;
    fnameValue = Consts.fbfisrtname;

    lname_controller.text = Consts.fblastname;
    lnameValue = Consts.fblastname;

    phone_controller.text = Consts.fbphone;
    mobileValue = Consts.fbphone;

    Consts.fbfisrtname = '';
    Consts.fblastname = '';
    Consts.fbphone = '';

    update_token();
  }

  Future update_token() async {
    var _myPreferenceManager = await MyPreferenceManager.getInstance();

    String token =
        _myPreferenceManager.getString(MyPreferenceManager.DEVICE_TOKEN);

    if (null == token || token.isEmpty) {
      FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
      _firebaseMessaging.getToken().then((token) {
        print("token====" + token);

        _myPreferenceManager.setString(
            MyPreferenceManager.DEVICE_TOKEN, token.toString());
      });
    }
  }

  String fnameValue = '';
  String lnameValue = '';
  String emalValue = '';
  String mobileValue = '';
  String pswdValue = '';
  String cpswdValue = '';
  bool IsProgressIndicatorShow = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Stack(
            children: <Widget>[
              SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(0, 10, 10, 0),
                          child: Text(
                            Api_constant.login,
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 17,
                                fontWeight: FontWeight.w600),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 15, 30, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          RichText(
                            textAlign: TextAlign.start,
                            text: TextSpan(
//                style: DefaultTextStyle.of(context).style,
                              children: <TextSpan>[
                                TextSpan(
                                  text: Api_constant.login_register_now,
                                  style: TextStyle(
                                      color: Colors.black54.withOpacity(0.8),
                                      fontSize: 32,
                                      fontWeight: FontWeight.w800),
                                ),
//                            TextSpan(
//                              text: Api_constant.register_continue,
//                              style: TextStyle(
//                                  color: Colors.black,
//                                  fontSize: 20,
//                                  fontWeight: FontWeight.w300),
//                            ),
                              ],
                            ),
                          ),
                          SizedBox(height: 30.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    CommonWidget.lblText( Api_constant.register_firstname),
                                    TextField(
                                      controller: fname_controller,
//                        maxLength: Consts.TextField_maxLength_forname,
                                      style: CommonWidget.TFCommnTextStyle(),
                                      decoration: CommonWidget.ETInputDecoration(),
                                      onChanged: (val) {
                                        fnameValue = val;
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(width: 10,),
                              Expanded(
                                flex: 1,
                                child:  Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    CommonWidget.lblText( Api_constant.register_lastname),
                                    TextField(
                                      controller: lname_controller,
//                        maxLength: Consts.TextField_maxLength_forname,
                                      style: CommonWidget.TFCommnTextStyle(),
                                      decoration: CommonWidget.ETInputDecoration(),
                                      onChanged: (val) {
                                        lnameValue = val;
                                      },
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),

                          SizedBox(height: 20.0),
                          CommonWidget.lblText( Api_constant.register_email),
                          TextField(
//                        maxLength: Consts.TextField_maxLength_for_email,
                            controller: email_controller,
                            style: CommonWidget.TFCommnTextStyle(),
                            decoration: CommonWidget.ETInputDecoration(),
                            onChanged: (val) {
                              emalValue = val.trim();
                            },
                          ),
                          SizedBox(height: 20.0),
                          CommonWidget.lblText( Api_constant.register_mobile_number),
                          TextField(
                            maxLength: 10,
                            controller: phone_controller,
//                        maxLength: Consts.TextField_phonenumber_maxLength,
                            keyboardType: TextInputType.number,
                            style: CommonWidget.TFCommnTextStyle(),
                            decoration: CommonWidget.ETInputNumberDecoration(),
                            onChanged: (val) {
                              mobileValue = val;
                            },
                          ),
                          SizedBox(height: 20.0),
                          CommonWidget.lblText( Api_constant.login_password),
                          TextField(
//                        maxLength: Consts.TextField_maxLength_password,
                            obscureText: true,
                            style: CommonWidget.TFCommnTextStyle(),
                            decoration: CommonWidget.ETInputDecoration(),
                            onChanged: (val) {
                              pswdValue = val;
                            },
                          ),
                          SizedBox(height: 20.0),
                          CommonWidget.lblText( Api_constant.register_confirm_password),
                          TextField(
//                        maxLength: Consts.TextField_maxLength_password,
                            obscureText: true,
                            style: CommonWidget.TFCommnTextStyle(),
                            decoration: CommonWidget.ETInputDecoration(),
                            onChanged: (val) {
                              cpswdValue = val;
                            },
                          ),
                          SizedBox(height: 30.0),
                          SubmitButtonRed(Api_constant.signup, onChanged, 1),
                          SizedBox(height: 15.0),
                          Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
//                              Checkbox(
//                                value: IsAgree,
//                                onChanged: (bool value) {
//                                  setState(() {
//                                    IsAgree = value;
//                                  });
//                                },
//                              ),
                                Flexible(
                                  child:  RichText(
                                    textAlign: TextAlign.start,
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: Api_constant.register_agree1,
                                          style: privacyPolicyTextStyle(),
                                        ),
                                        TextSpan(
                                          text: Api_constant.register_agree2,
                                          style:  privacyPolicyunderlineTextStyle(),
                                          recognizer: new TapGestureRecognizer()
                                            ..onTap = () => {
                                              /*  showDialog(
                                                context: context,
                                                builder: (BuildContext context) =>
                                                   PrivacyPolicyDialog(),
                                                )*/
                                              Navigator.of(context).push(
                                                  new MaterialPageRoute<Null>(
                                                      builder: (BuildContext
                                                      context) {
                                                        return new PrivacyPolicyDialog();
                                                      },
                                                      fullscreenDialog: true))
                                            },
                                        ),
                                        TextSpan(
                                          text: Api_constant.register_agree3,
                                          style:  privacyPolicyTextStyle(),
                                        ),
                                        TextSpan(
                                          text: Api_constant.register_agree4,
                                          style:  privacyPolicyunderlineTextStyle(),
                                          recognizer: new TapGestureRecognizer()
                                            ..onTap = () => {
                                              /*  showDialog(
                                                context: context,
                                                builder: (BuildContext context) =>
                                                   PrivacyPolicyDialog(),
                                                )*/
                                              Navigator.of(context).push(
                                                  new MaterialPageRoute<Null>(
                                                      builder: (BuildContext
                                                      context) {
                                                        return new PrivacyPolicyDialog();
                                                      },
                                                      fullscreenDialog: true))
                                            },
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
//                            Text(
//                              Api_constant.register_agree,
//                              style: TextStyle(
//                                  color: Colors.black45.withOpacity(0.6),
//                                  fontSize: 16,
//                                  height: 1.2,
//                                  fontWeight: FontWeight.w400),
//                              textAlign: TextAlign.center,
//                            ),
                                SizedBox(height: 35.0),
//                              Text(
//                                Api_constant.register_member,
//                                style: TextStyle(
//                                    color: Colors.black87.withOpacity(0.8),
//                                    fontSize: 16,
//                                    fontWeight: FontWeight.w400),
//                                textAlign: TextAlign.center,
//                              ),
//                              InkWell(
//                                onTap: () {
//                                  Navigator.pop(context);
//                                },
//                                child: Text(
//                                  Api_constant.register_login,
//                                  style: TextStyle(
//                                      color: Color(0xFF8b0000),
//                                      fontSize: 17,
//                                      fontWeight: FontWeight.w600),
//                                  textAlign: TextAlign.center,
//                                ),
//                              ),
//
                                CommonWidget.submitButtonBottomLine(),
                                SizedBox(height: 15.0),
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
//            Expanded(
//              flex: 1,
//              child:
              Center(
                child: Visibility(
                  visible: IsProgressIndicatorShow,
                  child: SizedBox(
                    child: CircularProgressIndicator(),
                  ),
                ),
              ),
//            )
            ],
          )),
    );
  }

  TextStyle privacyPolicyTextStyle(){
    return TextStyle(
        color: Colors.grey[600],
        fontSize: 12,
        fontWeight: FontWeight.w600);
  }
  TextStyle privacyPolicyunderlineTextStyle(){
    return TextStyle(
        color: Colors.grey[600],
        decoration: TextDecoration.underline,
        fontSize: 12,
        fontWeight: FontWeight.w600);
  }

  void onChanged() {
    if (fnameValue.isEmpty) {
      Toast.show(Api_constant.firstname_empty, context, gravity: Toast.CENTER);
    } else if (lnameValue.isEmpty) {
      Toast.show(Api_constant.lastname_empty, context, gravity: Toast.CENTER);
    } else if (emalValue.isEmpty) {
      Toast.show(Api_constant.email_empty, context, gravity: Toast.CENTER);
    } else if (!EmailValidator.validate(emalValue)) {
      Toast.show(Api_constant.email_invalid, context, gravity: Toast.CENTER);
    } else if (mobileValue.isEmpty) {
      Toast.show(Api_constant.mobile_empty, context, gravity: Toast.CENTER);
    } else if (pswdValue.isEmpty) {
      Toast.show(Api_constant.pswd_empty, context, gravity: Toast.CENTER);
//    } else if (!IsAgree) {
//      Toast.show(Api_constant.privacy_policy_empty, context,
//          gravity: Toast.CENTER);
    } else if (cpswdValue.isEmpty) {
      Toast.show(Api_constant.cpassword_empty, context, gravity: Toast.CENTER);
    } else if (!CommonWidget.isPasswordCompliant(pswdValue, context)) {
    } else if (pswdValue != cpswdValue) {
      Toast.show(Api_constant.compare_paswd_error_msg, context,
          gravity: Toast.CENTER);
    } else {
      Utility.checkInternetConnection().then((intenet) {
        if (intenet != null && intenet) {
//          RegistrationUser();
//          sendOtp();
          verify_emai();
        } else {
          Toast.show(Api_constant.no_internet_connection, context,
              gravity: Toast.CENTER);
        }
      });
    }
  }

  void RegistrationUser() async {
    var map = new Map<String, dynamic>();
    map[Api_constant.email] = emalValue;
    map[Api_constant.password] = pswdValue;
    map[Api_constant.lng] = Api_constant.en;

    map[Api_constant.first_name] = fnameValue;
    map[Api_constant.last_name] = lnameValue;
    map[Api_constant.phone_number] = mobileValue;
    map[Api_constant.lng] = Api_constant.en;
    map[Api_constant.gender] = '';

    var instance = await MyPreferenceManager.getInstance();
    map[Api_constant.device_token] =
        instance.getString(MyPreferenceManager.DEVICE_TOKEN);
    map[Api_constant.device_type] = Api_constant.DEVICE_TYPE;

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.post(Api_constant.signup_customer_api, body: map);

    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var loginResponseObj =
          new LoginResponse.fromJsonMap(userMap) as LoginResponse;

      if (loginResponseObj.status == 1) {
        Toast.show(loginResponseObj.message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);

        var loginUserObj = loginResponseObj.data;

        MyPreferenceManager _myPreferenceManager =
            await MyPreferenceManager.getInstance();
        _myPreferenceManager.setBool(MyPreferenceManager.IS_USER_LOGIN, true);
        _myPreferenceManager.setString(
            MyPreferenceManager.CURRENT_LOGIN_USER_ID,
            loginUserObj.id.toString());

        _myPreferenceManager.setString(
            MyPreferenceManager.LOGIN_NAME,
            CommonWidget.replaceNullWithEmpty(loginUserObj.first_name) +
                " " +
                CommonWidget.replaceNullWithEmpty(loginUserObj.last_name));

        print("userid " + loginUserObj.id.toString());

//      Navigator.pushNamed(context, MyRequestTabsScreen.routeName);
        Navigator.of(context).pushReplacementNamed(PostJob1New.routeName);
      } else {
        Toast.show(loginResponseObj.message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  Future<void> sendOtp() async {
    var map = new Map<String, dynamic>();
    map[Api_constant.phone_number] = removeCountryCode(mobileValue.trim());
    map['first_name'] = fnameValue.trim();
    map['last_name'] = lnameValue.trim();
    map['email'] = emalValue.trim();

    print(map.toString() + "send otp..............");
    print(Api_constant.send_otp);

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.post(Api_constant.send_otp, body: map);
    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

//      var loginResponseObj = new LoginResponse.fromJsonMap(userMap) as LoginResponse;
      var loginResponseObj =
          new GetOtpResponse.fromJsonMap(userMap) as GetOtpResponse;
      if (loginResponseObj.status == 1) {
//        Toast.show(userMap.toString(), context,duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);

//        showDialog(
//            context: context,
//            builder: (BuildContext context) =>
//                OtpDialog(mobileValue.trim(), this));
        Navigator.of(context).push(new MaterialPageRoute<Null>(
            builder: (BuildContext context) {
              return new OtpDialog(removeCountryCode(mobileValue.trim()),
                  emalValue.trim(), pswdValue, fnameValue, lnameValue, this);
            },
            fullscreenDialog: true));
      } else {
        Toast.show(loginResponseObj.message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  Future<void> verify_emai() async {
    var map = new Map<String, dynamic>();

    map['email'] = emalValue.trim();

    print(map.toString() + "verify_email..............");
    print(Api_constant.verify_email);

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.post(Api_constant.verify_email, body: map);
    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

//      var loginResponseObj = new LoginResponse.fromJsonMap(userMap) as LoginResponse;
      var loginResponseObj =
          new StatusMsgResponse.fromJsonMap(userMap) as StatusMsgResponse;
      if (loginResponseObj.status == 1) {
        sendOtp();
      } else {
        Toast.show(loginResponseObj.message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  @override
  void getotp(String phone_otp, String email_otp) {
//    verifyOtp(phone_otp, email_otp);
    RegistrationUser();
  }

  Future<void> verifyOtp(String phone_otp, String email_otp) async {
    var map = new Map<String, dynamic>();
    map[Api_constant.phone_number] = mobileValue.trim();
    map[Api_constant.otp_param] = phone_otp;
    map['email'] = emalValue.trim();
    map['email_otp'] = email_otp;

    print(map.toString() + "verifyOtp..............");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    print(Api_constant.verify_otp + "verifyOtp..............");

    var response = await http.post(Api_constant.verify_otp, body: map);
    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
          new GetOtpResponse.fromJsonMap(userMap) as GetOtpResponse;
      if (loginResponseObj.status == 1) {
        Toast.show(loginResponseObj.message.toString(), context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);

        RegistrationUser();
      } else {
        Toast.show(loginResponseObj.message.toString(), context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  String removeCountryCode(String phone) {
    if (phone != null && phone.isNotEmpty && phone.contains('213')) {
      return phone.replaceFirst('213', '');
    } else {
      return phone;
    }
  }
}

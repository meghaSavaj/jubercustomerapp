import 'dart:convert';

import 'package:arabacustomer/PostJob1New.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SideMenu.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

import 'Utils/Api_constant.dart';
import 'Utils/Consts.dart';
import 'Utils/MyPreferenceManager.dart';
import 'model/ProfileResponse.dart';
import 'myProfileEdit.dart';

class MyProfile extends StatefulWidget {
  static const routeName = '/myprofile';
  @override
  MyProfileState createState() {
    return MyProfileState();
  }
}

class MyProfileState extends State<MyProfile> {

  bool IsProgressIndicatorShow = false;
  ProfileResponse profileResponseObj = null ;
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController addressController = TextEditingController();
  final TextEditingController changepswdController = TextEditingController();
  String name= '';
  String imagePath= '';

  @override
  void initState() {
    super.initState();
    getProfileApiCall();
  }

  Future<bool> _onWillPop() async {
    print("_onWillPop===========" );
    Navigator.of(context)
        .pushNamedAndRemoveUntil(
        PostJob1New.routeName, (Route<dynamic> route) => false, arguments: 0);

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
           // brightness: Brightness.light,
            centerTitle: true,
            elevation: 0,
            title: CommonWidget.getActionBarTitleText(''),
            flexibleSpace: CommonWidget.ActionBarBg(context),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.edit, color: Colors.white, size: 24,),
                onPressed: () {
                  goToEditProfile();
                },
              )
            ],
          ),
          drawer: MainDrawer(context),
          body: Stack(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  CommonWidget.ActionBarBg(context),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 100, 0, 0),
                    child: Container(
                      color: Colors.grey[200],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 55, 20, 15),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                          BorderRadius.all(Radius.circular(5.0))),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Center(
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: 50.0),
                                CommonWidget.priceDarkRedcolorText(name, context),
                                SizedBox(height: 15.0),
                                Container(height: 3.5, width: 30, color:  Colors.redAccent[200].withOpacity(0.7),),
//                              CommonWidget.submitButtonBottomLine(),
                              ],
                            ),
                          ),
                          Padding(padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                TextField(
//                                maxLength: Consts.TextField_phonenumber_maxLength,
                                  decoration: InputDecoration(labelText: Api_constant.register_mobile_number,
                                    labelStyle: TextStyle(color: Colors.black87, fontSize: 12),

                                    hintStyle: TextStyle(
                                        color: Colors.grey[500], fontSize: 12
                                    ),

                                  ),
                                  controller: phoneNumberController,
                                  readOnly: true,
                                ),
                                SizedBox(height: 20.0),
                                TextField(
                                  decoration: InputDecoration(labelText: Api_constant.login_email,
                                      labelStyle: TextStyle(color: Colors.black87, fontSize: 12),
                                      hintStyle: TextStyle(
                                          color: Colors.grey[500], fontSize: 12
                                      )
                                  ),
                                  controller: emailController,
                                  readOnly: true,
                                ),
                                SizedBox(height: 20.0),
                                Visibility(
                                  visible: false,
                                  child : TextField(
                                    decoration: InputDecoration(labelText: Api_constant.Address,
                                        labelStyle: TextStyle(color: Colors.black87, fontSize: 12),
                                        hintStyle: TextStyle(
                                            color: Colors.grey[500], fontSize: 12
                                        )),
                                    controller: addressController,
                                    readOnly: true,
                                  ),
                                ),
                                SizedBox(height: 40.0),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(Api_constant.Change_your_password),
                                    Text('********')
                                  ],
                                ),
                                SizedBox(height: 10.0),
                                Divider(
                                  thickness: 0.5,
                                  color: Colors.grey[800],)
                              ],
                            ),)
                        ],
                      ),
                    ),
                  ),

                  Align(alignment : Alignment.topCenter,
                    child: Container(
                        width: 90.0,
                        height: 90.0,
                        child: ClipOval(
                          child: Hero(
                            tag: 'testd',
                            child: Image(
                              image: getProfileImage(),
                            ),
                          ),
                        ),
//                        decoration: new BoxDecoration(
//                            shape: BoxShape.circle,
//                            image: new DecorationImage(
//                              fit: BoxFit.fill,
//                              //image: AssetImage('assets/images/splash.png'),
//                              image:  getProfileImage(),
////                          image:  profileResponseObj.profile_pic.toString().isEmpty ? AssetImage('assets/images/splash.png') : new NetworkImage(
////                                profileResponseObj.profile_pic.toString()),
//                            )
//                        )

//                ClipRRect(
//                  borderRadius: BorderRadius.circular(30.0),
//                  child:Image(image: AssetImage('assets/images/splash.png'), height: 90, width: 90,),
                    ),
                  )
                ],
              ),
//            Expanded(
//              flex: 1,
//              child:
              Center(child:  Visibility(
                visible: IsProgressIndicatorShow,
                child: SizedBox(
                  child: CircularProgressIndicator(),
                ),
              ),
              ),
//            )
            ],
          ),
        ),
        onWillPop: _onWillPop);
  }

  Future<void> getProfileApiCall() async {

    MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
    String id =  _myPreferenceManager.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);

    print("id===="+id);

    setState(() {
      IsProgressIndicatorShow = true;
    });

    final uri = Uri.encodeFull(Api_constant.get_profile_customer_api +
        '?id=${id}');

    var response = await http.get(uri);

    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str +"..............");
      Map userMap = jsonDecode(response_json_str);
      var data = userMap["data"];
      profileResponseObj = new ProfileResponse.fromJsonMap(data) as ProfileResponse;

      updateData();
    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  void goToEditProfile() async{
    // Navigator.pushNamed(context, MapAndSearchbarWithPlaceApi.routeName);
    await Navigator.of(context).pushNamed(MyProfileEdit.routeName, arguments: profileResponseObj);

//    await Navigator.push(context, MaterialPageRoute(builder: (context) => MyProfileEdit(profileResponseObj: args as ProfileResponse)));
//    await Navigator.push(context, MaterialPageRoute(builder: (context) => MapAndSearchbarWithPlaceApi()));
    getProfileApiCall();
    setState(() {
//      profileResponseObj.first_name =Consts.tempSaveProfileFname;
//      profileResponseObj.last_name = Consts.tempSaveProfileLname;
//      profileResponseObj.email = Consts.tempSaveProfileEmail;
//      profileResponseObj.phone_number = Consts.tempSaveProfilePhoneNumber;
//      imagePath = Consts.tempSaveProfilePic;
//      updateData();
    });
  }

  void updateData() {
    setState(() {
      phoneNumberController.text = profileResponseObj.phone_number;
      emailController.text = profileResponseObj.email;
      addressController.text = profileResponseObj.address;
      imagePath = profileResponseObj.profile_pic;
      name = profileResponseObj.first_name+' '+profileResponseObj.last_name;
    });
  }


  ImageProvider getProfileImage(){
     if(imagePath.isNotEmpty){
      return  new NetworkImage(
          imagePath);
    }else{
      return AssetImage("assets/images/camera.png");
    }

  }
}

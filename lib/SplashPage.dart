import 'package:arabacustomer/Login.dart';
import 'package:arabacustomer/MyRequestDetailNew2.dart';
import 'package:arabacustomer/PostJob1New.dart';
import 'package:arabacustomer/widgets/MyRequestTabsScreen.dart';
//import 'package:dart_notification_center/dart_notification_center.dart';
import 'package:arabacustomer/widgets/lib_dart_noti_center/dart_notification_center.dart';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'dart:async';

import 'LoginTirth.dart';
import 'MyRequestDetail.dart';
import 'MyRequestDetailNew.dart';
import 'Utils/Consts.dart';
import 'Utils/MyPreferenceManager.dart';

class SplashPage extends StatefulWidget {
  static const routeName = '/SplashPage';

  @override
  SplashPageState createState() => SplashPageState();
}


final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();
final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

class SplashPageState extends State<SplashPage> {
// THIS FUNCTION WILL NAVIGATE FROM SPLASH SCREEN TO HOME SCREEN.    // USING NAVIGATOR CLASS.
  bool alreadyNavigateByNotificationClick = false;

  Future navigationToNextPage() async {

    MyPreferenceManager _myPreferenceManager =
    await MyPreferenceManager.getInstance();

    if (alreadyNavigateByNotificationClick) {
      // alreadyNavigate to detail screen by ByNotificationClick
    } else if (_myPreferenceManager.getBool(MyPreferenceManager.IS_USER_LOGIN)) {
//      Navigator.pushNamed(context, MyRequestTabsScreen.routeName);
      Navigator.of(context).pushReplacementNamed(PostJob1New.routeName);
//      Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName);

    } else {
      Navigator.of(context).pushReplacementNamed(LoginTirth.routeName);

    }
  }

  startSplashScreenTimer() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationToNextPage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/splash.png'),
                fit: BoxFit.fill,
              ),
            ),
//                child: new Image.asset('assets/images/splash_center_image_logo.png',
//                    height: MediaQuery.of(context).size.height * 0.50,
//                    width: MediaQuery.of(context).size.width * 0.30,)
          child: Image(
            image: AssetImage('assets/side_menu/tirthtravels_logo.png'),
            height: 160,
            width: MediaQuery.of(context).size.width * 0.65,
          ),
        ),
//            child: new Image.asset('assets/images/splash.png', fit: BoxFit.fill))
    );
  }


  @override
  void initState()  {
    super.initState();


    initNoti();
    _requestIOSPermissions();

//    displlayNotification();

//    setupLocator();
//
      FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

      _firebaseMessaging.configure(
        //      onBackgroundMessage: (Map<String, dynamic> message) {
//          displlayNotification();
//          print('on BackgroundMessage $message');
//        },
//        onBackgroundMessage: myBackgroundMessageHandler,
//      onBackgroundMessage: Platform.isIOS ? null : myBackgroundMessageHandler,
      onMessage: (Map<String, dynamic> message) {
        displlayNotification(message);

        var data = message['data'] ?? message;
        String notify_type = data['notify_type'];
        print("notify_type"+notify_type);

        // Expected: received: with options!!
        if(notify_type == Consts.JobAcceptedChanel ||
            notify_type == Consts.DriverArrivedChanel ||
            notify_type == Consts.JobStartedChanel ||
            notify_type == Consts.DriveronthewayChanel ||
            notify_type == Consts.PaymentCompletedChanel ||
            notify_type == Consts.SuccessfullPaymentChanel ||
            notify_type == Consts.JobCompletedChanel ||
            notify_type == Consts.PaymentRequestChanel ||
            notify_type == Consts.GoForPayment){

          Consts.CurrentChanelType = notify_type;

          if(Consts.CurrentChanelType==Consts.JobAcceptedChanel){
            Consts.isAcceptedJob=true;
          }


          //new job added noti
          DartNotificationCenter.post(
            channel: Consts.JobAcceptedChanel,
            options: 'with options!!',
          );
          DartNotificationCenter.post(
            channel: Consts.DriverArrivedChanel,
            options: 'with options!!',
          );
          DartNotificationCenter.post(
            channel: Consts.JobCancelTimer,
            options: 'with options!!',
          );
          DartNotificationCenter.post(
            channel: Consts.GoForPayment,
            options: 'with options!!',
          );

          DartNotificationCenter.post(
            channel: Consts.DetialChanel,
            options: 'with options!!',
          );

        }


        print('on message $message');
        print('notify_type $notify_type');
      },
      onResume: (Map<String, dynamic> message) {
      print('on resume $message');
      var data = message['data'] ?? message;
      print('on resume $data');

//        if(message.containsKey('job_id')){
//          print('+++++++++++++'+ message['job_id']);
        alreadyNavigateByNotificationClick = true;
        _navigateToItemDetail(context, data['job_id']);
//        }
      },
      onLaunch: (Map<String, dynamic> message) {
      print('on launch $message');
      var data = message['data'] ?? message;
      print('on resume $data');
      alreadyNavigateByNotificationClick = true;
      _navigateToItemDetail(context, data['job_id']);
      },
      );
      _firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(sound: true, badge: true, alert: true));
      _firebaseMessaging.getToken().then((token) {

      print("token===="+token);

      update_token(token);
    });
    startSplashScreenTimer();

  }

  Future update_token(String token) async {
    var _myPreferenceManager = await MyPreferenceManager.getInstance();
    _myPreferenceManager.setString(MyPreferenceManager.DEVICE_TOKEN, token.toString());
  }


//  Future<void> displlayNotification(Map<String, dynamic> message) async {
  static Future<void> displlayNotification(Map<String, dynamic> message) async {
    var data = message['data'] ?? message;


    String notify_type = '';
    String title = '';
    String body = data['body'];
    String icon = data['icon'];

    notify_type = data['notify_type'];
    title = data['title'];
    String job_id = data['job_id'];

    Consts.notificationJobId=data['job_id'];

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'TirthTravel', 'TirthTravel', 'your channel description',
        importance: Importance.Max, priority: Priority.High, ticker: 'ticker',
      color: const Color.fromARGB(255,230,74,25),
//      ledColor: const Color.fromARGB(255, 198, 40, 40),
      icon: 'app_icon',);

    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    if(flutterLocalNotificationsPlugin != null){
      await flutterLocalNotificationsPlugin.show(
          0, title, body, platformChannelSpecifics,
          payload: job_id);
    }

  }

  static void _requestIOSPermissions() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );
  }
  static NotificationAppLaunchDetails notificationAppLaunchDetails;

//  static Future<void> initNoti(bool IsSowiDisplay) async {
   Future<void> initNoti() async {
    // needed if you intend to initialize in the `main` function
    WidgetsFlutterBinding.ensureInitialized();

    notificationAppLaunchDetails =
    await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

//    var initializationSettingsAndroid = AndroidInitializationSettings('ic_launcher');
    var initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
    // Note: permissions aren't requested here just to demonstrate that can be done later using the `requestPermissions()` method
    // of the `IOSFlutterLocalNotificationsPlugin` class
    var initializationSettingsIOS = IOSInitializationSettings(
    onDidReceiveLocalNotification: onDidReceiveLocalNotification
//        requestAlertPermission: false,
//        requestBadgePermission: false,
//        requestSoundPermission: false,
//        onDidReceiveLocalNotification:
//            (int id, String title, String body, String payload) async {
////        didReceiveLocalNotificationSubject.add(ReceivedNotification(
////            id: id, title: title, body: body, payload: payload));
//        }
        );
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String payload) async {
          print('onSelectNotification++ $payload');
//          Toast.show('getting++', context,
//              gravity: Toast.CENTER);

//          _navigateToItemDetail(context, payload);
          if (payload != null) {
            Consts.navigatorKey.currentState.pushNamed(MyRequestDetailNew2.routeName, arguments: payload);

            debugPrint('notification payload: ' + payload);
          }
//        selectNotificationSubject.add(payload);
        });
  }

  static Future<dynamic> myBackgroundMessageHandler(
      Map<String, dynamic> message) {
    print('on message++ $message');

//    initNoti();
//    _requestIOSPermissions();
//
//    displlayNotification();

//    if (message.containsKey('data')) {
//      // Handle data message
//      final dynamic data = message['data'];
//    }
//
//    if (message.containsKey('notification')) {
//      // Handle notification message
//      final dynamic notification = message['notification'];
//    }

//    return null;
    // Or do other work.
  }

  //PRIVATE METHOD TO HANDLE NAVIGATION TO SPECIFIC PAGE
  static Future<void> _navigateToItemDetail(BuildContext context, String job_id) async {
    print('==================='+context.toString()+" "+job_id+" "+Consts.navigatorKey.currentState.toString());
    Consts.navigatorKey.currentState.pushNamed(MyRequestDetailNew2.routeName, arguments: job_id);

//   await Navigator.push(
//     context,
//     MaterialPageRoute(builder: (context) => MyRequestDetail(
//       postid: job_id,
//     )),
//   );

  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () async {
              Consts.navigatorKey.currentState.pushNamed(MyRequestDetailNew2.routeName, arguments: payload);
            },
          )
        ],
      ),
    );
  }
}


class NotificationModal {
  int id = null;
  String sender_name= "";
  String message = "";
  String type= "";
  String date= "";

  NotificationModal.fromJson(Map<String, dynamic> json):
        id = json['id'],
        sender_name = json['sender_name'],
        message = json['message'],
        type = json['type'],
        date = json['date'];


}


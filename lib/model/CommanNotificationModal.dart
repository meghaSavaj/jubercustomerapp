class CommanNotificationModal {
  List<Object> data  = null;
  String message = "";
  int status = null;
  String unread_count= "";

  CommanNotificationModal.fromJson(Map<String, dynamic> json):
        data = json['data'],
        message = json['message'],
        status = json['status'],
        unread_count = json['unread_count'];
}


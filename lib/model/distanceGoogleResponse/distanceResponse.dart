
import 'package:arabacustomer/model/distanceGoogleResponse/rows.dart';


class DistanceResponse {

  List<String> destination_addresses;
  List<String> origin_addresses;
  List<Rows> rows;
  String status;

	DistanceResponse.fromJsonMap(Map<String, dynamic> map): 
		destination_addresses = List<String>.from(map["destination_addresses"]),
		origin_addresses = List<String>.from(map["origin_addresses"]),
		rows = List<Rows>.from(map["rows"].map((it) => Rows.fromJsonMap(it))),
		status = map["status"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['destination_addresses'] = destination_addresses;
		data['origin_addresses'] = origin_addresses;
		data['rows'] = rows != null ? 
			this.rows.map((v) => v.toJson()).toList()
			: null;
		data['status'] = status;
		return data;
	}
}

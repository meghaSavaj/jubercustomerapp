import 'distance.dart';

class Elements {

  Distance distance;
  Duration duration;
  String status;

	Elements.fromJsonMap(Map<String, dynamic> map): 
		distance = Distance.fromJsonMap(map["distance"]),
//		duration = Duration.fromJsonMap(map["duration"]),
		status = map["status"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['distance'] = distance == null ? null : distance.toJson();
//		data['duration'] = duration == null ? null : duration.toJson();
		data['status'] = status;
		return data;
	}
}

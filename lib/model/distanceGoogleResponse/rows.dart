import 'elements.dart';

class Rows {

  List<Elements> elements;

	Rows.fromJsonMap(Map<String, dynamic> map): 
		elements = List<Elements>.from(map["elements"].map((it) => Elements.fromJsonMap(it)));

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['elements'] = elements != null ? 
			this.elements.map((v) => v.toJson()).toList()
			: null;
		return data;
	}
}

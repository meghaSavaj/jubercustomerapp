
class Duration {

  String text;
  int value;

	Duration.fromJsonMap(Map<String, dynamic> map): 
		text = map["text"],
		value = map["value"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['text'] = text;
		data['value'] = value;
		return data;
	}
}

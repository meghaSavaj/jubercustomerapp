class ApplyPromoCode {

  String coupon_id;
	String coupon_name;
	String coupon_amount;
  String coupon_type;

	ApplyPromoCode.fromJsonMap(Map<String, dynamic> map):
				coupon_id = map["coupon_id"],
				coupon_name = map["coupon_name"],
				coupon_amount = map["coupon_amount"],
				coupon_type = map["coupon_type"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['coupon_id'] = coupon_id;
		data['coupon_name'] = coupon_name;
		data['coupon_amount'] = coupon_amount;
		data['coupon_type'] = coupon_type;
		return data;
	}
}

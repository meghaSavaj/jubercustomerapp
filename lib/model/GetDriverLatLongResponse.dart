import 'getDriverLatLong.dart';

class GetDriverLatLongResponse {

  int status;
	GetDriverLatLong data;

	GetDriverLatLongResponse.fromJsonMap(Map<String, dynamic> map):
		status = map["status"],
		data = GetDriverLatLong.fromJsonMap(map["data"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
//		data['data'] = data == null ? null : data.toJson();
		return data;
	}
}

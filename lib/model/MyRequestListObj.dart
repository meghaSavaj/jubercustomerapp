import 'package:json_annotation/json_annotation.dart';

@JsonSerializable(nullable: false)
class MyRequestListObj {

  int user_id;
  int job_id;
  int corporate_id;
  String job_title;
  String job_number;
  String vehicle_category;
  String vrn;
	String vehicle_platnumber;
  String phone_number;
  String price;
  String pickup_address= '';
  String pickup_lattitude;
  String pickup_longitude;
  String delivery_address = '';
  String delivery_lattitude ;
  String delivery_longitude ;
  String description ;
  String date;
  String distance;
//  double distance;
	int payment_type;
  String status;
  String transporter_name;
  String transporter_ratings;
  String customer_ratings;
  String current_datetime;
  String job_rating;

  String driver_name;
  String driver_phone_number;
  String driver_email;
  String driver_licence;

  String delay_charges;
  String delay_mins;
  String total_amount;
  String budget_from;
  String go_for_payment;

  int customer_rated;
  int JobListType ;
	int rating_list ;

	String charged_tax_amount;
	String job_assigned_date;
	String job_completed_date;
	String gprs_lat;
	String gprs_lng;
	String amount_without_tax_string;
	num tax_percentage;
	num amount_without_tax;
	num charged_tax_amount_number;


	MyRequestListObj.fromJsonMap(Map<String, dynamic> map):
		user_id = map["user_id"],
		job_id = map["job_id"],
				corporate_id = map["corporate_id"],
		job_title = map["job_title"],
		job_number = map["job_number"],
		vehicle_category = map["vehicle_category"],
		vrn = map["vrn"],
				vehicle_platnumber = map["vehicle_platnumber"],
		phone_number = map["phone_number"],
		price = map["price"],
		pickup_address = map["pickup_address"],
		pickup_lattitude = map["pickup_lattitude"],
		pickup_longitude = map["pickup_longitude"],
		delivery_address  = map["delivery_address"],
		delivery_lattitude  = map["delivery_lattitude"],
		delivery_longitude  = map["delivery_longitude"],
		description  = map["description"],
		date = map["date"],
		distance = map["distance"],
		status = map["status"],
		transporter_name = map["transporter_name"],
		transporter_ratings = map["transporter_ratings"],
				customer_ratings = map["customer_ratings"],
		current_datetime = map["current_datetime"],
  	job_rating = map["job_rating"],
		customer_rated = map["customer_rated"],
				driver_name = map["driver_name"],
				driver_phone_number = map["driver_phone_number"],

				driver_email = map["driver_email"],
				rating_list = map["rating_list"],
				delay_charges = map["delay_charges"],
				delay_mins = map["delay_mins"],
				total_amount = map["total_amount"],
			driver_licence = map["driver_licence"],
			payment_type = map["payment_type"],
	budget_from = map["budget_from"],
	go_for_payment = map["go_for_payment"],
				job_assigned_date = map["job_assigned_date"],
				job_completed_date = map["job_completed_date"],
		charged_tax_amount = map["charged_tax_amount"],
	tax_percentage = map["tax_percentage"],
	amount_without_tax = map["amount_without_tax"],
				gprs_lat = map["gprs_lat"],
	gprs_lng = map["gprs_lng"],
	amount_without_tax_string = map["amount_without_tax_string"],
	charged_tax_amount_number = map["charged_tax_amount_number"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['total_amount'] = total_amount;
		data['user_id'] = user_id;
		data['corporate_id'] = corporate_id;
		data['job_id'] = job_id;
		data['job_title'] = job_title;
		data['job_number'] = job_number;
		data['vehicle_category'] = vehicle_category;
		data['vrn'] = vrn;
		data['vehicle_platnumber'] = vehicle_platnumber;
		data['phone_number'] = phone_number;
		data['price'] = price;
		data['pickup_address'] = pickup_address;
		data['pickup_lattitude'] = pickup_lattitude;
		data['pickup_longitude'] = pickup_longitude;
		data['delivery_address'] = delivery_address ;
		data['delivery_lattitude'] = delivery_lattitude ;
		data['delivery_longitude'] = delivery_longitude ;
		data['description'] = description ;
		data['date'] = date;
		data['distance'] = distance;
		data['status'] = status;
		data['transporter_name'] = transporter_name;
		data['transporter_ratings'] = transporter_ratings;
		data['customer_ratings'] = customer_ratings;
		data['current_datetime'] = current_datetime;
		data['job_rating'] = job_rating;
		data['customer_rated'] = customer_rated;
		data['driver_name'] = driver_name;
		data['driver_phone_number'] = driver_phone_number;
		data['driver_email'] = driver_email;
		data['rating_list'] = rating_list;
		data['driver_licence'] = driver_licence;
		data['delay_mins'] = delay_mins;
		data['delay_charges'] = delay_charges;
		data['budget_from'] = budget_from;
		data['go_for_payment'] = go_for_payment;
		data['job_assigned_date'] = job_assigned_date;
		data['job_completed_date'] = job_completed_date;
		data['charged_tax_amount'] = charged_tax_amount;
		data['tax_percentage'] = tax_percentage;
		data['amount_without_tax'] = amount_without_tax;
		data['gprs_lat'] = gprs_lat;
		data['gprs_lng'] = gprs_lng;
		data['charged_tax_amount_number'] = charged_tax_amount_number;
		return data;
	}
}


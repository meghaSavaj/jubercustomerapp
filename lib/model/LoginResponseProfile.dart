class LoginResponseProfile {
  String name;
  String first_name;
  String last_name;
  String email;
  String phonenumber;
  String id;

//  MyContibution();
  LoginResponseProfile.fromJson(Map<String, dynamic> json):
        name = json['name'],
        first_name = json['first_name'],
        last_name = json['last_name'],
        email = json['email'],
        phonenumber = json['phonenumber'],
        id = json['id'];
}


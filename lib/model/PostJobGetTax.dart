class PostJobGetTax {

  int status;
  String message;
  String tax;

	PostJobGetTax.fromJsonMap(Map<String, dynamic> map): 
		status = map["status"],
		message = map["message"],
		tax = map["tax"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		data['message'] = message;
		data['tax'] = tax;
		return data;
	}
}

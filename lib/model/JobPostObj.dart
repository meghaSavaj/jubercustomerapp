
class JobPostObj {

  int job_id;
  int is_today;


	JobPostObj.fromJsonMap(Map<String, dynamic> map):
				job_id = map["job_id"],
			is_today = map["is_today"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['job_id'] = job_id;
		data['is_today'] = is_today;

		return data;
	}
}

import 'package:json_annotation/json_annotation.dart';

@JsonSerializable(nullable: false)
class MyRequestListObj {

  String id;
  String first_name;
  String last_name;




	MyRequestListObj.fromJsonMap(Map<String, dynamic> map):
				id = map["id"],
				first_name = map["first_name"],
				last_name = map["last_name"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();

		data['id'] = id;
		data['first_name'] = first_name;
		data['last_name'] = last_name;


		return data;
	}
}


class VehicleCategory {

  int id;
  String category_name;
  String rate_per_km;
  String min_price;
  String rate_per_km_with_currency;
  String min_price_with_currency;
  String image;

	VehicleCategory.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		category_name = map["category_name"],
		rate_per_km = map["rate_per_km"],
	  min_price = map["min_price"],
				rate_per_km_with_currency = map["rate_per_km_with_currency"],
				min_price_with_currency = map["min_price_with_currency"],
		image = map["image"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['category_name'] = category_name;
		data['rate_per_km'] = rate_per_km;
		data['min_price'] = min_price;
		data['rate_per_km_with_currency'] = rate_per_km_with_currency;
		data['min_price_with_currency'] = min_price_with_currency;
		data['image'] = image;
		return data;
	}
}

import 'package:arabacustomer/model/JobPostObj.dart';

class JobStatusMsgResponse {

  int status;
  String message;
	JobPostObj data;

	JobStatusMsgResponse.fromJsonMap(Map<String, dynamic> map):
		status = map["status"],
		message = map["message"],
				data = JobPostObj.fromJsonMap(map["data"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		data['message'] = message;
		return data;
	}
}

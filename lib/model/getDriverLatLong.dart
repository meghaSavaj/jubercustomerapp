
class GetDriverLatLong {

  int id;
  String gprs_lat;
  String gprs_lng;
  String gprs_address;
  String pickup_lattitude;
  String pickup_longitude;
  String delivery_lattitude;
  String delivery_longitude;
  String status;

	GetDriverLatLong.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		gprs_lat = map["gprs_lat"],
		gprs_lng = map["gprs_lng"],
		gprs_address = map["gprs_address"],
		pickup_lattitude = map["pickup_lattitude"],
		pickup_longitude = map["pickup_longitude"],
		delivery_lattitude = map["delivery_lattitude"],
		delivery_longitude = map["delivery_longitude"],
		status = map["status"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['gprs_lat'] = gprs_lat;
		data['gprs_lng'] = gprs_lng;
		data['gprs_address'] = gprs_address;
		data['pickup_lattitude'] = pickup_lattitude;
		data['pickup_longitude'] = pickup_longitude;
		data['delivery_lattitude'] = delivery_lattitude;
		data['delivery_longitude'] = delivery_longitude;
		data['status'] = status;
		return data;
	}
}

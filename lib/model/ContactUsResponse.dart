class ContactUsResponse {
  int status;
  String message;
  String data;
  String email;
  String email2;
  String phone2;
  String address;
  String phone;

  ContactUsResponse.fromJsonMap(Map<String, dynamic> map)
      : status = map["status"],
        message = map["message"],
        data = map["data"],
        email = map["email"],
        email2 = map["email2"],
        phone2 = map["phone2"],
        address = map["address"],
        phone = map["phone"];

/*Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		data['message'] = message;
		return data;
	}*/
}

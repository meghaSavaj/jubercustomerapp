
class GetOtpResponse {

  int status;
  String message;
  int otp;

	GetOtpResponse.fromJsonMap(Map<String, dynamic> map): 
		status = map["status"],
		message = map["message"],
		otp = map["otp"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		data['message'] = message;
		data['otp'] = otp;
		return data;
	}
}

class AboutUsResponse {

  int status;
  String message;
  String data;

	AboutUsResponse.fromJsonMap(Map<String, dynamic> map):
		status = map["status"],
		message = map["message"],
	  data = map["data"];

	/*Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		data['message'] = message;
		return data;
	}*/
}


class NotificationUnReadCount {

  int status;
  String message;
  String unread_count;

	NotificationUnReadCount.fromJsonMap(Map<String, dynamic> map): 
		status = map["status"],
		message = map["message"],
		unread_count = map["unread_count"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		data['message'] = message;
		data['unread_count'] = unread_count;
		return data;
	}
}

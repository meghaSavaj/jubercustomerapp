import 'LoginUserObj.dart';

class SocialLoginResponse {

  int status;
  int profile_complete;
  int first_time_login;
  String message;
	LoginUserObj data;

	SocialLoginResponse.fromJsonMap(Map<String, dynamic> map): 
		status = map["status"],
		profile_complete = map["profile_complete"],
		first_time_login = map["first_time_login"],
		message = map["message"],
		data = LoginUserObj.fromJsonMap(map["data"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		data['profile_complete'] = profile_complete;
		data['first_time_login'] = first_time_login;
		data['message'] = message;
//		data['data'] = data == null ? null : data.toJson();
		return data;
	}
}

//import 'package:araba_customer/model/LoginUserObj.dart';



class MyWalletResponseObj {

  int status;
  String message;
	int wallet_balance;
	int max_amount_use_from_wallet;



				MyWalletResponseObj.fromJsonMap(Map<String, dynamic> map):
		status = map["status"],
		message = map["message"],
				wallet_balance = map["wallet_balance"],
				max_amount_use_from_wallet = map["max_amount_use_from_wallet"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		data['message'] = message;
		data['wallet_balance'] = wallet_balance;
		data['max_amount_use_from_wallet'] = max_amount_use_from_wallet;

		return data;
	}
}

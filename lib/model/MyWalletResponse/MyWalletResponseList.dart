
import 'MyWalletResponseListObj.dart';

class MyWalletResponseList {

  int status;
	String message = "";
  List<MyWalletResponseListObj> data;
  int payabel_amount;
  String my_reference_code;


	MyWalletResponseList.fromJsonMap(Map<String, dynamic> map):
		status = map["status"],
				message = map["message"],
		data = List<MyWalletResponseListObj>.from(map["data"].map((it) => MyWalletResponseListObj.fromJsonMap(it))),
				payabel_amount = map["payabel_amount"],
				my_reference_code = map["my_reference_code"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		data['message'] = message;
		data['data'] = data != null ?
			this.data.map((v) => v.toJson()).toList()
			: null;
		data['payabel_amount'] = payabel_amount;
		data['my_reference_code'] = my_reference_code;
		return data;
	}
}

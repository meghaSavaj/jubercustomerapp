
class MyWalletResponseListObj {

  String amount;
  String is_paid;
  String text;

	MyWalletResponseListObj.fromJsonMap(Map<String, dynamic> map):
				amount = map["amount"],
				is_paid = map["is_paid"],
				text = map["text"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['amount'] = amount;
		data['is_paid'] = is_paid;
		data['text'] = text;

		return data;
	}
}

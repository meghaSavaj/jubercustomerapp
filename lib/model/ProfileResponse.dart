
class ProfileResponse {

  Object id;
  Object title;
  String first_name;
  Object middle_name;
  String last_name;
  Object gender;
  Object username;
  Object role;
  Object is_proffessional;
  Object driver_transporter_id;
  Object approved;
  Object auth_key;
  Object password_hash;
  Object password_reset_token;
  String email;
	String profile_pic;
  String address;
  Object country;
  Object state;
  Object street;
  Object pincode;
  Object latitude;
  Object longitude;
  String phone_number;
  Object birth_date;
  Object business_name;
  Object business_description;
  Object type_of_cmpany;
  Object vat_registered;
  Object work_area;
  Object certifications;
  Object licence;
  Object device_token;
  Object device_type;
  int status;
  Object created_at;
  Object updated_at;
  String profile_pic_path;

	ProfileResponse.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		title = map["title"],
		first_name = map["first_name"],
		middle_name = map["middle_name"],
		last_name = map["last_name"],
		gender = map["gender"],
		username = map["username"],
		role = map["role"],
		is_proffessional = map["is_proffessional"],
		driver_transporter_id = map["driver_transporter_id"],
		approved = map["approved"],
		auth_key = map["auth_key"],
		password_hash = map["password_hash"],
		password_reset_token = map["password_reset_token"],
		email = map["email"],
		profile_pic = map["profile_pic"],
		address = map["address"],
		country = map["country"],
		state = map["state"],
		street = map["street"],
		pincode = map["pincode"],
		latitude = map["latitude"],
		longitude = map["longitude"],
		phone_number = map["phone_number"],
		birth_date = map["birth_date"],
		business_name = map["business_name"],
		business_description = map["business_description"],
		type_of_cmpany = map["type_of_cmpany"],
		vat_registered = map["vat_registered"],
		work_area = map["work_area"],
		certifications = map["certifications"],
		licence = map["licence"],
		device_token = map["device_token"],
		device_type = map["device_type"],
		status = map["status"],
		created_at = map["created_at"],
		updated_at = map["updated_at"],
		profile_pic_path = map["profile_pic_path"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['title'] = title;
		data['first_name'] = first_name;
		data['middle_name'] = middle_name;
		data['last_name'] = last_name;
		data['gender'] = gender;
		data['username'] = username;
		data['role'] = role;
		data['is_proffessional'] = is_proffessional;
		data['driver_transporter_id'] = driver_transporter_id;
		data['approved'] = approved;
		data['auth_key'] = auth_key;
		data['password_hash'] = password_hash;
		data['password_reset_token'] = password_reset_token;
		data['email'] = email;
		data['profile_pic'] = profile_pic;
		data['address'] = address;
		data['country'] = country;
		data['state'] = state;
		data['street'] = street;
		data['pincode'] = pincode;
		data['latitude'] = latitude;
		data['longitude'] = longitude;
		data['phone_number'] = phone_number;
		data['birth_date'] = birth_date;
		data['business_name'] = business_name;
		data['business_description'] = business_description;
		data['type_of_cmpany'] = type_of_cmpany;
		data['vat_registered'] = vat_registered;
		data['work_area'] = work_area;
		data['certifications'] = certifications;
		data['licence'] = licence;
		data['device_token'] = device_token;
		data['device_type'] = device_type;
		data['status'] = status;
		data['created_at'] = created_at;
		data['updated_at'] = updated_at;
		data['profile_pic_path'] = profile_pic_path;
		return data;
	}
}

import 'package:json_annotation/json_annotation.dart';

@JsonSerializable(nullable: false)
class DriverLocationObj {


  String driver_name;
  String latitude;
  String longitude;
  String phone_number;

	DriverLocationObj.fromJsonMap(Map<String, dynamic> map):
				driver_name = map["driver_name"],
				latitude = map["latitude"],
				longitude = map["longitude"],
				phone_number = map["phone_number"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();

		data['driver_name'] = driver_name;
		data['latitude'] = latitude;
		data['longitude'] = longitude;
		data['phone_number'] = phone_number;
		return data;
	}
}


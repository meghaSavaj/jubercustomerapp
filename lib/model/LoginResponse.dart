//import 'package:araba_customer/model/LoginUserObj.dart';

import 'LoginUserObj.dart';

class LoginResponse {

  int status;
  int profile_complete;
  int is_corporate_profile;
  String message;
	LoginUserObj data;

	LoginResponse.fromJsonMap(Map<String, dynamic> map): 
		status = map["status"],
		profile_complete = map["profile_complete"],
				is_corporate_profile = map["is_corporate_profile"],
		message = map["message"],
		data = LoginUserObj.fromJsonMap(map["data"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		data['profile_complete'] = profile_complete;
		data['is_corporate_profile'] = is_corporate_profile;
		data['message'] = message;
//		data['data'] = Data.toJson();
//		data['data'] = data == null ? null : data.toJson();
		return data;
	}
}

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/Utils/Utility.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/SideMenu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:toast/toast.dart';

import 'MapAndSearchbarWithPlaceApi.dart';
import 'Utils/Consts.dart';
import 'Utils/MyPreferenceManager.dart';
import 'dialog/VehicleList.dart';
import 'package:http/http.dart' as http;
import 'model/CommanModal.dart';
import 'model/DriverLocationObj.dart';
import 'model/LoginResponse.dart';
import 'package:geocoder/services/distant_google.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';

class PostJob1New extends StatefulWidget {
  static const routeName = '/PostJob1New';

  @override
  PostJob1NewState createState() {
    /* final ph = PermissionHandler();
    final requested = await ph.requestPermissions(
        [PermissionGroup.locationAlways, PermissionGroup.locationWhenInUse]);

    final alwaysGranted =
        requested[PermissionGroup.locationAlways] == PermissionStatus.granted;
    final whenInUseGranted = requested[PermissionGroup.locationWhenInUse] ==
        PermissionStatus.granted;*/

    // You can request multiple permissions at once.

    return PostJob1NewState();
  }


}

class PostJob1NewState extends State<PostJob1New>
    implements CallbackOfvlistdDialog {
  Completer<GoogleMapController> _mapController = Completer();

  final TextEditingController dateValue = TextEditingController();
  final TextEditingController jobnameValue = TextEditingController();
  final TextEditingController vahicle_namec = TextEditingController();

  bool IsStartWithDate = false;
  bool IsTimeSelected = false;

  String selectedDAte = '';
  String vahicle_name = '';
  String IsPersonal = '';
  String img = '';
  String rat_per_km = '0.0';
  String min_price = '0.0';
  int vahicleid;
  CameraPosition _position = _kInitialPosition;
  String _address;
  var selectedItemPostionOfUserType = 0;
  Set<Marker> markers = Set();
  bool IsSourceFocused = true;




  @override
  void dispose() {
    dateValue.dispose();
    jobnameValue.dispose();
    vahicle_namec.dispose();
    super.dispose();

  }

  final CameraPosition _initialCamera = CameraPosition(
    target: LatLng(
   // target: LatLng(20.5937, 78.9629),
      CommonWidget.returnAlgeriaLatWhenGettingBlanck(
          Consts.currentLat.toString()),
      CommonWidget.returnAlgeriaLongWhenGettingBlanck(
          Consts.currentLong.toString())),
    zoom: 14,
  );
  static final CameraPosition _kInitialPosition = CameraPosition(
    target: LatLng(
        CommonWidget.returnAlgeriaLatWhenGettingBlanck(
            Consts.currentLat.toString()),
        CommonWidget.returnAlgeriaLongWhenGettingBlanck(
            Consts.currentLong.toString())),
    zoom: 14.0,
  );

//  CommonWidget.returnAlgeriaLatWhenGettingBlanck
  final Map<String, Marker> _markers = {};

  void _getLocation() async {
//    var currentLocation = await Geolocator()
//        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    LocationData _locationData = await location.getLocation();

    LatLng latLng = new LatLng(_locationData.latitude, _locationData.longitude);
    CameraUpdate cameraUpdate = CameraUpdate.newLatLngZoom(latLng, 14);
    final GoogleMapController controller = await _mapController.future;

    controller.animateCamera(cameraUpdate);
    Consts.currentLat = _locationData.latitude;
    Consts.currentLong = _locationData.longitude;
    getDriverLocationListData();
  }
  final GlobalKey<ScaffoldState> _scaffoldKey =  GlobalKey<ScaffoldState>();

  Future<List<DriverLocationObj>> getDriverLocationListData() async {

    var map = new Map<String, dynamic>();
//    map[Api_constant.phone_number] = widget.phoneNumber.trim();
    map['live_latitude'] = Consts.currentLat.toString();
    map['live_longitude'] = Consts.currentLong.toString();

    print(map.toString() + "get_driver_locations..............");
    print(Api_constant.get_driver_locations);

    var response = await http.post(Api_constant.get_driver_locations, body: map);

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var CommanModal_obj = new CommanModal.fromJson(userMap) as CommanModal;
      if(CommanModal_obj.status == 1) {
        List<Object> resultList = CommanModal_obj.data;

        List<DriverLocationObj> myDriverList = new List(resultList.length);
        markers.clear();
        for (int i = 0; i < resultList.length; i++) {
          Object obj = resultList[i];
          try{
            DriverLocationObj g = new DriverLocationObj.fromJsonMap(obj);
            myDriverList[i] = g;

            var dLattiude = double.parse(myDriverList[i].latitude);
            var dLongitude = double.parse(myDriverList[i].longitude);
            final icon = await BitmapDescriptor.fromAssetImage(
                ImageConfiguration(size: Size(24, 24)), 'assets/images/car.png');
            var markerIdVal = markers.length + 1;
            String mar = markerIdVal.toString();
            MarkerId markerId = MarkerId(mar);
            Marker marker2=Marker(markerId: markerId,position: LatLng(dLattiude, dLongitude),
              infoWindow: InfoWindow(
                title: myDriverList[i].driver_name,onTap: (){},
                //snippet: myDriverList[i].phone_number
              ),
              // icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
              icon: icon,
            );
            markers.add(marker2);
          } catch(e){
            print( "CompleteListException.............."+e.toString());
          }
        }

        return myDriverList;
      } else {
//        Toast.show(Api_constant.no_record_found, context,
//            gravity: Toast.CENTER);

        return new List<DriverLocationObj>();
      }

    } else {
      Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
      return new List<DriverLocationObj>();
    }
  }

  Future _getAddress() async {
    List<Placemark> placemark = await Geolocator().placemarkFromCoordinates(
        Consts.currentLat, Consts.currentLong); // fr_FR, fr_DZ, DZA 	fre/fra
//        _position.target.latitude, _position.target.longitude, localeIdentifier: 'fr_DZ'); // fr_FR, fr_DZ, DZA 	fre/fra
    print(placemark.elementAt(0).locality);
    print(Consts.currentLat);
    final coordinates =
    new Coordinates(_position.target.latitude, _position.target.longitude);

    print(coordinates.toString() + "kkkkkkkkkkkkkkkkkkkk");

    getAddressFromLatlong2(coordinates);

  }
  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  Future getAddressFromLatlong2(Coordinates coordinates) async {

    var addresses = await GoogleGeocoding(Consts.googleMapApiKEY, language: 'en').findAddressesFromCoordinates(coordinates); //final

    _address = addresses.first.addressLine;
    print(_address);
    setState(() {
      Consts.pickup_lattitude = coordinates.latitude.toString();
      Consts.pickup_longitude = coordinates.longitude.toString();
//        sourcePlaceHolder = _address;
      Consts.sourceAdd = _address;
      Consts.delivery_lattitude = coordinates.latitude.toString();
      Consts.delivery_long = coordinates.longitude.toString();
//        destiPlaceHolder = _address;
      Consts.dstAdd = _address;
      // _keysource.currentState.setSerachText(_address);

      showToast(_address, gravity: Toast.CENTER);


    });
  }

  @override
  Widget build(BuildContext context) {
//    Toast.show(widget.myRequestListObj.toString(), context,
//        gravity: Toast.CENTER);
    return Scaffold(
        key: _scaffoldKey,

//          appBar: AppBar(
////            centerTitle: true,
////            title: CommonWidget.getActionBarTitleText(Api_constant.POST_A_JOB),
//            backgroundColor: Colors.transparent,
//              toolbarOpacity: 0.0,
//            iconTheme: new IconThemeData(color: Colors.green),
//
//          ),
//          extendBodyBehindAppBar: true,
        drawer: MainDrawer(context),
        body:  FutureBuilder<List<DriverLocationObj>>(
            future: getDriverLocationListData(),
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return Center(child: CircularProgressIndicator());
        return Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Expanded(
//                flex: 1,
//                  height: MediaQuery.of(context).size.height * 0.67,
                  child: GoogleMap(
                    mapType: MapType.normal,
                    initialCameraPosition: _initialCamera,
                    myLocationEnabled: true,
                    myLocationButtonEnabled: true,
                    onMapCreated: (GoogleMapController controller) {
                     // _mapController.complete(controller);
                    controller.animateCamera(CameraUpdate.newLatLng(LatLng(
                          CommonWidget
                              .returnAlgeriaLatWhenGettingBlanck(
                              Consts.currentLat.toString()),
                          CommonWidget
                              .returnAlgeriaLongWhenGettingBlanck(
                              Consts.currentLong.toString()))));
                      _mapController.complete(controller);
                    },
                    onCameraMove: ((pinPosition) {
                      _position = pinPosition;
                    }),
                    onCameraIdle: _getAddress,

                    gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                      new Factory<OneSequenceGestureRecognizer>(
                            () => new EagerGestureRecognizer(),
                      ),
                    ].toSet(),
                    markers: markers,
//                  markers: _markers.values.toSet(),
                  ),
                ),
                Wrap(
//                  mainAxisAlignment: MainAxisAlignment.start,
//                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
//                    height: 3,
                      color: Colors.blueGrey[100],
                    ),
//                    Container(
//                      width: double.infinity,
//                      height: 50.0,
//                      color: Colors.pink[50],
//                      child: Padding(
//                        padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
//                        child: TextField(
//                          style: TextStyle(
//                              fontSize: 18,
//                              color: Colors.black87,
//                              fontWeight: FontWeight.w500
//                          ),
//                          decoration: InputDecoration(
//                              border: InputBorder.none,
//                              focusedBorder: InputBorder.none,
//                              enabledBorder: InputBorder.none,
//                              errorBorder: InputBorder.none,
//                              disabledBorder: InputBorder.none,
//                              hintText: Api_constant.Job_Name,
//                              hintStyle: TextStyle(
//                                  fontSize: 18,
//                                  color: Colors.grey[600],
//                                  fontWeight: FontWeight.w500
//                              )),
//                          onChanged: (val) {
//                            Consts.job_name = val;
//                          },
//                        ),
//                      ),
//                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(15, 0, 15, 10),
                      child: Column(
                        children: <Widget>[
                          Visibility(
                            visible: false,
                            child: Column(
                              children: <Widget>[
                                headerLayout(Api_constant.JOB_NAME,
                                    'assets/images/post_job/comments.png'),
                                Padding(
                                  padding: EdgeInsets.only(left: 50),
                                  child: TextField(
//                                      maxLength: Consts.TextField_maxLength_forname,
                                    decoration: InputDecoration(
                                      hintText: Api_constant.Job_Name,
                                      hintStyle: TextStyle(
                                          color: Colors.grey[600], fontSize: 15),
                                    ),
                                    controller: jobnameValue,
//                                          onChanged: (val) {
//                                            dateValue = val;
//                                          },
                                  ),
//                                        InkWell(
//                                          onTap: (){
//                                            _selectDate(context);
//                                          },
//                                          child:
//                                        )
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                headerLayout(Api_constant.VEHICLE_TYPE,
                                    'assets/images/post_job/vehicle.png'),
                                Padding(
                                    padding: EdgeInsets.only(left: 50),
                                    child: TextField(
                                      controller: vahicle_namec,
                                      onTap: () {
                                        print('========');
                                        /*showDialog(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        VehicleList(this)
//                                  SearchDialog(filtered_data, this),
                                );*/
                                        Navigator.of(context)
                                            .push(new MaterialPageRoute<Null>(
                                            builder: (BuildContext context) {
                                              return new VehicleList(this);
                                            },
                                            fullscreenDialog: true));
                                      },
                                      decoration: InputDecoration(
                                          hintText: Api_constant.Types_of_vehicle_required,
                                          hintStyle: TextStyle(
                                              color: Colors.grey[600], fontSize: 15)),
                                      readOnly: true,
                                    )),

                                SizedBox(
                                  height: 5,
                                ),
                                headerLayout(Api_constant.PICKUP_DATE,
                                    'assets/images/post_job/date.png'),

                                Padding(
                                  padding: EdgeInsets.only(left: 50),
                                  child: TextField(
                                    onTap: () {
                                      _selectDate(context);

                                    },
                                    readOnly: true,
//                                            focusNode: _focus,
                                    autofocus: false,
                                    decoration: InputDecoration(
                                      hintText: Api_constant.date_hint,
                                      hintStyle: TextStyle(
                                          color: Colors.grey[600], fontSize: 15),
                                    ),
                                    controller: dateValue,
//                                          onChanged: (val) {
//                                            dateValue = val;
//                                          },
                                  ),

                                ),

                              ],
                            ),
                          ),

                        ],
                      ),
                    ),

                    IsCorrporateUser(),

                    Padding(
                      padding: EdgeInsets.fromLTRB(10, 15, 15, 10),
                      child: SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: FlatButton(
//                            highlightColor: Colors.grey,
//                            splashColor: Colors.grey,
                          onPressed: () {
                            moveToNextScreen();

                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Text(Api_constant.where_to,
                                  style: TextStyle(
                                      fontSize: 21,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 10, 5, 10),
                                child: Container(
                                  width: 0.5,
                                  color: Colors.black26,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 7, 0, 7),
                                child: InkWell(
                                  child:  Container(
                                    decoration:  BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(color: Colors.white),
                                        borderRadius : BorderRadius.all(Radius.circular(15.0))

                                    ),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(width: 10,),
                                        Align(
                                            alignment: Alignment.center,
                                            child: Icon(
                                              Icons.watch_later,
                                              color: Colors.black87,
                                              size: 20,
                                            )
//                                          IconButton(
//                                            icon: ,
//                                            onPressed: () {},
//                                          ),
                                        ),
                                        SizedBox(width: 5,),
                                        Text(Api_constant.Now),
                                        SizedBox(width: 5,),
                                        Icon(
                                          Icons.keyboard_arrow_down,
                                          color: Colors.black,
                                          size: 14,
                                        ),
//                                        IconButton(
//                                          icon:
//                                          onPressed: () {},
//                                        )
                                        SizedBox(width: 10,),

                                      ],
                                    ),
                                  ),
                                  onTap: (){
//                                    _selectDate(context);
                                    DatePicker.showDateTimePicker(context,
                                        showTitleActions: true,
                                        minTime: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day,
                                            DateTime.now().hour, DateTime.now().minute),
                                        maxTime: DateTime(2101, 6, 7),
                                        onChanged: (date) {
                                          print('change $date');
                                        },
                                        onConfirm: (date) {
//                                          Consts.date
                                          selectedDAte = date.year.toString() +
                                              '-' +
                                              date.month.toString() +
                                              '-' +
                                              date.day.toString() +
                                              ' ' +
                                              date.hour.toString() +
                                              ':' +
                                              date.minute.toString() +
                                              ':00';

                                          print('confirm== '+Consts.date);
//                                          print('confirm== '+ Utility.convertdateFormat( Consts.date));

                                          moveToNextScreen();
                                        },
                                        currentTime: DateTime.now(), locale: LocaleType.en);
                                  },
                                ),
                              ),


                            ],
                          ),
                          color: Colors.grey[200],
                          shape: OutlineInputBorder(
                              borderSide: BorderSide(
                                  style: BorderStyle.solid,
                                  width: 1.0,
                                  color: Colors.grey[600]),
                              borderRadius: new BorderRadius.circular(8.0)),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 1,
//                          height: 15,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(15, 0, 0, 15),
                      child: Text(Api_constant.where_to_2ed_line, style: TextStyle(fontSize: 13, color: Colors.black87),),
                    )
                  ],
                )

              ],
            ),
            InkWell(
              child: Padding(
                  padding: EdgeInsets.fromLTRB(15, 40, 0, 0),
                  child:  Image(
                    image: AssetImage('assets/images/three_lines.png'),
                    height: 24,
                    width: 24, color: Colors.black
                  )
              ),
              onTap: (){
                if(_scaffoldKey.currentState.isDrawerOpen){
                  _scaffoldKey.currentState.openEndDrawer();
                } else{
                  _scaffoldKey.currentState.openDrawer();
                }
              },
            )
          ],
        );}));
  }

  void resetSavedData(){
    Consts.vehicle_id = '';
    Consts.vahicle_name = '';
    Consts.vehicle_img = '';
    Consts.date_bool = "";
    Consts.comments = "";
    Consts.distance = 0.0;
    Consts.rate_per_km = '';
    Consts.is_corporate = '';
    Consts.min_price = '';
    Consts.sourceAdd = "";
    Consts.dstAdd = "";
    Consts.delivery_lattitude = "0.0";
    Consts.delivery_long = "0.0";
    Consts.pickup_longitude = "0.0";
    Consts.pickup_lattitude = "0.0";
    Consts.date = '';
    Api_constant.Promo ='Promo';

  }

  Widget headerLayout(String title, String icon) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          CommonWidget.getIconImge(icon),
          SizedBox(
            width: 15,
          ),
          Expanded(
            child: CommonWidget.redHeaderLbl(title, context),
          ),
        ],
      ),
    );
  }

  Future<Null> _selectDate(BuildContext context) async {
    DateTime selectedDate = DateTime.now();
    final DateTime picked = await showDatePicker(
        context: context,
        helpText: Api_constant.SELECT_DATE,
//        locale: const Locale("en"),
        locale: const Locale("fr", "FR"),
        initialDate: selectedDate,
        firstDate: DateTime(
            DateTime.now().year, DateTime.now().month, DateTime.now().day),
        lastDate: DateTime(2101)
//        firstDate: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day),
//        lastDate: DateTime(2101),
//        locale: const Locale('fr', 'FR')
//        firstDate: null,
//        lastDate: null
        );

    if (picked != null
//            && picked != selectedDate
        )
      setState(() {
        selectedDate = picked;
        //2020-05-25 18:12:23
        dateValue.text = selectedDate.year.toString() +
            '-' +
            selectedDate.month.toString() +
            '-' +
            selectedDate.day.toString();

        IsTimeSelected =false;
        _pickTime(picked);
//           _showTimePicker();
      });
  }

  _pickTime(DateTime picked) async {
    TimeOfDay t = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child,
        );
      },
    );

    if (t != null) {
      var now = new DateTime.now();
//      var second = new DateTime.now();
      var earlier = new DateTime(picked.year, picked.month, picked.day, t.hour, t.minute, now.second, now.millisecond, now.microsecond);
//      var earlier = selected.subtract(const Duration(seconds: 5));
//      assert(earlier.isBefore(now));
      if(earlier.isBefore(now)){
        Toast.show(Api_constant.past_time_validation, context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      } else{
        setState(() {

          IsTimeSelected =true;

//        time = t; 2020-05-25 18:12:23
          // dateValue.text = dateValue.text + ' '+t.hour.toString()+':'+t.minute.toString()+':00';
          var dateStr = dateValue.text +
              ' ' +
              t.hour.toString() +
              ':' +
              t.minute.toString() +
              ':00';

          resetSavedData();

          Consts.date = dateStr;

          if(selectedItemPostionOfUserType == 1){
            Consts.is_corporate = '1';
          } else{
            Consts.is_corporate = '0';
          }


          dateValue.text = Utility.convertdateFormat(dateStr);

          if(selectedItemPostionOfUserType == 1){
            Consts.is_corporate = '1';
          } else{
            Consts.is_corporate = '0';
          }
          print(Consts.is_corporate + ".....is_corporate.........");

          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      MapAndSearchbarWithPlaceApi()));

          /*  var dateFormat = DateFormat(
            "yyyy-MM-dd HH:mm:ss"
        );

        DateTime dateTime = dateFormat.parse(dateStr);

        var timeFormat = DateFormat("MMM dd,yyyy hh:mm a");
        var finalDate = timeFormat.format(dateTime);*/

          //  print("actualDate===="+finalDate);
        });
      }

    }
  }

  void onChanged() {}

  @override
  void vlCallback(
      int id, String name, String _rate_per_km, String minPrice, String _img) {
    setState(() {
      vahicleid = id;
      vahicle_name = name;
      img = _img;
      vahicle_namec.text = name;
      rat_per_km = _rate_per_km;
      min_price = minPrice;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    EnableLocation();
    requestPermission();
    Consts.job_name = "";
    Consts.date = "";
    Consts.vehicle_id = "";
    Consts.vahicle_name = "";

    Consts.sourceAddCity = "";
    Consts.dstAddCity = "";

    Consts.sourcepostal_code = "";
    Consts.dstpostal_code = "";

    Consts.sourcestate = "";
    Consts.dststate = "";

    Consts.sourcecountry = "";
    Consts.dstcountry = "";
    UpdateToken();
  }

  Location location = new Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;

  Future<void> EnableLocation() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
//      Toast.show(_serviceEnabled.toString(), context, gravity: Toast.CENTER);
      if (!_serviceEnabled) {
        EnableLocation();
      } else{
        _getLocation();

      }
    } else{
      _getLocation();

    }
  }

  Future<void> requestPermission() async {
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
//      Toast.show(_permissionGranted.toString(), context, gravity: Toast.CENTER);
      if (_permissionGranted != PermissionStatus.granted) {
        requestPermission();
      } else{
        EnableLocation();
      }
    } else{
      EnableLocation();
    }
  }

  void moveToNextScreen() {
    print('confirm== moveToNextScreen'+Consts.date);
    resetSavedData();

    if(selectedDAte.isNotEmpty){
      Consts.date = selectedDAte;
    } else{
      Consts.date = selectedDAte = DateTime.now().year.toString() +
          '-' +
          DateTime.now().month.toString() +
          '-' +
          DateTime.now().day.toString() +
          ' ' +
          DateTime.now().hour.toString() +
          ':' +
          DateTime.now().minute.toString() +
          ':00';;
    }
    print(Consts.date + ".....date.........");

    if(selectedItemPostionOfUserType == 1){
      Consts.is_corporate = '1';
    } else{
      Consts.is_corporate = '0';
    }
    print(Consts.is_corporate + ".....is_corporate.........");

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                MapAndSearchbarWithPlaceApi()));
  }
  void moveToLatterNextScreen() {
    print('confirm== moveToNextScreen'+Consts.date);
    resetSavedData();

    if(selectedDAte.isNotEmpty){
      Consts.date = selectedDAte;
    } else{
      Consts.date = selectedDAte = DateTime.now().year.toString() +
          '-' +
          DateTime.now().month.toString() +
          '-' +
          DateTime.now().day.toString() +
          ' ' +
          DateTime.now().hour.toString() +
          ':' +
          DateTime.now().minute.toString() +
          ':00';
    }
    print(Consts.date + ".....date.........");

    if(selectedItemPostionOfUserType == 1){
      Consts.is_corporate = '1';
    } else{
      Consts.is_corporate = '0';
    }
    print(Consts.is_corporate + ".....is_corporate.........");

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                MapAndSearchbarWithPlaceApi()));
  }
  _getColorOfItem(int index) {
    if(selectedItemPostionOfUserType == -1){
      return Colors.grey[300];
    } else  if(index == selectedItemPostionOfUserType) {
      return Colors.deepOrangeAccent[700];
    } else {
      return Colors.grey[300];
//      return Colors.transparent;
    }
  }

  _getColorOfItem2(int index) {
    if(selectedItemPostionOfUserType == -1){
      return Colors.grey[100];
    } else  if(index == selectedItemPostionOfUserType) {
      return Colors.lime[50];
    } else {
      return Colors.grey[100];
    }
  }

  UserTypeWidget(String lbl, int type) {
    return  Container(
        width: double.infinity,
        height: 40,
        decoration: BoxDecoration(
          color: _getColorOfItem2(type),
          borderRadius: BorderRadius.all(Radius.circular(4.0)),
          border: Border.all(
            color: _getColorOfItem(type), //                   <--- border color
            width: 1.0,
          ),),
        child: InkWell(
          onTap: (){
            setState(() {
              selectedItemPostionOfUserType = type;
            });
          },
          child: Align(
            child: Padding(
              padding: EdgeInsets.fromLTRB(15,0,0,0),
              child:  Text(lbl),
            ),
            alignment: Alignment.centerLeft,
          ),
        )
    );
  }

  Widget IsCorrporateUser() {
     return FutureBuilder<bool>(
        future: CommonWidget.getIsCorporateUserFromPrefrence(),
        builder:(BuildContext context, AsyncSnapshot<bool> snapshot){
          if (snapshot.data == false){
            return  Padding(
              padding: EdgeInsets.fromLTRB(12, 0, 12, 0),
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child:  UserTypeWidget('Personal', 0),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    flex: 1,
                    child: UserTypeWidget('Corporate', 1),
                  )
                ],
              ),
            );
          }
          else{
            return Container();
          }
        }
    );
  }

//  String _format = 'HH:mm:ss';
//  void _showTimePicker() {
//    DateTime _dateTime;
//
//    DateTime now = DateTime.now();
//    String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);
//
//    String MIN_DATETIME = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);
////    const String MAX_DATETIME = '2021-11-25 22:45:10';
//    String MAX_DATETIME = DateFormat('yyyy-MM-dd').format(now)+'24:60:60';
//    String INIT_DATETIME = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);
//
////    TimeOfDay.now()
//    DatePicker.showDatePicker(
//      context,
//      minDateTime: DateTime.parse(MIN_DATETIME),
//      maxDateTime: DateTime.parse(MAX_DATETIME),
//      initialDateTime: DateTime.parse(INIT_DATETIME),
////      initialDateTime: DateTime.parse(INIT_DATETIME),
//      dateFormat: _format,
//      pickerMode: DateTimePickerMode.time, // show TimePicker
//      pickerTheme: DateTimePickerTheme(
//        title: Container(
//          decoration: BoxDecoration(color: Color(0xFFEFEFEF)),
//          width: double.infinity,
//          height: 56.0,
//          alignment: Alignment.center,
//          child: Text(
//            'custom Title',
//            style: TextStyle(color: Colors.green, fontSize: 24.0),
//          ),
//        ),
//        titleHeight: 56.0,
//      ),
//      onCancel: () {
//        debugPrint('onCancel');
//      },
//      onChange: (dateTime, List<int> index) {
//        setState(() {
////          _dateTime = dateTime;
//        });
//      },
//      onConfirm: (dateTime, List<int> index) {
//        setState(() {
//          _dateTime = dateTime;
//
////        time = t; 2020-05-25 18:12:23
//            // dateValue.text = dateValue.text + ' '+t.hour.toString()+':'+t.minute.toString()+':00';
//            var dateStr = dateValue.text +
//                ' ' +
//                dateTime.hour.toString() +
//                ':' +
//                dateTime.minute.toString() +
//                ':00';
//
//            Consts.date = dateStr;
//
//            dateValue.text = Utility.convertdateFormat(dateStr);
//
//            /*  var dateFormat = DateFormat(
//            "yyyy-MM-dd HH:mm:ss"
//        );
//
//        DateTime dateTime = dateFormat.parse(dateStr);
//
//        var timeFormat = DateFormat("MMM dd,yyyy hh:mm a");
//        var finalDate = timeFormat.format(dateTime);*/
//
//            //  print("actualDate===="+finalDate);
//
//        });
//      },
//    );
//  }

  Future<void> UpdateToken() async {
    var map = new Map<String, dynamic>();

    MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
    String token = _myPreferenceManager.getString(MyPreferenceManager.DEVICE_TOKEN);
    String id = _myPreferenceManager.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);
    map['user_id'] = id;
    map['device_token'] = token;

    if (Platform.isIOS) {
      map[Api_constant.DEVICE_TYPE] = Api_constant.DEVICE_TYPE_IOS;
    } else {
      map[Api_constant.DEVICE_TYPE] = Api_constant.DEVICE_TYPE;
    }

    print(map.toString() + "==update token");
    print(Api_constant.update_token);

    var response = await http.post(Api_constant.update_token, body: map);


    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
      new LoginResponse.fromJsonMap(userMap) as LoginResponse;

      if (loginResponseObj.status == 1) {

      } else {
        Toast.show(loginResponseObj.message, context,  duration: Toast.LENGTH_LONG,gravity: Toast.CENTER);
      }
    }

  }
}
import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/model/PostJobGetTax.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/MyRequestTabsScreen.dart';
import 'package:arabacustomer/widgets/SubmitButton.dart';
import 'package:arabacustomer/widgets/SubmitButtonRed.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:toast/toast.dart';

import 'AddPayment.dart';
import 'PostJob2.dart';
import 'Utils/Consts.dart';
import 'Utils/MyPreferenceManager.dart';
import 'Utils/Utility.dart';
import 'dialog/LocationScanningDialog.dart';
import 'map_request.dart';
import 'model/ApplyPromoCode.dart';
import 'model/CommanModal.dart';
import 'model/CommanModalWithObj.dart';
import 'model/JobStatusMsgResponse.dart';
import 'model/LoginResponse.dart';
import 'model/StatusMsgResponse.dart';
import 'model/Vehicle_category.dart';

class VehicleCategoryList extends StatefulWidget {
  static const routeName = '/VehicleCategoryList';

  @override
  VehicleCategoryListState createState() {
    return VehicleCategoryListState();
  }
}

class VehicleCategoryListState extends State<VehicleCategoryList>
    implements CallbackOfGetpAYMENTParam {
  Completer<GoogleMapController> _mapController = Completer();
  bool IsProgressIndicatorShow = false;
  bool promocodeApply = false;
  String promoCodeStr = '';

  String coupon_name = '';
  String coupon_type = '';
  String coupon_value = '';
  String coupon_amount = '';
  String coupon_id = '';

  static final String payment_type1 = 'Au Chargement';
  static final String payment_type2 = 'À la Livraison';
  static final String Payement_NONE = 'Payement';

  String dropdownValue = Payement_NONE;
  String taxValue = '';
  String PaymentTypeForPass = '';
  bool OnlinePaymentComplete = false;

  bool IsCorporateUser;

  String TotalPrice = '';
  double taxValuePrice = 0.0;
  String PromocodeDiscountPrice = '';
  String totalPriceBeforeApplyPromoCode = '';

  int selectedRadioTile = 1;
  bool radio1IsSelected = true;
  bool radio2IsSelected = false;

  GoogleMapController _controller;
  GoogleMapsServices _googleMapsServices = GoogleMapsServices();
  final Set<Polyline> _polyLines = {};

  Set<Polyline> get polyLines => _polyLines;

  BitmapDescriptor SourcepinLocationIcon;
  BitmapDescriptor DestipinLocationIcon;


  final CameraPosition _initialCamera = CameraPosition(
    target: LatLng(22.3143, 73.1587),
    zoom: 15.0000,
  );
  final Set<Marker> _markers = {};

  void _getLocation() async {
    var currentLocation = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);

    LatLng latLng =
        new LatLng(currentLocation.latitude, currentLocation.longitude);
    CameraUpdate cameraUpdate = CameraUpdate.newLatLngZoom(latLng, 16);
    final GoogleMapController controller = await _mapController.future;

    controller.animateCamera(cameraUpdate);
  }

  Future<List<VehicleCategory>> getVehicleCategoryListtData() async {
    setState(() {
      IsProgressIndicatorShow = true;
    });

    var api = '';

    MyPreferenceManager _myPreferenceManager =
        await MyPreferenceManager.getInstance();
    String CORPORATE_ID =
        _myPreferenceManager.getString(MyPreferenceManager.CORPORATE_ID);

    if (Consts.is_corporate == null || Consts.is_corporate.isEmpty) {
      api = Api_constant.get_service_api + '?corporate_id=${'0'}';
    } else {
      if (Consts.is_corporate == '1') {
        //corporate 6e
        api = Api_constant.get_service_api + '?corporate_id=${CORPORATE_ID}';
      } else {
        api = Api_constant.get_service_api + '?corporate_id=${'0'}';
      }
    }

    print(api + "...vehiclelist..api");
    var response = await http.get(api);
    setState(() {
      IsProgressIndicatorShow = false;
    });
    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var CommanModal_obj = new CommanModal.fromJson(userMap) as CommanModal;
      List<Object> resultList = CommanModal_obj.data;

      List<VehicleCategory> myContributionList = new List(resultList.length);

      for (int i = 0; i < resultList.length; i++) {
        Object obj = resultList[i];
        VehicleCategory g = new VehicleCategory.fromJsonMap(obj);
        myContributionList[i] = g;
      }

      setState(() {
        _MyRequestOpenObjList = myContributionList;
      });

      return myContributionList;
    } else {
      return new List<VehicleCategory>();
    }
  }

  List<VehicleCategory> _MyRequestOpenObjList = new List();
  List<String> PaymentList = new List();
  bool IsListSelect = false;
  int selectedItemPostion = -1;
  int selectedoaymentItemPostion = 0;

  @override
  void initState() {


    GetTax();
    try {
      BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.5),
              'assets/images/red_circle.png')
          .then((onValue) {
        SourcepinLocationIcon = onValue;
      });

      BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.5),
              'assets/images/black_marker.png')
          .then((onValue) {
        DestipinLocationIcon = onValue;
      });
    } catch (e) {
      print("pinLocationIcon===" + e.toString());
    }

    _onAddMarkerButtonPressed();
    getVehicleCategoryListtData();
    sendRequest();

    PaymentList.add('Cash');
    PaymentList.add('Card');

    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
//    GetTax();
  }

  void GetTax() async {
    var response = await http.get(Api_constant.postjob_gettax__api);
    print(Api_constant.postjob_gettax__api + "..............");

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var PostJobGetTax_obj =
          new PostJobGetTax.fromJsonMap(userMap) as PostJobGetTax;

      setState(() {
        taxValue = PostJobGetTax_obj.tax;
        TotalPrice = priceWitTax();
      });

      print(Consts.rate_per_km.toString() + 'test...........');
      print(Consts.distance.toString() + 'test...........');
      print(Consts.min_price.toString() + 'test...........');
      print(taxValue.toString() + 'test...........');
      print(Consts.pickup_lattitude.toString() + 'test...........');
      print(Consts.pickup_longitude.toString() + 'test...........');
      print(Consts.delivery_lattitude.toString() + 'test...........');
      print(Consts.delivery_long.toString() + 'test...........');
      // Toast.show(PostJobGetTax_obj.tax, context, gravity: Toast.CENTER);

    } else {
      setState(() {
        taxValue = "";
//        TotalPrice = priceWithoutTax(Consts.min_price.toString(),Consts.rate_per_km.toString());
      });
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  List<LatLng> _convertToLatLng(List points) {
    List<LatLng> result = <LatLng>[];
    for (int i = 0; i < points.length; i++) {
      if (i % 2 != 0) {
        result.add(LatLng(points[i - 1], points[i]));
      }
    }
    return result;
  }

  void sendRequest() async {
    LatLng destination = LatLng(double.parse(Consts.delivery_lattitude),
        double.parse(Consts.delivery_long));
    LatLng origin = LatLng(double.parse(Consts.pickup_lattitude),
        double.parse(Consts.pickup_longitude));
    String route =
        await _googleMapsServices.getRouteCoordinates(origin, destination);
    print("route===" + route);
    createRoute(route);
    _addMarker(destination, Consts.dstAdd);
  }

  void createRoute(String encondedPoly) {
    _polyLines.add(Polyline(
        polylineId: PolylineId("Track"),
        width: 4,
        points: _convertToLatLng(_decodePoly(encondedPoly)),
        color: Colors.red));

    setState(() {});
    // print("_polyLines"+_polyLines.toString());
  }

  void _addMarker(LatLng location, String address) {
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId("112"),
          position: location,
          // infoWindow: InfoWindow(title: address, snippet: "go here"),
          infoWindow: InfoWindow(title: address),
       //   icon: DestipinLocationIcon));
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen)
            ),);

      var bounds = getBounds(_markers);
      CameraUpdate u2 = CameraUpdate.newLatLngBounds(bounds, 50);
      _controller.animateCamera(u2).then((void v) {
        check(u2, this._controller);
      });
    });
  }

  List _decodePoly(String poly) {
    var list = poly.codeUnits;
    var lList = new List();
    int index = 0;
    int len = poly.length;
    int c = 0;
    do {
      var shift = 0;
      int result = 0;

      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      } while (c >= 32);
      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lList.add(result1);
    } while (index < len);

    for (var i = 2; i < lList.length; i++) lList[i] += lList[i - 2];

    print(lList.toString());

    return lList;
  }

  void _onAddMarkerButtonPressed() {
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId("111"),
          position: LatLng(double.parse(Consts.pickup_lattitude),
              double.parse(Consts.pickup_longitude)),
          // infoWindow: InfoWindow(title: address, snippet: "go here"),
          infoWindow: InfoWindow(title: Consts.sourceAdd),
          icon: SourcepinLocationIcon));
    });
  }

  LatLngBounds getBounds(Set<Marker> markers) {
    var lngs = markers.map<double>((m) => m.position.longitude).toList();
    var lats = markers.map<double>((m) => m.position.latitude).toList();

    double topMost = lngs.reduce(max);
    double leftMost = lats.reduce(min);
    double rightMost = lats.reduce(max);
    double bottomMost = lngs.reduce(min);

    LatLngBounds bounds = LatLngBounds(
      northeast: LatLng(rightMost, topMost),
      southwest: LatLng(leftMost, bottomMost),
    );

    return bounds;
  }

  String priceWithoutTax(String minPrice, String ratePerkm) {
    double priceWitTaxValue;
    print(minPrice + "minPrice");
    print(ratePerkm + "ratePerkm");
    double DR = ((Consts.distance) * double.parse(ratePerkm));
    print(Consts.distance.toString() + "Distance");
    print(DR.toString() + "DR");

    if (DR < double.parse(minPrice)) {
      priceWitTaxValue = double.parse(minPrice);
    } else {
      priceWitTaxValue = DR;
    }

////    (distance * per km rate)+min_price
//    return priceWithoutTaxValue.toStringAsFixed(2);

//    double priceWithoutTaxValue = ((Consts.distance) *
//        double.parse(Consts.rate_per_km)) + double.parse(Consts.min_price);
    return priceWitTaxValue.toStringAsFixed(2);
  }

  String priceWitTax() {
    double priceWitTaxValue;

    double DR = ((Consts.distance) * double.parse(Consts.rate_per_km));

    if (DR < double.parse((Consts.min_price))) {
      priceWitTaxValue = double.parse((Consts.min_price));
    } else {
      priceWitTaxValue = DR;
    }

    // double priceWitTaxValue = ((Consts.distance) * double.parse(Consts.rate_per_km)) + double.parse(Consts.min_price);
    double finalVal = (priceWitTaxValue * double.parse(taxValue)) / 100;
    double total_price = finalVal + priceWitTaxValue;

    print('testingggg distance ' + Consts.distance.toString());
    print('testingggg rate_per_km ' + Consts.rate_per_km.toString());
    print('testingggg DR ' + DR.toString());
    print('testingggg min_price ' + Consts.min_price.toString());
    print('testingggg finalVal ' + finalVal.toString());
    print('testingggg priceWitTaxValue ' + priceWitTaxValue.toString());
    print('testingggg total_price ' + total_price.toString());
    print('testingggg finl ' + total_price.toStringAsFixed(2).toString());

    return total_price.toStringAsFixed(2);
  }

  int priceWitTaxForPassInOnlinePayment() {
    double priceWitTaxValue;

    double DR = ((Consts.distance) * double.parse(Consts.rate_per_km));

    if (DR < double.parse((Consts.min_price))) {
      priceWitTaxValue = double.parse((Consts.min_price));
    } else {
      priceWitTaxValue = DR;
    }

    // double priceWitTaxValue = ((Consts.distance) * double.parse(Consts.rate_per_km)) + double.parse(Consts.min_price);
    double finalVal = (priceWitTaxValue * double.parse(taxValue)) / 100;
    double total_price = finalVal + priceWitTaxValue;

    double finalprice = total_price * double.parse('100');

    print('testingggg total_price ' + total_price.toString());
    print('testingggg taxValue ' + taxValue.toString());
    print('testingggg finl ' + finalprice.toString());

    return finalprice.toInt();
    // return finalprice.round();
  }

  @override
  Widget build(BuildContext context) {
//    Toast.show(widget.myRequestListObj.toString(), context,
//        gravity: Toast.CENTER);

    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: GoogleMap(
                      polylines: polyLines,
                      markers: _markers,
                      mapType: MapType.normal,
                      initialCameraPosition: CameraPosition(
                        target: LatLng(double.parse(Consts.pickup_lattitude),
                            double.parse(Consts.pickup_longitude)),
                        zoom: 13,
                      ),
                      myLocationEnabled: true,
                      myLocationButtonEnabled: true,
                      onMapCreated: (GoogleMapController controller) {
                        _controller = controller;
                        _mapController.complete(controller);
                        //   _getLocation();
                        // sourceDestinationMarkerSet();
                      },
                      // markers: _markers.values.toSet(),
                    ),
                  ),
                  Expanded(
                      flex: 1,
                      child: Container(
                        color: Colors.black12,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15.0),
                                topRight: Radius.circular(15.0)),
                          ),
                          child: Stack(
                            alignment: Alignment.bottomRight,
                            children: <Widget>[
                              Align(
                                  alignment: Alignment.topLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                                    child: Text(
                                      'CHOOSE VEHICLE',
                                      style: TextStyle(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700),
                                    ),
                                  )),
                              // getCoporateUSerWidget(),
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(10, 40, 10, 0),
                                  child: ListView.builder(
                                    itemBuilder: (context, index) {
                                      return Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            child: ListTile(
                                              onTap: () {
                                                Consts.vahicle_name =
                                                    _MyRequestOpenObjList[index]
                                                        .category_name
                                                        .toString();
                                                Consts.vehicle_id =
                                                    _MyRequestOpenObjList[index]
                                                        .id
                                                        .toString();
                                                Consts.vehicle_img =
                                                    _MyRequestOpenObjList[index]
                                                        .image
                                                        .toString();
                                                Consts.rate_per_km =
                                                    _MyRequestOpenObjList[index]
                                                        .rate_per_km
                                                        .toString();
                                                Consts.min_price =
                                                    _MyRequestOpenObjList[index]
                                                        .min_price
                                                        .toString();

                                                IsListSelect = true;
                                                setState(() {
                                                  selectedItemPostion = index;
                                                });
                                              },
                                              trailing: SizedBox(
                                                child: Image.network(
                                                    _MyRequestOpenObjList[index]
                                                        .image),
                                                width: 50,
                                                height: 45,
                                              ),
                                              title: Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 0, 0, 8),
                                                child: Text(
                                                    _MyRequestOpenObjList[index]
                                                        .category_name
                                                        .toString(),
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 14)),
                                              ),

                                              /*subtitle: Text(
                                                Api_constant.rate_per_km +
                                                    _MyRequestOpenObjList[index]
                                                        .rate_per_km_with_currency
                                                        .toString(),
                                                style: TextStyle(fontSize: 16),
                                              ),*/
                                              subtitle: getPriceWidget(
                                                  _MyRequestOpenObjList[index]
                                                      .min_price
                                                      .toString(),
                                                  _MyRequestOpenObjList[index]
                                                      .rate_per_km
                                                      .toString()),
                                            ),
                                            decoration: BoxDecoration(
                                              color: _getColorOfItem2(index),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(4.0)),
                                              border: Border.all(
                                                color: _getColorOfItem(index),
                                                //                   <--- border color
                                                width: 1.0,
                                              ),
                                            ),
//                                        color: _getColorOfItem(index),
                                          ),
                                          Container(
                                            height: 10,
                                            color: Colors.white,
                                          ),
//
                                        ],
                                      );
                                    },
                                    itemCount: _MyRequestOpenObjList.length,
                                  ),
                                ),
                              ),
                              Center(
                                child: Visibility(
                                  visible: IsProgressIndicatorShow,
                                  child: SizedBox(
                                    child: CircularProgressIndicator(),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      )),
                  SizedBox(height: 10,),
                  Column(
                    children: <Widget>[
                      Divider(
                        thickness: 2,
                        color: Colors.white,
                      ),
                      getPersonalUSerWidget(),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20, 10, 20, 5),
                        child: SubmitButtonRed(
                            Api_constant.book_now, onChanged, 10),
//                        SubmitButton(Api_constant.Confirm_JuberGo, onChanged, 10),
                      ),
                    ],
                  )
                ],
              ),
              Align(
                alignment: Alignment.topLeft,
                child: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.black,
                      size: 20,
                    ),
                    onPressed: () => Navigator.pop(context)),
              )
            ],
          ),
        ));
  }

  Future<void> ApigetPromocodeValue() async {
    MyPreferenceManager _myPreferenceManager =
        await MyPreferenceManager.getInstance();
    String id = _myPreferenceManager
        .getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);

//    final uri = Uri.encodeFull(Api_constant.applycoupon +
//        '?coupon_name=${promoCodeStr}'+
//        '&customer_id=${id}');

    var map = new Map<String, dynamic>();
    map['coupon_name'] = promoCodeStr;
    map['customer_id'] = id;

    print(Api_constant.applycoupon.toString() + "..............");
    print(map.toString() + "..............");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.post(Api_constant.applycoupon, body: map);

    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var CommanModal_obj =
          new CommanModalWithObj.fromJson(userMap) as CommanModalWithObj;
      if (CommanModal_obj.status == 1) {
        Object obj = CommanModal_obj.data;
        ApplyPromoCode applyPromoCodeObj = new ApplyPromoCode.fromJsonMap(obj);
        promocodeApply = true;

        if (applyPromoCodeObj.coupon_type == 'fixed') {
          setState(() {
            PromocodeDiscountPrice = applyPromoCodeObj.coupon_amount;
          });
        }

        if (applyPromoCodeObj.coupon_type == 'percentage') {
          try {
            var temp_total_price = (double.parse(TotalPrice) -
                    double.parse(Consts.insurance_rate)) -
                taxValuePrice;

            double finalVal = (temp_total_price *
                    double.parse(applyPromoCodeObj.coupon_amount)) /
                100;
            setState(() {
              PromocodeDiscountPrice = (finalVal.toStringAsFixed(3)).toString();
            });
          } catch (e) {
            print(e);
          }
        }

        try {
          setState(() {
            totalPriceBeforeApplyPromoCode = TotalPrice;
            double temp = (double.parse(totalPriceBeforeApplyPromoCode) -
                double.parse(PromocodeDiscountPrice));
            TotalPrice = (temp.toStringAsFixed(3)).toString();
          });
        } catch (e) {
          print(e);
        }

        coupon_name = applyPromoCodeObj.coupon_name;
        coupon_type = applyPromoCodeObj.coupon_type;
        coupon_value = PromocodeDiscountPrice;
        coupon_amount = applyPromoCodeObj.coupon_amount;
        coupon_id = applyPromoCodeObj.coupon_id;
      } else {
        Toast.show(CommanModal_obj.message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  void promoCodeBottomSheetDialog(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Container(
            color: Colors.black26,
            child: AnimatedPadding(
              duration: Duration(milliseconds: 150),
              curve: Curves.easeOut,
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15.0),
                      topRight: Radius.circular(15.0)),
                ),
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: new Wrap(
                    children: <Widget>[
                      CommonWidget.bottomSheetTitle('Add promo code'),
                      CommonWidget.bottomSheetTitle2(
                          'Please note that only one promo code can be\napplied per ride.'),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                        child: TextField(
                          style: CommonWidget.TFCommnTextStyle(),
                          decoration: new InputDecoration(
                            hintText: "Promo code",
                            hintStyle: TextStyle(
                                color: Colors.grey[400], fontSize: 12),
                            contentPadding: EdgeInsets.fromLTRB(10, 3, 10, 3),
                            border: new OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(5.0),
                              ),
                              borderSide: new BorderSide(
                                color: Colors.black26,
                                width: 0.7,
                              ),
                            ),
                          ),
                          onChanged: (val) {
                            promoCodeStr = val;
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                        child: SubmitButtonRed(
                            'APPLY PROMO CODE', onPromoCodeSubmit, 2),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  void CommentsBottomSheetDialog(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext bc) {
          return Container(
            color: Colors.black38,
            child: AnimatedPadding(
              duration: Duration(milliseconds: 150),
              curve: Curves.easeOut,
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0)),
                ),
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: new Wrap(
                    children: <Widget>[
                      CommonWidget.bottomSheetTitle('Note to driver'),
                      CommonWidget.bottomSheetTitle2(
                          'Let driver know more about your requests.'),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                        child: TextField(
//                          obscureText: true,
                          style: CommonWidget.TFCommnTextStyle(),
                          keyboardType: TextInputType.multiline,
                          minLines: 3,
                          maxLines: 3,
                          decoration: new InputDecoration(
                            hintText: "Eg: I'm in front of the bus stop",
                            hintStyle: TextStyle(
                                color: Colors.grey[400], fontSize: 12),
//                            contentPadding: EdgeInsets.fromLTRB(10, 6, 10, 3),
                            border: new OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(5.0),
                              ),
                              borderSide: new BorderSide(
                                color: Colors.black26,
                                width: 0.7,
                              ),
                            ),
                          ),
                          onChanged: (val) {
                            Consts.comments = val;
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                        child: SubmitButtonRed('CONFIRM', onCommentSubmit, 2),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  void check(CameraUpdate u, GoogleMapController c) async {
    c.animateCamera(u);
    _controller.animateCamera(u);
    LatLngBounds l1 = await c.getVisibleRegion();
    LatLngBounds l2 = await c.getVisibleRegion();
    print(l1.toString());
    print(l2.toString());
    if (l1.southwest.latitude == -90 || l2.southwest.latitude == -90)
      check(u, c);
  }

  void onChanged() {
    if (IsListSelect) {
//      showDialog(
//          context: context,
//          builder: (BuildContext context) =>
//              PhoneOtpDialog());

      // if (IsCorporateUser) {
      //   postjob();
      // } else {
      if (PaymentTypeForPass.isEmpty) {
        Toast.show(Api_constant.payment_validation, context,
            gravity: Toast.CENTER);
      } /*else if (!OnlinePaymentComplete) {
        Toast.show('Please make payment in order to book ride', context,
            gravity: Toast.CENTER);
      }*/ else {
        postjob();
      }
      // }
//      Navigator.push(context, MaterialPageRoute(builder: (context) => PostJob2()));

    } else {
      Toast.show(Api_constant.vehicle_empty, context);
    }
  }

  postjob() {
    Utility.checkInternetConnection().then((intenet) {
      if (intenet != null && intenet) {
        try {
         /* print("distance=="+(Consts.distance).toString());*/
          PostJobUser();
        } catch (e) {}
//          Toast.show("Distance"+Consts.distance.toString(), context,
//              gravity: Toast.CENTER);
      } else {
        Toast.show(Api_constant.no_internet_connection, context,
            gravity: Toast.CENTER);
      }
    });
  }

  Future<void> PostJobUser() async {
    print('test...........');
    var map = new Map<String, dynamic>();
    MyPreferenceManager _myPreferenceManager =
        await MyPreferenceManager.getInstance();
    String id = _myPreferenceManager
        .getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);
    String CORPORATE_ID =
        _myPreferenceManager.getString(MyPreferenceManager.CORPORATE_ID);

    map[Api_constant.user_id] = id;
    map[Api_constant.job_title] = Consts.job_name;
    map[Api_constant.service_id] = Consts.vehicle_id;

    map[Api_constant.total_price] = priceWitTax();
    map[Api_constant.pickup_address] = Consts.sourceAdd;
    map[Api_constant.delivery_address] = Consts.dstAdd;
    map[Api_constant.description] = Consts.comments;
    map[Api_constant.date] = Consts.date;
    map[Api_constant.is_corporate] = Consts.is_corporate;

    if (Consts.is_corporate == null || Consts.is_corporate.isEmpty) {
      map['corporate_id'] = '0';
    } else {
      if (Consts.is_corporate == '1') {
        //corporate 6e
        map['corporate_id'] = CORPORATE_ID;
      } else {
        map['corporate_id'] = '0';
      }
    }

    map[Api_constant.pickup_street] = Consts.sourceAddCity;
    map[Api_constant.pickup_postal_code] = Consts.sourcepostal_code;
    map[Api_constant.pickup_state] = Consts.sourcestate;
    map[Api_constant.pickup_country] = Consts.sourcecountry;
    map[Api_constant.pickup_lattitude] = Consts.pickup_lattitude;
    map[Api_constant.pickup_longitude] = Consts.pickup_longitude;
    map[Api_constant.delivery_street] = Consts.dstAddCity;
    map[Api_constant.delivery_postal_code] = Consts.dstpostal_code;
    map[Api_constant.delivery_state] = Consts.dststate;
    map[Api_constant.delivery_country] = Consts.dstcountry;
    map[Api_constant.delivery_lattitude] = Consts.delivery_lattitude;
    map[Api_constant.delivery_longitude] = Consts.delivery_long;
    map[Api_constant.start_with_date] = Consts.date_bool;
    map[Api_constant.online_payment] = '1';
//    map[Api_constant.online_payment] = PaymentType;
    if (PaymentTypeForPass.isEmpty) {
      PaymentTypeForPass = '0'; //set for temporary
    }
    map['payment_type'] = PaymentTypeForPass;
    map[Api_constant.distance] = (Consts.distance).toString();
//    map[Api_constant.total_price_without_tax] = '100';
    map[Api_constant.total_price_without_tax] =
        priceWithoutTax(Consts.min_price, Consts.rate_per_km);
    map[Api_constant.service_price_per_km] = Consts.rate_per_km;
    map[Api_constant.tax] = taxValue;

    print(map.toString() + ".....map.........");
    print(PaymentTypeForPass + "...PaymentTypeForPass...........");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.post(Api_constant.post_job_api, body: map);

    setState(() {
      IsProgressIndicatorShow = false;
    });

    print(response.statusCode.toString() + "..............");
    print(response.body + "PostJob..............");

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var createJobResponse =
          new StatusMsgResponse.fromJsonMap(userMap) as StatusMsgResponse;
      var JobStatusResponseObj = new JobStatusMsgResponse.fromJsonMap(userMap) as JobStatusMsgResponse;
      if (createJobResponse.status == Api_constant.STATUS) {
        Toast.show(Api_constant.create_post, context, gravity: Toast.CENTER);

        var jobstatusObj = JobStatusResponseObj.data;

        int job_id=jobstatusObj.job_id;
        int is_today=jobstatusObj.is_today;
        /*  Navigator.of(context)
           .pushNamedAndRemoveUntil(MyRequestTabsScreen.routeName, (Route<dynamic> route) => false);*/

        Navigator.of(context).pushNamedAndRemoveUntil(
            MyRequestTabsScreen.routeName, (Route<dynamic> route) => false,
            arguments: 0);

        if (Consts.is_corporate == '1') {

        }else{
         /* var now = new DateTime.now();
          var formatter = new DateFormat('yyyy-M-d hh:mm:ss');
          String formattedDate = formatter.format(now);
          print("formated date.."+formattedDate.toString());
          var selectedDate=Consts.date;
          print("selectedDate.."+selectedDate.toString());
          if(selectedDate.compareTo(formattedDate) ==0){

          }*/

          if(is_today==1){
            showDialog(
                barrierDismissible: false,
                context: context,
                builder: (BuildContext context) =>
                    LocationscanningDialog(job_id)
            );


            Consts.isLokkingForDriverPopupOpen=true;
          }
        }
        
      } else {
        Toast.show(Api_constant.Error, context, gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);
      throw Exception('Failed to load internet');
    }
  }

  /* void sourceDestinationMarkerSet() {
    setState(() {
      _markers['0'] = Marker(
        markerId: MarkerId('sourcePin'),
        position: LatLng(double.parse(Consts.pickup_lattitude), double.parse(Consts.pickup_longitude)),
      );
    });
  }*/

  _getColorOfItem(int index) {
    if (selectedItemPostion == -1) {
      return Colors.grey[300];
//      return Colors.transparent;
    } else if (index == selectedItemPostion) {
      return Colors.deepOrangeAccent[700];
    } else {
      return Colors.grey[300];
//      return Colors.transparent;
    }
  }

  _getColorOfItem2(int index) {
    if (selectedItemPostion == -1) {
      return Colors.grey[100];
    } else if (index == selectedItemPostion) {
      return Colors.lime[50];
    } else {
      return Colors.grey[100];
    }
  }

  _getColorOfpayment(int index) {
    if (selectedoaymentItemPostion == -1) {
      return Colors.grey[100];
    } else if (index == selectedoaymentItemPostion) {
      return Colors.lime[50];
    } else {
      return Colors.grey[100];
    }
  }

//  void sourceDestinationMarkerSet() async {
//    sourceIcon = await BitmapDescriptor.fromAssetImage(
//        ImageConfiguration(devicePixelRatio: 2.5), 'assets/location.png');
//    destinationIcon = await BitmapDescriptor.fromAssetImage(
//        ImageConfiguration(devicePixelRatio: 2.5),
//        'assets/location.png');
//  }

  void onPromoCodeSubmit() {
    Navigator.of(context).pop();
    setState(() {
      Api_constant.Promo = promoCodeStr;
    });
    if (promocodeApply) {
      Toast.show(Api_constant.promocode_already_apply, context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.CENTER);
    } else {
      if (promoCodeStr.isEmpty) {
        Toast.show(Api_constant.promocode_empty, context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.CENTER);
      } else {
        ApigetPromocodeValue();
      }
    }
  }

  void onCommentSubmit() {
    Navigator.of(context).pop();
  }

  @override
  void paymentType(String price) {
    PaymentTypeForPass = price;
    print(PaymentTypeForPass + " j=====");

    if (PaymentTypeForPass == '1') {
      OnlinePaymentComplete = false;
     // makeOnlinePayment();
    } else {
      OnlinePaymentComplete = true;
    }
  }

  getCoporateUSerWidget() {
    return FutureBuilder<bool>(
        future: CommonWidget.getIsCorporateUserFromPostjob1stScreen(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.data == false) {
            IsCorporateUser = true;

            return Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: IconButton(
                    icon: Icon(
                      Icons.edit,
                      color: Colors.grey[700],
                      size: 14,
                    ),
                    onPressed: () {
                      CommentsBottomSheetDialog(context);
                    },
                  ),
                ));
          } else {
            IsCorporateUser = false;
            return Container();
          }
        });
  }

  getPriceWidget(String min_pricee, String rate_pr_km) {
    return FutureBuilder<bool>(
        future: CommonWidget.getIsCorporateUserFromPostjob1stScreen(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.data == false) {
            IsCorporateUser = true;
            return Container();
          } else {
            IsCorporateUser = false;
            return Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    Consts.currencySymbol +
                        priceWithoutTax(min_pricee, rate_pr_km),
                    style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.w600,
                        fontSize: 14),
                  ),
                  flex: 1,
                ),
//                                                  Expanded(
//                                                    flex: 1,
//                                                    child: Text('4 Seats',
//                                                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w400, fontSize: 13),),
//                                                  )
              ],
            );
          }
        });
  }

  getPersonalUSerWidget() {
    bool showCouponButton  = false; //If you want to show coupon then just set true to this variable. But is work only for corporate users.
    return FutureBuilder<bool>(
        future: CommonWidget.getIsCorporateUserFromPostjob1stScreen(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.data == false) {
            IsCorporateUser = true;
          } else {
            IsCorporateUser = false;
          }
          return SizedBox(
//                          height: MediaQuery.of(context).size.height * 0.08,
            height: 30,
            child: Align(
              child: Padding(
                  padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: InkWell(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          child: Row(
//                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Align(
                                      alignment: Alignment.center,
                                      child: Icon(
                                        Icons.payment,
                                        color: Colors.black87,
                                        size: 20.0,
                                      )),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Text(
                                    Api_constant.cash_payment,
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black87),
                                  )
                                ],
                              ),
                              Align(
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.black87,
                                      size: 12.0,
                                    ),
                                  )),
                            ],
                          ),
                          onTap: () {
                            print(IsListSelect.toString() + "ooooooooooooooo");
                            showModalBottomSheet<void>(
                              context: context,
                              builder: (BuildContext context) {
                                return BottomSheetPayment(
                                    this, IsListSelect, IsCorporateUser);
                              },
                            );
//                                        PaymentBottomSheetDialog(context);
                          },
                        ),
                        Row(
                          children: [
                            if (IsCorporateUser && showCouponButton)
                              InkWell(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(4, 0, 12, 0),
                                  child: Text(
                                    'Coupon',
//                                    Api_constant.Promo,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.grey[700]),
                                  ),
                                ),
                                onTap: () {
                                  if (!promocodeApply) {
                                    promoCodeBottomSheetDialog(context);
                                  } else {
                                    Toast.show(
                                        'You already apply promo code', context,
                                        gravity: Toast.CENTER);
                                  }
                                },
                              ),
//                                        Text(Api_constant.Promo, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.grey[700] ),),
                            if (!IsCorporateUser && showCouponButton)
                              Padding(
                                padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
                                child: Container(
                                  height: 13,
                                  width: 2,
                                  color: Colors.grey[400],
                                ),
                              ),
                            InkWell(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(12, 0,3, 0),
                                child: Text(
                                  'Note',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.grey[700]),
                                ),
                              ),
//                                Icon(
//                                  Icons.edit,
//                                  color: Colors.grey[700],
//                                  size: 14.0,
//                                ),
                              onTap: () {
                                CommentsBottomSheetDialog(context);
                              },
                            ),
//                                        Padding(
//                                          padding: EdgeInsets.fromLTRB(5, 0, 8 ,0),
//                                          child: Container(
//                                            height: 13,
//                                            width: 2,
//                                            color: Colors.grey[400],
//                                          ),
//                                        ),
//                                        InkWell(
//                                          child: Icon(
//                                            Icons.calendar_today,
//                                            color: Colors.grey[700],
//                                            size: 14.0,
//                                          ),
//                                          onTap: (){
//                                            DatePicker.showDateTimePicker(context,
//                                                showTitleActions: true,
//                                                minTime: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day,
//                                                    DateTime.now().hour, DateTime.now().minute),
//                                                maxTime: DateTime(2101, 6, 7), onChanged: (date) {
//                                                  print('change $date');
//                                                }, onConfirm: (date) {
//                                                  Consts.date  = date.year.toString() +
//                                                      '-' +
//                                                      date.month.toString() +
//                                                      '-' +
//                                                      date.day.toString() +
//                                                      ' ' +
//                                                      date.hour.toString() +
//                                                      ':' +
//                                                      date.minute.toString() +
//                                                      ':00';
//
//                                                  print('confirm== '+Consts.date);
//                                                  print('confirm== '+ Utility.convertdateFormat( Consts.date));
//                                                }, currentTime: DateTime.now(), locale: LocaleType.en);
//                                          },
//                                        )
                          ],
                        )
                      ],
                    ),
                    onTap: () {
//                                  Navigator.of(context).pushReplacementNamed(AddPayment.routeName);
                    },
                  )),
              alignment: Alignment.bottomCenter,
            ),
          );
        });
  }

  Razorpay _razorpay;

  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    OnlinePaymentComplete = true;
    postjob();
    Toast.show("SUCCESS: " + response.paymentId, context, gravity: Toast.TOP);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Toast.show("ERROR: " + response.code.toString() + " - " + response.message,
        context,
        gravity: Toast.TOP);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Toast.show("EXTERNAL_WALLET: " + response.walletName, context,
        gravity: Toast.TOP);
  }

  void makeOnlinePayment() async {
    if (!IsListSelect) {
      Toast.show(Api_constant.vehicle_empty, context, gravity: Toast.TOP);
    } else {
      MyPreferenceManager _myPreferenceManager =
          await MyPreferenceManager.getInstance();
      String phone_number =
          _myPreferenceManager.getString(MyPreferenceManager.PHONE_NUMBER);

      // >Mechant id : EcMJf5xfm7lYwu

      print(priceWitTax() + " plssss");
      print(priceWitTaxForPassInOnlinePayment().toString() + " plssss int");
      var options = {
        // 'key': 'rzp_test_TMa20FHiPiFsDT', //client test key
        'key': 'rzp_live_3PnhiM7vSXlEdJ', //client live key
        // 'key': 'rzp_test_hGmLEAthGerAr5', //testtechnology410 test key
        'amount': priceWitTaxForPassInOnlinePayment(),
        'name': '',
        'description': '',
        'prefill': {'contact': phone_number, 'email': ''},
        // 'prefill': {'contact': '9999976052', 'email': ''},
        'external': {
          'wallets': ['paytm']
        }
      };

      print(options);

      try {
        _razorpay.open(options);
      } catch (e) {
        debugPrint(e);
      }
    }
  }
}

class BottomSheetPayment extends StatefulWidget {
//  BottomSheetPayment({@required this.switchValue, @required this.valueChanged});

//  final bool switchValue;
//  final ValueChanged valueChanged;

  CallbackOfGetpAYMENTParam _callbackOfGetFilterMaxParam;
  bool isListSelect;
  bool IsCorporateUser;

  BottomSheetPayment(this._callbackOfGetFilterMaxParam, this.isListSelect,
      this.IsCorporateUser);

  @override
  _BottomSheetPayment createState() => _BottomSheetPayment();
}

class CallbackOfGetpAYMENTParam {
  void paymentType(String price) {}
}

class _BottomSheetPayment extends State<BottomSheetPayment> {
  String PaymentType = '';
  String choice;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black38,
      child: AnimatedPadding(
        duration: Duration(milliseconds: 150),
        curve: Curves.easeOut,
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0)),
          ),
          child: Padding(
            padding: EdgeInsets.all(20),
            child: new Wrap(
              children: <Widget>[
                CommonWidget.bottomSheetTitle('Payment methods'),
                Padding(
                  padding: EdgeInsets.fromLTRB(1, 0, 1, 0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                            width: double.infinity,
                            height: 35,
                            decoration: BoxDecoration(
                              color: _getColorOfItem2('1'),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.0)),
                              border: Border.all(
                                color: _getColorOfItem('1'),
                                //                   <--- border color
                                width: 1.0,
                              ),
                            ),
                            child: InkWell(
                              onTap: () {
                                if (widget.isListSelect) {
                                  setState(() {
                                    PaymentType = '1';
                                    widget._callbackOfGetFilterMaxParam
                                        .paymentType(PaymentType);
                                    //Navigator.of(context).pop();
                                    //  selectedItemPostionOfUserType = type;
                                  });
                                } else {
                                  Toast.show(
                                      Api_constant.vehicle_empty, context,
                                      gravity: Toast.CENTER);
                                  // Toast.show(Api_constant.vehicle_empty, context,duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                                  Navigator.of(context).pop();
                                }
                              },
                              child: Align(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                                  child: Text('Online'),
                                ),
                                alignment: Alignment.centerLeft,
                              ),
                            )),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                            width: double.infinity,
                            height: 35,
                            decoration: BoxDecoration(
                              color: _getColorOfItem2('2'),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.0)),
                              border: Border.all(
                                color: _getColorOfItem('2'),
                                //                   <--- border color
                                width: 1.0,
                              ),
                            ),
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  PaymentType = '2';
                                  widget._callbackOfGetFilterMaxParam
                                      .paymentType(PaymentType);
                                });
                              },
                              child: Align(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                                  child: Text('Cash'),
                                ),
                                alignment: Alignment.centerLeft,
                              ),
                            )),
                      ),
                      if (widget.IsCorporateUser)
                        SizedBox(
                          width: 10,
                        ),
                      if (widget.IsCorporateUser)
                        Expanded(
                          flex: 1,
                          child: Container(
                              width: double.infinity,
                              height: 35,
                              decoration: BoxDecoration(
                                color: _getColorOfItem2(
                                    Consts.PAYMENT_TYPE_COMPANY),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4.0)),
                                border: Border.all(
                                  color: _getColorOfItem(
                                      Consts.PAYMENT_TYPE_COMPANY),
                                  //                   <--- border color
                                  width: 1.0,
                                ),
                              ),
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    PaymentType = Consts.PAYMENT_TYPE_COMPANY;
                                    widget._callbackOfGetFilterMaxParam
                                        .paymentType(PaymentType);
                                  });
                                },
                                child: Align(
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                                    child: Text('Company'),
                                  ),
                                  alignment: Alignment.centerLeft,
                                ),
                              )),
                        )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: SubmitButtonRed('CONFIRM', onPaymentTypeSubmit, 2),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _getColorOfItem(String index) {
    if (index == PaymentType) {
      return Colors.deepOrangeAccent[700];
    } else {
      return Colors.grey[300];
//      return Colors.transparent;
    }
  }

  _getColorOfItem2(String index) {
    if (index == PaymentType) {
      return Colors.lime[50];
    } else {
      return Colors.grey[100];
    }
  }

  void onPaymentTypeSubmit() {
    Navigator.of(context).pop();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch(state){
      case AppLifecycleState.resumed:
        //print("app in resumed");
        break;
      case AppLifecycleState.inactive:
       // print("app in inactive");
        break;
      case AppLifecycleState.paused:
       // print("app in paused");
        break;
      case AppLifecycleState.detached:
       // print("app in detached");
        break;
    }
  }


}
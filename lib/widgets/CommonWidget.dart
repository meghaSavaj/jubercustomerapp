import 'dart:ffi';
//import 'dart:html';

import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/Utils/Consts.dart';
import 'package:arabacustomer/Utils/MyPreferenceManager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:toast/toast.dart';

class CommonWidget {
  static Widget submitButtonBottomLine() {
    return SizedBox(
      height: 10.0,
      width: 90.0,
      child: new Center(
        child: new Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: <Color>[
                Color(0xFF120101),
//                Color(0xFF0d0c0c),
                Color(0xFF1f1c1c),
              ],
            ),
          ),
          margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
          height: 4.0,
//                                color: Colors.red,
        ),
      ),
    );
  }

  static Widget ActionBarBg(BuildContext context) {
    return Container(
      color: Colors.white,
//      color: Theme.of(context).primaryColor,
    );
  }

  static Widget getActionBarTitleText(String title) {
    return Text(
      title,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500, color: Colors.black87),
    );
  }

  static Widget getNewActionBarTitleText(String title) {
    return Text(
      title,
      textAlign: TextAlign.left,
      style: TextStyle(fontSize: 23, fontWeight: FontWeight.w400, color: Colors.white),
    );
  }
  static Widget priceDarkGreencolorText(String price){
    return Text(price,
        style: TextStyle(color: Colors.green[700],
            fontWeight: FontWeight.w600,
            fontSize: 14));
  }

  static Widget getNewActionBarTitleTextBlack(String title) {
    return Text(
      title,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500, color: Colors.black87),
    );
  }
  static Widget priceDarkRedcolorText(String price, BuildContext context) {
    return Text(price,
        style: TextStyle(
            color: Theme.of(context).primaryColor, fontWeight: FontWeight.w600, fontSize: 17));
  }

  static Widget lblText(String lbl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 6),
      child: Text(lbl, style: TextStyle(fontSize : 14, color: Colors.grey[600], fontWeight: FontWeight.w600),),
    );
  }
  static Widget plblText(String lbl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 6),
      child: Text(lbl, style: TextStyle(fontSize : 12, color: Colors.grey[600], fontWeight: FontWeight.w600),),
    );
  }
  static TextStyle TFCommnTextStyle() {
    return  TextStyle(fontSize : 15, color: Colors.black87, fontWeight: FontWeight.w600);
  }
  static Widget blackText(String lbl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 6),
      child: Text(lbl, style: TextStyle(fontSize : 15, color: Colors.black87, fontWeight: FontWeight.w600),),
    );
  }
  static Widget blackBigText(String lbl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 6),
      child: Text(lbl, style: TextStyle(fontSize : 20, color: Colors.black, fontWeight: FontWeight.w800),),
    );
  }
  static InputDecoration ETInputDecoration() {
    return new InputDecoration(
        labelStyle: TextStyle(color: Colors.black87, fontSize: 12),
        hintStyle: TextStyle(
            color: Colors.grey[500], fontSize: 12
        ),
      contentPadding: EdgeInsets.fromLTRB(10, 2, 10, 2),
      border: new OutlineInputBorder(
        borderRadius: const BorderRadius.all(
          const Radius.circular(5.0),
        ),
        borderSide: new BorderSide(
          color: Colors.black12,
          width: 0.7,
        ),
      ),
    );
  }

  static InputDecoration ETInputNumberDecoration() {
    return new InputDecoration(
      labelStyle: TextStyle(color: Colors.black87, fontSize: 12),
      prefixText: "+91",
      prefixStyle: TextStyle(fontSize : 15, color: Colors.black87, fontWeight: FontWeight.w600),
      hintStyle: TextStyle(
          color: Colors.grey[500], fontSize: 12
      ),
      contentPadding: EdgeInsets.fromLTRB(10, 2, 10, 2),
      border: new OutlineInputBorder(
        borderRadius: const BorderRadius.all(
          const Radius.circular(5.0),
        ),

        borderSide: new BorderSide(
          color: Colors.black12,
          width: 0.7,
        ),
      ),
    );
  }

  static InputDecoration PETInputDecoration() {
    return new InputDecoration(
      labelStyle: TextStyle(color: Colors.black87, fontSize: 10),
      hintStyle: TextStyle(
          color: Colors.grey[500], fontSize: 12
      ),
      contentPadding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
//      contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      border: new OutlineInputBorder(
          gapPadding: 2.0,
        borderRadius: const BorderRadius.all(
          const Radius.circular(5.0),
        ),
        borderSide: new BorderSide(
          color: Colors.black12,
          width: 0.7,
        ),
      ),
    );
  }
  static Widget priceBlackText(String price){
    return Text(price,
        style: TextStyle(color: Colors.black,
            fontWeight: FontWeight.w600,
            fontSize: 17));
  }
  static Widget leadidDarkRedcolorText(String leadid) {
//    return Text(
//      "Référence "+leadid,
//      style: TextStyle(
//          color: Colors.yellow.shade800,
//          fontWeight: FontWeight.w400,
//          fontSize: 17),
//    );

//    if(leadid.contains('(')){
//      var arr = leadid.split('(');
//      var valueWithLbl = Api_constant.Reference +arr[0]+"\n("+Api_constant.Locate+arr[1];
//      return Expanded(
//        child: Text(valueWithLbl,
//          style: TextStyle(color: Colors.yellow.shade800, fontWeight: FontWeight.w400, fontSize: 17),),
//      );
//
//    } else{
//      return Expanded(
//        child: Text(Api_constant.Reference +leadid,
//          style: TextStyle(color: Colors.yellow.shade800, fontWeight: FontWeight.w400, fontSize: 17),),
//      );
//
//    }
   return
      Padding(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
        child: Text(leadid,
          style: TextStyle(color: Colors.blueAccent[700], fontWeight: FontWeight.w400, fontSize: 14),),
      );

  }

  static Widget divider() {
    return Divider(
      thickness: 0.4,
      color: Colors.black87,
    );
  }

  static Widget jobnameText(String jobname) {
    return Visibility(
      visible: false,
      child: Text(
        jobname,
        style: TextStyle(
            color: Colors.black87, fontWeight: FontWeight.w600, fontSize: 20),
      ),
    );
  }

  static Widget redHeaderLbl(String title, BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        fontFamily: 'RobotoCondensed',
        fontSize: 17,
        color: Theme.of(context).primaryColor,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  static Widget redHeadersmallLbl(String title, BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        fontFamily: 'RobotoCondensed',
        fontSize: 14,
        color: Theme.of(context).primaryColor,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  static Widget redHeaderBigLbl(String title, BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        fontFamily: 'RobotoCondensed',
        fontSize: 16,
        color: Theme.of(context).primaryColor,
//        color: Colors.red[900],
        fontWeight: FontWeight.w600,
      ),
    );
  }

  static Widget blackDiscriptionText(String title) {
    return Expanded(
      child: Text(
        title,
        style: TextStyle(
          fontSize: 15,
          color: Colors.black,
          height: 1.5,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }

  static Widget grayDiscriptionText(String title) {
    return Expanded(
      child: Text(
        title,
        style: TextStyle(
            color: Colors.grey[700], fontWeight: FontWeight.w400, fontSize: 15),
      ),
    );
  }

  static Widget gryIconNdGrayTextheaderLayout(String title, String icon) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 6, 0, 6),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          getIconImge(icon),
          SizedBox(
            width: 15,
          ),
          grayDiscriptionText(title),
        ],
      ),
    );
  }

  static Widget gryCustomIconNdGrayTextheaderLayout(
      String title, String icon, double size) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 6, 0, 6),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          getIconImgeWithCustomSize(icon, size),
          SizedBox(
            width: 15,
          ),
          grayDiscriptionText(title),
        ],
      ),
    );
  }

  static Widget PHONEfooterLayout(String title, String icon, double size, BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 6, 0, 6),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            getIconImgeWithCustomSize(icon, size),
            Image(
              image: AssetImage(icon),
              height: size,
              color: Theme.of(context).primaryColor,
              width: size,
            ),
            SizedBox(
              width: 15,
            ),
            Expanded(
              child: Text(
                title,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                    fontSize: 16),
              ),
            ),
          ],
        ),
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  static bool IsDisplayRating(String rating) {
    if (null == rating || rating.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

  static String RatingStrWithBracket(String rating) {
    if (null == rating || rating.isEmpty) {
      return '';
    } else {
      return ' \(' + CommonWidget.replaceNullWithEmpty(rating) + ' ';
    }
  }

  static Widget GrayTextheaderLayout(String title) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 6, 0, 6),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
//          getIconImgeWithCustomSize(icon, size),
//          SizedBox(
//            width: 15,
//          ),
          grayDiscriptionText(title),
        ],
      ),
    );
  }

  static Widget gryIconNdBlackTextheaderLayout(String title, IconData icon) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 6, 0, 6),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
         Align(
           alignment: Alignment.topLeft,
           child:  Padding(
             padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
             child: Icon(
               icon,
               size: 22,
             ),
           ),
         ),
          SizedBox(
            width: 15,
          ),
          blackDiscriptionText(title),
        ],
      ),
    );
  }

  static Widget getIconImge(String str) {
    return Image(
      image: AssetImage(str),
      height: 22,
      width: 22,
    );
  }
  static ThemeData getAppTheme() {
    const MaterialColor black = const MaterialColor(
      0xFF000000,
      const <int, Color>{
        50: const Color(0xFFE64A19),
        100: const Color(0xFFE64A19),
        200: const Color(0xFFE64A19),
        300: const Color(0xFFE64A19),
        400: const Color(0xFFE64A19),
        500: const Color(0xFFE64A19),
        600: const Color(0xFFE64A19),
        700: const Color(0xFFE64A19),
        800: const Color(0xFFE64A19),
        900: const Color(0xFFE64A19),
      },
    );

    const MaterialColor orange = const MaterialColor(
      0xFF000000,
      const <int, Color>{
        50: const Color(0xFFE64A19),
        100: const Color(0xFFE64A19),
        200: const Color(0xFFE64A19),
        300: const Color(0xFFE64A19),
        400: const Color(0xFFE64A19),
        500: const Color(0xFFE64A19),
        600: const Color(0xFFE64A19),
        700: const Color(0xFFE64A19),
        800: const Color(0xFFE64A19),
        900: const Color(0xFFE64A19),
      },
    );

    return new ThemeData(
        primaryColor : Colors.deepOrange[700],
//        primaryColor : Colors.black,
        primarySwatch: orange,
        splashColor: Colors.deepOrange[700],
        accentColor: Colors.deepOrange[700],
//      canvasColor: Colors.transparent,
//      primarySwatch: Colors.red,
//      accentColor: Colors.redAccent,
        hintColor: Colors.deepOrange[700],
//        inputDecorationTheme: new InputDecorationTheme(
//             contentPadding:  EdgeInsets.fromLTRB(10, 3, 10, 3),
////            labelStyle: new TextStyle(color: Colors.white),
//            border: new OutlineInputBorder(
//              borderRadius: const BorderRadius.all(
//                const Radius.circular(5.0),
//              ),
//              borderSide: new BorderSide(
//                color: Colors.black26,
//                width: 0.7,
//              ),
//            ))
    );
  }


  static Widget getIconImgeWithCustomSize(String str, double size) {
    return Image(
      image: AssetImage(str),
      height: size,
      width: size,
    );
  }

  static String replaceNullWithEmpty(String str) {
    if (null == str) {
      return '';
    } else {
      return str;
    }
  }

  static String getstatusFrenchNameFordisplay(String str) {
    if (null == str || str.isEmpty) {
      return '';
    } else {

      if(str.toLowerCase() == Consts.onthewayStatusText){
        return Api_constant.ontheway;
      } else if(str.toLowerCase() == Consts.paymentpendingStatusText){
        return Api_constant.paymentpending;
      } else if(str.toLowerCase() == Consts.driverarrivedStatusText){
        return Api_constant.driverarrived;
//        return 'CHAUFFEUR ARRIVÈ';
      } else if(str.toLowerCase() == Consts.inprogressStatusText){
        return Api_constant.inprogress;    //daut
//        return 'LIVRAISON EN COUR';    //daut
      } else if(str.toLowerCase() == Consts.pendingStatusText){
        return Api_constant.pending;    //daut
      } else if(str.toLowerCase() == Consts.confirmedStatusText){
        return Api_constant.confirmed;    //daut
      } else{
        return str;
      }
    }
  }

  static double getDoubleFromStrForRating(String str) {
    if (null == str || str.isEmpty) {
      return 0.0;
    } else {
      return double.parse(str);
    }
  }

  static Color getColorFromStatus(String status) {
    if (status.toLowerCase() == 'Driver Arrived'.toLowerCase()) {
      return Colors.green[500];
    } else if (status.toLowerCase() == 'Job Accepted'.toLowerCase() ) {
      return Colors.blue[500];
    } else if (status.toLowerCase() == 'Driver on the way'.toLowerCase()) {
      return Colors.yellow[700];
    } else if (status.toLowerCase() == 'Job Started'.toLowerCase()) {
      return Colors.red[500];
    } else if (status.toLowerCase() == 'payment pending') {
      return Colors.deepPurple[300];
    } else if (status.toLowerCase() == 'in progress') {
      return Colors.greenAccent;
      //Payment Pending
    }
    return Colors.grey[600];
  }


  static getboolFromInt(int intValue) {
    if (null == intValue) {
      return false;
    } else if (intValue == 0) {
      return true;
    } else if (intValue == 1) {
      return false;
    } else {
      return false;
    }
  }
  static String addCurrency(String delay_charges) {
    if(delay_charges == null || delay_charges.isEmpty){
      return "";
    } else{
      return Consts.currencySymbol +delay_charges;
    }
  }

  static forEmptyReturnHide(String str) {
    if (null == str || str.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

  static forCancelReturnHide(String str) {
    if (str=="CANCELLED") {
      return false;
    } else {
      return true;
    }
  }

  static double IsvalidDouble(String double) {}

  static bool isPasswordCompliant(String password, BuildContext context) {
    int minLength = 5;
//    int minLength = 7;
    if (password == null || password.isEmpty) {
      return false;
    }
//
//    bool hasUppercase = password.contains(new RegExp(r'[A-Z]'));
//    bool hasDigits = password.contains(new RegExp(r'[0-9]'));
//    bool hasLowercase = password.contains(new RegExp(r'[a-z]'));
//    bool hasSpecialCharacters = password.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'));
    bool hasMinLength = 13 > password.length && password.length > minLength ;

    if(12 < password.length){
      Toast.show(Api_constant.passwd_validaion12, context, gravity: Toast.CENTER);
//      Toast.show("Veuillez saisir un mot de passe d'au moins 8 chiffres", context, gravity: Toast.CENTER);
    }

    if(password.length < 6 ){
      Toast.show(Api_constant.passwd_validaion6, context, gravity: Toast.CENTER);
//      Toast.show("Veuillez saisir un mot de passe d'au moins 8 chiffres", context, gravity: Toast.CENTER);
    }
//    else if(!hasDigits){
//      Toast.show("Le mot de passe doit contenir au moins un caractère numérique", context, gravity: Toast.CENTER);
//    } else if(!hasLowercase){
//      Toast.show("Le mot de passe doit contenir au moins un caractère minuscule", context, gravity: Toast.CENTER);
//    } else if(!hasUppercase){
//      Toast.show("Le mot de passe doit contenir au moins un caractère majuscule", context, gravity: Toast.CENTER);
//    }else if(!hasSpecialCharacters){
//      Toast.show("Le mot de passe doit contenir au moins un caractère de caractères spéciaux", context, gravity: Toast.CENTER);
//    }
//
//    return hasDigits & hasUppercase & hasLowercase & hasSpecialCharacters & hasMinLength;
      return hasMinLength;

  }

  static double returnAlgeriaLatWhenGettingBlanck(String latlong) {
    if(null == latlong || latlong.isEmpty || latlong == '0.0' || latlong == '0'){
      return double.parse('21.705723');
    } else{
      return double.parse(latlong);
    }
  }

  static double returnAlgeriaLongWhenGettingBlanck(String latlong) {
    if(null == latlong || latlong.isEmpty || latlong == '0.0' || latlong == '0'){
      return double.parse('72.998199');
    } else{
      return double.parse(latlong);
    }
  }
  static bool ISCompletedStatus(String status){
    if(status != null && status.isNotEmpty && status.toLowerCase()==Consts.completedStatusText ) {
      return   true;
    } else {
      return false;
    }
  }

  static Widget bottomSheetTitle(String s) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 5, 0, 15),
      child: Text(
        s,
        textAlign: TextAlign.left,
        style: TextStyle(fontSize: 22, fontWeight: FontWeight.w700, color: Colors.black87),
      ),
    );
  }
  static Widget bottomSheetTitle2(String s) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
      child:  Text(s,
        style: TextStyle(
            fontWeight: FontWeight.w400, color: Colors.black87, fontSize: 12
        ),),
    );
  }

  static Future<bool> getIsCorporateUserFromPrefrence() async {
    MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
    String IsCORPORATE =  _myPreferenceManager.getString(MyPreferenceManager.CORPORATE);


    if(IsCORPORATE == "1"){
      return false;
//      return true;
    } else{
      return true;
    }
  }

  static Future<bool> getIsCorporateUserFromPostjob1stScreen() async {

    if(Consts.is_corporate == '1'){
      return false;
//      return true;
    } else{
      return true;
    }
  }

  static bool ISPriceDisplay(String status,int corporateid ){
    if(status != null && status.isNotEmpty && status.toLowerCase() == Consts.completedStatusText ) {
      return   true;
    } else {
      if(corporateid != null && corporateid != 0){
        return false;

      } else{
        return true;

      }
    }
  }

  static bool ISCorporateUser(int corporateid){
    if(corporateid != null && corporateid != 0){
      return true;

    } else{
      return false;
    }
  }
}

import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:flutter/material.dart';

class LeadId extends StatelessWidget{
  String leadid;
  LeadId(this.leadid);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return   Padding(
      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(Api_constant.LEAD_ID,
            style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w400, fontSize: 15),),

          CommonWidget.leadidDarkRedcolorText('')
        ],
      ),
    );
  }

}
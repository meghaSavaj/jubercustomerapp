import 'package:flutter/material.dart';

class PayNowSubmitButtonRed extends StatelessWidget{

  String textOfBtn = '';
  VoidCallback onChanged;
  double paddingg;

  PayNowSubmitButtonRed(this.textOfBtn, this.onChanged, this.paddingg);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: EdgeInsets.fromLTRB(paddingg, 0, paddingg, 0),
      child:  Center(
        child: SizedBox(
          width: double.infinity,
          child: RaisedButton(
            onPressed: () {
//        Navigator.pushNamed(context, '/DashboardPage');
              onChanged();
            },
            textColor: Colors.white,
            padding: const EdgeInsets.all(0.0),
            child: Container(
              width: double.infinity,
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: <Color>[
                      Color(0xFFE64A19),
                      Color(0xFFE64A19),
//                      Color(0xFF1f1c1c),
                    ],
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(5.0))
              ),
              padding: const EdgeInsets.fromLTRB(0, 13, 0, 13),
              child: new Text(textOfBtn, style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                  textAlign: TextAlign.center),
            ),),
        ),
      ),
    );
  }


}
library search_map_place;

//import 'dart:html';

import 'package:arabacustomer/Utils/Consts.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'dart:convert' as JSON;

import 'package:http/http.dart' as http;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:toast/toast.dart';

part 'src/geocoding.dart';
part 'src/geolocation.dart';
part 'src/place.dart';
part 'src/placeType.dart';
part 'src/searchMapPlaceWidget.dart';

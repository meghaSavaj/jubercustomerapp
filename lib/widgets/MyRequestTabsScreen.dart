import 'dart:async';

import 'package:arabacustomer/MyRequestCompleted.dart';
import 'package:arabacustomer/MyRequestCancel.dart';
import 'package:arabacustomer/MyRequestInprogress.dart';
import 'package:arabacustomer/MyRequestOpen.dart';
import 'package:arabacustomer/PostJob1New.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/Utils/Consts.dart';
//import 'package:dart_notification_center/dart_notification_center.dart';
import 'package:arabacustomer/widgets/lib_dart_noti_center/dart_notification_center.dart';

import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../MyRequestDetailNew2.dart';
import 'CommonWidget.dart';
import 'SideMenu.dart';

class MyRequestTabsScreen extends StatefulWidget {
  static const routeName = '/MyRequestTabsScreen';

  int iinitialIndex = 0;
  MyRequestTabsScreen({Key key, this.iinitialIndex}) : super(key: key);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<MyRequestTabsScreen> with WidgetsBindingObserver {
  Timer timerJobAcceptedChek;
  int i = 1;
  var data;
  @override
  void initState() {

    DartNotificationCenter.subscribe(
      channel: Consts.JobAcceptedChanel,
      observer: i,
      onNotification: (result) => {
        print('received:NewJobPostChanel****** $result'),
        setState(() {
          new MyRequestTabsScreen();

      if(Consts.CurrentChanelType == Consts.JobAcceptedChanel){
        if(Consts.is_corporate == '1'){
         // Navigator.pop(context, true);

          Navigator.of(context, rootNavigator: true).pop();
          DartNotificationCenter.post(
            channel: Consts.JobCancelTimer,
            options: 'with options!!',
          );
        }else{
         // Navigator.pop(context, true);
          Navigator.of(context, rootNavigator: true).pop(); //this for dismiss dialog looking for driver when accept job
          DartNotificationCenter.post(
            channel: Consts.JobCancelTimer,
            options: 'with options!!',
          );
        }
        _navigateToItemDetail(context, Consts.notificationJobId);
       // _navigateToItemDetail(context, data['job_id']);
          }
          Consts.CurrentChanelType = '';

        })
      },
    );
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);

  }
  static Future<void> _navigateToItemDetail(BuildContext context, String job_id) async {
    print('==================='+context.toString()+" "+job_id+" "+Consts.navigatorKey.currentState.toString());
    Consts.navigatorKey.currentState.pushNamed(MyRequestDetailNew2.routeName, arguments: job_id);

//   await Navigator.push(
//     context,
//     MaterialPageRoute(builder: (context) => MyRequestDetail(
//       postid: job_id,
//     )),
//   );

  }
//  @override
//  void didChangeAppLifecycleState(AppLifecycleState state) {
//    if(state == AppLifecycleState.resumed){
//      Toast.show('resumed', context, gravity: Toast.CENTER);
//      print('resumed:****** ');
//      // user returned to our app
//    }else if(state == AppLifecycleState.inactive){
//      Toast.show('inactive', context, gravity: Toast.CENTER);
//      // app is inactive
//    }else if(state == AppLifecycleState.paused){
//      Toast.show('paused', context, gravity: Toast.CENTER);
//      // user is about quit our app temporally
//    }else if(state == AppLifecycleState.detached){
//      Toast.show('detached', context, gravity: Toast.CENTER);
//      // app suspended (not used in iOS)
//    }
//  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    DartNotificationCenter.unsubscribe(observer: i, channel: Consts.JobAcceptedChanel);
//    DartNotificationCenter.unregisterChannel(channel: Consts.PaymentCompletedChanel);

    print('received:dispose****** ');
    super.dispose();

  }

  TabController tabController;

  Future<bool> _onWillPop() async {
    Consts.SideMenuCurrentlySelectedPosition = 1;

    print("_onWillPop===========" );
    Navigator.of(context).pushNamedAndRemoveUntil(PostJob1New.routeName, (Route<dynamic> route) => false, arguments: 0);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(child:  DefaultTabController(
      length: 3,
      initialIndex: widget.iinitialIndex,
      child: Scaffold(
        appBar: AppBar(
          elevation :0,
          centerTitle: true,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          title: CommonWidget.getActionBarTitleText(Api_constant.yourtrips.toUpperCase()),
          flexibleSpace: CommonWidget.ActionBarBg(context),
          actions: <Widget>[

          ],

        ),

        body: Column(
          children: [
            Wrap(
              children: [
                Container(
//              height: MediaQuery.of(context).size.height * 0.24,
                  color: Colors.white,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
//

                      TabBar(
                        controller: tabController,
                        labelColor: Theme.of(context).primaryColor,
                        unselectedLabelColor: Colors.grey,
                        indicatorColor: Colors.deepOrange,
                        labelStyle: TextStyle(fontSize: 13, fontWeight: FontWeight.w700),

                        tabs: <Widget>[

                          Container(
                            child:  Tab(
                              text: Api_constant.UPCOMIN,

                            ),
                            height: 30,
                          ),
                          Container(
                            child:  Tab(
                              text: Api_constant.My_Request_COMPLETED,
                            ),
                            height: 30,
                          ),
                          Container(
                            child:  Tab(
                              text: Api_constant.My_Request_CANCEL,
//                text: "ANNULÉ",
                            ),
                            height: 30,
                          ),


                        ],
                      ),

                    ],
                  ),
                )
              ],
            ),

            Expanded(
//              height: MediaQuery.of(context).size.height * 0.76,
              child:  TabBarView(
                controller: tabController,
                children: <Widget>[
//            MyRequestOpen(),
                  MyRequestInprogress(),
                  MyRequestCompleted(),
                  MyRequestCancel()
                ],
              ),
            )
          ],
        ),
        drawer: MainDrawer(context),
      ),
    ), onWillPop: _onWillPop);
  }
}
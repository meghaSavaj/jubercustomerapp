import 'dart:convert';

import 'package:arabacustomer/ContactUs.dart';
import 'package:arabacustomer/MyWallet.dart';
import 'package:arabacustomer/NotificationList.dart';
import 'package:arabacustomer/RatingList.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/Utils/Consts.dart';
import 'package:arabacustomer/Utils/MyPreferenceManager.dart';
import 'package:arabacustomer/dialog/TrackItNowDialog.dart';
import 'package:arabacustomer/model/CommanModalWithObj.dart';
import 'package:arabacustomer/model/NotificationUnReadCount.dart';
import 'package:arabacustomer/model/PassDataToTrackingScreen.dart';
import 'package:arabacustomer/myProfileNew.dart';
import 'package:arabacustomer/widgets/MyCompletedJobTabsScreen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';

import '../AboutUs.dart';
import '../Login.dart';
import '../LoginTirth.dart';
import '../MyRequestCompleted.dart';
import '../PostJob1New.dart';
import '../ProcessingTrackingDetail2.dart';
import 'MyRequestTabsScreen.dart';

class MainDrawer extends StatefulWidget {
  BuildContext ccontext;
  MainDrawer(this.ccontext);

  @override
  MainDrawerState createState() {
    return MainDrawerState();
  }


}
class MainDrawerState extends State<MainDrawer> implements CallbackOfTrackItNowDialog{
  String notificationUnreadCount = '0';
  String customer_name = '';

  @override
  void initState() {
    super.initState();
//    getNotificationUnreadCount();
    updateCustomerName();
  }

  Future getNotificationUnreadCount() async {
    MyPreferenceManager _myPreferenceManager =
    await MyPreferenceManager.getInstance();
    String id = _myPreferenceManager
        .getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);

    final uri = Uri.encodeFull(Api_constant.notification_unread_count +
        '?id=${id}');
    print(uri.toString() + "..............");

    var response = await http.get(uri);

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var CommanModal_obj = new NotificationUnReadCount.fromJsonMap(userMap) as NotificationUnReadCount;
      if (CommanModal_obj.status == 1) {
         setState(() {
           notificationUnreadCount = CommanModal_obj.unread_count;
         });

      } else {
        Toast.show(CommanModal_obj.message, context,
            gravity: Toast.CENTER);
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);
    }
  }

  Widget buildListTileWithCount(String title, String count, String icon, Function tapHandler) {
    return InkWell(
      onTap:  tapHandler,
      child: Padding(
        padding:  EdgeInsets.fromLTRB(10, 5, 10, 10),
        child: Row(
          children: <Widget>[
            getIconImge(icon),
            SizedBox(width: 20,),
            RichText(
              textAlign: TextAlign.start,
              text: TextSpan(
//                style: DefaultTextStyle.of(context).style,
                children: <TextSpan>[
                  TextSpan(
                    text: title,
                    style: TextStyle(
                      fontFamily: 'RobotoCondensed',
                      fontSize: 14,
                      color: Colors.black,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  TextSpan(
                      text: count,
                      style: TextStyle(
                        fontFamily: 'RobotoCondensed',
                        fontSize: 18,
                        color: Colors.blue[900],
                        fontWeight: FontWeight.w700,
                      )),
                ],
              ),
            ),
//            Text(
//              title,
//              style: TextStyle(
//                fontFamily: 'RobotoCondensed',
//                fontSize: 14,
//                color: Colors.white,
//                fontWeight: FontWeight.w400,
//              ),
//            ),
          ],
        ),
      ),
    );
//    return ListTile(
////      leading: Icon(
////        icon,
////        size: 26,
////      ),
//      leading: getIconImge(icon),
//      title: Column(
//        crossAxisAlignment: CrossAxisAlignment.start,
//        children: <Widget>[
//          Text(
//            title,
//            style: TextStyle(
//              fontFamily: 'RobotoCondensed',
//              fontSize: 15,
//              color: Colors.white,
//              fontWeight: FontWeight.w400,
//            ),
//          ),
//        ],
//      ),
//      onTap: tapHandler,
//    );
  }

  Widget buildListTilewithBg(int indexInList, String title, String icon, Function tapHandler) {
    return InkWell(
      onTap: tapHandler,
      child: Padding(
        padding:  EdgeInsets.fromLTRB(0, 0, 35, 0),
        child: Container(
          decoration:  BoxDecoration(
              borderRadius: BorderRadius.only(topLeft : Radius.circular(0.0), topRight: Radius.circular(20.0),
                  bottomLeft: Radius.circular(0.0), bottomRight: Radius.circular(20.0)),
              color: Consts.SideMenuCurrentlySelectedPosition == indexInList ? Colors.deepOrange[700]: Colors.transparent
          ),
          child: Padding(
            padding: EdgeInsets.fromLTRB(0,10,0,10),
            child: Row(
              children: <Widget>[
                SizedBox(width: 10,),
                Image(image: AssetImage(icon),
                  color: Consts.SideMenuCurrentlySelectedPosition == indexInList ? Colors.white :Colors.deepOrange[700],
                  height: 23, width: 23,),
                SizedBox(width: 20,),
                Text(
                  title,
                  style: TextStyle(
                    fontFamily: 'RobotoCondensed',
                    fontSize: 16,
                    color: Consts.SideMenuCurrentlySelectedPosition == indexInList ? Colors.white :Colors.black87,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Widget buildListTilewithBgABoutUs(int indexInList, String title, String icon, Function tapHandler) {
    return InkWell(
      onTap: tapHandler,
      child: Padding(
        padding:  EdgeInsets.fromLTRB(0, 0, 35, 0),
        child: Container(
          decoration:  BoxDecoration(
              borderRadius: BorderRadius.only(topLeft : Radius.circular(0.0), topRight: Radius.circular(20.0),
                  bottomLeft: Radius.circular(0.0), bottomRight: Radius.circular(20.0)),
              color: Consts.SideMenuCurrentlySelectedPosition == indexInList ? Colors.deepOrange[700]: Colors.transparent
          ),
          child: Padding(
            padding: EdgeInsets.fromLTRB(0,10,0,10),
            child: Row(
              children: <Widget>[
                SizedBox(width: 10,),
                Icon(
                  Icons.info_outline,
                  color: Consts.SideMenuCurrentlySelectedPosition == indexInList ? Colors.white :Colors.deepOrange[700],
                  size: 23.0,
                ),
//                Image(image: Icons.info_outline,
//                  color: Consts.SideMenuCurrentlySelectedPosition == indexInList ? Colors.white :Colors.deepOrange[700],
//                  height: 23, width: 23,),
                SizedBox(width: 20,),
                Text(
                  title,
                  style: TextStyle(
                    fontFamily: 'RobotoCondensed',
                    fontSize: 14,
                    color: Consts.SideMenuCurrentlySelectedPosition == indexInList ? Colors.white :Colors.black87,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildListTilewithBgNdCount(int indexInList, String title, String count, String icon, Function tapHandler) {
    return InkWell(
      onTap: tapHandler,
      child: Padding(
        padding:  EdgeInsets.fromLTRB(0, 0, 40, 0),
        child: Container(
          decoration:  BoxDecoration(
              borderRadius: BorderRadius.only(topLeft : Radius.circular(0.0), topRight: Radius.circular(20.0),
                  bottomLeft: Radius.circular(0.0), bottomRight: Radius.circular(20.0)),
              color: Consts.SideMenuCurrentlySelectedPosition == indexInList ? Colors.deepOrange[700]: Colors.transparent
          ),
          child: Padding(
            padding: EdgeInsets.fromLTRB(0,10,0,10),
            child: Row(
              children: <Widget>[
                SizedBox(width: 10,),
                Image(image: AssetImage(icon),color: Consts.SideMenuCurrentlySelectedPosition == indexInList ? Colors.white :Colors.deepOrange[700],
                  height: 23, width: 23,),
                SizedBox(width: 20,),
                RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
//                style: DefaultTextStyle.of(context).style,
                    children: <TextSpan>[
                      TextSpan(
                        text: title,
                          style: TextStyle(
                            fontFamily: 'RobotoCondensed',
                            fontSize: 14,
                            color: Consts.SideMenuCurrentlySelectedPosition == indexInList ? Colors.white :Colors.black87,
                            fontWeight: FontWeight.w400,
                          )
                      ),
                      TextSpan(
                          text: count,
                          style: TextStyle(
                            fontFamily: 'RobotoCondensed',
                            fontSize: 18,
                            color: Colors.blue[900],
                            fontWeight: FontWeight.w700,
                          )),
                    ],
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildListTile(String title, String icon, Function tapHandler) {
    return InkWell(
      onTap:  tapHandler,
      child: Padding(
        padding:  EdgeInsets.fromLTRB(10, 5, 10, 10),
        child: Row(
          children: <Widget>[
            getIconImge(icon),
            SizedBox(width: 20,),
            Text(
              title,
              style: TextStyle(
                fontFamily: 'RobotoCondensed',
                fontSize: 14,
                color: Colors.black,
                fontWeight: FontWeight.w400,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getIconImge(String str) {
    return  Image(image: AssetImage(str),color: Theme.of(context).primaryColor, height: 23, width: 23,);
  }

  Widget divider(){
    return Padding(
      padding: EdgeInsets.fromLTRB(50.0, 0.0, 0.0, 0.0),
      child: Container(
        height:  5,
        color: Colors.white24,
      )
//      Divider(
//        color: Colors.white24,
//        thickness: 0.8,
//      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.7,
      child: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                height: 110,
//              height: MediaQuery.of(context).size.height * 0.08,
//              color: Colors.deepOrange,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/side_menu/sidemenu_bg.png'),
                    fit: BoxFit.fill,
                  ),
//                  image: AssetImage('assets/side_menu/sidemenu_bg.png')
                ),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: 10,
                      ),
                      Image(
                        image: AssetImage('assets/side_menu/side_menu_horce_icon.png'),
                        height: 40,
                        color: Colors.white,
                        width: 40,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        child: Text(customer_name.toUpperCase(),
                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.white),),
                      )
                    ],
                  ),
                ),
//                child: Center(
//                  child:  Image(
//                    image: AssetImage('assets/side_menu/tirthtravels_logo.png'),
//                    height: 140,
//                    color: Colors.white,
//                    width: MediaQuery.of(context).size.width * 0.5,
//                  ),
//                ),

              ),

              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child:  Column(
//                        crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(width: 20,),


                    buildListTilewithBg(0 ,Api_constant.POST_A_JOB, 'assets/side_menu/job.png', () {
                      setState(() {
                        Consts.SideMenuCurrentlySelectedPosition = 0;
                      });
                      Navigator.of(widget.ccontext).pushReplacementNamed(PostJob1New.routeName);
                    }),

                    divider(),

                    buildListTilewithBg(1, Api_constant.yourtrips, 'assets/side_menu/request.png',  () {
                      setState(() {
                        Consts.SideMenuCurrentlySelectedPosition = 1;
                      });
                      Navigator.of(widget.ccontext).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: 0);
                    }),

                    divider(),
                   /* buildListTilewithBg(2, Api_constant.completed_rides, 'assets/side_menu/request.png',  () {
                      setState(() {
                        Consts.SideMenuCurrentlySelectedPosition = 2;
                      });
                      Navigator.of(widget.ccontext).pushReplacementNamed(MyCompletedJobTabsScreen.routeName, arguments: 0);
                    }),

                    divider(),*/
                    buildListTilewithBg(2 ,Api_constant.MY_PROFILE, 'assets/side_menu/profile.png', () {
                      setState(() {
                        Consts.SideMenuCurrentlySelectedPosition = 2;
                      });
                      Navigator.of(widget.ccontext).pushReplacementNamed(myProfileNew.routeName);
                    }),


                    divider(),


                    buildListTilewithBg(3, Api_constant.MY_RATING, 'assets/side_menu/rating.png', () {
                      setState(() {
                        Consts.SideMenuCurrentlySelectedPosition = 3;
                      });
                      Navigator.of(widget.ccontext).pushReplacementNamed(RatingList.routeName);
                    }),

                    divider(),
                    buildListTilewithBg(4, Api_constant.MY_WALLET, 'assets/side_menu/wallet.png', () {
                      setState(() {
                        Consts.SideMenuCurrentlySelectedPosition = 4;
                      });
                      Navigator.of(widget.ccontext).pushReplacementNamed(MyWallet.routeName);
                    }),

                    divider(),

//                    buildListTilewithBgNdCount(4, Api_constant.NOTIFICATION,''
////                        " ("+notificationUnreadCount+")"
//                        , 'assets/side_menu/notification.png', () {
//                      setState(() {
//                        Consts.SideMenuCurrentlySelectedPosition = 4;
//                      });
//                      Navigator.of(widget.ccontext).pushReplacementNamed(NotificationList.routeName);
//                    }),

                    buildListTilewithBg(5, Api_constant.NOTIFICATION,
//                        " ("+notificationUnreadCount+")"
                         'assets/side_menu/notification.png', () {
                          setState(() {
                            Consts.SideMenuCurrentlySelectedPosition = 5;
                          });
                          Navigator.of(widget.ccontext).pushReplacementNamed(NotificationList.routeName);
                        }),

                    divider(),

                    buildListTilewithBgABoutUs(6,Api_constant.ABOUT_US, 'assets/side_menu/about.png', () {
                      setState(() {
                        Consts.SideMenuCurrentlySelectedPosition = 6;
                      });
                      Navigator.of(widget.ccontext).pushReplacementNamed(AboutUs.routeName);
                    }),

                    divider(),

                    buildListTilewithBg(7,Api_constant.CONTACT_US, 'assets/side_menu/call.png', () {
                      setState(() {
                        Consts.SideMenuCurrentlySelectedPosition = 7;
                      });
                      Navigator.of(widget.ccontext).pushReplacementNamed(ContactUs.routeName);
//                        Navigator.of(ccontext).pushReplacementNamed(ContactUs.routeName);
                    }),

                    divider(),

                    buildListTilewithBg(8,Api_constant.LOGOUT,'assets/side_menu/logout.png', () {
                      setState(() {
                        Consts.SideMenuCurrentlySelectedPosition = 8;
                      });
                       Navigator.of(widget.ccontext).pop();
                      needy_logout(context);
                    }),
                    SizedBox(width: 30,),

                  ],
                ),
//              child: SingleChildScrollView(
//
//              ),
              )
            ],
          )
      ),
    );
  }

  void needy_logout(BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(Api_constant.Araba),
          content: new Text(Api_constant.sure_logout),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(Api_constant.CANCEL),
              textColor: Theme.of(context).primaryColor,
              onPressed: () {
                Navigator.of(dialogContext).pop();
              },
            ),

            new FlatButton(
              child: new Text('LOGOUT'),
              textColor: Theme.of(context).primaryColor,
              onPressed: () async {
                logout_api_call(dialogContext);
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> logout_api_call(BuildContext context) async {
    var map = new Map<String, dynamic>();

    MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();
    String id =  _myPreferenceManager.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);
    map[Api_constant.user_id] = id;

    print("logout "+ _myPreferenceManager.getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID));
    var response = await http.post(Api_constant.logout, body: map);

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str +"..............");
      print("logout "+ response_json_str);

      Map userMap = jsonDecode(response_json_str);

      var CommanModal_obj = new CommanModalWithObj.fromJson(userMap) as CommanModalWithObj;

      if(CommanModal_obj.status.toString() == '1') {
        MyPreferenceManager _myPreferenceManager = await MyPreferenceManager.getInstance();

        _myPreferenceManager.clearData();
        Navigator.of(context).pop();
        Navigator.of(context).pushReplacementNamed(LoginTirth.routeName);

//        Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
        return true;
      } else {
        return false;
      }

    } else {
      Toast.show(Consts.something_went_wrong, context, gravity: Toast.TOP);
      throw Exception('Failed to load internet');
      return false;

    }
  }

  void openTrackitDialog(context) {
    showDialog(
        context: context,
        builder: (BuildContext context) =>
            TrackItNowDialog(this)
    );
  }

  @override
  void trackitNumber(PassDataToTrackingScreen passDataToTrackingScreenObj, BuildContext context) {
//    Toast.show('called..', context, gravity: Toast.CENTER);
//    Navigator.of(context).pushReplacementNamed(ContactUs.routeName);
//    Navigator.of(widget.ccontext).pushReplacementNamed(ContactUs.routeName);
//    Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));

//    Navigator.of(widget.ccontext).pushReplacementNamed(ProcessingTrackingDetail2.routeName, arguments: passDataToTrackingScreenObj);
//    Navigator.of(context).pushReplacementNamed(ProcessingTrackingDetail2.routeName, arguments: passDataToTrackingScreenObj);
  }

  @override
  MainDrawerState createState() {
    return MainDrawerState();
  }

  Future<void> updateCustomerName() async {
    MyPreferenceManager _myPreferenceManager =
        await MyPreferenceManager.getInstance();
    String name = _myPreferenceManager
        .getString(MyPreferenceManager.LOGIN_NAME);

    setState(() {
      customer_name = name;
    });
  }
}























//

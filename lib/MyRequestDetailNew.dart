import 'dart:async';
import 'dart:convert';
//import 'dart:html';
import 'dart:math';

import 'package:arabacustomer/ProcessingTrackingDetail2.dart';
import 'package:arabacustomer/Utils/Api_constant.dart';
import 'package:arabacustomer/Utils/Consts.dart';
import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:arabacustomer/widgets/MyRequestTabsScreen.dart';
import 'package:arabacustomer/widgets/lib_dart_noti_center/src/dart_notification_center_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;

import 'MyRequestCompleted.dart';
import 'RatingList.dart';
import 'dialog/CancelConfirmationDialog.dart';
import 'dialog/LocationScanningDialog.dart';
import 'dialog/RatingDialog.dart';
import 'map_request.dart';
import 'model/CommanModalWithObj.dart';
import 'model/MyRequestListObj.dart';
import 'model/PassDataToTrackingScreen.dart';
import 'model/StatusMsgResponse.dart';

class MyRequestDetailNew extends StatefulWidget {
  static const routeName = '/MyRequestDetailNew';

  MyRequestListObj myRequestListObj;

//  MyRequestDetail({Key key, this.myRequestListObj}) : super(key: key);

  String  postid;
  MyRequestDetailNew({Key key, this.postid}) : super(key: key);

  @override
  MyRequestDetailNewState createState() {
    return MyRequestDetailNewState();
  }
}

class MyRequestDetailNewState extends State<MyRequestDetailNew> implements CallbackOfCanceldDialog , CallbackOfRatinngdDialog {

  Completer<GoogleMapController> _mapController = Completer();

  GoogleMapController _controller;
  GoogleMapsServices _googleMapsServices = GoogleMapsServices();
  final Set<Polyline> _polyLines = {};
  Set<Polyline> get polyLines => _polyLines;
  final Set<Marker> _markers = {};

  BitmapDescriptor SourcepinLocationIcon;
  BitmapDescriptor DestipinLocationIcon;

  final CameraPosition _initialCamera = CameraPosition(
    target: LatLng(20.5937, 78.9629),
//    target: LatLng(28.0339, 1.6596),
    zoom: 14,
  );

  bool IsTrackingBtnShow(String status){
    if(status == null || status.isEmpty){
      return false;
    } else{
      if(status.toLowerCase()=='in progress' || status.toLowerCase() == 'driver arrived' ||
          status.toLowerCase()=='on the way'){
//        status.toLowerCase()=='on the way'){
        return true;
      } else{
        return false;
      }
    }

  }
  bool IsProgressIndicatorShow = false;

  bool IsPandingPaymentShow(String status){
    if(status != null &&
        status.isNotEmpty && status.toLowerCase() =='payment pending'){
      return true;
    } else{
      return false;
    }
  }

  bool IsExtraChargesShow(String status){
    if(status == null || status.isEmpty ){
      return false;
    } else if(status.toLowerCase() == Consts.paymentpendingStatusText || status.toLowerCase() == Consts.completedStatusText){
      return true;
    }else{
      return false;
    }
  }

  Future<bool> _onWillPop() async {
    print("_onWillPop===========" );
    goToDetailScreen();
//    Navigator.of(context)
//        .pushNamedAndRemoveUntil(MyJobTabsScreen.routeName,
//            (Route<dynamic> route) => false);

    return true;
  }
  void goToDetailScreen() {
//    Toast.show(widget.myRequestListObj.status, context,
//        gravity: Toast.CENTER);

     int arg = 0;
      if(widget.myRequestListObj.status == null &&
          widget.myRequestListObj.status.isEmpty){
        int arg = 0;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);

      }else if(widget.myRequestListObj.status.toLowerCase() == Consts.pendingStatusText){
        arg = 0;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);


      }else if(widget.myRequestListObj.status.toLowerCase() == Consts.confirmedStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.onthewayStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.driverarrivedStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.inprogressStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.paymentpendingStatusText){
        arg = 0;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);

      } else if(widget.myRequestListObj.status.toLowerCase() == Consts.completedStatusText){
        arg = 1;
        if(Consts.FromRatingScreen){
          Navigator.of(context).pushReplacementNamed(RatingList.routeName);
        } else{
//          Navigator.of(context).pushReplacementNamed(MyRequestCompleted.routeName, arguments: arg);
          Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);
        }

      } else if(widget.myRequestListObj.status.toLowerCase() == Consts.cancelledStatusText){
        arg = 2;
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);

      }else{
        Navigator.of(context).pushReplacementNamed(MyRequestTabsScreen.routeName, arguments: arg);
      }

  }
  int i = 3;

  @override
  void initState() {
    getDetail(widget.postid);

    DartNotificationCenter.subscribe(
      channel: Consts.DetialChanel,
      observer: i,
      onNotification: (result) => {
        print('received:DetialChanel****** $result'),
        if(widget.postid != null && widget.postid.isNotEmpty){
          getDetail(widget.postid)
        }

//        setState(() {
////          new MyRequestDetail();
//        })
      },
    );

    super.initState();

    try {
      BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/red_circle.png')
          .then((onValue) {
        SourcepinLocationIcon = onValue;
      });

      BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/red_square.png')
          .then((onValue) {
        DestipinLocationIcon = onValue;
      });
    } catch (e) {
      print("pinLocationIcon===" + e.toString());
    }

    if (null != widget.myRequestListObj.status && widget.myRequestListObj.status.toLowerCase() == 'driver arrived' && widget.myRequestListObj.payment_type==1) {

    }
  }

  void sendRequest() async {
    _onAddMarkerButtonPressed();
    LatLng destination = LatLng(
        CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.delivery_lattitude),
        CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.delivery_longitude));
    LatLng origin = LatLng(
        CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.pickup_lattitude),
        CommonWidget.returnAlgeriaLatWhenGettingBlanck(widget.myRequestListObj.pickup_longitude));
    String route =
    await _googleMapsServices.getRouteCoordinates(origin, destination);
    print("route===" + route);
    createRoute(route);
    _addMarker(destination, widget.myRequestListObj.delivery_address);
  }

  void createRoute(String encondedPoly) {
    _polyLines.add(Polyline(
        polylineId: PolylineId("Track"),
        width: 4,
        points: _convertToLatLng(_decodePoly(encondedPoly)),
        color: Colors.red));

    setState(() {

    });
    // print("_polyLines"+_polyLines.toString());
  }
  List _decodePoly(String poly) {
    var list = poly.codeUnits;
    var lList = new List();
    int index = 0;
    int len = poly.length;
    int c = 0;
    do {
      var shift = 0;
      int result = 0;

      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      } while (c >= 32);
      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lList.add(result1);
    } while (index < len);

    for (var i = 2; i < lList.length; i++) lList[i] += lList[i - 2];

    print(lList.toString());

    return lList;
  }
  List<LatLng> _convertToLatLng(List points) {
    List<LatLng> result = <LatLng>[];
    for (int i = 0; i < points.length; i++) {
      if (i % 2 != 0) {
        result.add(LatLng(points[i - 1], points[i]));
      }
    }
    return result;
  }
  void _addMarker(LatLng location, String address) {
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId("112"),
          position: location,
          // infoWindow: InfoWindow(title: address, snippet: "go here"),
          infoWindow: InfoWindow(title: address),
          icon: DestipinLocationIcon));
//          icon: BitmapDescriptor.defaultMarker));

      var bounds = getBounds(_markers);
      CameraUpdate u2 = CameraUpdate.newLatLngBounds(bounds, 50);
      _controller.animateCamera(u2).then((void v){
        check(u2,this._controller);
      });
    });
  }
  void check(CameraUpdate u, GoogleMapController c) async {
    c.animateCamera(u);
    _controller.animateCamera(u);
    LatLngBounds l1=await c.getVisibleRegion();
    LatLngBounds l2=await c.getVisibleRegion();
    print(l1.toString());
    print(l2.toString());
    if(l1.southwest.latitude==-90 ||l2.southwest.latitude==-90)
      check(u, c);
  }

  LatLngBounds getBounds(Set<Marker> markers) {

    var lngs = markers.map<double>((m) => m.position.longitude).toList();
    var lats = markers.map<double>((m) => m.position.latitude).toList();

    double topMost = lngs.reduce(max);
    double leftMost = lats.reduce(min);
    double rightMost = lats.reduce(max);
    double bottomMost = lngs.reduce(min);

    LatLngBounds bounds = LatLngBounds(
      northeast: LatLng(rightMost, topMost),
      southwest: LatLng(leftMost, bottomMost),
    );

    return bounds;
  }
  void _onAddMarkerButtonPressed() {
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId("111"),
          position: LatLng(double.parse(Consts.pickup_lattitude), double.parse(Consts.pickup_longitude)),
          // infoWindow: InfoWindow(title: address, snippet: "go here"),
          infoWindow: InfoWindow(title: Consts.sourceAdd),
          icon: SourcepinLocationIcon
      ));
    });
  }
  @override
  void dispose() {
    DartNotificationCenter.unsubscribe(observer: i, channel: Consts.DetialChanel);
    print('received:dispose****** ');
    super.dispose();

  }


  @override
  Widget build(BuildContext context) {
//    Toast.show(widget.myRequestListObj.toString(), context,
//        gravity: Toast.CENTER);
    print('received:build****');

    return WillPopScope(
      child: Scaffold(
        body:  IsProgressIndicatorShow ? SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Center(
            child:  CircularProgressIndicator(),
          ),
        ) :
        SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: ListView(
            children: <Widget>[
              Container(
                color: Colors.deepOrange[700],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
//              size: 20,
                      ),
                      onPressed: () {
                        _onWillPop();
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(15, 3, 0, 15),
                      child:  CommonWidget.getNewActionBarTitleText(Api_constant.Trip_Detail),
                    ),
                  ],
                ),
              ),

              Container(
                height: MediaQuery.of(context).size.height * 0.25,
                child: GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: _initialCamera,
                  myLocationEnabled: true,
                  myLocationButtonEnabled: true,
                  onMapCreated: (GoogleMapController controller) {
                    _controller=controller;
                    _mapController.complete(controller);
//                    _getLocation();
                  },
                  polylines: polyLines,
                  markers: _markers,
//                  markers: _markers.values.toSet(),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 12),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(widget.myRequestListObj.date,
                          style: TextStyle(
                              fontWeight: FontWeight.w400, color: Colors.black, fontSize: 14
                          ),),

                        Text(CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.price),
                          style: TextStyle(
                              fontWeight: FontWeight.w400, color: Colors.black, fontSize: 14
                          ),)
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(widget.myRequestListObj.vehicle_category,
                      style: TextStyle(
                          fontWeight: FontWeight.w400, color: Colors.grey[700], fontSize: 14
                      ),),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child:  Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  CommonWidget.getIconImgeWithCustomSize(
                                      'assets/images/black_circle.png', 8),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Flexible(
                                    child: Text(widget.myRequestListObj.pickup_address,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400, color: Colors.grey[700], fontSize: 14
                                      ),),
                                  )
//                              Expanded(
//                                child: Text(widget.myRequestListObj.pickup_address),
//                              )
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: <Widget>[
                                  CommonWidget.getIconImgeWithCustomSize('assets/images/red_square.png', 8),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Flexible(
                                    child: Text(widget.myRequestListObj.delivery_address,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400, color: Colors.grey[700], fontSize: 14
                                      ),),
                                  )
//
                                ],
                              )
                            ],
                          ),

                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          decoration:  BoxDecoration(
                              color: Colors.grey[300],
                              border: Border.all(color: Colors.grey[300]),
                              borderRadius : BorderRadius.all(Radius.circular(15.0))
                          ),
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(9, 5, 9, 5),
                            child: Center(
                              child: Text(Api_constant.Receipt, style:  TextStyle(fontSize: 14, color: Colors.black),),
                            ),
                          ),
                        )

                      ],
                    )
                  ],
                ),
              ),
              Divider(
                thickness: 1,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                child:  Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                          widget.myRequestListObj.transporter_name
                      ),
                      flex: 1,
                    ),
                    Visibility(
                      visible: true,
//                    visible: IsRatingBarDispllay(),
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: 20.0),
                          RatingBarIndicator(
                            rating: CommonWidget.getDoubleFromStrForRating(widget.myRequestListObj.customer_ratings),
                            direction: Axis.horizontal,
                            unratedColor: Colors.grey.withAlpha(95),
                            itemCount: 5,
                            itemSize: 22.0,
                            itemPadding:
                            EdgeInsets.symmetric(horizontal: 1.2),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.black,
                            ),
                          ),
                          SizedBox(height: 10.0),

                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                thickness: 1,
              ),
            ],
          ),
        )
      ),
      onWillPop: _onWillPop,

    );
  }

  String _radioValue; //Initial definition of radio button value
  String choice;
  String PaymentType='';

  void radioButtonChanges(String value) {
    setState(() {
      _radioValue = value;
      switch (value) {
        case 'one':
          PaymentType= '1';
          print(PaymentType + "..............");
          choice = value;
          break;
        case 'two':
          PaymentType= '0';
          print(PaymentType + "..............");
          choice = value;
          break;
//        case 'three':
//          choice = value;
//          break;
        default:
          choice = null;
      }
//      Toast.show(PaymentType, context,
//          gravity: Toast.CENTER);
      debugPrint(choice); //Debug the choice in console
    });
  }

  void onChanged() {
   // Toast.show("Track the job", context,gravity: Toast.CENTER);
    PassDataToTrackingScreen passDataToTrackingScreenObj = new PassDataToTrackingScreen();
    passDataToTrackingScreenObj.job_id = widget.myRequestListObj.job_id;
    passDataToTrackingScreenObj.pickup_address = widget.myRequestListObj.pickup_address;
    passDataToTrackingScreenObj.pickup_lattitude = widget.myRequestListObj.pickup_lattitude;
    passDataToTrackingScreenObj.pickup_longitude = widget.myRequestListObj.pickup_longitude;
    passDataToTrackingScreenObj.delivery_address = widget.myRequestListObj.delivery_address;
    passDataToTrackingScreenObj.delivery_lattitude = widget.myRequestListObj.delivery_lattitude;
    passDataToTrackingScreenObj.delivery_longitude = widget.myRequestListObj.delivery_longitude;
    passDataToTrackingScreenObj.driver_phone_number = widget.myRequestListObj.driver_phone_number;
    passDataToTrackingScreenObj.IsFromSideMenu = false;
    Navigator.of(context).pushReplacementNamed(ProcessingTrackingDetail2.routeName, arguments: passDataToTrackingScreenObj);
  }

  IsDisplayDriverinfo(int jobListType) {
    if((CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.status)).toLowerCase()  == Consts.pendingStatusText ){
      return false;
    } else{
      return CommonWidget.forEmptyReturnHide(widget.myRequestListObj.driver_name);
    }
//    if(jobListType==Consts.JobListTypeInprogress || jobListType == Consts.JobListTypeComple){
//    } else{
//      return false;
//    }
  }

  Future<void> apiCAllPandingPaymentComplete() async {
    print('called' + "..............");

      var map = new Map<String, dynamic>();
      map[Api_constant.job_id] = widget.myRequestListObj.job_id.toString();
      map[Api_constant.online_payment] = PaymentType.toString();

      print(Api_constant.pp_complete_job_api + "..............");
   //cancel
   
      var response = await http.post(
          Api_constant.pp_complete_job_api, body: map);

      print(response.statusCode.toString() + "..............");
      print(response.body + "..............");
      if (response.statusCode == 200) {
        String response_json_str = response.body;
        print(response_json_str + "..............");
        Map userMap = jsonDecode(response_json_str);

        var loginResponseObj = new StatusMsgResponse.fromJsonMap(
            userMap) as StatusMsgResponse;

        if(loginResponseObj.status == 1) {
          Toast.show(Api_constant.job_complete_msg, context, gravity: Toast.CENTER);
          getDetail(widget.myRequestListObj.job_id.toString());

//          Navigator.of(context)
//              .pushNamedAndRemoveUntil(MyRequestTabsScreen.routeName, (Route<dynamic> route) => false);
        }else{
          var loginUserObj = loginResponseObj.message;
          Toast.show(Api_constant.something_went_wrong, context, gravity: Toast.CENTER);
        }
      }
  }


  void onppCompleteJobApiCall() {
    if(PaymentType.isNotEmpty){

      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text(Api_constant.Araba),
            content: new Text("Voulez-vous procéder au paiement?"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text(Api_constant.CANCEL),
                textColor: Theme.of(context).primaryColor,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),

              new FlatButton(
                child: new Text('CONTINUER'),
                textColor: Theme.of(context).primaryColor,
                onPressed: () async {

                  apiCAllPandingPaymentComplete();
                  Navigator.of(context).pop();

                },
              ),
            ],
          );
        },
      );

    } else{
      Toast.show(Api_constant.choose_payment, context, gravity: Toast.CENTER);
    }
  }

  Future<void> getDetail(String id) async {

    final uri = Uri.encodeFull(Api_constant.detailScreenApi + '?id=${id}' );
    print(uri.toString() + "..............");

    setState(() {
      IsProgressIndicatorShow = true;
    });

    var response = await http.get(uri);

    setState(() {
      IsProgressIndicatorShow = false;
    });

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var CommanModal_obj = new CommanModalWithObj.fromJson(userMap) as CommanModalWithObj;
      if (CommanModal_obj.status == 1) {

        Object obj = CommanModal_obj.data;
        MyRequestListObj g = new MyRequestListObj.fromJsonMap(obj);

        setState(() {
          widget.myRequestListObj = g;
          print("***===========" +g.toString());

          sendRequest();
//          setData();
        });

//        setState(() {
//        });

      } else {
        Toast.show(Api_constant.no_record_found, context,
            gravity: Toast.CENTER);

      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);

    }
  }

  IsCancelShow(String status) {
    if (null == status || status.isEmpty) {
      return false;
    } else if (status.toLowerCase() == Consts.confirmedStatusText ||
        status.toLowerCase() == 'driver arrived' ||
        status.toLowerCase() == 'on the way'||
        status.toLowerCase() == Consts.pendingStatusText) {
      return true;
    } else {
      return false;
    }
  }


  void onCanceljob() {
    showDialog(
        context: context,
        builder: (BuildContext
        context) =>
            CancelConfirmationDialog(
                this, widget.myRequestListObj.job_id.toString()));

  }

  @override
  void cancelJob(String jobid, String cancel_reason) {
    cancleJobApiCall(jobid, cancel_reason);
  }
  Future<void> cancleJobApiCall(String job_id, String cancel_reason) async {
    print('called' + "..............");

    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = job_id;
    map[Api_constant.reasone_for_cancel] = cancel_reason;

    print(map.toString() + "..............");

    var response = await http.post(Api_constant.cancelJob, body: map);

    print(response.statusCode.toString() + "..............");
    print(response.body + "..............");
    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
      new StatusMsgResponse.fromJsonMap(userMap) as StatusMsgResponse;

      var loginUserObj = loginResponseObj.message;

      if(loginResponseObj.status == Api_constant.STATUS) {
        Toast.show(Api_constant.cancel_job_success_msg, context, gravity: Toast.CENTER);

        getDetail(widget.myRequestListObj.job_id.toString());

      } else{
        Toast.show(loginUserObj.toString(), context, gravity: Toast.CENTER);
      }

//      setState(() {
//
//      });
//      _MyRequestOpenObjList.forEach((element) {
//        //not working
//        if (element.job_id == job_id) {
//          int index = _MyRequestOpenObjList.indexOf(element);
//
//          setState(() {
//            _MyRequestOpenObjList.remove(index);
//          });
//        }
//      });
    }
  }

  IsRateDispllay() {
    if(widget.myRequestListObj.status != null &&
        widget.myRequestListObj.status.isNotEmpty &&
        widget.myRequestListObj.status.toLowerCase()==Consts.completedStatusText ) {

      if(CommonWidget.getboolFromInt(widget.myRequestListObj.customer_rated)){
        return true;
      } else{
        return false;
      }
    }else{
      return  false;
    }

  }

  IsRatingBarDispllay() {
    if(widget.myRequestListObj.status != null &&
        widget.myRequestListObj.status.isNotEmpty &&
        widget.myRequestListObj.status.toLowerCase()==Consts.completedStatusText ) {

      if(CommonWidget.getboolFromInt(widget.myRequestListObj.customer_rated)){
        return false;
      } else{
        return true;
      }
    }else{
      return  false;
    }
  }

  void onRate() {
    showDialog(
        context: context,
        builder: (BuildContext context) =>
            RatingDialog(widget.myRequestListObj.job_id.toString(),'',
                this)
    );
  }

  @override
  void ratingComplete() {
    getDetail(widget.myRequestListObj.job_id.toString());

  }

  bool IsStatusDisplayShow(String status) {
    if(null == status || status.isEmpty){
      return false;
    }  else{
      if(widget.myRequestListObj.status.toLowerCase() == Consts.confirmedStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.onthewayStatusText ||
          widget.myRequestListObj.status.toLowerCase() == Consts.driverarrivedStatusText ){
        return true;
      } else {
        return false;
      }
    }
  }

  String getprice(String status) {
    if(status != null && status.isNotEmpty && status.toLowerCase() == Consts.paymentpendingStatusText){

      double total = 0;

      String pricee = CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.price);
      String delay_chargess = CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.delay_charges);

//      if(delay_chargess.contains(Consts.currencySymbol)){
        if((delay_chargess.replaceFirst(Consts.currencySymbol, '')).isEmpty){
          return widget.myRequestListObj.price;
        }

        if((delay_chargess.replaceFirst(Consts.currencySymbol, '')).isNotEmpty &&
            (pricee.replaceFirst(Consts.currencySymbol, '')).isNotEmpty){
          total = double.parse(pricee.replaceFirst(Consts.currencySymbol, '')) +
              double.parse(delay_chargess.replaceFirst(Consts.currencySymbol, ''));

          return Consts.currencySymbol+ total.toString();
        }

//      } else if(delay_chargess.contains(Consts.oldcurrencySymbol)){
//        if((delay_chargess.replaceFirst(Consts.oldcurrencySymbol, '')).isEmpty){
//          return widget.myRequestListObj.price;
//        }
//
//        if((delay_chargess.replaceFirst(Consts.oldcurrencySymbol, '')).isNotEmpty &&
//            (pricee.replaceFirst(Consts.oldcurrencySymbol, '')).isNotEmpty){
//          total = double.parse(pricee.replaceFirst(Consts.oldcurrencySymbol, '')) +
//              double.parse(delay_chargess.replaceFirst(Consts.oldcurrencySymbol, ''));
//
//          return Consts.oldcurrencySymbol+ total.toString();
//        }
//      }

    } else if(status != null && status.isNotEmpty && status.toLowerCase() == Consts.paymentpendingStatusText){
      double total = 0;
      String pricee = CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.price);
      String delay_chargess = CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.delay_charges);

      total = double.parse(pricee) + double.parse(delay_chargess);
      return total.toString();

    } else if(IsExtraChargesShow(status)){
      return CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.total_amount);
    } else{
      return CommonWidget.replaceNullWithEmpty(widget.myRequestListObj.price);
    }
  }
}
//https://reds.a2hosted.com/araba/api/web/v1/post-jobs/customer-complete-job
//
//GET
//
//job_id
//online_payment (1/0)
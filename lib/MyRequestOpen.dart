import 'dart:convert';

import 'package:arabacustomer/widgets/CommonWidget.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import 'MyRequestDetail.dart';
import 'Utils/Api_constant.dart';
import 'Utils/Consts.dart';
import 'dialog/CancelConfirmationDialog.dart';
import 'model/CommanModal.dart';
import 'package:http/http.dart' as http;
import 'Utils/MyPreferenceManager.dart';
import 'model/MyRequestListObj.dart';
import 'model/StatusMsgResponse.dart';

class MyRequestOpen extends StatefulWidget {
  @override
  MyRequestOpenState createState() {
    return MyRequestOpenState();
  }
}

class MyRequestOpenState extends State<MyRequestOpen>
    implements CallbackOfCanceldDialog {
  Future<List<MyRequestListObj>> geMyRequestOpenObjListtData() async {
//    String id = '38';

    MyPreferenceManager _myPreferenceManager =
        await MyPreferenceManager.getInstance();
    String id = _myPreferenceManager
        .getString(MyPreferenceManager.CURRENT_LOGIN_USER_ID);

    String page_offset = '0';
    final uri = Uri.encodeFull(Api_constant.MyRequestListOpen +
        '?id=${id}' +
        '&page_offset=${page_offset}');
    print(uri.toString() + "..............");

    var response = await http.get(uri);

    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");

      Map userMap = jsonDecode(response_json_str);
//      var data = userMap["data"];

      var CommanModal_obj = new CommanModal.fromJson(userMap) as CommanModal;

      if (CommanModal_obj.status == 1) {
        List<Object> resultList = CommanModal_obj.data;

        List<MyRequestListObj> myContributionList = new List(resultList.length);

        for (int i = 0; i < resultList.length; i++) {
          Object obj = resultList[i];
          //   print(obj.toString() + "ActualObject..............");

          try {
            MyRequestListObj g = new MyRequestListObj.fromJsonMap(obj);
            //print(g.job_title + "JobTitle..............");
            // print(g.pickup_address + "pickup_address..............");

            myContributionList[i] = g;
          } catch (Exception) {
            //print( "Exception..............");
            print("Exception.............." + Exception.toString());
          }
        }
        return myContributionList;
      } else {
        Toast.show(Api_constant.no_record_found, context,
            gravity: Toast.CENTER);

        return new List<MyRequestListObj>();
      }
    } else {
      Toast.show(Api_constant.something_went_wrong, context,
          gravity: Toast.CENTER);

      return new List<MyRequestListObj>();
    }
  }

  List<MyRequestListObj> _MyRequestOpenObjList;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<MyRequestListObj>>(
      future: geMyRequestOpenObjListtData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(child: CircularProgressIndicator());

        _MyRequestOpenObjList = snapshot.data;

        return Container(
          color: Colors.grey[200],
          child: Stack(
            alignment: Alignment.bottomRight,
            children: <Widget>[
              Center(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0.0),
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      return Column(
                        children: <Widget>[
                          InkWell(
                            onTap: () {
//                              Navigator.push(context, MaterialPageRoute(builder: (context) => MyRequestDetail()));
                            },
                            child: Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(3.0))),
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                          EdgeInsets.fromLTRB(15, 15, 15, 5),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          CommonWidget.leadidDarkRedcolorText(
                                              _MyRequestOpenObjList[index]
                                                  .job_number),
                                          CommonWidget.priceDarkRedcolorText(
                                              _MyRequestOpenObjList[index]
                                                  .price, context)
                                        ],
                                      ),
                                    ),
                                    CommonWidget.divider(),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          10.0, 5.0, 15.0, 12.0),
                                      child: Stack(
                                        alignment: Alignment.bottomRight,
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              CommonWidget.jobnameText(
                                                  _MyRequestOpenObjList[index]
                                                      .job_title),
                                              SizedBox(height: 5.0),
                                              CommonWidget
                                                  .gryIconNdGrayTextheaderLayout(
                                                      _MyRequestOpenObjList[
                                                              index]
                                                          .date,
                                                      'assets/images/post_job/date_copy.png'),
                                              CommonWidget
                                                  .gryIconNdGrayTextheaderLayout(
                                                      _MyRequestOpenObjList[
                                                              index]
                                                          .vehicle_category
                                                          .toString(),
                                                      'assets/images/post_job/vehicle_copy.png'),
                                              SizedBox(height: 8.0),
                                              CommonWidget.redHeaderLbl(
                                                  Api_constant.DISTANCE, context),
                                              SizedBox(height: 3.0),
                                              CommonWidget
                                                  .gryIconNdGrayTextheaderLayout(
                                                      Api_constant.From +
                                                          _MyRequestOpenObjList[
                                                                  index]
                                                              .pickup_address,
                                                      'assets/images/post_job/location_copy.png'),
                                              CommonWidget
                                                  .gryIconNdGrayTextheaderLayout(
                                                      Api_constant.To +
                                                          _MyRequestOpenObjList[
                                                                  index]
                                                              .delivery_address,
                                                      'assets/images/post_job/location.png'),
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    35, 15, 0, 6),
                                                child: Row(
                                                  children: <Widget>[
                                                    CommonWidget.redHeaderLbl(
                                                        Api_constant.Total, context),
                                                    CommonWidget
                                                        .grayDiscriptionText(
                                                            _MyRequestOpenObjList[index].distance.toString() +
                                                                Api_constant.KM),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: <Widget>[
                                              InkWell(
                                                  child: Container(
                                                    height: 37,
                                                    width: 100,
                                                    decoration: BoxDecoration(
                                                        gradient: LinearGradient(
                                                          begin:
                                                          Alignment.centerLeft,
                                                          end:
                                                          Alignment.centerRight,
                                                          colors: <Color>[
                                                            Color(0xFF8b0000),
                                                            Color(0xFFCB0101),
                                                            //            Color(0xFFFF0000),
                                                          ],
                                                        ),
                                                        borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                5.0))),
                                                    child: Center(
                                                      child: Text(
                                                        Api_constant.CANCEL,
                                                        style: TextStyle(
                                                            fontSize: 15,
                                                            color: Colors.white),
                                                      ),
                                                    ),
                                                  ),
                                                  onTap: () {
                                                    showDialog(
                                                        context: context,
                                                        builder: (BuildContext
                                                        context) =>
                                                            CancelConfirmationDialog(
                                                                this, _MyRequestOpenObjList[index].job_id.toString()));
                                                  }
                                              ),

                                              Padding(
                                                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                child: SizedBox(
                                                  width: 37,
                                                  height: 37,
                                                  child: FlatButton(
                                                      onPressed: (){
                                                        var obj = _MyRequestOpenObjList[index];
                                                        obj.JobListType = Consts.JobListTypeOpne;

//                                                        Navigator.of(context).pushNamed(
//                                                            MyRequestDetail.routeName,
//                                                            arguments: obj);
                                                        Navigator.of(context).pushReplacementNamed(MyRequestDetail.routeName, arguments: obj.job_id.toString());

                                                      },
                                                      padding:
                                                      EdgeInsets.all(0.0),
                                                      child: Image.asset(
                                                          'assets/images/goto_detail_square.png')),
                                                ),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                )),
                          ),
                          SizedBox(
                            height: 15,
                          )
                        ],
                      );
                    },
                    itemCount: _MyRequestOpenObjList.length,
                  ),
                ),
              ),
//              Padding(
//                padding: EdgeInsets.fromLTRB(0, 0, 40, 40),
//                child: FloatingActionButton(
//                  onPressed: () {},
//                  child: Icon(Icons.add),
//                ),
//              )
            ],
          ),
        );
      },
    );
  }

  Future<void> cancleJobApiCall(String job_id, String cancel_reason) async {
    print('called' + "..............");

    var map = new Map<String, dynamic>();
    map[Api_constant.job_id] = job_id;
    map[Api_constant.reasone_for_cancel] = cancel_reason;

    print(map.toString() + "..............");

    var response = await http.post(Api_constant.cancelJob, body: map);

    print(response.statusCode.toString() + "..............");
    print(response.body + "..............");
    if (response.statusCode == 200) {
      String response_json_str = response.body;
      print(response_json_str + "..............");
      Map userMap = jsonDecode(response_json_str);

      var loginResponseObj =
          new StatusMsgResponse.fromJsonMap(userMap) as StatusMsgResponse;

      var loginUserObj = loginResponseObj.message;

      if(loginResponseObj.status == Api_constant.STATUS) {
        Toast.show(Api_constant.cancel_job_success_msg, context, gravity: Toast.CENTER);
      } else{
        Toast.show(loginUserObj.toString(), context, gravity: Toast.CENTER);
      }

      setState(() {

      });
//      _MyRequestOpenObjList.forEach((element) {
//        //not working
//        if (element.job_id == job_id) {
//          int index = _MyRequestOpenObjList.indexOf(element);
//
//          setState(() {
//            _MyRequestOpenObjList.remove(index);
//          });
//        }
//      });
    }
  }

  @override
  void cancelJob(String jobid,String cancel_reason) {
    cancleJobApiCall(jobid, cancel_reason);
  }
}
